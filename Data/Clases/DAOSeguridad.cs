﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Data;
using NpgsqlTypes;
using Utilitaria.Clases;
using Data.Clases;
using System.Data.Entity;

public class DAOSeguridad
{
    public DAOSeguridad()
    {

    }

    public static void solicitarRestablecerContrasena(Int64 idUsuario, string hash)
    {
        using (var dbc = new DBC("seguridad"))
        {
            ESolicitudContrasena eSolicitudContrasena = new ESolicitudContrasena();
            eSolicitudContrasena.IdUsuario = idUsuario;
            eSolicitudContrasena.Hash = hash;
            eSolicitudContrasena.Fecha = DateTime.Now.ToString();

            dbc.solicitudContrasena.Add(eSolicitudContrasena);
            dbc.SaveChanges();
        }
    }

    public static ESolicitudContrasena getSolicitudContrasena(string hash)
    {
        using (var dbc = new DBC("seguridad"))
        {
            ESolicitudContrasena eSolicitudContrasena = (from x in dbc.solicitudContrasena where x.Hash == hash orderby x.Id descending select x).FirstOrDefault();
            return eSolicitudContrasena;
        }
    }

    public static ESolicitudContrasena getSolicitudContrasena(Int64 idUsuario)
    {
        using (var dbc = new DBC("seguridad"))
        {
            ESolicitudContrasena eSolicitudContrasena = (from x in dbc.solicitudContrasena where x.IdUsuario == idUsuario orderby x.Id descending select x).FirstOrDefault();
            return eSolicitudContrasena;
        }
    }

    public static void removeSolicitudContrasena(int id)
    {
        using (var dbc = new DBC("seguridad"))
        {
            ESolicitudContrasena eSolicitudContrasena = dbc.solicitudContrasena.Find(id);
            dbc.Entry(eSolicitudContrasena).State = EntityState.Deleted;
            dbc.SaveChanges();
        }
    }
}