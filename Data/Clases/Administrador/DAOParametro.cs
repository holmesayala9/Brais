﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Clases.Administrador
{
    public class DAOParametro
    {
        public static Parametro get()
        {
            using (var dbc = new DBC(Parametro.ESQUEMA))
            {
                Parametro parametro = dbc.parametro.Find(1);

                parametro = JsonConvert.DeserializeObject<Parametro>(parametro.SParametro);

                parametro.Id = 1;

                return parametro;
            }
        }

        public static void update(Parametro parametro, EAcceso eAcceso)
        {
            using (var dbc = new DBC(Parametro.ESQUEMA))
            {
                DAOAuditoria.update(parametro, DAOParametro.get(), eAcceso, Parametro.ESQUEMA, Parametro.TABLA);
                parametro.SParametro = JsonConvert.SerializeObject(parametro);
                dbc.Entry(parametro).State = EntityState.Modified;
                dbc.SaveChanges();
            }
        }
    }
}