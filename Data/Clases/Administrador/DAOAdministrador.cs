﻿using Data.Clases;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

public class DAOAdministrador
{
    public DAOAdministrador()
    {

    }

    public static EAdministrador get(Int64 id)
    {
        using (var dbc = new DBC("administrador"))
        {
            EAdministrador eAdministrador = dbc.administrador.Find(id);
            return eAdministrador != null ? eAdministrador : EAdministrador.newEmpty();
        }
    }

    public static EAdministrador get(string identificacion)
    {
        using (var dbc = new DBC("administrador"))
        {
            EAdministrador eAdministrador = (from x in dbc.administrador where x.Identificacion == identificacion select x).First();

            return eAdministrador != null ? eAdministrador : EAdministrador.newEmpty();
        }
    }

    public static void update(EAdministrador eAdministrador, EAcceso eAcceso)
    {
        using(var dbc = new DBC("administrador"))
        {
            DAOAuditoria.update(eAdministrador, DAOAdministrador.get(eAdministrador.Id), eAcceso, EAdministrador.ESQUEMA, EAdministrador.TABLA);
            eAdministrador.IdAcceso = eAcceso.Id;
            dbc.Entry(eAdministrador).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }
}