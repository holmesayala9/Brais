﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using NpgsqlTypes;
using Utilitaria.Clases.Idioma;

namespace Data.Clases.Administrador
{
    public abstract class DAOIdioma
    {
        public static string getComponentesPorIdioma(int idIdioma)
        {
            using (var dbc = new DBC("idioma"))
            {
                var list = (from c in dbc.componente
                            join ic in dbc.idiomaComponente on c.Id equals ic.IdComponente
                            where ic.IdIdioma == idIdioma
                            select new Traduccion { Componente = c.Name, Txt = ic.Txt }).ToList();

                JObject jObject = new JObject();

                list.ForEach(l => jObject[l.Componente] = l.Txt);

                string comp = jObject.ToString();

                return comp;
            }
        }

        public static List<Traduccion> getTraduccion(int idIdioma, int idFormulario)
        {
            using(var dbc = new DBC("idioma"))
            {
                var list = (from c in dbc.componente
                            join fc in dbc.formularioComponente on c.Id equals fc.IdComponente
                            join ic in dbc.idiomaComponente on c.Id equals ic.IdComponente
                            where ic.IdIdioma == idIdioma && fc.IdFormulario == idFormulario
                            select new Traduccion { Componente = c.Name, Txt = ic.Txt }).ToList();

                return list;
            }
        }

        public static EIdioma getIdioma(int id)
        {
            using(var dbc = new DBC("idioma"))
            {
                EIdioma eIdioma = dbc.idioma.Find(id);
                return eIdioma != null ? eIdioma : EIdioma.newEmpty();
            }
        }

        public static List<EIdioma> getAllIdioma()
        {
            using (var dbc = new DBC("idioma"))
            {
                return dbc.idioma.ToList();
            }

        }

        public static void addIdioma(EIdioma eIdioma, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                eIdioma.IdAcceso = eAcceso.Id;
                dbc.idioma.Add(eIdioma);
                dbc.SaveChanges();
            }
        }

        public static void updateIdioma(EIdioma eIdioma, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                dbc.Entry(eIdioma).State = EntityState.Modified;
                dbc.SaveChanges();
            }
        }

        public static void deleteIdioma(EIdioma eIdioma, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                eIdioma.IdAcceso = eAcceso.Id;
                dbc.Entry(eIdioma).State = EntityState.Deleted;
                dbc.SaveChanges();
            }
        }

        public static EFormulario getFormulario(int id)
        {
            using(var dbc = new DBC("idioma"))
            {
                EFormulario eFormulario = dbc.formulario.Find(id);
                return eFormulario != null ? eFormulario : EFormulario.newEmpty();
            }
        }

        public static List<EFormulario> getAllFormulario()
        {
            using (var dbc = new DBC("idioma"))
            {
                return dbc.formulario.ToList();
            }
        }

        public static void addFormulario(EFormulario eFormulario, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                eFormulario.IdAcceso = eAcceso.Id;
                dbc.formulario.Add(eFormulario);
                dbc.SaveChanges();
            }
        }

        public static void updateFormulario(EFormulario eFormulario, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                eFormulario.IdAcceso = eAcceso.Id;
                dbc.Entry(eFormulario).State = EntityState.Modified;
                dbc.SaveChanges();
            }
        }

        public static void deleteFormulario(int id, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                EFormulario eFormulario = dbc.formulario.Find(id);
                dbc.Entry(eFormulario).State = EntityState.Deleted;
                dbc.SaveChanges();
            }
        }

        public static EComponente getComponente(int id)
        {
            using (var dbc = new DBC("idioma"))
            {
                EComponente eComponente = dbc.componente.Find(id);
                return eComponente != null ? eComponente : EComponente.newEmpty();
            }
        }

        public static List<EComponente> getAllComponente()
        {
            using (var dbc = new DBC("idioma"))
            {
                return dbc.componente.ToList();
            }
        }

        public static void addComponente(EComponente eComponente, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                eComponente.IdAcceso = eAcceso.Id;
                dbc.componente.Add(eComponente);
                dbc.SaveChanges();
            }
        }

        public static void updateComponente(EComponente eComponente, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                eComponente.IdAcceso = eAcceso.Id;
                dbc.Entry(eComponente).State = EntityState.Modified;
                dbc.SaveChanges();
            }
        }

        public static void deleteComponente(int id, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                EComponente eComponente = dbc.componente.Find(id);
                dbc.Entry(eComponente).State = EntityState.Deleted;
                dbc.SaveChanges();
            }
        }

        public static List<EIdiomaComponente> getAllIdiomaComponente()
        {
            using(var dbc = new DBC("idioma"))
            {
                List<EIdiomaComponente> list = dbc.idiomaComponente.ToList();

                foreach (EIdiomaComponente eIdiomaComponente in list)
                {
                    eIdiomaComponente.EComponente = DAOIdioma.getComponente(eIdiomaComponente.IdComponente);
                    eIdiomaComponente.EIdioma = DAOIdioma.getIdioma(eIdiomaComponente.IdIdioma);
                }

                return list;
            }
        }

        public static void addIdiomaToComponente(int idIdioma, int idComponente, string txt, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                EIdiomaComponente eIdiomaComponente = new EIdiomaComponente();
                eIdiomaComponente.IdIdioma = idIdioma;
                eIdiomaComponente.IdComponente = idComponente;
                eIdiomaComponente.Txt = txt;
                eIdiomaComponente.IdAcceso = eAcceso.Id;

                dbc.idiomaComponente.Add(eIdiomaComponente);
                dbc.SaveChanges();
            }
        }

        public static void updateIdiomaComponente(EIdiomaComponente eIdiomaComponente, EAcceso eAcceso)
        {
            using(var dbc = new DBC("idioma"))
            {
                eIdiomaComponente.IdAcceso = eAcceso.Id;
                dbc.Entry(eIdiomaComponente).State = EntityState.Modified;
                dbc.SaveChanges();
            }
        }

        public static void deleteIdiomaComponente(int id, EAcceso eAcceso)
        {
            using (var dbc = new DBC("idioma"))
            {
                EIdiomaComponente eIdiomaComponente = dbc.idiomaComponente.Find(id);

                dbc.Entry(eIdiomaComponente).State = EntityState.Deleted;
                dbc.SaveChanges();
            }
        }

        public static void addComponenteToFormulario(int idFormulario, int idComponente, EAcceso eAcceso)
        {
            using(var dbc = new DBC("idioma"))
            {
                EFormularioComponente formularioComponente = EFormularioComponente.newEmpty();
                formularioComponente.IdFormulario = idFormulario;
                formularioComponente.IdComponente = idComponente;
                formularioComponente.IdAcceso = eAcceso.Id;

                dbc.formularioComponente.Add(formularioComponente);
                dbc.SaveChanges();
            }
        }

        public static List<EFormularioComponente> getAllFormularioComponente()
        {
            using(var dbc = new DBC("idioma"))
            {
                List<EFormularioComponente> list = dbc.formularioComponente.ToList();

                foreach (EFormularioComponente formularioComponente in list)
                {
                    formularioComponente.EFormulario = DAOIdioma.getFormulario(formularioComponente.IdFormulario);
                    formularioComponente.EComponente = DAOIdioma.getComponente(formularioComponente.IdComponente);
                }

                return list;
            }
        }

        public static void deleteFormularioComponente(int id, EAcceso eAcceso)
        {
            using(var dbc = new DBC("idioma"))
            {
                EFormularioComponente eFormularioComponente = dbc.formularioComponente.Find(id);

                dbc.Entry(eFormularioComponente).State = EntityState.Deleted;
                dbc.SaveChanges();
            }
        }
        
        public static EMensaje obtenerMensajeTraducido(int id_idioma, string txt_original)
        {
            using (var dbc = new DBC("idioma"))
            {
                EMensaje eMensaje = (from x in dbc.mensaje where x.IdIdioma == id_idioma && x.TxtOriginal == txt_original select x).FirstOrDefault();
                return eMensaje;
            }
        }
    }
}
