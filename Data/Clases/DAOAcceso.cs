﻿using Data.Clases;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

public class DAOAcceso
{
    const string USUARIO = "usuario";
    const string MEDICO = "medico";
    const string ADMINISTRADOR = "administrador";

    public DAOAcceso()
    {

    }

    public static EAcceso registrarYObtenerAcceso(Int64 idUsuario)
    {
        using (var dbc = new DBC("seguridad"))
        {
            EAcceso eAcceso = new EAcceso();

            eAcceso.IdUsuario = idUsuario;
            eAcceso.FechaInicio = DateTime.Now.ToString("dd/MM/yyy");
            eAcceso.Ip = EAcceso.obtenerIP();
            eAcceso.Mac = EAcceso.obtenerMAC();
            eAcceso.UltimaActividad = eAcceso.FechaInicio;

            dbc.acceso.Add(eAcceso);
            dbc.SaveChanges();

            eAcceso = dbc.acceso.Where(x => x.IdUsuario == idUsuario && x.FechaInicio == eAcceso.FechaInicio).First();

            return eAcceso != null ? eAcceso : new EAcceso();
        }
    }

    public static void cerrarAcceso(int id)
    {
        using (var dbc = new DBC("seguridad"))
        {
            EAcceso eAcceso = dbc.acceso.Find(id);
            eAcceso.FechaFin = DateTime.Now.ToString();
            update(eAcceso);
        }
    }

    public static EAcceso get(int id)
    {
        using (var dbc = new DBC("seguridad"))
        {
            EAcceso eAcceso = dbc.acceso.Find(id);

            if (eAcceso == null)
            {
                throw new Exception("Acceso no encontrado");
            }

            return eAcceso;
        }
    }

    public static List<EAcceso> getAll()
    {
        using (var dbc = new DBC("seguridad"))
        {
            return dbc.acceso.ToList();
        }
    }

    public static void update(EAcceso eAcceso)
    {
        using (var dbc = new DBC("seguridad"))
        {
            dbc.Entry(eAcceso).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

}