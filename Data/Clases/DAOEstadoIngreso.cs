﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;

namespace Data.Clases
{
    public class DAOEstadoIngreso
    {

        public static void add(Int64 idUsuario)
        {
            using (var dbc = new DBC("seguridad"))
            {
                EEstadoIngreso eEstadoIngreso = EEstadoIngreso.newEmpty();
                eEstadoIngreso.IdUsuario = idUsuario;

                dbc.estadoIngreso.Add(eEstadoIngreso);
                dbc.SaveChanges();
            }
        }

        public static EEstadoIngreso get(int id)
        {
            using(var dbc = new DBC("seguridad"))
            {
                EEstadoIngreso eEstadoIngreso = dbc.estadoIngreso.Find(id);
                return eEstadoIngreso != null ? eEstadoIngreso : EEstadoIngreso.newEmpty();
            }
        }

        public static List<EEstadoIngreso> getAll()
        {
            using (var dbc = new DBC("seguridad"))
            {
                return dbc.estadoIngreso.ToList();
            }
        }

        public static void update(EEstadoIngreso eEstadoIngreso)
        {
            using (var dbc = new DBC("seguridad"))
            {
                dbc.Entry(eEstadoIngreso).State = EntityState.Modified;
                dbc.SaveChanges();
            }
        }

        public static void delete(int id)
        {
            using (var dbc = new DBC("seguridad"))
            {
                EEstadoIngreso eEstadoIngreso = dbc.estadoIngreso.Find(id);
                dbc.Entry(eEstadoIngreso).State = EntityState.Deleted;
                dbc.SaveChanges();
            }
        }
    }
}
