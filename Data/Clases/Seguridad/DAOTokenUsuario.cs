﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases.Seguridad;

namespace Data.Clases.Seguridad
{
    public class DAOTokenUsuario
    {
        public TokenUsuario get(string token)
        {
            using (var dbc = new DBC("seguridad"))
            {
                var obj = (from x in dbc.TokenUsuario where x.Token == token select x).FirstOrDefault();

                return obj;
            }
        }

        public void add(TokenUsuario tokenUsuario)
        {
            using (var dbc = new DBC("seguridad"))
            {
                dbc.TokenUsuario.Add(tokenUsuario);
                dbc.SaveChanges();
            }
        }
    }
}
