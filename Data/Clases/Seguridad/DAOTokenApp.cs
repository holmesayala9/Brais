﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases.Seguridad;

namespace Data.Clases.Seguridad
{
    public class DAOTokenApp
    {
        public static TokenApp get(string token)
        {
            using(var dbc = new DBC("seguridad"))
            {
                var obj = (from x in dbc.TokenApp where x.Token == token select x).FirstOrDefault();

                return obj;
            }
        }

        public static void add(TokenApp tokenApp)
        {
            using(var dbc = new DBC("seguridad"))
            {
                dbc.TokenApp.Add(tokenApp);
                dbc.SaveChanges();
            }
        }

        //public static List<TokenApp> getTokensUsuario(string usuario)
        //{
        //    using(var dbc = new DBC("seguridad"))
        //    {
        //        var list = (from x in dbc.TokenApp where x.UsuarioApp == usuario select x).ToList();

        //        return list;
        //    }
        //}
    }
}
