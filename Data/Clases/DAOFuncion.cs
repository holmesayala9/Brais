﻿using Data.Clases;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Linq;

public class DAOFuncion
{
    public DAOFuncion()
    {

    }

    public static Boolean validarInicioSesion( string identificacion, string contrasena )
    {
        using(var dbc = new DBC("funcion"))
        {
            Boolean usuarioValido = false;
            Boolean medicoValido = false;
            Boolean administradorValido = false;

            usuarioValido = (from x in dbc.usuario where x.Identificacion == identificacion && x.Contrasena == contrasena select x).Count() > 0;
            medicoValido = (from x in dbc.medico where x.Identificacion == identificacion && x.Contrasena == contrasena select x).Count() > 0;
            administradorValido = (from x in dbc.administrador where x.Identificacion == identificacion && x.Contrasena == contrasena select x).Count() > 0;

            return usuarioValido || medicoValido || administradorValido;
        }
    }

    public static string getTipoUsuario(Int64 id)
    {
        using(var dbc = new DBC("funcion"))
        {
            if ((from x in dbc.usuario where x.Id == id select x).Count() > 0)
            {
                return "usuario";
            }
            else if ((from x in dbc.medico where x.Id == id select x).Count() > 0)
            {
                return "medico";
            }
            else if ((from x in dbc.administrador where x.Id == id select x).Count() > 0)
            {
                return "administrador";
            }

            return "";
        }
    }

    public static string getTipoUsuario(string identificacion)
    {
        using (var dbc = new DBC("funcion"))
        {
            if ((from x in dbc.administrador where x.Identificacion == identificacion select x).Count() > 0)
            {
                return "administrador";
            }
            else if ((from x in dbc.usuario where x.Identificacion == identificacion select x).Count() > 0)
            {
                return "usuario";
            }
            else if ((from x in dbc.medico where x.Identificacion == identificacion select x).Count() > 0)
            {
                return "medico";
            }

            return "";
        }
    }

    public static Boolean verificarIdentificacion(Int64 id, string identificacion)
    {
        using(var dbc = new DBC("funcion"))
        {
            return (from x in dbc.usuario where x.Id != id && x.Identificacion == identificacion select x).Count() == 0 &&
                (from x in dbc.medico where x.Id != id && x.Identificacion == identificacion select x).Count() == 0 &&
                (from x in dbc.administrador where x.Id != id && x.Identificacion == identificacion select x).Count() == 0;
        }
    }

    public static Boolean verificarCorreo(Int64 id, string correo)
    {
        using (var dbc = new DBC("funcion"))
        {
            return (from x in dbc.usuario where x.Id != id && x.Correo == correo select x).Count() == 0 &&
                (from x in dbc.medico where x.Id != id && x.Correo == correo select x).Count() == 0 &&
                (from x in dbc.administrador where x.Id != id && x.Correo == correo select x).Count() == 0;
        }
    }
}