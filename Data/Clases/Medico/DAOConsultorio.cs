﻿using Data.Clases;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

public class DAOConsultorio
{
    public DAOConsultorio()
    {
    }

    public static void add(EConsultorio eConsultorio, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EConsultorio.ESQUEMA))
        {
            eConsultorio.IdAcceso = eAcceso.Id;
            dbc.consultorio.Add(eConsultorio);
            dbc.SaveChanges();
            DAOAuditoria.insert(eConsultorio, eAcceso, EConsultorio.ESQUEMA, EConsultorio.TABLA);
        }
    }

    public static EConsultorio get(int id)
    {
        using (var dbc = new DBC(EConsultorio.ESQUEMA))
        {
            EConsultorio eConsultorio = dbc.consultorio.Find(id);
            return eConsultorio != null ? eConsultorio : EConsultorio.newEmpty();
        }
    }

    public static List<EConsultorio> getAll()
    {
        using (var dbc = new DBC(EConsultorio.ESQUEMA))
        {
            return dbc.consultorio.ToList();
        }
    }

    public static List<EConsultorio> buscar(string parametro)
    {
        using(var dbc = new DBC(EConsultorio.ESQUEMA))
        {
            var list = (from x in dbc.consultorio where x.Nombre.ToLower().Contains(parametro.ToLower()) || x.Descripcion.ToLower().Contains(parametro.ToLower()) select x).ToList();

            return list;
        }
    }

    public static void update(EConsultorio eConsultorio, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EConsultorio.ESQUEMA))
        {
            DAOAuditoria.update(eConsultorio, DAOConsultorio.get(eConsultorio.Id), eAcceso, EConsultorio.ESQUEMA, EConsultorio.TABLA);
            eConsultorio.IdAcceso = eAcceso.Id;
            dbc.Entry(eConsultorio).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

    public static void delete(int id, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EConsultorio.ESQUEMA))
        {
            DAOAuditoria.delete(DAOConsultorio.get(id), eAcceso, EConsultorio.ESQUEMA, EConsultorio.TABLA);
            EConsultorio eConsultorio = dbc.consultorio.Find(id);
            dbc.Entry(eConsultorio).State = EntityState.Deleted;
            dbc.SaveChanges();
        }
    }
}