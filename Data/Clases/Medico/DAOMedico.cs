﻿using Data.Clases;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Utilitaria.Clases.Medico;

public class DAOMedico
{
    public const string ACTIVOS = "activos";
    public const string INACTIVOS = "inactivos";
    public const string TODOS = "todos";

    public DAOMedico()
    {
    }

    public static EMedico get(Int64 id)
    {
        using (var dbc = new DBC(EMedico.ESQUEMA))
        {
            EMedico eMedico = dbc.medico.Find(id);

            if(eMedico != null)
            {
                eMedico.EEspecialidad = DAOEspecialidad.get(eMedico.IdEspecialidad);
                eMedico.EConsultorio = DAOConsultorio.get(eMedico.IdConsultorio);
                eMedico.Horario = JsonConvert.DeserializeObject<Horario>(eMedico.SHorario);
            }
            return eMedico != null ? eMedico : EMedico.newEmpty();
        }
    }

    public static EMedico get(string identificacion)
    {
        using (var dbc = new DBC(EMedico.ESQUEMA))
        {
            EMedico eMedico = (from x in dbc.medico where x.Identificacion == identificacion select x).FirstOrDefault();

            if (eMedico != null)
            {
                eMedico.EEspecialidad = DAOEspecialidad.get(eMedico.IdEspecialidad);
                eMedico.EConsultorio = DAOConsultorio.get(eMedico.IdConsultorio);
                eMedico.Horario = JsonConvert.DeserializeObject<Horario>(eMedico.SHorario);
            }
            return eMedico != null ? eMedico : EMedico.newEmpty();
        }
    }

    public static List<EMedico> getMedicosByEspecialidad(int idEspecialidad)
    {
        using(var dbc = new DBC(EMedico.ESQUEMA))
        {
            var list = (from x in dbc.medico where x.IdEspecialidad == idEspecialidad select x).ToList();

            return list;
        }
    }

    public static List<EMedico> getAll()
    {
        using(var dbc = new DBC(EMedico.ESQUEMA))
        {
            List<EMedico> list = dbc.medico.ToList();

            foreach (EMedico eMedico in list)
            {
                eMedico.EEspecialidad = DAOEspecialidad.get(eMedico.IdEspecialidad);
                eMedico.EConsultorio = DAOConsultorio.get(eMedico.IdConsultorio);
                eMedico.Horario = JsonConvert.DeserializeObject<Horario>(eMedico.SHorario);
            }

            return list;
        }
    }

    public static List<EMedico> buscarMedico(string parametro)
    {
        using (var dbc = new DBC(EMedico.ESQUEMA))
        {
            List<EMedico> list = (from x in dbc.medico
                                  where x.Identificacion.ToLower().Contains(parametro) ||
                                      x.Nombre.ToLower().Contains(parametro) ||
                                      x.Apellido.ToLower().Contains(parametro) ||
                                      x.Telefono.ToLower().Contains(parametro) ||
                                      x.Correo.ToLower().Contains(parametro)
                                  select x).ToList();

            foreach (EMedico eMedico in list)
            {
                eMedico.EEspecialidad = DAOEspecialidad.get(eMedico.IdEspecialidad);
                eMedico.EConsultorio = DAOConsultorio.get(eMedico.IdConsultorio);
                eMedico.Horario = JsonConvert.DeserializeObject<Horario>(eMedico.SHorario);
            }

            return list;
        }
    }

    public static void agregarMedico(EMedico eMedico, EAcceso eAcceso)
    {
        using(var dbc = new DBC(EMedico.ESQUEMA))
        {
            eMedico.IdAcceso = eAcceso.Id;
            eMedico.Id = DateTime.Now.Ticks;
            dbc.medico.Add(eMedico);
            dbc.SaveChanges();
            DAOAuditoria.insert(eMedico, eAcceso, EMedico.ESQUEMA, EMedico.TABLA);
        }
    }

    public static void update(EMedico eMedico, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EMedico.ESQUEMA))
        {
            DAOAuditoria.update(eMedico, DAOMedico.get(eMedico.Id), eAcceso, EMedico.ESQUEMA, EMedico.TABLA);
            eMedico.IdAcceso = eAcceso.Id;
            dbc.Entry(eMedico).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

    public static void delete(Int64 id, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EMedico.ESQUEMA))
        {
            DAOAuditoria.delete(DAOMedico.get(id), eAcceso, EMedico.ESQUEMA, EMedico.TABLA);
            EMedico eMedico = dbc.medico.Find(id);
            dbc.Entry(eMedico).State = EntityState.Deleted;
            dbc.SaveChanges();
        }
    }

    public static void modificarHorario(EMedico eMedico, EAcceso eAcceso)
    {
        eMedico.SHorario = JsonConvert.SerializeObject(eMedico.Horario);
        DAOMedico.update(eMedico, eAcceso);
    }

}