﻿using Data.Clases;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Utilitaria.Clases.Medico;

/// <summary>
/// Descripción breve de DAOEspecialidad
/// </summary>
public class DAOEspecialidad
{
    public DAOEspecialidad()
    {
    }

    public static void add(EEspecialidad eEspecialidad, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EEspecialidad.ESQUEMA))
        {
            eEspecialidad.IdAcceso = eAcceso.Id;
            dbc.especialidad.Add(eEspecialidad);
            dbc.SaveChanges();
            DAOAuditoria.insert(eEspecialidad, eAcceso, EEspecialidad.ESQUEMA, EEspecialidad.TABLA);
        }
    }

    public static EEspecialidad get(int id)
    {
        using(var dbc = new DBC(EEspecialidad.ESQUEMA))
        {
            EEspecialidad eEspecialidad = dbc.especialidad.Find(id);
            return eEspecialidad != null ? eEspecialidad : EEspecialidad.newEmpty();
        }
    }

    public static List<EEspecialidad> getAll()
    {
        using (var dbc = new DBC(EEspecialidad.ESQUEMA))
        {
            return dbc.especialidad.ToList();
        }
    }

    public static List<EEspecialidad> buscar(string Parametro)
    {
        using(var dbc = new DBC(EEspecialidad.ESQUEMA))
        {
            var list = (from x in dbc.especialidad where x.Nombre.ToLower().Contains(Parametro.ToLower()) select x).ToList();
            return list;
        }
    }

    public static void update(EEspecialidad eEspecialidad, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EEspecialidad.ESQUEMA))
        {
            DAOAuditoria.update(eEspecialidad, DAOEspecialidad.get(eEspecialidad.Id), eAcceso, EEspecialidad.ESQUEMA, EEspecialidad.TABLA);
            eEspecialidad.IdAcceso = eAcceso.Id;
            dbc.Entry(eEspecialidad).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

    public static void delete(EEspecialidad eEspecialidad, EAcceso eAcceso)
    {
        using(var dbc = new DBC(EEspecialidad.ESQUEMA))
        {
            DAOAuditoria.delete(eEspecialidad, eAcceso, EEspecialidad.ESQUEMA, EEspecialidad.TABLA);
            dbc.Entry(eEspecialidad).State = EntityState.Deleted;
            dbc.SaveChanges();
        }
    }
}