﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;
using Utilitaria.Clases.Seguridad;
using Utilitaria.Clases.Usuario;

namespace Data.Clases
{
    public class DBC : DbContext
    {
        static DBC()
        {
            Database.SetInitializer<DBC>(null);
        }

        private readonly string schema;

        public DBC(string schema) : base("name=Postgres")
        {
            this.schema = schema;
        }

        public DbSet<EUsuario> usuario { get; set; }

        public DbSet<EHistorial> historial { get; set; }

        public DbSet<EMedico> medico { get; set; }

        public DbSet<ECita> cita { get; set; }

        public DbSet<EAdministrador> administrador { get; set; }

        public DbSet<Parametro> parametro { get; set; }

        public DbSet<EAcceso> acceso { get; set; }

        public DbSet<EEstadoIngreso> estadoIngreso { get; set; }

        public DbSet<EEspecialidad> especialidad { get; set; }

        public DbSet<EConsultorio> consultorio { get; set; }

        public DbSet<EIdioma> idioma { get; set; }

        public DbSet<EFormulario> formulario { get; set; }

        public DbSet<EComponente> componente { get; set; }

        public DbSet<EIdiomaComponente> idiomaComponente { get; set; }

        public DbSet<EFormularioComponente> formularioComponente { get; set; }

        public DbSet<EMensaje> mensaje { get; set; }

        public DbSet<ESolicitudContrasena> solicitudContrasena { get; set; }

        public DbSet<EAuditoria> auditoria { get; set; }

        public DbSet<TokenApp> TokenApp { get; set; }

        public DbSet<TokenUsuario> TokenUsuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(this.schema);
            base.OnModelCreating(modelBuilder);
        }
    }
}
