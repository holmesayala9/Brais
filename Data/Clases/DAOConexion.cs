﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DAOConexion
/// </summary>
public class DAOConexion
{
    public DAOConexion()
    {

    }

    public static NpgsqlConnection obtenerConexion()
    {
        return new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Postgres"].ConnectionString);
    }
}