﻿using Npgsql;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data.Clases;
using System.Data.Entity;

public class DAOHistorial
{
    public DAOHistorial()
    {

    }

    public static void add(EHistorial eHistorial, EAcceso eAcceso)
    {
        using(var dbc = new DBC(EHistorial.ESQUEMA))
        {
            eHistorial.IdAcceso = eAcceso.Id;
            dbc.historial.Add(eHistorial);
            dbc.SaveChanges();
            DAOAuditoria.insert(eHistorial, eAcceso, EHistorial.ESQUEMA, EHistorial.TABLA);
        }
    }

    public static void update(EHistorial eHistorial, EAcceso eAcceso)
    {
        using(var dbc = new DBC(EHistorial.ESQUEMA))
        {
            DAOAuditoria.update(eHistorial, DAOHistorial.get(eHistorial.Id), eAcceso, EHistorial.ESQUEMA, EHistorial.TABLA);
            eHistorial.IdAcceso = eAcceso.Id;
            dbc.Entry(eHistorial).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

    public static EHistorial get( int id )
    {
        using(var dbc = new DBC(EHistorial.ESQUEMA))
        {
            EHistorial eHistorial = dbc.historial.Find(id);

            if (eHistorial != null)
            {
                eHistorial.ECita = DAOCita.get(eHistorial.IdCita);
                eHistorial.EUsuario = DAOUsuario.get(eHistorial.IdUsuario);
            }

            return eHistorial != null ? eHistorial : EHistorial.newEmpty();
        }
    }

    public static List<EHistorial> obtenerHistorialPorUsuario(Int64 idUsuario)
    {
        using(var dbc = new DBC(EHistorial.ESQUEMA))
        {
            List<EHistorial> list = (from x in dbc.historial where x.IdUsuario == idUsuario orderby x.IdCita descending select x).ToList();

            foreach (EHistorial eHistorial  in list)
            {
                eHistorial.ECita = DAOCita.get(eHistorial.IdCita);
                eHistorial.EUsuario = DAOUsuario.get(eHistorial.IdUsuario);
            }

            return list;
        }
    }

    public static EHistorial obtenerHistorialPorCita(int idCita)
    {
        using(var dbc = new DBC(EHistorial.ESQUEMA))
        {
            EHistorial eHistorial = (from x in dbc.historial where x.IdCita == idCita select x).FirstOrDefault();

            if (eHistorial != null)
            {
                eHistorial.ECita = DAOCita.get(eHistorial.IdCita);
                eHistorial.EUsuario = DAOUsuario.get(eHistorial.IdUsuario);
            }

            return eHistorial != null ? eHistorial : EHistorial.newEmpty();
        }
    }

}