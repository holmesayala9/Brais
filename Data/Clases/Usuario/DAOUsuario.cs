﻿using Data.Clases;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Utilitaria.Clases.Usuario;

public class DAOUsuario
{

    public DAOUsuario()
    {
    }

    public static EUsuario get(Int64 id)
    {
        using (var dbc = new DBC(EUsuario.ESQUEMA))
        {
            EUsuario eUsuario = dbc.usuario.Find(id);

            return eUsuario != null ? eUsuario : EUsuario.newEmpty();
        }
    }

    public static EUsuario get(string identificacion)
    {
        using(var dbc = new DBC(EUsuario.ESQUEMA))
        {
            EUsuario eUsuario = (from x in dbc.usuario where x.Identificacion == identificacion select x).First();
            return eUsuario != null ? eUsuario : EUsuario.newEmpty();
        }
    }

    public static List<EUsuario> buscarUsuario(string parametro)
    {
        using(var dbc = new DBC(EUsuario.ESQUEMA))
        {
            return (from x in dbc.usuario where x.Identificacion.ToLower().Contains(parametro) ||
                   x.Nombre.ToLower().Contains(parametro) ||
                   x.Apellido.ToLower().Contains(parametro) ||
                   x.Telefono.ToLower().Contains(parametro) || 
                   x.Correo.ToLower().Contains(parametro) select x).ToList();
        }
    }

    public static void add( EUsuario eUsuario, EAcceso eAcceso )
    {
        using(var dbc = new DBC(EUsuario.ESQUEMA))
        {
            eUsuario.IdAcceso = eAcceso.Id;
            eUsuario.Id = DateTime.Now.Ticks;
            dbc.usuario.Add(eUsuario);
            dbc.SaveChanges();
            DAOAuditoria.insert(eUsuario, eAcceso, EUsuario.ESQUEMA, EUsuario.TABLA);
        }
    }

    public static void update(EUsuario eUsuario, EAcceso eAcceso)
    {
        using(var dbc = new DBC(EUsuario.ESQUEMA))
        {
            DAOAuditoria.update(eUsuario, DAOUsuario.get(eUsuario.Id), eAcceso, EUsuario.ESQUEMA, EUsuario.TABLA);
            eUsuario.IdAcceso = eAcceso.Id;
            dbc.Entry(eUsuario).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

    public static void delete(Int64 id, EAcceso eAcceso)
    {
        using (var dbc = new DBC(EUsuario.ESQUEMA))
        {
            DAOAuditoria.delete(DAOUsuario.get(id), eAcceso, EUsuario.ESQUEMA, EUsuario.TABLA);
            EUsuario eUsuario = dbc.usuario.Find(id);
            dbc.Entry(eUsuario).State = EntityState.Deleted;
            dbc.SaveChanges();
        }
    }
}