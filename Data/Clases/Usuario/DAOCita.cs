﻿using Data.Clases;
using Data.Clases.Administrador;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Utilitaria.Clases.Medico;

public class DAOCita
{
    public DAOCita()
    {

    }

    public static void add(ECita eCita, EAcceso eAcceso)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            eCita.IdAcceso = eAcceso.Id;
            dbc.cita.Add(eCita);
            dbc.SaveChanges();
            DAOAuditoria.insert(eCita, eAcceso, ECita.ESQUEMA, ECita.TABLA);
        }
    }

    public static void update(ECita eCita, EAcceso eAcceso)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            DAOAuditoria.update(eCita, DAOCita.get(eCita.Id), eAcceso, ECita.ESQUEMA, ECita.TABLA);
            eCita.IdAcceso = eAcceso.Id;
            dbc.Entry(eCita).State = EntityState.Modified;
            dbc.SaveChanges();
        }
    }

    public static void delete(ECita eCita, EAcceso eAcceso)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            DAOAuditoria.delete(eCita, eAcceso, ECita.ESQUEMA, ECita.TABLA);
            eCita.IdAcceso = eAcceso.Id;
            dbc.Entry(eCita).State = EntityState.Deleted;
            dbc.SaveChanges();
        }
    }

    public static ECita get(int id)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            ECita eCita = dbc.cita.Find(id);

            if(eCita != null)
            {
                eCita.EMedico = DAOMedico.get(eCita.IdMedico);
                eCita.EUsuario = DAOUsuario.get(eCita.IdUsuario);
            }

            return eCita != null ? eCita : ECita.newEmpty();
        }
    }

    public static List<ECita> obtenerCitasDeUsuario(Int64 idUsuario)
    {
        using(var dbc = new DBC(ECita.ESQUEMA))
        {
            List <ECita> list = (from x in dbc.cita where x.IdUsuario == idUsuario orderby x.Fecha, x.HoraInicio ascending select x).ToList();

            foreach (ECita eCita in list)
            {
                eCita.EUsuario = DAOUsuario.get(eCita.IdUsuario);
                eCita.EMedico = DAOMedico.get(eCita.IdMedico);
            }

            return list;
        }
    }

    public static Boolean medicoTieneCitasPendientes(Int64 idMedico)
    {
        using(var dbc = new DBC(ECita.ESQUEMA))
        {
            List<ECita> list = (from c in dbc.cita where c.IdMedico == idMedico select c).ToList();

            list = list.FindAll(c => DateTime.Parse(c.Fecha) >= DateTime.Today && c.Estado == ECita.RESERVADO);

            return list.Count > 0 ? true : false;
        }
    }

    public static List<ECita> obtenerCitasDeMedicoPorFecha(EMedico eMedico, string fecha)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            List<ECita> list = (from x in dbc.cita
                                where x.Fecha == fecha && x.IdMedico == eMedico.Id orderby x.HoraInicio ascending select x).ToList();

            foreach (ECita eCita in list)
            {
                eCita.EUsuario = DAOUsuario.get(eCita.IdUsuario);
                eCita.EMedico = DAOMedico.get(eCita.IdMedico);
            }

            return list;
        }
    }

    public static List<ECita> obtenerCitasDeMedicoPorFecha(Int64 idMedico, string fecha)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            List<ECita> list = (from x in dbc.cita
                                where x.Fecha == fecha && x.IdMedico == idMedico
                                orderby x.HoraInicio ascending
                                select x).ToList();

            foreach (ECita eCita in list)
            {
                eCita.EUsuario = DAOUsuario.get(eCita.IdUsuario);
                eCita.EMedico = DAOMedico.get(eCita.IdMedico);
            }

            return list;
        }
    }

    public static List<ECita> obtenerCitasActualesSegunEspecialidad(int idEspecialidad)
    {
        using(var dbc = new DBC(ECita.ESQUEMA))
        {
            List<ECita> list = (from c in dbc.cita
                    join e in dbc.especialidad on c.Servicio.ToLower() equals e.Nombre.ToLower()
                    where c.Estado == ECita.DISPONIBLE && 
                    e.Id == idEspecialidad
                    orderby c.Fecha, c.HoraInicio select c).ToList();

            list = list.FindAll(x => DateTime.Parse(x.Fecha+" "+x.HoraInicio) > DateTime.Now);

            foreach (ECita eCita in list)
            {
                eCita.EUsuario = DAOUsuario.get(eCita.IdUsuario);
                eCita.EMedico = DAOMedico.get(eCita.IdMedico);
            }

            return list;
        }
    }

    public static void generarCitasDeMedico(Int64 idMedico, EAcceso eAcceso)
    {
        DateTime fecha = DateTime.Today;
        List<int> diasLaborales = DAOParametro.get().DiasLaborales;
        DAOCita dAOCita = new DAOCita();

        List<ECita> citasGeneradas = new List<ECita>();

        while (true)
        {
            int diaSemana = (int)fecha.DayOfWeek;
            if (diasLaborales.Exists(d => d == diaSemana))
            {
                citasGeneradas.AddRange(dAOCita.crearCitasMedicoPorFecha(idMedico, fecha, eAcceso));
            }
            if (fecha.DayOfWeek == DayOfWeek.Sunday)
            {
                break;
            }
            fecha = fecha.AddDays(1);
        }

        using(var dbc = new DBC(ECita.ESQUEMA))
        {
            List<ECita> disponibles = (from x in dbc.cita where x.IdMedico == idMedico && x.Estado == ECita.DISPONIBLE select x).ToList();
            disponibles.ForEach(c => DAOCita.delete(c, eAcceso));

            citasGeneradas.ForEach(c => DAOCita.add(c, eAcceso));
        }
    }

    public List<ECita> crearCitasMedicoPorFecha(Int64 idMedico, DateTime fecha, EAcceso eAcceso)
    {
        using (var dbc = new DBC(ECita.ESQUEMA))
        {
            int duracionCitas = int.Parse(DAOParametro.get().DuracionCitas);
            int numDiaSemana = (int)fecha.DayOfWeek;

            string f = fecha.ToShortDateString();
            string servicio = DAOEspecialidad.get(DAOMedico.get(idMedico).IdEspecialidad).Nombre;
            List<ECita> citasReservadas = (from x in dbc.cita where x.Estado != ECita.DISPONIBLE && x.IdMedico == idMedico && x.Fecha == f select x).ToList();

            EMedico eMedico = DAOMedico.get(idMedico);

            List<ECita> citasGeneradas = new List<ECita>();

            foreach (Rango rango in eMedico.Horario.Rangos.Where(r => r.Dia == numDiaSemana))
            {
                List<Rango> rangos = generarRangos(DateTime.Parse(rango.Inicio), DateTime.Parse(rango.Fin), numDiaSemana, duracionCitas);

                foreach (Rango r in rangos)
                {
                    if (validarCruceCitasReservadas(r, citasReservadas))
                    {
                        citasGeneradas.Add(new ECita {
                            HoraInicio = r.Inicio,
                            HoraFin = r.Fin,
                            Fecha = f,
                            Servicio = servicio,
                            IdMedico = idMedico,
                            IdUsuario = -1,
                            Estado = ECita.DISPONIBLE,
                            IdAcceso = eAcceso.Id
                        });
                    }
                }
            }

            return citasGeneradas;
        }
    }

    public List<Rango> generarRangos( DateTime inicio, DateTime fin, int numDiaSemana, int intervaloMinutos )
    {
        List<Rango> list = new List<Rango>();

        fin = fin.AddMinutes(-intervaloMinutos);

        for (; inicio <= fin; inicio = inicio.AddMinutes(intervaloMinutos))
        {
            list.Add( new Rango {
                Inicio = inicio.ToString("HH:mm"),
                Fin = inicio.AddMinutes(intervaloMinutos).ToString("HH:mm"),
                Dia = numDiaSemana
            });
        }

        return list;
    }

    public Boolean validarCruceCitasReservadas(Rango rango, List<ECita> citasReservadas)
    {
        foreach (ECita eCita in citasReservadas)
        {
            Rango rangoCita = new Rango();
            rangoCita.Dia = (int)DateTime.Parse(eCita.Fecha).DayOfWeek;
            rangoCita.Inicio = eCita.HoraInicio;
            rangoCita.Fin = eCita.HoraFin;

            if (rango.seCruzaCon(rangoCita))
            {
                return false;
            }
        }

        return true;
    }
}