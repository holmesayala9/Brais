﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using Logica.Clases;
using Logica.Clases.Administrador;
using Newtonsoft.Json;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;

public partial class View_Principal_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void auditoria()
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_Ingresar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        Respuesta respuesta = Respuesta.newRespuesta();
        try
        {
            respuesta = LFuncion.iniciarSesion(TB_Identificacion.Text.Trim(), TB_Contrasena.Text.Trim());
            Session["usuario"] = respuesta.Data;
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex)
        {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
        GestorRedireccion.manage(this, respuesta);
    }

}