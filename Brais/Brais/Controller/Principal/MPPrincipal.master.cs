﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;

public partial class View_Principal_MPPrincipal : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UsuarioBasico usuarioBasico = (UsuarioBasico)Session["usuario"];

            Int64 idPrueba = usuarioBasico.Id;

            //Session.Abandon();
        }
        catch (Exception) { }

        cargarDDL_Idioma();
        traducir();
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {

    }

    protected void traducir()
    {
        try
        {

            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch (Exception ex)
        {
            int idIdioma = LIdioma.validarIdioma("0");
            Session["idiomaSeleccionado"] = idIdioma;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
    }

    protected void cargarDDL_Idioma()
    {
        string value = "";
        try
        {
            value = DDL_Idioma.SelectedValue;
        }
        catch (Exception) { }
        try
        {
            DDL_Idioma.DataSource = LIdioma.getIdioma();
            DDL_Idioma.DataBind();
            DDL_Idioma.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, new Panel()); }
        try
        {
            DDL_Idioma.Items.FindByValue(value).Selected = true;
        }
        catch (Exception) { }
    }

    protected void BTN_PaginaPrincipal_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Principal/PaginaPrincipal.aspx");
    }

    protected void BTN_SobreNosotros_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Principal/SobreNosotros.aspx");
    }

    protected void BTN_Acceder_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Principal/Login.aspx");
    }

    protected void BTN_Crear_Cuenta_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Principal/CrearCuenta.aspx");
    }

    protected void DDL_Idioma_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idIdioma = int.Parse(DDL_Idioma.SelectedValue);
        Session["idiomaSeleccionado"] = idIdioma;
        Response.Redirect(Page.Request.Url.ToString(), true);
    }
}
