﻿using System;
using System.Web.UI;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Principal_CrearCuenta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_CrearCuenta_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EUsuario eUsuario = obtenerDatosEncapsulados();

            EAcceso eAcceso = new EAcceso();

            Respuesta respuesta = LUsuario.agregarUsuario(eUsuario, eAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            TB_Identificacion.Text = "";
            TB_Nombre.Text = "";
            TB_Apellido.Text = "";
            TB_FechaNacimiento.Text = "";
            DDL_Genero.SelectedValue = "Ninguno";
            TB_Correo.Text = "";
            TB_Telefono.Text = "";
            TB_Contrasena.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }

    protected EUsuario obtenerDatosEncapsulados()
    {
        EUsuario eUsuario = new EUsuario();

        eUsuario.Identificacion = TB_Identificacion.Text.Trim();
        eUsuario.Nombre = TB_Nombre.Text.Trim();
        eUsuario.Apellido = TB_Apellido.Text.Trim();
        eUsuario.FechaNacimiento = TB_FechaNacimiento.Text;
        eUsuario.Genero = DDL_Genero.Text;
        eUsuario.Correo = TB_Correo.Text.Trim();
        eUsuario.Telefono = TB_Telefono.Text.Trim();
        eUsuario.Contrasena = TB_Contrasena.Text.Trim();

        return eUsuario;
    }
}