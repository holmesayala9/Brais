﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases;
using Logica.Clases.Medico;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;

public partial class View_Medico_ActualizarDatos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarDatos();
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarDatos()
    {
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];

            LB_Identificacion.Text = eMedico.Identificacion;
            LB_Nombre.Text = eMedico.Nombre;
            LB_Apellido.Text = eMedico.Apellido;
            LB_Correo.Text = eMedico.Correo;
            LB_Telefono.Text = eMedico.Telefono;
            LB_Especialidad.Text = eMedico.EEspecialidad.Nombre;
            LB_ConsultorioActual.Text = "Consultorio asignado: " + (eMedico.EConsultorio != null ? eMedico.EConsultorio.Nombre + "<br /> Descripcion: " + eMedico.EConsultorio.Descripcion : "Ninguno");
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Modificar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];

            eMedico.Correo = TB_Correo.Text;
            eMedico.Telefono = TB_Telefono.Text;

            Respuesta respuesta = LMedico.update(eMedico, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            GestorRedireccion.manage(this, respuesta);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }

    }

    protected EMedico obtenerDatosEncapsulados()
    {
        EMedico eMedico = new EMedico();

        eMedico.Identificacion = LB_Identificacion.Text.Trim();
        eMedico.Nombre = LB_Nombre.Text.Trim();
        eMedico.Apellido = LB_Apellido.Text.Trim();
        eMedico.Correo = TB_Correo.Text;
        eMedico.Telefono = TB_Telefono.Text;
        eMedico.Contrasena = TB_NuevaContrasena.Text.Trim();

        return eMedico;
    }

    protected void CB_ModificarContrasena_CheckedChanged(object sender, EventArgs e)
    {
        P_ModificarContrasena.Visible = ((CheckBox)sender).Checked;
    }


    protected void BTN_ModificarContrasena_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];

            Respuesta respuesta = LMedico.updateContrasena(TB_Contrasena.Text, TB_NuevaContrasena.Text, eMedico.Id, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }
}