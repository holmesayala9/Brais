﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Medico;
using Utilitaria.Clases.Medico;

public partial class View_Medico_ReporteHorario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];
            List<ReporteHorario> reportehorarios = LReportesMedico.dataHorario(eMedico.Horario);

            CRS_Reporte.ReportDocument.SetDataSource(reportehorarios);

            CRV_Reporte.ReportSource = CRS_Reporte;
        }
        catch (Exception ex)
        {
            Response.Redirect("~/View/Medico/Agenda.aspx");
        }
    }
}