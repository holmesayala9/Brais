﻿using System;
using System.Web.UI;
using Utilitaria.Clases;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;

public partial class View_Medico_MPMedico : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetNoStore();
        Respuesta respuesta = Respuesta.newRespuesta();

        try
        {
            Session["usuario"] = LSeguridad.renovarUsuario((UsuarioBasico)Session["usuario"]);

            respuesta = LSeguridad.verificarUsuario((UsuarioBasico)Session["usuario"], AppRelativeVirtualPath);
        }
        catch (Exception ex)
        {
            Response.Redirect("~/View/Principal/Login.aspx");
        }

        GestorRedireccion.manage(this.Response, respuesta);

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_Agenda_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Medico/Agenda.aspx");
    }

    protected void BTN_MisDatos_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Medico/ActualizarDatos.aspx");
    }

    protected void BTN_Horario_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Medico/Horario.aspx");
    }

    protected void BTN_CerrarSesion_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Redirect("~/View/Principal/Login.aspx");
        }
        catch (Exception) { }
    }

    protected void T_Actividad_Tick(object sender, EventArgs e)
    {
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];

            LSeguridad.reportarActividad(eMedico.EAcceso);
        }
        catch (Exception) { }
    }
}
