﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Logica.Clases.Usuario;
using Logica.Clases.Medico;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;

public partial class View_Medico_Horario : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            EMedico medic = (EMedico)Session["usuario"];
            EMedico medic3 = new EMedico();

            LFuncion.validarSession(medic);
            medic3 = LFuncion.validar_postback_medico(medic, IsPostBack);
            MaintainScrollPositionOnPostBack = true;
            try
            {
                cargarDias();
                cargarHoraInicial();
                cargarHoraFinal();
                avisarCambiosNoGuardados();
                Session["horario"] = ((EMedico)Session["usuario"]).Horario;
            }
            catch {}
        }
        catch { }
        try
        {
            mostrarHorario();
        }
        catch(Exception) { }
        
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void avisarCambiosNoGuardados()
    {
        try
        {
            string horarioMedico = JsonConvert.SerializeObject(((EMedico)Session["usuario"]).Horario);
            string horarioModificado = JsonConvert.SerializeObject((Horario)Session["horario"]);

            LMedico.validarHorarioSinGuardar(horarioMedico, horarioModificado);
        }
        catch (Exception ex) {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesHorario);
        }
    }

    protected void cargarDias()
    {
        try
        {
            Parametro parametro = LParametro.get();

            foreach (ListItem item in DDL_Dia.Items)
            {
                item.Enabled = parametro.DiasLaborales.Exists( d => d == int.Parse(item.Value) );
            }
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesRango); }
    }

    protected void cargarHoraInicial()
    {
        try
        {
            Parametro parametro = LParametro.get();

           // DDL_HoraInicio = LMedico.retornarHorario(parametro);

            DateTime dateTime = DateTime.Parse(parametro.HoraInicio);

            
            DDL_HoraInicio.DataSource = LMedico.cargarHorarioInicio(parametro, dateTime); ;
            DDL_HoraInicio.DataValueField = "INDICE";
            DDL_HoraInicio.DataTextField = "HORA";
            DDL_HoraInicio.DataBind();

            //while (dateTime < DateTime.Parse(parametro.HoraFin))
            //{
              //  DDL_HoraInicio.Items.Add(dateTime.ToShortTimeString());
               // dateTime = dateTime.AddHours(1);
            //}
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesRango); }
    }

    protected void cargarHoraFinal()
    {
        try
        {
            Parametro parametro = LParametro.get();

            DDL_HoraFin.Items.Clear();

            DateTime dateTime = DateTime.Parse(DDL_HoraInicio.SelectedItem.Text).AddHours(1);

            DDL_HoraFin.DataSource = LMedico.cargarHorarioFin(parametro, dateTime); ;
            DDL_HoraFin.DataValueField = "INDICE";
            DDL_HoraFin.DataTextField = "HORA";
            DDL_HoraFin.DataBind();

            //while (dateTime <= DateTime.Parse(parametro.HoraFin))
            //{

            //  DDL_HoraFin.Items.Add(dateTime.ToShortTimeString());
            // dateTime = dateTime.AddHours(1);
            //}
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesRango); }
    }

    protected void DDL_HoraInicio_SelectedIndexChanged(object sender, EventArgs e)
    {
        cargarHoraFinal();
    }

    protected void BTN_Agregar_Click(object sender, EventArgs e)
    {
        Rango rango = new Rango();
        rango.Dia = int.Parse(DDL_Dia.SelectedValue);
        rango.Inicio = DateTime.Parse(DDL_HoraInicio.SelectedItem.Text).ToString("HH:mm");
        rango.Fin = DateTime.Parse(DDL_HoraFin.SelectedItem.Text).ToString("HH:mm");

        try
        {
            Horario horario = new Horario();
            horario = (Horario)Session["horario"];
            horario.agregarRango(rango);
            Session["horario"] = horario;
            Mensaje.mostrarMensaje("Rango agregado", Mensaje.CORRECTO, P_MensajesRango);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesRango); }

        mostrarHorario();
    }

    protected void BTN_Guardar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];
            eMedico.Horario = (Horario)Session["horario"];

            Respuesta respuesta = LMedico.modificarHorario(eMedico, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, "Horario guardado");
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesHorario);
            mostrarHorario();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHorario); }
    }

    protected void mostrarHorario()
    {
        try
        {
            Horario horario = (Horario)Session["horario"];

            P_Horario.Controls.Clear();

            for (int numeroDia = 0; numeroDia < Rango.DIAS.Count; numeroDia++)
            {
                try
                {
                    LAdministrador.mostrarHorario(horario, numeroDia);
                    P_Horario.Controls.Add(crearPanelDia(numeroDia, horario.Rangos.FindAll(r => r.Dia == numeroDia)));
                }
                catch { }
            }
        }
        catch { }
        
    }

    protected Panel crearPanelDia(int dia, List<Rango> rangos)
    {
        Panel pDia = new Panel();

        pDia.CssClass = "dia flex flex_vertical";

        Label lbTitulo = new Label();
        lbTitulo.CssClass = "titulo_dia";
        lbTitulo.Text = Rango.DIAS[dia];
        pDia.Controls.Add(lbTitulo);

        rangos.Sort( (a, b) => DateTime.Compare(DateTime.Parse(a.Inicio), DateTime.Parse(b.Inicio)) );

        rangos.ForEach( r => pDia.Controls.Add(crearPanelRango(r)) );

        return pDia;
    }

    protected Panel crearPanelRango(Rango rango)
    {
        Panel pRango = new Panel();
        pRango.CssClass = "rango flex";

        Label lbInicio = new Label();
        lbInicio.CssClass = "hora hora_inicio";
        lbInicio.Text = DateTime.Parse(rango.Inicio).ToString("hh:mm tt");

        Label lbFin = new Label();
        lbFin.CssClass = "hora hora_fin";
        lbFin.Text = DateTime.Parse(rango.Fin).ToShortTimeString();

        Panel pAuxiliar = new Panel();
        pAuxiliar.CssClass = "flex flex_vertical";
        pAuxiliar.Controls.Add(lbInicio);
        pAuxiliar.Controls.Add(lbFin);
        pRango.Controls.Add(pAuxiliar);

        Button btnEliminar = new Button();
        btnEliminar.CssClass = "BTN_Rojo";
        btnEliminar.Text = "X";
        btnEliminar.CommandName = JsonConvert.SerializeObject(rango);
        btnEliminar.Click += new EventHandler(BTN_EliminarRango_Click);
        pRango.Controls.Add(btnEliminar);

        return pRango;
    }

    protected void BTN_EliminarRango_Click(object sender, EventArgs e)
    {
        Mensaje.mostrarMensaje("Rango eliminado", Mensaje.CORRECTO, P_MensajesHorario);
        Rango rango = JsonConvert.DeserializeObject<Rango>(((Button)sender).CommandName);

        Horario horario = (Horario)Session["horario"];
        horario.eliminarRango(rango);
        Session["horario"] = horario;

        mostrarHorario();
    }

    protected void BTN_GenerarCitas_Click(object sender, EventArgs e)
    {
        EMedico eMedico = (EMedico)Session["usuario"];
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            Respuesta respuesta = LCita.generarcCitasMedico(eMedico, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesHorario);
            mostrarHorario();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHorario); }
    }

    protected void BTN_ReporteHorario_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Medico/ReporteHorario.aspx");
    }
}