﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Logica.Clases.Usuario;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Medico;

public partial class View_Medico_Agenda : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        actualizarTablaCitas();
        actualizarFechaSeleccionada();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void C_Cita_SelectionChanged(object sender, EventArgs e)
    {
        //actualizarTablaCitas();
    }

    protected void actualizarFechaSeleccionada()
    {
        Mensaje.mostrarMensaje(C_Cita.SelectedDate.ToString("dddd dd-MMMM-yyyy"), Mensaje.INFORMATIVO, P_FechaSeleccionada);
    }

    protected void actualizarTablaCitas()
    {
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];

            Respuesta respuesta = LCita.getAgendaDeMedicoPorFecha(eMedico, C_Cita.SelectedDate.ToShortDateString());
            
            List<ECita> eCitas = (List<ECita>)respuesta.Data;

            eCitas.Sort((a,b) => DateTime.Parse(a.HoraInicio).CompareTo(DateTime.Parse(b.HoraInicio)));

            GV_Citas.DataSource = eCitas;
            GV_Citas.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_VerInformacion_Click(object sender, EventArgs e)
    {
        Button button = (Button)sender;

        Mensaje.mostrarMensaje(button.CommandName, Mensaje.CORRECTO, P_Mensajes);

        Session["idCita"] = button.CommandName;
        Response.Redirect("~/View/Medico/VerCita.aspx");
    }

    protected void GV_Citas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Citas.PageIndex = e.NewPageIndex;
    }

    protected void BTN_ReporteAgenda_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Medico/ReporteAgenda.aspx");
    }
}