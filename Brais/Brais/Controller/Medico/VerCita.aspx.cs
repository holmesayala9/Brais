﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Usuario;
using Utilitaria.Clases;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;
using Utilitaria.Clases.Usuario;
using Utilitaria.Clases.Usuario;
using Utilitaria.Clases.Medico;

public partial class View_Medico_VerCita : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());
        }
        catch (Exception)
        {
            Response.Redirect("~/View/Medico/Agenda.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarInformacion();
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarInformacion()
    {
        mostrarInformacionDeCita();

        mostrarInformacionDeUsuario();

        mostrarHistorialesDeUsuario();

        mostrarHistorialDeCitaActual();
    }

    protected void mostrarInformacionDeCita()
    {
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            LB_IdCita.Text = eCita.Id.ToString();
            LB_Fecha.Text = DateTime.Parse(eCita.Fecha).ToString("dddd dd-MMMM-yyyy");
            LB_HoraInicio.Text = DateTime.Parse(eCita.HoraInicio).ToShortTimeString();
            LB_HoraFin.Text = DateTime.Parse(eCita.HoraFin).ToShortTimeString();
            LB_Servicio.Text = eCita.Servicio;
            LB_Estado.Text = eCita.Estado;

            P_CambiarEstado.Visible = DateTime.Today == DateTime.Parse(eCita.Fecha) && eCita.IdUsuario > 0;
        }
        catch (Exception ex)
        { Mensaje.mostrarExcepcion(ex, P_MensajesCita); }
    }

    protected void mostrarInformacionDeUsuario()
    {
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            EUsuario eUsuario = LUsuario.obtenerUsuario(eCita.IdUsuario);

            LB_Identificacion.Text = eUsuario.Identificacion;
            LB_Nombre.Text = eUsuario.Nombre;
            LB_Apellido.Text = eUsuario.Apellido;

            LB_Edad.Text = String.Format("{0} años {1} meses {2} dias", eUsuario.getEdad()["anios"], eUsuario.getEdad()["meses"], eUsuario.getEdad()["dias"]);
            LB_Genero.Text = eUsuario.Genero;
            LB_Telefono.Text = eUsuario.Telefono;
            LB_Correo.Text = eUsuario.Correo;

            P_InformacionPaciente.Visible = DateTime.Today == DateTime.Parse(eCita.Fecha) && eUsuario.Id != -1;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesCita); }
    }

    protected void mostrarHistorialDeCitaActual()
    {
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            P_HistorialCitaActual.Visible = DateTime.Today == DateTime.Parse(eCita.Fecha) && eCita.IdUsuario > 0;

            BTN_AgregarHistorial.Visible = true;
            BTN_ModificarHistorial.Visible = false;

            EHistorial eHistorial = (EHistorial)LHistorial.getByIdCita(idCita).Data;

            JObject jObject = JObject.Parse(eHistorial.Datos);

            TB_MotivoConsulta.Text = jObject["MotivoConsulta"].ToString();
            TB_Observaciones.Text = jObject["Observaciones"].ToString();

            BTN_AgregarHistorial.Visible = false;
            BTN_ModificarHistorial.Visible = true;
        }
        catch (Exception ex) {
            //Mensaje.mostrarExcepcion(ex, P_MensajesHistorialCitaActual);
        }
    }

    protected void mostrarHistorialesDeUsuario()
    {
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            GV_HistorialesUsuario.DataSource = (List<EHistorial>)LHistorial.getByIdUsuarioAndIdEspecialidad(eCita.IdUsuario, eCita.EMedico.EEspecialidad.Id).Data;
            GV_HistorialesUsuario.DataBind();
            P_HistorialesDeUsuario.Visible = DateTime.Parse(eCita.Fecha) == DateTime.Today && eCita.IdUsuario > 0;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_HistorialesDeUsuario); }
    }

    protected void BTN_CambiarEstado_Click( object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;
            eCita.Estado = DDL_CambioEstado.Text;

            EMedico eMedico = (EMedico)Session["usuario"];

            Respuesta respuesta = LCita.update(eCita, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesCita);

            mostrarInformacionDeCita();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesCita); }
    }

    protected void BTN_AgregarHistorial_Click(object sender, EventArgs e)
    {
        agregarHistorial();
    }

    protected void BTN_ModificarHistorial_Click(object sender, EventArgs e)
    {
        modificarHistorial();
    }

    protected void agregarHistorial()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            int idCita = int.Parse(Session["idCita"].ToString());
            ECita eCita = (ECita)LCita.get(idCita).Data;

            JObject jObject = new JObject();
            jObject["MotivoConsulta"] = TB_MotivoConsulta.Text.Trim();
            jObject["Observaciones"] = TB_Observaciones.Text.Trim();

            EHistorial eHistorial = new EHistorial();
            eHistorial.IdUsuario = eCita.EUsuario.Id;
            eHistorial.EUsuario = eCita.EUsuario;
            eHistorial.IdCita = eCita.Id;
            eHistorial.ECita = eCita;
            eHistorial.Datos = jObject.ToString();

            EMedico eMedico = (EMedico)Session["usuario"];

            Respuesta respuesta = LHistorial.add(eHistorial, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesHistorialCitaActual);

            mostrarHistorialDeCitaActual();
            mostrarHistorialesDeUsuario();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHistorialCitaActual); }
    }

    protected void modificarHistorial()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EMedico eMedico = (EMedico)Session["usuario"];

            int idCita = int.Parse(Session["idCita"].ToString());
            ECita eCita = (ECita)LCita.get(idCita).Data;

            JObject jObject = new JObject();
            jObject["MotivoConsulta"] = TB_MotivoConsulta.Text.Trim();
            jObject["Observaciones"] = TB_Observaciones.Text.Trim();

            EHistorial eHistorial = (EHistorial)LHistorial.getByIdCita(idCita).Data;
            eHistorial.Datos = jObject.ToString();

            Respuesta respuesta = LHistorial.update(eHistorial, eMedico.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesHistorialCitaActual);

            mostrarHistorialesDeUsuario();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHistorialCitaActual); }
    }
}