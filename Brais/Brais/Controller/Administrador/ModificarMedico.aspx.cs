﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Logica.Clases.Medico;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;

public partial class View_Administrador_ModificarMedico : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Int64 idMedico = Int64.Parse(Session["idMedico"].ToString());
        }
        catch (Exception)
        {
            Response.Redirect("~/View/Administrador/VerMedicos.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarDatos();
        cargarEspecialidades();
        cargarConsultorios();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarDatos()
    {
        try
        {
            Int64 idMedico = Int64.Parse(Session["idMedico"].ToString());
            EMedico eMedico = (EMedico)LMedico.get(idMedico).Data;

            LB_Identificacion.Text = eMedico.Identificacion;
            LB_Nombre.Text = eMedico.Nombre;
            LB_Apellido.Text = eMedico.Apellido;
            LB_Correo.Text = eMedico.Correo;
            LB_Telefono.Text = eMedico.Telefono;
            LB_Especialidad.Text = eMedico.EEspecialidad.Nombre;
            LB_Consultorio.Text = eMedico.EConsultorio.Nombre;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void cargarEspecialidades()
    {
        try
        {
            DDL_Especialidad.DataSource = LEspecialidad.getAll();
            DDL_Especialidad.DataBind();
            DDL_Especialidad.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void cargarConsultorios()
    {
        try
        {
            DDL_Consultorio.DataSource = (List<EConsultorio>)LConsultorio.getDisponibles().Data;
            DDL_Consultorio.DataBind();
            DDL_Consultorio.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Modificar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Int64 idMedico = Int64.Parse(Session["idMedico"].ToString());
            EMedico eMedico = (EMedico)LMedico.get(idMedico).Data;

            eMedico.Identificacion = TB_Identificacion.Text;
            eMedico.Nombre = TB_Nombre.Text;
            eMedico.Apellido = TB_Apellido.Text;
            eMedico.Correo = TB_Correo.Text;
            eMedico.Telefono = TB_Telefono.Text;
            eMedico.Contrasena = TB_Contrasena.Text;
            eMedico.IdEspecialidad = int.Parse(DDL_Especialidad.SelectedValue);
            eMedico.IdConsultorio = int.Parse(DDL_Consultorio.SelectedValue);

            Respuesta respuesta = LMedico.update(eMedico, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            GestorRedireccion.manage(this, respuesta);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }

    protected void BTN_LiberarConsultorio_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Int64 idMedico = Int64.Parse(Session["idMedico"].ToString());
            EMedico eMedico = (EMedico)LMedico.get(idMedico).Data;

            Respuesta respuesta = LConsultorio.liberarConsultorioDe(eMedico, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }
}