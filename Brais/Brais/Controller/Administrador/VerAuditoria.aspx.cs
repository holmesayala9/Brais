﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;

public partial class Controller_Administrador_VerAuditoria : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string nombreTabla = Session["nombre_tabla"].ToString();
            LB_NombreTabla.Text = Session["nombre_tabla"].ToString();
            LB_Titulo.Text = "Auditoria: " + LB_NombreTabla.Text;
        }
        catch (Exception)
        {
            Response.Redirect("~/View/Administrador/VerUsuarios.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        actualizarGV_Auditoria();
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void actualizarGV_Auditoria()
    {
        try
        {
            string nombreTabla = Session["nombre_tabla"].ToString();

            List<EAuditoria> list = LAuditoria.getAuditoriaTabla(nombreTabla); ;

            GV_Auditoria.DataSource = list;
            GV_Auditoria.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_VerAcceso_Click(object sender, EventArgs e)
    {

    }

    protected void GV_Auditoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Auditoria.PageIndex = e.NewPageIndex;
        actualizarGV_Auditoria();
    }
}