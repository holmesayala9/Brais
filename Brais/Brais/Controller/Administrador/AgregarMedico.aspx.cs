﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Logica.Clases.Medico;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;

public partial class View_Administrador_AgregarMedico : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        cargarEspecialidades();
        cargarConsultorios();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void cargarEspecialidades()
    {
        try
        {
            DDL_Especialidad.DataSource = LEspecialidad.getAll();
            DDL_Especialidad.DataBind();
            DDL_Especialidad.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void cargarConsultorios()
    {
        try
        {
            DDL_Consultorio.DataSource = (List<EConsultorio>)LConsultorio.getDisponibles().Data;
            DDL_Consultorio.DataBind();
            DDL_Consultorio.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_AgregarMedico_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EMedico eMedico = encapsularDatos();

            Respuesta respuesta = LMedico.add(eMedico, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            Mensaje.mostrarMensaje(eMensaje.TxtTraducido, respuesta.Estado, P_Mensajes);

            GestorRedireccion.manage(this, respuesta);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }

    protected EMedico encapsularDatos()
    {
        EMedico eMedico = new EMedico();

        eMedico.Identificacion = TB_Identificacion.Text.Trim();
        eMedico.Nombre = TB_Nombre.Text.Trim();
        eMedico.Apellido = TB_Apellido.Text.Trim();
        eMedico.Correo = TB_Correo.Text.Trim();
        eMedico.Telefono = TB_Telefono.Text.Trim();
        eMedico.Contrasena = TB_Contrasena.Text.Trim();
        eMedico.IdEspecialidad = int.Parse(DDL_Especialidad.SelectedValue);
        eMedico.IdConsultorio = int.Parse(DDL_Consultorio.SelectedValue);
        
        return eMedico;
    }

}