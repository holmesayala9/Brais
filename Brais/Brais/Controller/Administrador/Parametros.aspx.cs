﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Administrador;
using Logica.Clases.Medico;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Medico;

public partial class Controller_Administrador_Parametros : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MaintainScrollPositionOnPostBack = true;
        traducir();
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarRestriccionDeAcceso();
        mostrarDiasSemanaLaborales();
        mostrarHorarioDeTrabajo();
        mostrarDuracionCitas();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }


    protected void BTN_GenerarCitas_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];
            Respuesta respuesta = LAdministrador.generarCitas(eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesGenerarCitas);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesGenerarCitas); }
    }

    protected void mostrarRestriccionDeAcceso()
    {
        try
        {
            Parametro parametro = LParametro.get();

            LB_SesionesSimultaneas_Data.Text = parametro.Sesiones.ToString();
            LB_IntentosAcceso_Data.Text = parametro.IntentosIngreso.ToString();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesRestriccionesAcceso); }
    }

    protected void BTN_EditarSesionesSimultaneas_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            int sesiones = int.Parse(TB_SesionesSimultaneas.Text.Trim());

            Respuesta respuesta = LParametro.updateSesionesSimultaneas(sesiones, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesRestriccionesAcceso);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesRestriccionesAcceso);
        }
    }

    protected void BTN_EditarIntentosAcceso_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            int intentos = int.Parse(TB_IntentosAcceso.Text.Trim());

            Respuesta respuesta = LParametro.updateIntentosIngreso(intentos, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesRestriccionesAcceso);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesRestriccionesAcceso);
        }
    }

    protected void mostrarDiasSemanaLaborales()
    {
        try
        {
            Parametro parametro = LParametro.get();

            // FALTANTE
            parametro.DiasLaborales.ForEach(dia => CBL_DiasSemana.Items.FindByValue(dia.ToString()).Selected = true);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesDiasSemana); }
    }

    protected void BTN_ModificarDiasSemana_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString()); 
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Dictionary<int, Boolean> diasSemana = CBL_DiasSemana.Items.Cast<ListItem>().ToDictionary(i => int.Parse(i.Value), i => i.Selected);

            Respuesta respuesta = LParametro.updateDiasSemana(diasSemana, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesDiasSemana);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesDiasSemana); }
    }

    protected void mostrarHorarioDeTrabajo()
    {
        try
        {
            Parametro parametro = LParametro.get();

            LB_HoraInicio_Data.Text = parametro.HoraInicio;
            LB_HoraFin_Data.Text = parametro.HoraFin;

            llenarHoraInicio();
            llenarHoraFin();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHorarioTrabajo); }
    }

    protected void llenarHoraInicio()
    {
        try
        {
            DDL_HoraInicio.DataSource = LAdministrador.getRangoDeHorarioTrabajoForView();
            DDL_HoraInicio.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHorarioTrabajo); }
    }

    protected void llenarHoraFin()
    {
        try
        {
            DDL_HoraFin.DataSource = LAdministrador.getRangoDeHorarioTrabajoForView();
            DDL_HoraFin.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesHorarioTrabajo); }
    }

    protected void BTN_ModificarHorario_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LParametro.updateHorarioDeTrabajo(DDL_HoraInicio.SelectedValue, DDL_HoraFin.SelectedValue, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesHorarioTrabajo);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesHorarioTrabajo);
        }
    }

    protected void mostrarDuracionCitas()
    {
        try
        {
            Parametro parametro = LParametro.get();

            L_DuracionCitasData.Text = parametro.DuracionCitas + " min";
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesDuracionCitas); }
    }

    protected void BTN_ModificarDuracionCitas_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LParametro.updateDuracionCitas(DDL_DuracionCitas.SelectedValue, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesDuracionCitas);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesDuracionCitas);
        }
    }

    protected void BTN_AgregarConsultorio_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EConsultorio eConsultorio = EConsultorio.newEmpty();
            eConsultorio.Nombre = TB_NombreConsultorio.Text.Trim();
            eConsultorio.Descripcion = TB_DescripcionConsultorio.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LConsultorio.add(eConsultorio, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesConsultorio);

            TB_NombreConsultorio.Text = "";
            TB_DescripcionConsultorio.Text = "";
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesConsultorio); }

        GV_Consultorios.DataBind();
    }

    protected void BTN_MostrarEditorConsultorio_Click(object sender, EventArgs e)
    {
        try
        {
            int idConsultorio = int.Parse(((Button)sender).CommandName);

            EConsultorio eConsultorio = (EConsultorio)LConsultorio.get(idConsultorio).Data;

            LB_IdConsultorio.Text = eConsultorio.Id.ToString();
            TB_NombreConsultorio_Editar.Text = eConsultorio.Nombre;
            TB_DescripcionConsultorio_Editar.Text = eConsultorio.Descripcion;
            CB_DisponibilidadConsultorio.Checked = eConsultorio.Disponibilidad;

            P_EditarConsultorio.Visible = true;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesConsultorio); }

        GV_Consultorios.DataBind();
    }

    protected void BTN_EditarConsultorio_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EConsultorio eConsultorio = EConsultorio.newEmpty();
            eConsultorio.Id = int.Parse(LB_IdConsultorio.Text);
            eConsultorio.Nombre = TB_NombreConsultorio_Editar.Text;
            eConsultorio.Descripcion = TB_DescripcionConsultorio_Editar.Text;
            eConsultorio.Disponibilidad = CB_DisponibilidadConsultorio.Checked;

            Respuesta respuesta = LConsultorio.update(eConsultorio, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesConsultorio);

            P_EditarConsultorio.Visible = false;

            GV_Consultorios.DataBind();
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesConsultorio);
        }
    }

    protected void BTN_EliminarConsultorio_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            int idConsultorio = int.Parse(((Button)sender).CommandName);

            Respuesta respuesta = LConsultorio.delete(idConsultorio, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesConsultorio);

            GV_Consultorios.DataBind();
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesConsultorio);
        }
    }

    protected void BTN_AgregarEspecialidad_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EEspecialidad eEspecialidad = EEspecialidad.newEmpty();
            eEspecialidad.Nombre = TB_NombreEspecialidad.Text.Trim();

            Respuesta respuesta = LEspecialidad.add(eEspecialidad, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesEspecialidad);

            TB_NombreEspecialidad.Text = "";

            GV_Especialidades.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesEspecialidad); }
    }

    protected void BTN_MostrarEditorEspecialidad_Click(object sender, EventArgs e)
    {
        try
        {
            int idEspecialidad = int.Parse(((Button)sender).CommandName);

            EEspecialidad eEspecialidad = (EEspecialidad)LEspecialidad.get(idEspecialidad).Data;

            LB_IdEspecialidad.Text = eEspecialidad.Id.ToString();
            TB_NombreEspecialidadEditar.Text = eEspecialidad.Nombre;

            P_EditarEspecialidad.Visible = true;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesEspecialidad); }
    }

    protected void BTN_EditarEspecialidad_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EEspecialidad eEspecialidad = EEspecialidad.newEmpty();
            eEspecialidad.Id = int.Parse(LB_IdEspecialidad.Text);
            eEspecialidad.Nombre = TB_NombreEspecialidadEditar.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LEspecialidad.update(eEspecialidad, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesEspecialidad);

            P_EditarEspecialidad.Visible = false;

            GV_Especialidades.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesEspecialidad); }
    }

    protected void BTN_EliminarEspecialidad_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            int idEspecialidad = int.Parse(((Button)sender).CommandName);

            Respuesta respuesta = LEspecialidad.delete(idEspecialidad, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_MensajesEspecialidad);

            GV_Especialidades.DataBind();
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesEspecialidad);
        }
    }

    protected void BTN_AuditoriaParametros_Click(object sender, EventArgs e)
    {
        Session["nombre_tabla"] = "parametro";
        Response.Redirect("~/View/Administrador/VerAuditoria.aspx");
    }

    protected void BTN_AuditoriaEspecialidad_Click(object sender, EventArgs e)
    {
        Session["nombre_tabla"] = "especialidad";
        Response.Redirect("~/View/Administrador/VerAuditoria.aspx");
    }

    protected void BTN_AuditoriaConsultorio_Click(object sender, EventArgs e)
    {
        Session["nombre_tabla"] = "consultorio";
        Response.Redirect("~/View/Administrador/VerAuditoria.aspx");
    }
}