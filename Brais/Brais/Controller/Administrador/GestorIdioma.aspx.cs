﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases;

public partial class View_Administrador_GestorIdioma : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        obtenerIdiomas();
        mostraFormularios();
        mostrarComponentes();

        cargarDDLIdiomaComponente();
        cargarDDL_Componente_IdiomaComponente();
        cargarDDL_Formulario_ComponenteFormulario();
        cargarDDL_Componente_ComponenteFormulario();
        mostrarIdiomaComponente();
        mostrarFormularioComponente();

        traducir();
    }
    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void obtenerIdiomas()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            GV_Idioma.DataSource = LIdioma.getIdioma();
            GV_Idioma.DataBind();
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, "Resultados");
            Mensaje.mostrarMensaje( eMensaje.TxtTraducido + ": " + GV_Idioma.Rows.Count, Mensaje.INFORMATIVO, P_MensajesIdioma);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdioma); }
    }

    protected void BTN_AgregarIdioma_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EIdioma eIdioma = EIdioma.newEmpty();

            eIdioma.Name = TB_NombreIdioma.Text.Trim();
            eIdioma.Terminacion = TB_DescripcionIdioma.Text.Trim();

            Respuesta respuesta = LIdioma.agregarIdioma(eIdioma, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            Mensaje.mostrarMensaje(eMensaje.TxtTraducido, respuesta.Estado, P_MensajesIdioma);

            TB_NombreIdioma.Text = "";
            TB_DescripcionIdioma.Text = "";

            GestorRedireccion.manage(this, respuesta);
        }
        catch(Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesIdioma);
        }
    }

    protected void BTN_EditarIdioma_Click(object sender, EventArgs e)
    {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EIdioma eIdioma = EIdioma.newEmpty();
            eIdioma.Id = int.Parse(LB_IdIdioma.Text);
            eIdioma.Name = TB_NombreIdioma_Editar.Text;
            eIdioma.Terminacion = TB_DescripcionIdioma_Editar.Text;

            Respuesta respuesta = LIdioma.update(eIdioma, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            Mensaje.mostrarMensaje(eMensaje.TxtTraducido, respuesta.Estado, P_MensajesIdioma);

            P_EditarIdioma.Visible = false;
            LB_IdIdioma.Text = "";
            TB_NombreIdioma_Editar.Text = "";
            TB_DescripcionIdioma_Editar.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesIdioma);
        }
    }

    protected void BTN_MostrarEditorIdioma_Click(object sender, EventArgs e)
    {
        try
        {
            int idIdioma = int.Parse(((Button)sender).CommandName);

            EIdioma eIdioma = LIdioma.getIdioma(idIdioma);

            LB_IdIdioma.Text = eIdioma.Id.ToString();
            TB_NombreIdioma_Editar.Text = eIdioma.Name;
            TB_DescripcionIdioma_Editar.Text = eIdioma.Terminacion;

            P_EditarIdioma.Visible = true;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdioma); }
    }

    protected void BTN_EliminarIdioma_Click(object sender, EventArgs e)
    {
        int idIdiomaSeleccionado = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            int idIdioma = int.Parse(((Button)sender).CommandName);

            Respuesta respuesta = LIdioma.remove(idIdioma, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdiomaSeleccionado, respuesta.Mensaje);

            Mensaje.mostrarMensaje(eMensaje.TxtTraducido, respuesta.Estado, P_MensajesIdioma);
            
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdiomaSeleccionado, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesIdioma);
        }
    }


    protected void mostraFormularios()
    {
        try
        {
            GV_Formulario.DataSource = (List<EFormulario>)LFormulario.buscar(TB_BuscarFormulario.Text.Trim()).Data;
            GV_Formulario.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesFormulario); }
    }

    protected void BTN_AgregarFormulario_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EFormulario eFormulario = EFormulario.newEmpty();

            eFormulario.Name = TB_NombreFormulario.Text.Trim();
            eFormulario.Url = TB_UrlFormulario.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LFormulario.add(eFormulario, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            Mensaje.mostrarMensaje(eMensaje.TxtTraducido, respuesta.Estado, P_MensajesFormulario);

            TB_NombreFormulario.Text = "";
            TB_UrlFormulario.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesFormulario);
        }
    }

    protected void BTN_MostrarEditorFormulario_Click(object sender, EventArgs e)
    {

        try
        {
            int idFormulario = int.Parse(((Button)sender).CommandName);

            EFormulario eFormulario = (EFormulario)LFormulario.get(idFormulario).Data;

            LB_IdFormulario.Text = eFormulario.Id.ToString();
            TB_NombreFormularioEditar.Text = eFormulario.Name;
            TB_UrlFormularioEditar.Text = eFormulario.Url;

            P_EditorFormulario.Visible = true;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesFormulario); }
    }

    protected void BTN_EditarFormulario_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EFormulario eFormulario = (EFormulario)LFormulario.get(int.Parse(LB_IdFormulario.Text)).Data;

            eFormulario.Name = TB_NombreFormularioEditar.Text.Trim();
            eFormulario.Url = TB_UrlFormularioEditar.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LFormulario.update(eFormulario, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesFormulario);

            P_EditorFormulario.Visible = false;
            LB_IdFormulario.Text = "";
            TB_NombreFormularioEditar.Text = "";
            TB_UrlFormularioEditar.Text = "";
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesFormulario); }
    }

    protected void BTN_EliminarFormulario_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            
        try
        {
            int idFormulario = int.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LFormulario.delete(idFormulario, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesFormulario);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesFormulario);
        }
    }


    protected void mostrarComponentes()
    {
        try
        {
            GV_Componente.DataSource = (List<EComponente>)LComponente.buscarComponente(TB_BuscarComponente.Text.Trim()).Data;
            GV_Componente.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesFormulario); }
    }

    protected void BTN_AgregarComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EComponente eComponente = EComponente.newEmpty();

            eComponente.Name = TB_NombreComponente.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.add(eComponente, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesComponente);

            TB_NombreComponente.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesComponente);
        }
    }

    protected void BTN_MostrarEditorComponente_Click(object sender, EventArgs e)
    {
        try
        {
            int idComponente = int.Parse(((Button)sender).CommandName);

            EComponente eComponente = (EComponente)LComponente.get(idComponente).Data;

            LB_IdComponente.Text = eComponente.Id.ToString();
            TB_NombreComponente_Editar.Text = eComponente.Name;

            P_EditorComponente.Visible = true;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesComponente); }
    }

    protected void BTN_EditarComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EComponente eComponente = (EComponente)LComponente.get(int.Parse(LB_IdComponente.Text)).Data;

            eComponente.Name = TB_NombreComponente_Editar.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.update(eComponente, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesComponente);

            P_EditorComponente.Visible = false;
            LB_IdComponente.Text = "";
            TB_NombreComponente_Editar.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesComponente);
        }
    }

    protected void BTN_EliminarComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            int idComponente = int.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.delete(idComponente, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesComponente);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesComponente); }
    }


    protected void cargarDDLIdiomaComponente()
    {
        string value = "";
        try
        {
            value = DDL_ComponenteIdioma.SelectedValue;
        }
        catch (Exception) {  }
        try
        {
            DDL_ComponenteIdioma.DataSource = LIdioma.getIdioma();
            DDL_ComponenteIdioma.DataBind();
            DDL_ComponenteIdioma.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
        try
        {
            DDL_ComponenteIdioma.Items.FindByValue(value).Selected = true;
        }
        catch (Exception) { }
    }

    protected void cargarDDL_Componente_IdiomaComponente()
    {
        //string value = "";
        //try
        //{
            //value = DDL_Componente_IdiomaComponente.SelectedValue;
        //}
        //catch (Exception) { }
        //try
        //{
            //DDL_Componente_IdiomaComponente.DataSource = LComponente.getComponente();
            //DDL_Componente_IdiomaComponente.DataBind();
        //}
        //catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
        //try
        //{
            //DDL_Componente_IdiomaComponente.Items.FindByValue(value).Selected = true;
        //}
        //catch (Exception) { }
    }

    protected void cargarDDL_Formulario_ComponenteFormulario()
    {
        string value = "";
        try
        {
            value = DDL_Formulario_ComponenteFormulario.SelectedValue;
        }
        catch (Exception) { }
        try
        {
            DDL_Formulario_ComponenteFormulario.DataSource = LFormulario.getFormulario();
            DDL_Formulario_ComponenteFormulario.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
        try
        {
            DDL_Formulario_ComponenteFormulario.Items.FindByValue(value).Selected = true;
        }
        catch (Exception) { }
    }

    protected void cargarDDL_Componente_ComponenteFormulario()
    {
        //string value = "";
        //try
        //{
            //value = DDL_Componente_ComponenteFormulario.SelectedValue;
        //}
        //catch (Exception) { }
        //try
        //{
            //DDL_Componente_ComponenteFormulario.DataSource = LComponente.getComponente();
            //DDL_Componente_ComponenteFormulario.DataBind();
        //}
        //catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
        //try
        //{
            //DDL_Componente_ComponenteFormulario.Items.FindByValue(value).Selected = true;
        //}
        //catch (Exception) { }
    }

    protected void mostrarIdiomaComponente()
    {
        try
        {
            GV_IdiomaComponente.DataSource = (List<EIdiomaComponente>)LComponente.buscarIdiomaComponente(TB_BuscarIdiomaComponente.Text.Trim()).Data;
            GV_IdiomaComponente.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
    }

    protected void BTN_AgregarIdiomaToComponente_Click(object sender, EventArgs e)
    {
        int idIdiomaSeleccionado = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            int idIdioma = int.Parse(DDL_ComponenteIdioma.SelectedValue);
            string nombreComponente = TB_NombreComponente_AgregarIdioma.Text.ToString();
            string txt = TB_Texto.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.addIdiomaToComponente(idIdioma, nombreComponente, txt, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdiomaSeleccionado, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesIdiomaComponente);

            //TB_NombreComponente_AgregarIdioma.Text = "";
            TB_Texto.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdiomaSeleccionado, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesIdiomaComponente);
        }
    }

    protected void BTN_MostrarEditorIdiomaComponente_Click(object sender, EventArgs e)
    {
        try
        {
            int idIdiomaComponente = int.Parse(((Button)sender).CommandName);

            EIdiomaComponente eIdiomaComponente = (EIdiomaComponente)LComponente.getIdiomaComponente(idIdiomaComponente).Data;

            LB_IdIdiomaComponente.Text = eIdiomaComponente.Id.ToString();
            TB_TextoEditar.Text = eIdiomaComponente.Txt;

            P_EditorIdiomaComponente.Visible = true;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
    }

    protected void BTN_EditarIdiomaComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            int idIdiomaComponente = int.Parse(LB_IdIdiomaComponente.Text);

            EIdiomaComponente eIdiomaComponente = (EIdiomaComponente)LComponente.getIdiomaComponente(idIdiomaComponente).Data;
            eIdiomaComponente.Txt = TB_TextoEditar.Text.Trim();

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.updateIdiomaComponente(eIdiomaComponente, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesIdiomaComponente);

            P_EditorIdiomaComponente.Visible = false;
            LB_IdIdiomaComponente.Text = "";
            TB_TextoEditar.Text = "";
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
    }

    protected void BTN_EliminarIdiomaComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            int idIdiomaComponente = int.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.deleteIdiomaComponente(idIdiomaComponente, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesIdiomaComponente);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesIdiomaComponente); }
    }


    protected void BTN_AgregarFormularioComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.addComponenteToFormulario(DDL_Formulario_ComponenteFormulario.SelectedItem.Text, TB_NombreComponenteFormulario.Text.ToString(), eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesFormularioComponente);

            //TB_NombreComponenteFormulario.Text = "";
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_MensajesFormularioComponente);
        }
    }

    protected void mostrarFormularioComponente()
    {
        try
        {
            GV_FormularioComponente.DataSource = (List<EFormularioComponente>)LComponente.buscarFormularioComponente(TB_BuscarFormularioComponente.Text.Trim()).Data;
            GV_FormularioComponente.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesFormularioComponente); }
    }

    protected void BTN_EliminarFormularioComponente_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            int idFormularioComponente = int.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LComponente.deleteFormularioComponente(idFormularioComponente, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_MensajesFormularioComponente);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_MensajesFormularioComponente); }
    }

    protected void GV_Idioma_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Idioma.PageIndex = e.NewPageIndex;
    }

    protected void GV_Formulario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Formulario.PageIndex = e.NewPageIndex;
    }

    protected void GV_Componente_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Componente.PageIndex = e.NewPageIndex;
    }

    protected void GV_IdiomaComponente_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_IdiomaComponente.PageIndex = e.NewPageIndex;
    }

    protected void GV_FormularioComponente_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_FormularioComponente.PageIndex = e.NewPageIndex;
    }

}