﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Utilitaria.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Administrador_ModificarUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Int64 idUsuario = Int64.Parse(Session["idUsuario"].ToString());
        }
        catch {
            Response.Redirect("~/View/Administrador/VerUsuarios.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        adecuarParaModificar();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_Accion_Click(object sender, EventArgs e)
    {
        modificarUsuario();
    }

    protected void adecuarParaModificar()
    {
        try
        {
            EUsuario eUsuario = LUsuario.obtenerUsuario(Int64.Parse(Session["idUsuario"].ToString()));

            LB_Identificacion_.Text = eUsuario.Identificacion;
            LB_Nombre_.Text = eUsuario.Nombre;
            LB_Apellido_.Text = eUsuario.Apellido;
            LB_FechaNacimiento_.Text = DateTime.Parse(eUsuario.FechaNacimiento).ToString("yyyy-MM-dd");
            LB_Genero_.Text = eUsuario.Genero;
            LB_Correo_.Text = eUsuario.Correo;
            LB_Telefono_.Text = eUsuario.Telefono;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void modificarUsuario()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EUsuario eUsuario = LUsuario.obtenerUsuario(Int64.Parse(Session["idUsuario"].ToString()));

            eUsuario.Identificacion = TB_Identificacion.Text.Trim();
            eUsuario.Nombre = TB_Nombre.Text.Trim();
            eUsuario.Apellido = TB_Apellido.Text.Trim();
            eUsuario.FechaNacimiento = TB_FechaNacimiento.Text;
            eUsuario.Genero = DDL_Genero.Text;
            eUsuario.Correo = TB_Correo.Text.Trim();
            eUsuario.Telefono = TB_Telefono.Text.Trim();
            eUsuario.Contrasena = TB_Contrasena.Text.Trim();

            Respuesta respuesta = LUsuario.modificarUsuario(eUsuario, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            respuesta.Mensaje = eMensaje.TxtTraducido;

            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            GestorRedireccion.manage(this, respuesta);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }
}