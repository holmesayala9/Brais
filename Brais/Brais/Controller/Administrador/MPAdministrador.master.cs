﻿using System;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Logica.Clases;
using Logica.Clases.Administrador;
using System.Web.UI;
using System.Web.Services;

public partial class View_Administrador_MPAdministrador : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetNoStore();
        Respuesta respuesta = Respuesta.newRespuesta();

        try
        {
            Session["usuario"] = LSeguridad.renovarUsuario((UsuarioBasico)Session["usuario"]);

            respuesta = LSeguridad.verificarUsuario((UsuarioBasico)Session["usuario"], AppRelativeVirtualPath);
        }
        catch (Exception ex)
        {
            Response.Redirect("~/View/Principal/Login.aspx");
        }

        GestorRedireccion.manage(this.Response, respuesta);

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_VerUsuarios_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Administrador/VerUsuarios.aspx");
    }

    protected void BTN_VerMedicos_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Administrador/VerMedicos.aspx");
    }

    protected void BTN_Parametros_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Administrador/Parametros.aspx");
    }

    protected void BTN_CerrarSesion_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Redirect("~/View/Principal/Login.aspx");
        }
        catch (Exception) { }
    }


    protected void BTN_Idioma_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Administrador/GestorIdioma.aspx");
    }


    protected void T_Actividad_Tick(object sender, EventArgs e)
    {
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            LSeguridad.reportarActividad(eAdministrador.EAcceso);
        }
        catch (Exception) {  }
    }
}
