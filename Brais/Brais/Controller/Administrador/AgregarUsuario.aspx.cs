﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Logica.Clases.Administrador;
using Logica.Clases.Medico;
using Utilitaria.Clases.Usuario;

public partial class View_Administrador_AgregarUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_Accion_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            EUsuario eUsuario = obtenerDatosEncapsulados();

            Respuesta respuesta = LUsuario.agregarUsuario(eUsuario, eAdministrador.EAcceso);

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);

            Mensaje.mostrarMensaje(eMensaje.TxtTraducido, respuesta.Estado, P_Mensajes);

            GestorRedireccion.manage(this, respuesta);

        }
        catch (Exception ex)
        {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }

    }

    protected EUsuario obtenerDatosEncapsulados()
    {
        EUsuario eUsuario = new EUsuario();

        eUsuario.Identificacion = TB_Identificacion.Text.Trim();
        eUsuario.Nombre = TB_Nombre.Text.Trim();
        eUsuario.Apellido = TB_Apellido.Text.Trim();
        eUsuario.FechaNacimiento = TB_FechaNacimiento.Text;
        eUsuario.Genero = DDL_Genero.Text;
        eUsuario.Correo = TB_Correo.Text.Trim();
        eUsuario.Telefono = TB_Telefono.Text.Trim();
        eUsuario.Contrasena = TB_Contrasena.Text.Trim();

        return eUsuario;
    }
}