﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Logica.Clases.Medico;
using Utilitaria.Clases;
using System.Web.Services;
using Newtonsoft;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Administrador_VerUsuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        buscarHabilitados();
        buscarDeshabilitados();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void buscarHabilitados()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        
        GV_Usuarios.DataSource = (List<EUsuario>)LUsuario.buscarUsuarioHabilitado(TB_Buscar.Text.Trim()).Data;
        GV_Usuarios.DataBind();
        EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, "Resultados");
        Mensaje.mostrarMensaje(eMensaje.TxtTraducido + ": " + GV_Usuarios.Rows.Count, Mensaje.INFORMATIVO, P_ResultadosActivos);
    }

    protected void BTN_Modificar_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        Session["idUsuario"] = btn.CommandName;

        Response.Redirect("~/View/Administrador/ModificarUsuario.aspx");
    }

    protected void BTN_Deshabilitar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Button btn = (Button)sender;
            Respuesta respuesta =  LUsuario.desactivarUsuario(Int64.Parse(btn.CommandName), eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex){ Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_AgregarUsuario_Click(object sender, EventArgs e)
    {
        Session["idUsuario"] = null;
        Response.Redirect("~/View/Administrador/AgregarUsuario.aspx");
    }

    protected void BTN_Reporte_Click(object sender, EventArgs e)
    {

    }

    protected void buscarDeshabilitados()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            GV_UsuariosDeshabilitados.DataSource = (List<EUsuario>)LUsuario.buscarUsuarioDeshabilitado(TB_BuscarDeshabilitados.Text.Trim()).Data;
            GV_UsuariosDeshabilitados.DataBind();

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, "Resultados");
            Mensaje.mostrarMensaje(eMensaje.TxtTraducido + ": " + GV_UsuariosDeshabilitados.Rows.Count, Mensaje.INFORMATIVO, P_ResultadosInactivos);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_ResultadosInactivos); }
    }

    protected void BTN_Activar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            Int64 idUsuario = Int64.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LUsuario.activarUsuario(idUsuario, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Eliminar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            Int64 idUsuario = Int64.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LUsuario.eliminarUsuario(idUsuario, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }
}