﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Medico;

public partial class View_Administrador_VerMedicos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        buscarMedico();
        buscarDeshabilitados();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void buscarMedico()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            Respuesta respuesta = LMedico.buscarMedicoHabilitado(TB_Buscar.Text.Trim());

            GV_Medicos.DataSource = (List<EMedico>)respuesta.Data;
            GV_Medicos.DataBind();

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, "Resultados");
            Mensaje.mostrarMensaje(eMensaje.TxtTraducido + ": " + GV_Medicos.Rows.Count, Mensaje.INFORMATIVO, P_ResultadosActivos);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_ResultadosActivos); }
    }

    protected void BTN_AgregarMedico_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Administrador/AgregarMedico.aspx");
    }

    protected void BTN_Reporte_Click(object sender, EventArgs e)
    {

    }

    protected void BTN_Deshabilitar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            Int64 idMedico = Int64.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];
            Respuesta respuesta = LMedico.desactivarMedico(idMedico, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Modificar_Click(object sender, EventArgs e)
    {
        Session["idMedico"] = ((Button)sender).CommandName;
        Response.Redirect("~/View/Administrador/ModificarMedico.aspx");
    }

    protected void GV_Medicos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Medicos.PageIndex = e.NewPageIndex;
    }

    protected void buscarDeshabilitados()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());

        try
        {
            string parametro = TB_BuscarDeshabilitados.Text.Trim();

            Respuesta respuesta = LMedico.buscarMedicoDeshabilitado(TB_BuscarDeshabilitados.Text.Trim());

            GV_MedicosDeshabilitados.DataSource = (List<EMedico>)respuesta.Data;
            GV_MedicosDeshabilitados.DataBind();

            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, "Resultados");
            Mensaje.mostrarMensaje(eMensaje.TxtTraducido + ": " + GV_MedicosDeshabilitados.Rows.Count, Mensaje.INFORMATIVO, P_ResultadosInactivos);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_ResultadosInactivos); }
    }

    protected void BTN_Activar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            Int64 idMedico = Int64.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LMedico.activarMedico(idMedico, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Eliminar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            Int64 idMedico = Int64.Parse(((Button)sender).CommandName);

            EAdministrador eAdministrador = (EAdministrador)Session["usuario"];

            Respuesta respuesta = LMedico.delete(idMedico, eAdministrador.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }
}