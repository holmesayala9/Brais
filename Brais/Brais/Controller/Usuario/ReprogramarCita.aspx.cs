﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Usuario;
using Utilitaria.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_ReprogramarCita : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int idCita = int.Parse(Session["id_cita"].ToString());
        }
        catch (Exception)
        {
            Response.Redirect("~/View/Usuario/CitasAgendadas.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarCitaActual();
        actualizarDiasDisponibles();
        actualizarHorarios();
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarCitaActual()
    {
        try
        {
            int idCita = int.Parse(Session["id_cita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            LB_Id.Text = eCita.Id.ToString();
            LB_Fecha.Text = "Fecha: " + DateTime.Parse(eCita.Fecha).ToString("dddd dd-MMMM-yyyy");
            LB_HoraInicio.Text = "Hora inicio: " + DateTime.Parse(eCita.HoraInicio).ToShortTimeString();
            LB_HoraFin.Text = "Hora fin: " + DateTime.Parse(eCita.HoraFin).ToShortTimeString();
            LB_Servicio.Text = "Servicio: " + eCita.Servicio;
            LB_Medico.Text = "Medico: " + eCita.EMedico.Nombre + " " + eCita.EMedico.Apellido;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void actualizarDiasDisponibles()
    {
        try
        {
            int idCita = int.Parse(Session["id_cita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            List<DateTime> diasDisponibles = (List<DateTime>)LCita.getFechasActualesDeCitasSegunEspecialidad(eCita.EMedico.EEspecialidad.Id).Data;

            Session["dias_disponibles"] = diasDisponibles;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void C_FechasDisponibles_DayRender(object sender, DayRenderEventArgs e)
    {
        try
        {
            e.Day.IsSelectable = false;

            List<DateTime> diasDisponibles = (List<DateTime>)Session["dias_disponibles"];

            // FALTA
            e.Day.IsSelectable = diasDisponibles.Exists(d => d == e.Day.Date);

            e.Cell.BorderWidth = e.Day.IsSelectable ? 2 : 0;
            e.Cell.BorderStyle = BorderStyle.Solid;
            e.Cell.BorderColor = e.Day.IsSelectable ? Color.Red : Color.White;
        }
        catch (Exception) { }
    }

    protected void C_FechasDisponibles_SelectionChanged(object sender, EventArgs e)
    {
        
    }

    protected void actualizarHorarios()
    {
        try
        {
            int idCita = int.Parse(Session["id_cita"].ToString());

            ECita eCita = (ECita)LCita.get(idCita).Data;

            EUsuario eUsuario = (EUsuario)Session["usuario"];

            List<ECita> eCitas = (List<ECita>)LCita.getCitasDisponiblesParaUsuario(eUsuario.Id, eCita.EMedico.EEspecialidad.Id, C_FechasDisponibles.SelectedDate.ToShortDateString()).Data;

            eCitas.Sort((a, b) => DateTime.Parse(a.HoraInicio).CompareTo(DateTime.Parse(b.HoraInicio)));

            GV_Horarios.DataSource = eCitas;
            GV_Horarios.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Reemplazar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            int idNewCita = int.Parse(((Button)sender).CommandName);

            int idOldCita = int.Parse(Session["id_cita"].ToString());

            EUsuario eUsuario = (EUsuario)Session["usuario"];

            Respuesta respuesta = LCita.reprogramarCita(idNewCita, idOldCita, eUsuario.Id, eUsuario.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            Session["id_cita"] = idNewCita;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

}