﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases.Usuario;
using Logica.Clases.Administrador;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_ConfirmarCita : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int idCita = int.Parse(Session["id_cita"].ToString());
            mostrarDatos();
        }
        catch (Exception)
        {
            Response.Redirect("~/View/Usuario/AsignarCita.aspx");
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        traducir();
    }

    protected void traducir()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
    }

    protected void mostrarDatos()
    {
        try
        {
            int idCita = int.Parse(Session["id_cita"].ToString());
            ECita eCita = (ECita)LCita.get(idCita).Data;

            LB_IdCita.Text = eCita.Id.ToString();
            LB_HoraInicio.Text = DateTime.Parse(eCita.HoraInicio).ToShortTimeString();
            LB_HoraFin.Text = DateTime.Parse(eCita.HoraFin).ToShortTimeString();
            LB_Fecha.Text = DateTime.Parse(eCita.Fecha).ToString("dddd dd MMMM yyyy");
            LB_Servicio.Text = eCita.Servicio;
            LB_Medico.Text = eCita.EMedico.Nombre + " " + eCita.EMedico.Apellido;
            LB_Estado.Text = eCita.Estado;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Reservar_Click(object sender, EventArgs e)
    {
        reservarCita();
    }

    protected void reservarCita()
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            int idCita = (int.Parse(LB_IdCita.Text));

            EUsuario eUsuario = (EUsuario)Session["usuario"];

            Respuesta respuesta = LCita.reservarCita(idCita, eUsuario.Id, eUsuario.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }
}