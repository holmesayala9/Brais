﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using Logica.Clases.Medico;
using Logica.Clases.Usuario;
using Utilitaria.Clases;
using System.Web.UI;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_AsignarCita : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        cargarEspecialidades();
        actualizarDiasDisponibles();
        actualizarHorarios();

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void cargarEspecialidades()
    {
        string idEspecialidad = "";
        try
        {
            idEspecialidad = DDL_Servicio.SelectedValue;
        }
        catch (Exception) { }

        try
        {
            DDL_Servicio.DataSource = LEspecialidad.getAll();
            DDL_Servicio.DataBind();
            DDL_Servicio.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }

        try
        {
            DDL_Servicio.Items.FindByValue(idEspecialidad).Selected = true;
        }
        catch (Exception) { }
    }

    protected void actualizarDiasDisponibles()
    {
        try
        {
            int idEspecialidad = int.Parse(DDL_Servicio.SelectedValue);

            List<DateTime> diasDisponibles = (List<DateTime>)LCita.getFechasActualesDeCitasSegunEspecialidad(idEspecialidad).Data;

            Session["dias_disponibles"] = diasDisponibles;
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void C_Fechas_DayRender(object sender, DayRenderEventArgs e)
    {
        try
        {
            e.Day.IsSelectable = false;
            
            List<DateTime> diasDisponibles = (List<DateTime>)Session["dias_disponibles"];

            // FALTA
            e.Day.IsSelectable = diasDisponibles.Exists( d => d == e.Day.Date );

            e.Cell.BorderWidth = e.Day.IsSelectable ? 2 : 0;
            e.Cell.BorderStyle = BorderStyle.Solid;
            e.Cell.BorderColor = e.Day.IsSelectable ? Color.Red : Color.White;
            //
        }
        catch (Exception) { }
    }

    protected void actualizarHorarios()
    {
        try
        {
            EUsuario eUsuario = (EUsuario)Session["usuario"];

            List<ECita> eCitas = (List<ECita>)LCita.getCitasDisponiblesParaUsuario(eUsuario.Id, int.Parse(DDL_Servicio.SelectedValue), C_Fechas.SelectedDate.ToShortDateString()).Data;

            eCitas.Sort((a, b) => DateTime.Parse(a.HoraInicio).CompareTo(DateTime.Parse(b.HoraInicio)));

            GV_Horarios.DataSource = eCitas;
            GV_Horarios.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Reservar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            int idCita = int.Parse(((Button)sender).CommandName);

            Respuesta respuesta = LCita.verificarCita(idCita);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);
            reservar(idCita);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }

    protected void reservar(int idCita)
    {
        Session["id_cita"] = idCita;
        Response.Redirect("~/View/Usuario/ConfirmarCita.aspx");
    }

    protected void GV_Horarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GV_Horarios.PageIndex = e.NewPageIndex;
    }
}