﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Utilitaria.Clases;
using Logica.Clases.Usuario;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_CitasAgendadas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarCitas();
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarCitas()
    {
        try
        {
            EUsuario eUsuario = (EUsuario)Session["usuario"];

            GV_Citas.DataSource = (List<ECita>)LCita.getCitasVigentesDeUsuario(eUsuario.Id).Data;
            GV_Citas.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }

    protected void BTN_Cancelar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EUsuario eUsuario = (EUsuario)Session["usuario"];

            int idCita = int.Parse(((Button)sender).CommandName);

            Respuesta respuesta = LCita.cancelarCita(idCita, eUsuario.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            mostrarCitas();
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }

    protected void BTN_Reprogramar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            int idCita = int.Parse(((Button)sender).CommandName);

            Respuesta respuesta = LCita.verificarReprogramacionDeCita(idCita);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            Session["id_cita"] = idCita;

            Response.Redirect("~/View/Usuario/ReprogramarCita.aspx");
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }
    }
}