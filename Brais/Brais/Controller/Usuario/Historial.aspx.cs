﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Usuario;
using Utilitaria.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_Historial : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        mostrarHistorial();
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarHistorial()
    {
        try
        {
            EUsuario eUsuario = (EUsuario)Session["usuario"];

            Respuesta respuesta = LHistorial.buscarByIdUsuarioAndParametro(eUsuario.Id, TB_Buscar.Text.Trim());

            GV_Historial.DataSource = (List<EHistorial>)respuesta.Data;
            GV_Historial.DataBind();
        }
        catch (Exception ex) { Mensaje.mostrarExcepcion(ex, P_Mensajes); }
    }
}