﻿
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Utilitaria.Clases.Medico;

public partial class View_Usuario_Reporte : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (true)
        {
            List<EMedico> eMedicos = DAOMedico.buscarMedico("");

            ReportDocument reportDocument = new ReportDocument();

            Mensaje.mostrarMensaje(MapPath("~/Reporte/Reporte.rpt"), Mensaje.INFORMATIVO, P_Mensajes);

            reportDocument.Load(MapPath("~/Reporte/Reporte.rpt"));

            //reportDocument.SetParameterValue("Titulo", "Titulo de este reporte.");

            reportDocument.SetDataSource(eMedicos);
            CRV_Reporte.ReportSource = reportDocument;

            //CRS_Reporte.ReportDocument.SetDataSource(eEspecialidades);

            //CRV_Reporte.ReportSource = CRS_Reporte;

        }
    }

}