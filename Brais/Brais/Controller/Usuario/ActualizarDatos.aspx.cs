﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_ActualizarDatos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            EUsuario eusuario = (EUsuario)Session["usuario"];
            EUsuario auxiliar = new EUsuario();

            LFuncion.validarSessionUsuario(eusuario);
            auxiliar = LFuncion.validar_postback_usuario(eusuario, IsPostBack);
            MaintainScrollPositionOnPostBack = true;
            try
            {
                mostrarDatos();
            }
            catch { }
        }
        catch { }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void mostrarDatos()
    {
        EUsuario eUsuario = (EUsuario)Session["usuario"];
        Session["idUsuario"] = eUsuario.Id;

        LB_Identificacion.Text = eUsuario.Identificacion;
        LB_Nombre.Text = eUsuario.Nombre;
        LB_Apellido.Text = eUsuario.Apellido;
        LB_Correo.Text = eUsuario.Correo;
        LB_Telefono.Text = eUsuario.Telefono;
        LB_FechaNacimiento.Text = DateTime.Parse(eUsuario.FechaNacimiento).ToString("dd MMMM yyyy");
        LB_Genero.Text = eUsuario.Genero;


        CB_ModificarContrasena.Checked = false;
        P_ModificarContrasena.Visible = false;
    }

    protected void BTN_Modificar_Click(object sender, EventArgs e)
    {
        int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
        try
        {
            EUsuario auxiliar = obtenerDatosEncapsulados();

            EUsuario eUsuario = LUsuario.obtenerUsuario(Int64.Parse(Session["idUsuario"].ToString()));

            eUsuario.Identificacion = auxiliar.Identificacion;
            eUsuario.Nombre = auxiliar.Nombre;
            eUsuario.Apellido = auxiliar.Apellido;
            eUsuario.FechaNacimiento = auxiliar.FechaNacimiento;
            eUsuario.Genero = auxiliar.Genero;
            eUsuario.Telefono = auxiliar.Telefono;
            eUsuario.Correo = auxiliar.Correo;
            eUsuario.Contrasena = auxiliar.Contrasena;
            eUsuario.EAcceso = LUsuario.obtenerAcceso(Int64.Parse(Session["idUsuario"].ToString()));

            Respuesta respuesta = LUsuario.modificarUsuario(eUsuario, eUsuario.EAcceso);
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, respuesta.Mensaje);
            respuesta.Mensaje = eMensaje.TxtTraducido;
            Mensaje.mostrarMensaje(respuesta, P_Mensajes);

            GestorRedireccion.manage(this, respuesta);
        }
        catch (Exception ex) {
            EMensaje eMensaje = LIdioma.obtenerMensaje(idIdioma, ex.Message.ToString());
            eMensaje = LIdioma.validarMensaje(eMensaje, ex);
            Mensaje.mostrarExcepcion(eMensaje.TxtTraducido, P_Mensajes);
        }

    }

    protected EUsuario obtenerDatosEncapsulados()
    {
        EUsuario eUsuario = new EUsuario();

        eUsuario.Identificacion = LB_Identificacion.Text.Trim();
        eUsuario.Nombre = LB_Nombre.Text.Trim();
        eUsuario.Apellido = LB_Apellido.Text.Trim();
        eUsuario.FechaNacimiento = DateTime.Parse(LB_FechaNacimiento.Text).ToString("yyyy-MM-dd"); ;
        eUsuario.Genero = LB_Genero.Text;
        eUsuario.Correo = TB_Correo.Text.Trim();
        eUsuario.Telefono = TB_Telefono.Text.Trim();
        eUsuario.Contrasena = TB_NuevaContrasena.Text.Trim();

        return eUsuario;
    }

    protected void CB_ModificarContrasena_CheckedChanged(object sender, EventArgs e)
    {
        P_ModificarContrasena.Visible = ((CheckBox)sender).Checked;
    }
}