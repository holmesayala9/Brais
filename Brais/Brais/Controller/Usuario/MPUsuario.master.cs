﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilitaria.Clases;
using Logica.Clases;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases.Usuario;

public partial class View_Usuario_MPUsuario : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetNoStore();
        Respuesta respuesta = Respuesta.newRespuesta();

        try
        {
            Session["usuario"] = LSeguridad.renovarUsuario((UsuarioBasico)Session["usuario"]);

            respuesta = LSeguridad.verificarUsuario((UsuarioBasico)Session["usuario"], AppRelativeVirtualPath);
        }
        catch (Exception ex)
        {
            Response.Redirect("~/View/Principal/Login.aspx");
        }

        GestorRedireccion.manage(this.Response, respuesta);

        traducir();
    }

    protected void traducir()
    {
        try
        {
            int idIdioma = LIdioma.validarIdioma(Session["idiomaSeleccionado"].ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", String.Format("establecerIdioma({0});", LIdioma.getTraduccion(idIdioma, this.AppRelativeVirtualPath)), true);
        }
        catch { }
    }

    protected void BTN_AsignarCita_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Usuario/AsignarCita.aspx");
    }

    protected void BTN_MisDatos_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Usuario/ActualizarDatos.aspx");
    }

    protected void BTN_CitasAgendadas_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Usuario/CitasAgendadas.aspx");
    }

    protected void BTN_Historial_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/View/Usuario/Historial.aspx");
    }

    protected void BTN_CerrarSesion_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Redirect("~/View/Principal/Login.aspx");
        }
        catch (Exception) { }
    }

    protected void T_Actividad_Tick(object sender, EventArgs e)
    {
        try
        {
            EUsuario eUsuario = (EUsuario)Session["usuario"];

            LSeguridad.reportarActividad(eUsuario.EAcceso);
        }
        catch (Exception) { }
    }
}
