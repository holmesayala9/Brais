﻿
var elementos = document.getElementsByClassName("json");

var jsons = [];

for (var i = 0; i < elementos.length; i++) {

    var json = JSON.parse(elementos[i].textContent);

    console.log(json);

    var div = jsonToComponent(json);

    elementos[i].innerHTML = "";
    elementos[i].appendChild(div);
}

function jsonToComponent(json) {
    var dp = document.createElement("div");

    if (json.Accion) {
        dp.classList.add(json.Accion);
    }

    if (json.new_parametro) {
        var da = jsonToComponent(JSON.parse(json.new_parametro));
        da.classList.add("UPDATE");

        var db = jsonToComponent(JSON.parse(json.old_parametro));
        db.classList.add("DELETE");

        dp.appendChild(da);
        dp.appendChild(db);

        return dp;
    }

    for (var key in json) {
        var d = document.createElement("div");
        d.classList.add("clave_valor");

        d.innerHTML = key + ": " + json[key];
        dp.appendChild(d);
    }

    return dp;
}