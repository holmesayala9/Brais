﻿using Logica.Clases.Medico;
using Logica.Clases.Usuario;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Utilitaria.Clases.Medico;

/// <summary>
/// Descripción breve de Servicio
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class Servicio : System.Web.Services.WebService
{

    public Servicio()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }
    
    [WebMethod]
    public string getEspecialidades()
    {
        List<EEspecialidad> especialidades = LEspecialidad.getAll();
        return JsonConvert.SerializeObject(especialidades);
    }

    [WebMethod]
    public string getCitas(int idEspecialidad, string fecha)
    {
        List<ECita> eCitas = DAOCita.obtenerCitasActualesSegunEspecialidad(idEspecialidad);

        eCitas = eCitas.FindAll(c => DateTime.Parse(c.Fecha) == DateTime.Parse(fecha));

        return JsonConvert.SerializeObject(eCitas);
    }

}
