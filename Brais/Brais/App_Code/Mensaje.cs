﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Utilitaria.Clases;

/// <summary>
/// Descripción breve de Mensaje
/// </summary>
public class Mensaje
{
    public static int ALERTA = 0;
    public static int CORRECTO = 1;
    public static int INFORMATIVO = 2;

    private Label lbMensaje;

    public Mensaje()
    {
        configuracionInicial();
    }

    public Mensaje(String mensaje)
    {
        configuracionInicial();
        establecerMensaje(mensaje);
    }

    public static void mostrarMensaje(String mensaje, int tipoMensaje, WebControl webControl)
    {
        Mensaje m = new Mensaje(mensaje);

        if (tipoMensaje == ALERTA)
        {
            m.mensajeAlerta();
        }
        if (tipoMensaje == CORRECTO)
        {
            m.mensajeCorrecto();
        }

        m.agregarMensajeA(webControl);
    }

    public static void mostrarMensaje(Respuesta respuesta, WebControl webControl)
    {
        if (respuesta.Mensaje != "")
        {
            if (respuesta.Estado == Respuesta.CORRECTO)
            {
                mostrarMensaje(respuesta.Mensaje, CORRECTO, webControl);
            }
            else if (respuesta.Estado == Respuesta.ALERTA)
            {
                mostrarMensaje(respuesta.Mensaje, ALERTA, webControl);
            }
            else
            {
                mostrarMensaje(respuesta.Mensaje, INFORMATIVO, webControl);
            }
        }
    }

    public static void mostrarExcepcion(Exception ex, WebControl webControl)
    {
        //mostrarMensaje(ex.Source+ " --> " + ex.TargetSite + " <br /> " + ex.Message, ALERTA, webControl);
        mostrarMensaje(ex.Message, ALERTA, webControl);
    }

    public static void mostrarExcepcion(String ex, WebControl webControl)
    {
        //mostrarMensaje(ex.Source+ " --> " + ex.TargetSite + " <br /> " + ex.Message, ALERTA, webControl);
        mostrarMensaje(ex, ALERTA, webControl);
    }

    public void establecerMensaje(string mensaje)
    {
        LbMensaje.Text = mensaje;
    }

    public void configuracionInicial()
    {
        LbMensaje = new Label();

        mensajeInformativo();
    }

    public void mensajeAlerta()
    {
        lbMensaje.CssClass = "alert alert-danger";
    }

    public void mensajeCorrecto()
    {
        lbMensaje.CssClass = "alert alert-success";
    }

    public void mensajeInformativo()
    {
        lbMensaje.CssClass = "alert alert-info";
    }

    public void agregarMensajeA(WebControl webControl)
    {
        webControl.Controls.Add(LbMensaje);
    }

    public void removerMensaje()
    {
        LbMensaje.Parent.Controls.Remove(LbMensaje);
    }

    public Label LbMensaje
    {
        get
        {
            return lbMensaje;
        }

        set
        {
            lbMensaje = value;
        }
    }
}