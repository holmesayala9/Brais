﻿
using System.Web;
using Utilitaria.Clases;

/// <summary>
/// Descripción breve de GestorRespuesta
/// </summary>
public abstract class GestorRedireccion
{

    public static void manage(System.Web.UI.Page page, Respuesta respuesta)
    {
        if (!respuesta.Url.Equals(Respuesta.NO_REDIRECCIONAR))
        {
            page.Response.Redirect(respuesta.Url, true);
        }
    }

    public static void manage(HttpResponse httpResponse, Respuesta respuesta)
    {
        if (!respuesta.Url.Equals(Respuesta.NO_REDIRECCIONAR))
        {
            httpResponse.Redirect(respuesta.Url);
        }
    }

}