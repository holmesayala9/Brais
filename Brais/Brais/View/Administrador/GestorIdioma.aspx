﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/GestorIdioma.aspx.cs" Inherits="View_Administrador_GestorIdioma" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .crud{
            margin-bottom: 20px;
        }

        .parametros{
            align-items: flex-start;
        }
        .buscador{
            margin-bottom: 20px;
        }
        .buscador .TB_Buscar{
            font-size: 1.2rem;
            height: 30px;
            width: 300px;
            border: 2px solid #d0d0d0;
            border-bottom-left-radius: 3px;
            border-top-left-radius: 3px;
        }
        .buscador .BTN_Buscar{
            margin-left: 0;
            border-radius: 0px;
            height: 30px;
            border-bottom-right-radius: 3px;
            border-top-right-radius: 3px;
        }
        .headerCssClass{
            background-color: var(--color_principal);
            color:white;
            border:1px solid black;
            padding:4px;
            margin-top:5px;
        }
        .headerSelectedCss{
            background-color:lightgray;
            color:white;
            border:1px solid black;
            margin-top:5px;
            padding:4px;
        }
    </style>

    <asp:label runat="server" class="titulo T-GestionarIdiomas"></asp:label>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>  
        <ajaxToolkit:Accordion ID="Accordion1" 
            HeaderCssClass="bg-primary p-2 mb-2 text-white"
            HeaderSelectedCssClass="bg-warning p-2 mb-2 text-white" 
            FadeTransitions="true"
            TransitionDuration="600"
            AutoSize="None"
            Height="100"
            SelectedIndex="0"
            runat="server">
            <Panes>
                <ajaxToolkit:AccordionPane ID="Idioma"  runat="server">
                    <Header>
                        <asp:Label CssClass="T-Idioma" runat="server" />
                    </Header>
                    <Content>
                        <asp:Label CssClass="subtitulo T-Idioma" runat="server" />

                        <div class="crud">
                            <div class="d-flex flex-column">
                                <div class="form-group">
                                    <asp:Label CssClass="LB_Nombre" runat="server" />
                                    <asp:TextBox CssClass="form-control" ID="TB_NombreIdioma" MaxLength="20" runat="server" />

                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreIdiomaCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarIdioma" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreIdioma" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />

                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreIdiomaCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreIdioma" 
                                        ValidationGroup="BTN_AgregarIdioma" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <div class="form-group">
                                    <asp:label runat="server" CssClass="LB_Terminacion"></asp:label>
                                    <asp:TextBox CssClass="form-control" ID="TB_DescripcionIdioma" MaxLength="20" runat="server" />

                                    <asp:RequiredFieldValidator
                                        ID="RFV_TerminacionIdiomaCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarIdioma" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_DescripcionIdioma" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    
                                    <asp:RegularExpressionValidator
                                        ID="RFV_TerminacionIdiomaCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_DescripcionIdioma" 
                                        ValidationGroup="BTN_AgregarIdioma" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <asp:Button CssClass="BTN_Comun BTN_Agregar" ID="BTN_AgregarIdioma" ValidationGroup="BTN_AgregarIdioma" runat="server" OnClick="BTN_AgregarIdioma_Click" />
                            </div>

                            <asp:Panel ID="P_EditarIdioma" class="flex flex_vertical" runat="server" Visible="False">
                                <asp:Label ID="LB_IdIdioma" Text="" Visible="false" runat="server" />
                                <div class="form-group">
                                    <asp:label runat="server" CssClass="LB_Nombre"></asp:label>
                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreEditarIdiomaCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_EditarIdioma" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreIdioma_Editar" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_NombreIdioma_Editar" MaxLength="20" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreEditarIdiomaCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreIdioma_Editar" 
                                        ValidationGroup="BTN_EditarIdioma" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>
                                <div class="form-group">
                                    <asp:label runat="server" CssClass="LB_Terminacion"></asp:label>
                                    <asp:RequiredFieldValidator
                                        ID="RFV_TerminacionEditarIdiomaCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_EditarIdioma" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_DescripcionIdioma_Editar" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_DescripcionIdioma_Editar" MaxLength="20" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_TerminacionEditarIdiomaCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_DescripcionIdioma_Editar" 
                                        ValidationGroup="BTN_EditarIdioma" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <asp:Button CssClass="BTN_Comun BTN_Editar" ID="BTN_EditarIdioma" ValidationGroup="BTN_EditarIdioma" runat="server" OnClick="BTN_EditarIdioma_Click" />
                            </asp:Panel>

                            <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesIdioma" runat="server"></asp:Panel>
            
                            <asp:GridView
                                CssClass="GV" 
                                ID="GV_Idioma" 
                                runat="server" 
                                AutoGenerateColumns="False" 
                                CellPadding="10" 
                                CellSpacing="5"
                                GridLines="None" 
                                EmptyDataText="No hay información que mostrar"
                                EmptyDataRowStyle-CssClass="GV_Vacio"
                                OnPageIndexChanging="GV_Idioma_PageIndexChanging" 
                                AllowPaging="True">

                                <AlternatingRowStyle BackColor="#E2E2E2" />
                                <Columns>
                                    <asp:BoundField DataField="name" HeaderText="Nombre" SortExpression="Nombre" HeaderStyle-CssClass="LB_Nombre" >
                                    <HeaderStyle CssClass="LB_Nombre" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderStyle-CssClass="LB_Terminacion" DataField="terminacion" HeaderText="Terminación" SortExpression="Terminación" >
                                    <HeaderStyle CssClass="LB_Terminacion" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Azul BTN_Editar" ID="BTN_MostrarEditorIdioma" CommandName='<%# Eval("Id") %>' runat="server" OnClick="BTN_MostrarEditorIdioma_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Rojo BTN_Eliminar" ID="BTN_EliminarIdioma" CommandName='<%# Eval("Id") %>' runat="server" OnClick="BTN_EliminarIdioma_Click"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="BTN_EliminarMedico" runat="server" ConfirmText="¿Esta seguro que desea eliminar este idioma?" TargetControlID="BTN_EliminarIdioma" />                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
                            </asp:GridView>
                            </div>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">

                    <Header>
                        <asp:Label CssClass="LB_Formulario" runat="server" />
                    </Header>
                    <Content>
                        <div class="crud">

                            <asp:label CssClass="subtitulo LB_Formulario" runat="server"></asp:label>

                            <div class="flex flex_vertical">
                                <div class="form-group">
                                    <asp:Label CssClass="LB_Nombre" runat="server" />
                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreFormularioCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarFormulario" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreFormulario" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_NombreFormulario" MaxLength="50" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreFormularioCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreFormulario" 
                                        ValidationGroup="BTN_AgregarFormulario" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>
                                <div class="form-group">
                                    <asp:Label CssClass="LB_Url" runat="server" text="URL:" />
                                    <asp:RequiredFieldValidator
                                        ID="RFV_UrlFormularioCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarFormulario" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_UrlFormulario" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_UrlFormulario" MaxLength="50" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_UrlFormularioCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_UrlFormulario" 
                                        ValidationGroup="BTN_AgregarFormulario" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <asp:Button
                                    CssClass="BTN_Comun BTN_Agregar" 
                                    ID="BTN_AgregarFormulario" 
                                    ValidationGroup="BTN_AgregarFormulario" 
                                    runat="server"
                                    OnClick="BTN_AgregarFormulario_Click"/>
                            </div>

                            <asp:Panel ID="P_EditorFormulario" class="flex flex_vertical" Visible="false" runat="server">
                                <asp:Label Visible="false" ID="LB_IdFormulario" Text="" runat="server" />

                                <div class="form-group">
                                    <asp:Label CssClass="LB_Nombre" runat="server" />
                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreEditarFormularioCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_EditarFormulario" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreFormularioEditar" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_NombreFormularioEditar" MaxLength="50" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreEditarFormularioCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreFormularioEditar" 
                                        ValidationGroup="BTN_EditarFormulario" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>
                                <div class="form-group">
                                    <asp:Label CssClass="LB_Url" runat="server" Text="URL: |"/>
                                    <asp:RequiredFieldValidator
                                        ID="RFV_UrlEditarFormularioCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_EditarFormulario" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_UrlFormularioEditar" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_UrlFormularioEditar" MaxLength="50" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_UrlEditarFormularioCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_UrlFormularioEditar" 
                                        ValidationGroup="BTN_EditarFormulario" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <asp:Button
                                    CssClass="BTN_Comun BTN_Editar" 
                                    ID="BTN_EditarFormulario" 
                                    ValidationGroup="BTN_EditarFormulario"
                                    OnClick="BTN_EditarFormulario_Click"
                                    runat="server"/>
                            </asp:Panel>

                            <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesFormulario" runat="server"></asp:Panel>
                
                            <div class="buscador flex">
                                <asp:TextBox CssClass="TB_Buscar" ID="TB_BuscarFormulario" placeholder="" MaxLength="50" runat="server"></asp:TextBox>
                                <asp:Button CssClass="BTN_Comun BTN_Buscar" ID="BTN_Buscar" ValidationGroup="BTN_Buscar" runat="server" />
                            </div>

                            <asp:GridView
                                CssClass="GV" 
                                ID="GV_Formulario" 
                                runat="server" 
                                AutoGenerateColumns="False"
                                CellPadding="10" 
                                CellSpacing="5"
                                GridLines="None" 
                                EmptyDataText="No hay información que mostrar"
                                EmptyDataRowStyle-CssClass="GV_Vacio"
                                OnPageIndexChanging="GV_Formulario_PageIndexChanging" 
                                AllowPaging="True">

                                <AlternatingRowStyle BackColor="#E2E2E2" />
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Nombre" HeaderStyle-CssClass="LB_Nombre" >
                                    <HeaderStyle CssClass="LB_Nombre" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderStyle-CssClass="LB_Terminacion" DataField="Url" HeaderText="URL" SortExpression="Terminación" >
                                    <HeaderStyle CssClass="LB_Terminacion" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Azul BTN_Editar" ID="BTN_MostrarEditorFormulario" CommandName='<%# Eval("Id") %>' runat="server" OnClick="BTN_MostrarEditorFormulario_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Rojo BTN_Eliminar" ID="BTN_EliminarFormulario" CommandName='<%# Eval("Id") %>' runat="server" OnClick="BTN_EliminarFormulario_Click"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="BTN_EliminarMedico" runat="server" ConfirmText="¿Esta seguro que desea eliminar este formulario?" TargetControlID="BTN_EliminarFormulario" />                                                                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
                            </asp:GridView>

                        </div>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                    <Header>
                        <asp:Label CssClass="LB_Componente" runat="server" />
                    </Header>
                    <Content>
                        <div class="crud">

                            <asp:label CssClass="subtitulo LB_Componente" runat="server"></asp:label>

                            <div class="flex flex_vertical">
                                <div class="form-group">
                                    <asp:Label CssClass="LB_Nombre" runat="server" />
                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreComponeneteCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarComponente" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreComponente" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_NombreComponente" MaxLength="50" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreComponeneteCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreComponente" 
                                        ValidationGroup="BTN_AgregarComponente" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <asp:Button
                                    CssClass="BTN_Comun BTN_Agregar" 
                                    ID="BTN_AgregarComponente" 
                                    ValidationGroup="BTN_AgregarComponente"
                                    runat="server"
                                    OnClick="BTN_AgregarComponente_Click"/>
                            </div>

                            <asp:Panel ID="P_EditorComponente" class="flex flex_vertical" Visible="false" runat="server">
                                <asp:Label Visible="false" ID="LB_IdComponente" Text="" runat="server" />

                                <div class="form-group">
                                    <asp:Label CssClass="LB_Nombre" runat="server" />
                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreEditarComponenteCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_EditarComponente" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreComponente_Editar" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />
                                    <asp:TextBox CssClass="form-control" ID="TB_NombreComponente_Editar" MaxLength="50" runat="server" />
                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreEditarComponenteCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreComponente_Editar" 
                                        ValidationGroup="BTN_EditarComponente" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />
                                </div>

                                <asp:Button 
                                    CssClass="BTN_Comun BTN_Editar" 
                                    ID="BTN_EditarComponente" 
                                    ValidationGroup="BTN_EditarComponente"
                                    OnClick="BTN_EditarComponente_Click"
                                    runat="server"/>
                            </asp:Panel>

                            <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesComponente" runat="server"></asp:Panel>
            
                            <div class="buscador flex">
                                <asp:TextBox CssClass="TB_Buscar" ID="TB_BuscarComponente" placeholder="" MaxLength="50" runat="server"></asp:TextBox>
                                <asp:Button CssClass="BTN_Comun BTN_Buscar" ID="BTN_BuscarComponente" ValidationGroup="BTN_Buscar" runat="server" />
                            </div>

                            <asp:GridView
                                CssClass="GV" 
                                ID="GV_Componente" 
                                runat="server" 
                                AutoGenerateColumns="False" 
                                CellPadding="10" 
                                CellSpacing="5"
                                GridLines="None" 
                                EmptyDataText="No hay información que mostrar"
                                EmptyDataRowStyle-CssClass="GV_Vacio"
                                OnPageIndexChanging="GV_Componente_PageIndexChanging" 
                                AllowPaging="True">

                                <AlternatingRowStyle BackColor="#E2E2E2" />
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Nombre" HeaderStyle-CssClass="LB_Nombre" >
                                    <HeaderStyle CssClass="LB_Nombre" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Azul BTN_Editar" ID="BTN_MostrarEditorComponente" CommandName='<%# Eval("Id") %>' runat="server" OnClick="BTN_MostrarEditorComponente_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Rojo BTN_Eliminar" ID="BTN_EliminarComponente" CommandName='<%# Eval("Id") %>' Text="Eliminar" runat="server" OnClick="BTN_EliminarComponente_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
                            </asp:GridView>

                        </div>
                    </Content>
                            
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
                    <Header>
                        <asp:Label CssClass="LB_IdiomaComponente" runat="server" />
                    </Header>
                    <Content>
                        <div class="crud">

                            <ASP:LABEL class="subtitulo LB_IdiomaComponente" runat="server"></ASP:LABEL>

                            <div class="flex flex_vertical">
                                <div class="form-group">
                                    <asp:Label CssClass="LB_NombreComponente" runat="server" />

                                    <asp:RequiredFieldValidator
                                        ID="RFV_NombreComponenteCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarIdiomaToComponente_Click" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreComponente_AgregarIdioma" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />

                                    <asp:RegularExpressionValidator
                                        ID="RFV_NombreComponenteCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreComponente_AgregarIdioma" 
                                        ValidationGroup="BTN_AgregarIdiomaToComponente_Click" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />

                                    <asp:TextBox CssClass="form-control" ID="TB_NombreComponente_AgregarIdioma" MaxLength="50" runat="server" />
                                    <%--<asp:DropDownList 
                                        ID="DDL_Componente_IdiomaComponente" 
                                        DataTextField="name" 
                                        DataValueField="id" 
                                        Width="300px"
                                        runat="server">--%>
                                    </%>
                                </div>

                                <div class="form-group">
                                    <asp:Label CssClass="T-Idioma" runat="server" />

                                    <asp:DropDownList 
                                        ID="DDL_ComponenteIdioma" 
                                        DataTextField="Name" 
                                        DataValueField="Id" 
                                        runat="server"
                                        Width="300px">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    <asp:Label CssClass="LB_Texto" runat="server" />

                                    <asp:RequiredFieldValidator
                                        ID="RFV_TextoComponenteCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarIdiomaToComponente_Click" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_Texto" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />

                                    <asp:RegularExpressionValidator
                                        ID="RFV_TextoComponenteCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_Texto" 
                                        ValidationGroup="BTN_AgregarIdiomaToComponente_Click" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />

                                    <asp:TextBox CssClass="form-control" ID="TB_Texto" MaxLength="50" runat="server" Width="300px"/>
                                </div>

                                <asp:Button
                                    CssClass="BTN_Comun BTN_Agregar" 
                                    ID="BTN_AgregarComponenteIdioma" 
                                    ValidationGroup="BTN_AgregarIdiomaToComponente_Click"
                                    runat="server"
                                    OnClick="BTN_AgregarIdiomaToComponente_Click"/>
                            </div>


                            <asp:Panel ID="P_EditorIdiomaComponente" class="flex flex_vertical" Visible="false" runat="server">

            <%--                    <div class="form-group">
                                    <asp:Label Text="Idioma:" runat="server" />

                                    <asp:DropDownList ID="DropDownList1" DataTextField="NameTerminacion" DataValueField="Id" runat="server">
                                    </asp:DropDownList>
                                </div>--%>

                                <asp:Label ID="LB_IdIdiomaComponente" Text="" Visible="false" runat="server" />

                                <div class="form-group">
                                    <asp:Label CssClass="LB_Texto" runat="server" />

                                    <asp:RequiredFieldValidator
                                        ID="RFV_TextoEditarIdiomaComponenteCampoVacio"
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_EditarIdiomaComponente" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_TextoEditar" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />

                                    <asp:RegularExpressionValidator
                                        ID="RFV_TextoEditarIdiomaComponenteCaracteresNoValidos"
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_TextoEditar" 
                                        ValidationGroup="BTN_EditarIdiomaComponente" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />

                                    <asp:TextBox CssClass="form-control" ID="TB_TextoEditar" MaxLength="50" runat="server" />
                                </div>

                                <asp:Button 
                                    CssClass="BTN_Comun BTN_Editar" 
                                    ID="BTN_EditarIdiomaComponente" 
                                    ValidationGroup="BTN_EditarIdiomaComponente"
                                    runat="server"
                                    OnClick="BTN_EditarIdiomaComponente_Click"/>
                            </asp:Panel>


                            <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesIdiomaComponente" runat="server"></asp:Panel>

                            <div class="buscador flex">
                                <asp:TextBox CssClass="TB_Buscar" ID="TB_BuscarIdiomaComponente" placeholder="" MaxLength="50" runat="server"></asp:TextBox>
                                <asp:Button CssClass="BTN_Comun BTN_Buscar" ID="BTN_BuscarIdiomaComponente" ValidationGroup="BTN_Buscar" runat="server" />
                            </div>

                            <asp:GridView
                                CssClass="GV" 
                                ID="GV_IdiomaComponente" 
                                runat="server" 
                                AutoGenerateColumns="False" 
                                CellPadding="10" 
                                CellSpacing="5"
                                GridLines="None" 
                                EmptyDataText="No hay información que mostrar"
                                EmptyDataRowStyle-CssClass="GV_Vacio"
                                OnPageIndexChanging="GV_IdiomaComponente_PageIndexChanging" 
                                AllowPaging="True" 
                                PageIndex="0">

                                <AlternatingRowStyle BackColor="#E2E2E2" />
                                <Columns>
                                    <asp:BoundField HeaderStyle-CssClass="LB_Componente" DataField="EComponente.Name" HeaderText="Componente" SortExpression="Id" />
                                    <asp:BoundField HeaderStyle-CssClass="T-Idioma" DataField="EIdioma.NameTerminacion" HeaderText="Idioma" SortExpression="Nombre" />
                                    <asp:BoundField HeaderStyle-CssClass="LB_Texto" DataField="Txt" HeaderText="Texto" SortExpression="Nombre" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Azul BTN_Editar" ID="BTN_MostrarEditorIdiomaComponente" CommandName='<%# Eval("Id") %>' runat="server" OnClick="BTN_MostrarEditorIdiomaComponente_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Rojo BTN_Eliminar" ID="BTN_EliminarIdiomaComponente" CommandName='<%# Eval("Id") %>' Text="Eliminar" runat="server" OnClick="BTN_EliminarIdiomaComponente_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
                            </asp:GridView>

                        </div>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane5" runat="server">
                    <Header>
                        <asp:Label CssClass="LB_AgregarComponenteFormulario" runat="server" />
                    </Header>
                    <Content>
                        <div class="crud">

                            <h1 class="subtitulo LB_AgregarComponenteFormulario"></h1>
                
                            <div class="flex flex_vertical">
                                <div class="form-group">
                                    <asp:Label CssClass="LB_NombreFormulario" runat="server" />
                                    <%--<asp:RequiredFieldValidator
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarFormularioComponente" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreFormularioComponente" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />

                                    <asp:RegularExpressionValidator
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreFormularioComponente" 
                                        ValidationGroup="BTN_AgregarFormularioComponente" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />

                                    <asp:TextBox CssClass="form-control" ID="TB_NombreFormularioComponente" MaxLength="50" runat="server" />--%>
                                    
                                    <asp:DropDownList 
                                        ID="DDL_Formulario_ComponenteFormulario" 
                                        DataTextField="name" 
                                        DataValueField="id" 
                                        runat="server"
                                        Width="300px">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    <asp:Label CssClass="LB_NombreComponente" runat="server" />

                                    <asp:RequiredFieldValidator
                                        CssClass="RFV_CampoVacio"
                                        ValidationGroup="BTN_AgregarFormularioComponente" 
                                        ErrorMessage="El campo esta vacio" 
                                        ControlToValidate="TB_NombreComponenteFormulario" 
                                        runat="server" Display="Dynamic" 
                                        ForeColor="#FF5050" />

                                    <asp:RegularExpressionValidator
                                        CssClass="RFV_CaracteresNoValidos"
                                        ControlToValidate="TB_NombreComponenteFormulario" 
                                        ValidationGroup="BTN_AgregarFormularioComponente" 
                                        ValidationExpression="^[^<>]*$" 
                                        ErrorMessage="Caracteres no validos" 
                                        Display="Dynamic" 
                                        runat="server" 
                                        ForeColor="#FF5050" />

                                    <asp:TextBox CssClass="form-control" ID="TB_NombreComponenteFormulario" MaxLength="50" runat="server" />
                                    <%--<asp:DropDownList 
                                        ID="DDL_Componente_ComponenteFormulario" 
                                        DataTextField="name" 
                                        DataValueField="id" 
                                        Width="300px"
                                        runat="server"
                                        Enabled="true">
                                    </asp:DropDownList>--%>
                                </div>

                                <asp:Button
                                    CssClass="BTN_Comun BTN_Agregar" 
                                    ID="BTN_AgregarFormularioComponente" 
                                    ValidationGroup="BTN_AgregarFormularioComponente" 
                                    runat="server"
                                    OnClick="BTN_AgregarFormularioComponente_Click"/>

                            </div>

                            <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesFormularioComponente" runat="server"></asp:Panel>

                            <div class="buscador flex">
                                <asp:TextBox CssClass="TB_Buscar" ID="TB_BuscarFormularioComponente" placeholder="" MaxLength="50" runat="server"></asp:TextBox>
                                <asp:Button CssClass="BTN_Comun BTN_Buscar" ID="BTN_BuscarFormularioComponente" ValidationGroup="BTN_Buscar" runat="server" />
                            </div>

                            <asp:GridView
                                CssClass="GV" 
                                ID="GV_FormularioComponente" 
                                runat="server" 
                                AutoGenerateColumns="False" 
                                CellPadding="10" 
                                CellSpacing="5"
                                GridLines="None" 
                                EmptyDataText="No hay información que mostrar"
                                EmptyDataRowStyle-CssClass="GV_Vacio"
                                OnPageIndexChanging="GV_FormularioComponente_PageIndexChanging" 
                                AllowPaging="True" 
                                PageIndex="0">
                    
                                <AlternatingRowStyle BackColor="#E2E2E2" />
                                <Columns>
                                    <asp:BoundField HeaderStyle-CssClass="LB_Formulario" DataField="EFormulario.Name" HeaderText="Formulario" />
                                    <asp:BoundField HeaderStyle-CssClass="LB_Componente" DataField="EComponente.Name" HeaderText="Componente" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button CssClass="BTN_Rojo BTN_Eliminar" ID="BTN_EliminarFormularioComponente" CommandName='<%# Eval("Id") %>' Text="Eliminar" runat="server" OnClick="BTN_EliminarFormularioComponente_Click"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
                            </asp:GridView>

                        </div>
                    </Content>
                </ajaxToolkit:AccordionPane>
            </Panes>
        </ajaxToolkit:Accordion>
        </ContentTemplate>  
    </asp:UpdatePanel>
</asp:Content>

