﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/VerUsuarios.aspx.cs" Inherits="View_Administrador_VerUsuarios" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel ID="P_Mensajes" CssClass="flex flex_vertical P_Mensajes" runat="server"></asp:Panel>

    <div class="usuarios_registrados">
        <asp:label 
            ID="LB_UsuariosRegistrados" 
            CssClass="titulo LB_UsuariosRegistrados"
            runat="server">
        </asp:label>

        <div class="form-inline justify-content-center">
            <asp:TextBox 
                CssClass="form-control mr-sm-2"
                ID="TB_Buscar" 
                MaxLength="20" runat="server">
            </asp:TextBox>

            <asp:Button 
                CssClass="btn btn-primary mr-sm-5 BTN_Buscar" 
                ID="BTN_Buscar"
                ValidationGroup="BTN_Buscar" 
                runat="server" />

            <asp:Button 
                CssClass="btn btn-success BTN_AgregarNuevoUsuario" 
                ID="BTN_AgregarUsuario" 
                runat="server" 
                OnClick="BTN_AgregarUsuario_Click" />
        </div>

        <div class="flex"> 
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos badge badge-warning"
                ID="ER_BuscarUsuario" 
                ControlToValidate="TB_Buscar" 
                ValidationGroup="BTN_Buscar" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" /> 
        </div>

        <asp:Panel ID="P_ResultadosActivos" class="flex flex_vertical" runat="server"></asp:Panel>

        <div class="table-responsive d-flex justify-content-center">
            <asp:GridView 
                CssClass="table table-hover"
                ID="GV_Usuarios" 
                runat="server" 
                AutoGenerateColumns="False" 
                CellPadding="0" GridLines="None" 
                EmptyDataText="No hay información que mostrar"
                EmptyDataRowStyle-CssClass="GV_Vacio" >
                <Columns>
                    <asp:BoundField HeaderStyle-CssClass="LB_Identificacion" DataField="Identificacion" HeaderText="Identificacion" SortExpression="Id" >
                        <HeaderStyle CssClass="LB_Identificacion"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="LB_Nombre" DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" >
                        <HeaderStyle CssClass="LB_Nombre"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="LB_Apellido" DataField="Apellido" HeaderText="Apellido" SortExpression="Apellido" >
                        <HeaderStyle CssClass="LB_Apellido"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="LB_Correo" DataField="Correo" HeaderText="Correo" SortExpression="Correo" >
                        <HeaderStyle CssClass="LB_Correo"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="LB_Telefono" DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" >
                        <HeaderStyle CssClass="LB_Telefono"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="LB_Contrasena" DataField="Contrasena" HeaderText="Contraseña" SortExpression="Contrasena" >
                        <HeaderStyle CssClass="LB_Contrasena"></HeaderStyle>
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button 
                                CssClass="btn btn-info BTN_Modificar" 
                                ID="BTN_Modificar" 
                                CommandName='<%# Eval("Id") %>'
                                OnClick="BTN_Modificar_Click" 
                                runat="server">
                            </asp:Button>
                            <asp:Button 
                                CssClass="btn btn-danger BTN_Deshabilitar" 
                                ID="BTN_Deshabilitar" 
                                CommandName='<%# Eval("Id") %>'
                                OnClick="BTN_Deshabilitar_Click" 
                                runat="server">
                            </asp:Button>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
                <HeaderStyle CssClass="thead-light" />
            </asp:GridView>
        </div>
    </div>

    <div>
        <asp:label runat="server" class="titulo LB_Deshabilitados"></asp:label>

        <div class="form-inline justify-content-center">
            <asp:TextBox 
                CssClass="form-control mr-sm-2" 
                ID="TB_BuscarDeshabilitados" 
                MaxLength="20" 
                runat="server"></asp:TextBox>
            <asp:Button
                CssClass="btn btn-primary BTN_Buscar" 
                ID="BTN_BuscarDeshabilitados"
                ValidationGroup="BTN_BuscarDeshabilitados" 
                runat="server" />
        </div>

        <div class="flex"> 
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos badge badge-warning" 
                ControlToValidate="TB_BuscarDeshabilitados" 
                ValidationGroup="BTN_BuscarDeshabilitados" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" /> 
        </div>

        <asp:Panel ID="P_ResultadosInactivos" class="flex flex_vertical" runat="server"></asp:Panel>

        <div class="table-responsive d-flex justify-content-center">
            <asp:GridView
                CssClass="table table-hover" 
                ID="GV_UsuariosDeshabilitados" 
                runat="server" 
                AutoGenerateColumns="False" 
                CellPadding="0" 
                GridLines="None" 
                EmptyDataText="No hay información que mostrar"
                EmptyDataRowStyle-CssClass="GV_Vacio">
                <Columns>
                    <asp:BoundField HeaderStyle-CssClass="LB_Identificacion" DataField="Identificacion" HeaderText="Identificacion" SortExpression="Id" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Nombre" DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Apellido" DataField="Apellido" HeaderText="Apellido" SortExpression="Apellido" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Correo" DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Telefono" DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Contrasena" DataField="Contrasena" HeaderText="Contrasena" SortExpression="Contrasena" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button 
                                CssClass="btn btn-info BTN_Activar" 
                                ID="BTN_Activar" 
                                CommandName='<%# Eval("Id") %>' 
                                OnClick="BTN_Activar_Click" 
                                runat="server">
                            </asp:Button>
                            <asp:Button 
                                CssClass="btn btn-danger BTN_Eliminar" 
                                ID="BTN_Eliminar" 
                                CommandName='<%# Eval("Id") %>'
                                OnClick="BTN_Eliminar_Click" 
                                runat="server">
                            </asp:Button>
                            <ajaxToolkit:ConfirmButtonExtender 
                                ID="BTN_EliminarMedico" 
                                runat="server" 
                                ConfirmText="¿Esta seguro que desea eliminar este usuario?" 
                                TargetControlID="BTN_Eliminar" />

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
                <HeaderStyle CssClass="thead-light" />
            </asp:GridView>
        </div>

    </div>

</asp:Content>

