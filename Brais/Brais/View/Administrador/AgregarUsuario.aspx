﻿<%@ Page Language="C#" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/AgregarUsuario.aspx.cs" Inherits="View_Administrador_AgregarUsuario" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender 
        ID="AVC" 
        runat="server" 
        HorizontalSide="Right" 
        VerticalSide="Bottom" 
        TargetControlID="P_Mensajes" />

    <asp:Panel ID="P_Mensajes" runat="server" CssClass="flex flex_vertical P_Mensajes"></asp:Panel>

    <div>
        <asp:Label CssClass="titulo LB_AgregarUsuario" ID="LB_Titulo" runat="server"></asp:Label>

        <div class="d-flex flex-column m-auto max-form">

            <div class="form-group">
                <asp:label CssClass="LB_Identificacion" ID="LB_Identificacion" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Identificacion" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator 
                    ID="RFV_Identificacion" 
                    CssClass="RFV_CampoVacio"
                    runat="server" 
                    ControlToValidate="TB_Identificacion" 
                    ErrorMessage="El campo esta vacio" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" 
                    id="ER_Caracteres_Identificacion"/>

                <asp:RegularExpressionValidator 
                    CssClass="RFV_SoloNumeros"
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[\s]*[\d]+[\s]*$" 
                    ErrorMessage="Digite solo numeros" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" 
                    id="ER_Numeros_Identificacion"/>
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Nombre" id="LB_Nombre" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Nombre" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator 
                    ID="RFV_Nombre" 
                    CssClass="RFV_CampoVacio"
                    runat="server" 
                    ControlToValidate="TB_Nombre" 
                    ErrorMessage="El campo esta vacio" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Nombre" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" 
                    id="ER_Caracteres_Nombre"/>
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Apellido" ID="LB_Apellido" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Apellido" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator 
                    ID="RFV_Apellido" 
                    CssClass="RFV_CampoVacio"
                    runat="server" 
                    ControlToValidate="TB_Apellido" 
                    ErrorMessage="El campo esta vacio" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Apellido" 
                    ControlToValidate="TB_Apellido" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_FechaNacimiento" ID="LB_FechaNacimiento" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_FechaNacimiento" runat="server" TextMode="Date" />

                <asp:RequiredFieldValidator 
                    ID="RFV_FechaNacimiento" 
                    runat="server" 
                    ControlToValidate="TB_FechaNacimiento" 
                    ErrorMessage="No ha seleccionado" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Genero" id="LB_Genero" runat="server"></asp:label>
                <asp:DropDownList CssClass="form-control" ID="DDL_Genero" runat="server">
                    <asp:ListItem Text="Ninguno" Value="Ninguno" />
                    <asp:ListItem Text="Masculino" Value="Masculino" />
                    <asp:ListItem Text="Femenino" Value="Femenino"></asp:ListItem>
                    <asp:ListItem Text="Otro" Value="Otro"></asp:ListItem>
                </asp:DropDownList>

                <asp:RequiredFieldValidator 
                    ID="RFV_Genero" 
                    runat="server" 
                    ControlToValidate="DDL_Genero" 
                    ErrorMessage="No ha seleccionado" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion" 
                    InitialValue="Ninguno">
                </asp:RequiredFieldValidator>
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Correo" ID="LB_Correo" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Correo" MaxLength="50" runat="server" TextMode="Email" />

                <asp:RequiredFieldValidator 
                    ID="RFV_Correo" 
                    CssClass="RFV_CampoVacio"
                    runat="server" 
                    ControlToValidate="TB_Correo" 
                    ErrorMessage="El campo esta vacio" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Correo" 
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator 
                    id="ER_Correo_Correo" 
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^\s*[a-zA-Z][\w-_+.]{1,20}@[\w]{1,20}([.][a-z]{2,4}){1,2}\s*$" 
                    ErrorMessage="Correo no valido, ejemplo: ejemplo@dominio.com" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Telefono" id="LB_Telefono" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Telefono" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator 
                    ID="RFV_Telefono" 
                    CssClass="RFV_CampoVacio"
                    runat="server" 
                    ControlToValidate="TB_Telefono" 
                    ErrorMessage="El campo esta vacio" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Telefono" 
                    ControlToValidate="TB_Telefono" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Contrasena" ID="LB_Contrasena" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Contrasena" MaxLength="20" runat="server" TextMode="Password" />

                <asp:RequiredFieldValidator 
                    ID="RFV_Contrasena" 
                    CssClass="RFV_CampoVacio"
                    runat="server" 
                    ControlToValidate="TB_Contrasena" 
                    ErrorMessage="El campo esta vacio" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:RequiredFieldValidator>

                <asp:CompareValidator 
                    ID="CM_Contrasena" 
                    CssClass="RFV_ContrasenaNoCoincide"
                    runat="server" 
                    ErrorMessage="Las contraseñas no coinciden" 
                    ControlToCompare="TB_RepetirContrasena" 
                    ControlToValidate="TB_Contrasena" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:CompareValidator>

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Contrasena" 
                    ControlToValidate="TB_Contrasena" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_RepetirContrasena" ID="LB_RepetirContrasena" runat="server"></asp:label>
                <asp:TextBox 
                    CssClass="form-control" 
                    ID="TB_RepetirContrasena" 
                    MaxLength="20" 
                    runat="server" 
                    TextMode="Password" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ID="ER_Caracteres_RepetirContrasena" 
                    ControlToValidate="TB_RepetirContrasena" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <asp:Button 
                CssClass="btn btn-success align-self-center BTN_Agregar" 
                ID="BTN_Accion" 
                Text="Agregar" 
                runat="server" 
                OnClick="BTN_Accion_Click" 
                ValidationGroup="BTN_Accion" />

        </div>

    </div>

</asp:Content>

