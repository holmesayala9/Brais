﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/AgregarMedico.aspx.cs" Inherits="View_Administrador_AgregarMedico" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender 
        ID="AVC" 
        runat="server"
        HorizontalSide="Right" 
        VerticalSide="Bottom" 
        TargetControlID="P_Mensajes" />

    <asp:Panel ID="P_Mensajes" runat="server" CssClass="flex flex_vertical P_Mensajes"></asp:Panel>

    <div>
        <asp:Label CssClass="titulo LB_AgregarMedico" runat="server" ID="L_Titulo"></asp:Label>

        <div class="d-flex flex-column max-form m-auto">

            <div class="form-group">
                <asp:label id="LB_identificacion" CssClass="LB_Identificacion" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Identificacion" MaxLength="20" runat="server"/>

                <asp:RequiredFieldValidator
                    CssClass="RFV_CampoVacio"
                    ValidationGroup="BTN_Accion" 
                    ErrorMessage="El campo esta vacio" 
                    ControlToValidate="TB_Identificacion" 
                    runat="server" Display="Dynamic" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_SoloNumeros"
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[\s]*[\d]+[\s]*$" 
                    ErrorMessage="Digite solo numeros" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" 
                    id="ER_Numeros"/>
            </div>

            <div class="form-group">
                <asp:label id="LB_Nombre" CssClass="LB_Nombre" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Nombre" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator
                    CssClass="RFV_CampoVacio"
                    ValidationGroup="BTN_Accion" 
                    ErrorMessage="El campo esta vacio" 
                    ControlToValidate="TB_Nombre" 
                    runat="server" Display="Dynamic" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Nombre" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label id="LB_Apellido" CssClass="LB_Apellido" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Apellido" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator
                    CssClass="RFV_CampoVacio"
                    ValidationGroup="BTN_Accion" 
                    ErrorMessage="El campo esta vacio" 
                    ControlToValidate="TB_Apellido" 
                    runat="server" Display="Dynamic" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Apellido" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label id="LB_Correo" CssClass="LB_Correo" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Correo" MaxLength="50" runat="server" TextMode="Email" />

                <asp:RequiredFieldValidator
                    CssClass="RFV_CampoVacio"
                    ValidationGroup="BTN_Accion" 
                    ErrorMessage="El campo esta vacio" 
                    ControlToValidate="TB_Correo" 
                    runat="server" Display="Dynamic" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_Correo"
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^\s*[a-zA-Z][\w-_+.]{1,20}@[\w]{1,20}([.][a-z]{2,4}){1,2}\s*$" 
                    ErrorMessage="Correo no valido, ejemplo: ejemplo@dominio.com" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050"  />
            </div>

            <div class="form-group">
                <asp:label id="LB_Telefono" CssClass="LB_Telefono" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Telefono" MaxLength="20" runat="server" />

                <asp:RequiredFieldValidator
                    CssClass="RFV_CampoVacio"
                    ValidationGroup="BTN_Accion" 
                    ErrorMessage="El campo esta vacio" 
                    ControlToValidate="TB_Telefono" 
                    runat="server" Display="Dynamic" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Telefono" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label id="LB_Contrasena" CssClass="LB_Contrasena" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Contrasena" MaxLength="20" runat="server" TextMode="Password" />

                <asp:RequiredFieldValidator
                    CssClass="RFV_CampoVacio"
                    ValidationGroup="BTN_Accion" 
                    ErrorMessage="El campo esta vacio" 
                    ControlToValidate="TB_Contrasena" 
                    runat="server" Display="Dynamic" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Contrasena" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                
                <asp:CompareValidator 
                    CssClass="RFV_ContrasenaNoCoincide"
                    runat="server" 
                    ErrorMessage="Las contraseñas no coinciden"
                    ControlToCompare="TB_RepetirContrasena" 
                    ControlToValidate="TB_Contrasena" 
                    Display="Dynamic" ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion" 
                    ID="CM_Contrasena"></asp:CompareValidator>
            </div>

            <div class="form-group">
                <asp:label id="LB_RepetirContrasena" CssClass="LB_RepetirContrasena" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_RepetirContrasena" MaxLength="20" runat="server" TextMode="Password" />

                <asp:RegularExpressionValidator
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_RepetirContrasena" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label id="LB_Especialidad" CssClass="LB_Especialidad" runat="server"></asp:label>
                <asp:DropDownList CssClass="form-control" ID="DDL_Especialidad" DataTextField="Nombre" DataValueField="Id" runat="server"></asp:DropDownList>

                <asp:RequiredFieldValidator 
                    CssClass="RFV_Seleccion" 
                    runat="server" 
                    ControlToValidate="DDL_Especialidad" 
                    ErrorMessage="No ha seleccionado"
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion" 
                    InitialValue="-1"></asp:RequiredFieldValidator>
            </div>

            <div class="form-group" runat="server">
                <asp:label id="LB_Consultorio" CssClass="LB_Consultorio" runat="server"></asp:label>
                <asp:DropDownList CssClass="form-control" ID="DDL_Consultorio" DataTextField="Nombre" DataValueField="Id" runat="server"></asp:DropDownList>
            </div>

            <asp:Button CssClass="btn btn-success align-self-center BTN_Agregar" ID="BTN_AgregarMedico" runat="server" OnClick="BTN_AgregarMedico_Click" ValidationGroup="BTN_Accion" />
        </div>

    </div>

</asp:Content>

