﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/ModificarMedico.aspx.cs" Inherits="View_Administrador_ModificarMedico" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender 
        ID="AVC" 
        runat="server" 
        HorizontalSide="Right"
        VerticalSide="Bottom" 
        TargetControlID="P_Mensajes" />

    <asp:Panel ID="P_Mensajes" CssClass="flex flex_vertical P_Mensajes" runat="server"></asp:Panel>

    <div>
        <asp:Label id="LB_Titulo" CssClass="titulo LB_ModificarMedico" runat="server"></asp:Label>

        <div class="d-flex flex-column max-form m-auto">
            <div class="form-group">
                <asp:label CssClass="LB_Identificacion" ID="LB_Identificacion_" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control mb-1" ID="TB_Identificacion" MaxLength="20" runat="server"/>
                <asp:Label CssClass="dato" ID="LB_Identificacion" Text="" runat="server" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_identificacion" 
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_SoloNumeros"
                    id="ER_Numeros_Identificacion" 
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[\s]*[\d]+[\s]*$" 
                    ErrorMessage="Digite solo numeros" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <ASP:label CssClass="LB_Nombre" ID="LB_Nombre_" runat="server" ></ASP:label>
                <asp:TextBox CssClass="form-control mb-1" ID="TB_Nombre" MaxLength="20" runat="server" />
                <asp:Label CssClass="dato" ID="LB_Nombre" Text="" runat="server" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ID="ER_Caracteres_Nombre" 
                    ControlToValidate="TB_Nombre" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Apellido" id="LB_Apellido_" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control mb-1" ID="TB_Apellido" MaxLength="20" runat="server" />
                <asp:Label CssClass="dato" ID="LB_Apellido" Text="" runat="server" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Apellido" 
                    ControlToValidate="TB_Apellido" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Correo" ID="LB_correo_" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control mb-1" ID="TB_Correo" MaxLength="50" runat="server" TextMode="Email" />
                <asp:Label CssClass="dato" ID="LB_Correo" Text="" runat="server" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Correo" 
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:RegularExpressionValidator 
                    CssClass="RFV_Correo"
                    id="ER_Correo_Correo" 
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^\s*[a-zA-Z][\w-_+.]{1,20}@[\w]{1,20}([.][a-z]{2,4}){1,2}\s*$" 
                    ErrorMessage="Correo no valido, ejemplo: ejemplo@dominio.com" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Telefono" id="LB_Telefono_" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control mb-1" ID="TB_Telefono" MaxLength="20" runat="server" />
                <asp:Label CssClass="dato" ID="LB_Telefono" Text="" runat="server" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_Telefono" 
                    ControlToValidate ="TB_Telefono" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Contrasena" id="LB_Contrasena" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Contrasena" MaxLength="20" runat="server" TextMode="Password" />

                <asp:CompareValidator 
                    CssClass="RFV_ContrasenaNoCoincide"
                    ID="CM_Contrasena" 
                    runat="server" 
                    ErrorMessage="Las contraseñas no coinciden" 
                    ControlToCompare="TB_RepetirContrasena" 
                    ControlToValidate="TB_Contrasena" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion">
                </asp:CompareValidator>
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_contrasena" 
                    ControlToValidate="TB_Contrasena" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_RepetirContrasena" ID="LB_RepetirContrasena" runat="server"></asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_RepetirContrasena" MaxLength="20" runat="server" TextMode="Password" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    id="ER_Caracteres_RepetirContrasena" 
                    ControlToValidate="TB_RepetirContrasena" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
            </div>

            <div class="form-group">
                <asp:label CssClass="LB_Especialidad" ID="LB_Especialidad_" runat="server" ></asp:label>
                <asp:DropDownList 
                    ID="DDL_Especialidad" 
                    CssClass="form-control mb-1"
                    DataValueField="Id" 
                    DataTextField="Nombre" 
                    runat="server">
                </asp:DropDownList>
                <asp:Label CssClass="dato" ID="LB_Especialidad" Text="" runat="server" />

                <asp:RequiredFieldValidator 
                    CssClass=""
                    ID="RFV_Especialidad" 
                    runat="server" 
                    ControlToValidate="DDL_Especialidad" 
                    ErrorMessage="No ha seleccionado" 
                    Display="Dynamic" 
                    ForeColor="#FF5050" 
                    ValidationGroup="BTN_Accion" 
                    InitialValue="0">
                </asp:RequiredFieldValidator>
            </div>

            <div ID="P_Consultorio" class="d-flex align-items-center" runat="server">
                <div class="form-group mr-3">
                    <asp:label CssClass="LB_Consultorio" id="LB_Consultorio_" runat="server" ></asp:label>
                    <asp:DropDownList 
                        ID="DDL_Consultorio"
                        CssClass="form-control mb-1"
                        DataValueField="Id" 
                        DataTextField="Nombre" 
                        runat="server">
                    </asp:DropDownList>
                    <asp:Label CssClass="dato" ID="LB_Consultorio" Text="" runat="server" />
                </div>
                <div class="d-flex">
                    <asp:Button CssClass="btn btn-danger BTN_LiberarConsultorio" ID="BTN_LiberarConsultorio" OnClick="BTN_LiberarConsultorio_Click" runat="server" />
                </div>
            </div>

            <asp:Button CssClass="btn btn-info align-self-center BTN_Modificar" ID="BTN_Modificar" runat="server" OnClick="BTN_Modificar_Click" ValidationGroup="BTN_Accion" />

        </div>

    </div>

</asp:Content>

