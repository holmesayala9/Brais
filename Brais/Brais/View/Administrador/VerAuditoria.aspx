﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/VerAuditoria.aspx.cs" Inherits="Controller_Administrador_VerAuditoria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender 
        ID="AVC" 
        runat="server" 
        HorizontalSide="Right" 
        VerticalSide="Bottom" 
        TargetControlID="P_Mensajes" />

    <asp:Label CssClass="titulo" ID="LB_Titulo" Text="" runat="server" />

    <asp:Label ID="LB_NombreTabla" Text="" Visible="false" runat="server" />

    <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

    <div class="flex">

        <asp:GridView 
            ID="GV_Auditoria" 
            EmptyDataText="No hay información que mostrar"
            EmptyDataRowStyle-CssClass="GV_Vacio"
            GridLines="None" 
            AutoGenerateColumns="False" 
            runat="server" 
            AllowPaging="True" 
            OnPageIndexChanging="GV_Auditoria_PageIndexChanging">
            <AlternatingRowStyle BackColor="#eeeeee"/>
            <Columns>
                <asp:TemplateField HeaderText="Acceso" HeaderStyle-CssClass="LB_Acceso">
                    <ItemTemplate>
                        <asp:Button CssClass="BTN_Azul BTN_VerAcceso" ID="BTN_VerAcceso" Text="Ver acceso" CommandName='<%# Eval("IdAcceso") %>' OnClick="BTN_VerAcceso_Click" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                <asp:BoundField DataField="Accion" HeaderText="Accion" SortExpression="Accion" />
                <asp:BoundField DataField="UsuarioDB" HeaderText="UsuarioDB" SortExpression="UsuarioDB" />
                <asp:BoundField DataField="Esquema" HeaderText="Esquema" SortExpression="Esquema" />
                <asp:BoundField DataField="Tabla" HeaderText="Tabla" SortExpression="Tabla" />

                <asp:TemplateField HeaderText="Datos" HeaderStyle-CssClass="LB_Datos">
                    <ItemTemplate>
                        <asp:Label CssClass="json" Text='<%# Eval("Data") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>

            <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
            <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
            <PagerSettings Position="TopAndBottom" />
            <PagerStyle BorderColor="#AAAAAA" BorderWidth="1px" Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
            <RowStyle BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
        </asp:GridView>

    </div>

    <script src="../../js/auditoria.js"></script>
</asp:Content>

