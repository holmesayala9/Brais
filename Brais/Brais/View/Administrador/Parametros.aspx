﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/Parametros.aspx.cs" Inherits="Controller_Administrador_Parametros" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .crud{
            margin-bottom: 20px;
        }
    </style>

    <asp:label class="titulo LB_Configuracion" id="LB_TituloConfiguracion" runat="server"></asp:label>

    <div class="flex flex_vertical">
        <asp:label id="LB_RegenerarCitas" class="subtitulo LB_RegenerarCitas" runat="server"></asp:label>

        <asp:Panel CssClass="d-flex" ID="P_MensajesGenerarCitas" runat="server"></asp:Panel>

        <asp:Button CssClass="btn btn-danger BTN_Generar" ID="BTN_GenerarCitas" OnClick="BTN_GenerarCitas_Click" runat="server" />
    </div>

    <div class="flex flex_vertical">
        <asp:label class="subtitulo LB_RestriccionesAcceso" runat="server"></asp:label>

        <div class="d-flex align-items-start mb-4">
            <div class="d-flex flex-column align-items-center mr-5">
                <div class="form-group">
                    <asp:label runat="server" CssClass="LB_CantidadSesiones"></asp:label>
                    <asp:TextBox CssClass="form-control mb-1" ID="TB_SesionesSimultaneas" min="1" max="5" MaxLength="5" TextMode="Number" runat="server" />
                    <asp:Label CssClass="dato" ID="LB_SesionesSimultaneas_Data" Text="" runat="server" />

                    <asp:RequiredFieldValidator 
                        CssClass="RFV_CampoVacio"
                        runat="server" 
                        ControlToValidate="TB_SesionesSimultaneas" 
                        ErrorMessage="El campo esta vacio" 
                        Display="Dynamic" 
                        ForeColor="#FF5050" 
                        ValidationGroup="BTN_EditarSesionesSimultaneas">
                    </asp:RequiredFieldValidator>

                    <asp:RegularExpressionValidator 
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_SesionesSimultaneas" 
                        ValidationGroup="BTN_EditarSesionesSimultaneas" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" 
                        id="ER_Caracteres_Identificacion"/>

                    <asp:RegularExpressionValidator 
                        CssClass="RFV_SoloNumeros"
                        ControlToValidate="TB_SesionesSimultaneas" 
                        ValidationGroup="BTN_EditarSesionesSimultaneas" 
                        ValidationExpression="^[\s]*[\d]+[\s]*$" 
                        ErrorMessage="Digite solo numeros" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" 
                        id="ER_Numeros_Identificacion"/>
                </div>

                <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_EditarSesionesSimultaneas" ValidationGroup="BTN_EditarSesionesSimultaneas" OnClick="BTN_EditarSesionesSimultaneas_Click" Text="Editar" runat="server" />
            </div>

            <div class="d-flex flex-column align-items-center">
                <div class="form-group">
                    <asp:label CssClass="LB_IntentosAcceso" runat="server" ></asp:label>
                    <asp:TextBox CssClass="form-control mb-1" ID="TB_IntentosAcceso" min="1" max="10" MaxLength="5" TextMode="Number" runat="server" />
                    <asp:Label CssClass="dato" ID="LB_IntentosAcceso_Data" Text="" runat="server" />

                    <asp:RequiredFieldValidator 
                        CssClass="RFV_CampoVacio"
                        runat="server" 
                        ControlToValidate="TB_IntentosAcceso" 
                        ErrorMessage="El campo esta vacio" 
                        Display="Dynamic" 
                        ForeColor="#FF5050" 
                        ValidationGroup="BTN_EditarIntentosAcceso">
                    </asp:RequiredFieldValidator>

                    <asp:RegularExpressionValidator 
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_IntentosAcceso" 
                        ValidationGroup="BTN_EditarIntentosAcceso" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" 
                        id="RegularExpressionValidator1"/>

                    <asp:RegularExpressionValidator 
                        CssClass="RFV_SoloNumeros"
                        ControlToValidate="TB_IntentosAcceso" 
                        ValidationGroup="BTN_EditarIntentosAcceso" 
                        ValidationExpression="^[\s]*[\d]+[\s]*$" 
                        ErrorMessage="Digite solo numeros" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" 
                        id="RegularExpressionValidator2"/>
                </div>

                <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_EditarIntentosAcceso" ValidationGroup="BTN_EditarIntentosAcceso" OnClick="BTN_EditarIntentosAcceso_Click" runat="server" />
            </div>
        </div>

        <asp:Panel CssClass="d-flex flex-column" ID="P_MensajesRestriccionesAcceso" runat="server"></asp:Panel>
    </div>


    <div class="d-flex justify-content-center align-items-start mb-4">
        <div class="d-flex flex-column align-items-center">
            <asp:label runat="server" ID="LB_DiasLaborales" class="subtitulo LB_DiasSemamaLaborales">Dias de semana laborales</asp:label>
                
            <asp:CheckBoxList ID="CBL_DiasSemana" runat="server">
                <asp:ListItem Value="1">Lunes</asp:ListItem>
                <asp:ListItem Value="2">Martes</asp:ListItem>
                <asp:ListItem Value="3">Miercoles</asp:ListItem>
                <asp:ListItem Value="4">Jueves</asp:ListItem>
                <asp:ListItem Value="5">Viernes</asp:ListItem>
                <asp:ListItem Value="6">Sabado</asp:ListItem>
                <asp:ListItem Value="0">Domingo</asp:ListItem>
            </asp:CheckBoxList>

            <asp:Panel CssClass="d-flex" ID="P_MensajesDiasSemana" runat="server"></asp:Panel>

            <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_ModificarDiasSemana" OnClick="BTN_ModificarDiasSemana_Click" runat="server" />
        </div>

        <div class="d-flex flex-column align-items-center">
            <asp:label runat="server" id="LB_HorarioTrabajo" class="subtitulo LB_HorarioTrabajo"></asp:label>

            <div class="form-group">
                <asp:Label CssClass="LB_HoraInicio" ID="LB_HoraInicio" runat="server" />
                <asp:DropDownList CssClass="form-control mb-1" ID="DDL_HoraInicio" runat="server" DataTextField="text" DataValueField="value"></asp:DropDownList>
                <asp:Label CssClass="dato" ID="LB_HoraInicio_Data" Text="" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label CssClass="LB_HoraFin" ID="LB_HoraFin" runat="server" />
                <asp:DropDownList CssClass="form-control mb-1" ID="DDL_HoraFin" DataTextField="text" DataValueField="value" runat="server"></asp:DropDownList>
                <asp:Label CssClass="dato" ID="LB_HoraFin_Data" Text="" runat="server" />
            </div>

            <asp:Panel CssClass="d-flex" ID="P_MensajesHorarioTrabajo" runat="server"></asp:Panel>

            <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_ModificarHorario" OnClick="BTN_ModificarHorario_Click" runat="server" />
        </div>

        <div class="d-flex flex-column align-items-center">
            <asp:label runat="server" ID="LB_DuracionCitas" class="subtitulo LB_DuracionCitas"></asp:label>

            <div class="form-group">
                <asp:label ID="LB_DuracionSub" CssClass="LB_Duracion" runat="server" ></asp:label>
                <asp:DropDownList CssClass="form-control mb-1" ID="DDL_DuracionCitas" DataTextField="text" DataValueField="value" runat="server" DataSourceID="ODS_DuracionCitas"></asp:DropDownList>
                <asp:Label CssClass="dato" ID="L_DuracionCitasData" Text="text" runat="server" />

                <asp:ObjectDataSource ID="ODS_DuracionCitas" runat="server" SelectMethod="getDuracionCitasForView" TypeName="Logica.Clases.Administrador.LAdministrador"></asp:ObjectDataSource>
            </div>

            <asp:Panel CssClass="d-flex" ID="P_MensajesDuracionCitas" runat="server"></asp:Panel>

            <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_ModificarDuracionCitas" OnClick="BTN_ModificarDuracionCitas_Click" runat="server" />
        </div>
    </div>

    <div class="d-flex mb-5 justify-content-center"><asp:Button CssClass="btn btn-dark BTN_AuditoriaParametros" ID="BTN_AuditoriaParametros" OnClick="BTN_AuditoriaParametros_Click" runat="server" /></div>


    <asp:UpdatePanel class="crud" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:label runat="server" ID="LB_GestionConsultorios" class="subtitulo LB_GestionarConsultorios"></asp:label>

            <div class="d-flex flex-column align-items-center">
                <div class="form-group">
                    <asp:label CssClass="LB_Nombre" runat="server" ID="LB_NombreConsultorio" ></asp:label>
                    <asp:TextBox CssClass="form-control" ID="TB_NombreConsultorio" MaxLength="20" runat="server" />

                    <asp:RequiredFieldValidator
                        CssClass="RFV_CampoVacio"
                        ValidationGroup="BTN_AgregarConsultorio" 
                        ErrorMessage="El campo esta vacio" 
                        ControlToValidate="TB_NombreConsultorio" 
                        runat="server" Display="Dynamic" 
                        ForeColor="#FF5050" />

                    <asp:RegularExpressionValidator
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_NombreConsultorio" 
                        ValidationGroup="BTN_AgregarConsultorio" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>
                <div class="form-group">
                    <asp:label CssClass="LB_Descripcion" runat="server" ID="LB_DescripcionConsultorio" ></asp:label>
                    <asp:TextBox CssClass="form-control" ID="TB_DescripcionConsultorio" MaxLength="20" runat="server" />

                    <asp:RequiredFieldValidator
                        CssClass="RFV_CampoVacio"
                        ValidationGroup="BTN_AgregarConsultorio" 
                        ErrorMessage="El campo esta vacio" 
                        ControlToValidate="TB_DescripcionConsultorio" 
                        runat="server" Display="Dynamic" 
                        ForeColor="#FF5050" />

                    <asp:RegularExpressionValidator
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_DescripcionConsultorio" 
                        ValidationGroup="BTN_AgregarConsultorio" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>

                <asp:Button CssClass="btn btn-success BTN_Agregar" ID="BTN_AgregarConsultorio" ValidationGroup="BTN_AgregarConsultorio" OnClick="BTN_AgregarConsultorio_Click" runat="server" />
            </div>

            <asp:Panel ID="P_EditarConsultorio" class="d-flex flex-column align-items-center" runat="server" Visible="False">
                <asp:Label ID="LB_IdConsultorio" Text="" Visible="false" runat="server" />

                <div class="form-group">
                    <asp:label runat="server" CssClass="LB_Nombre" ID="LB_NombreConsultorio_Modificar"></asp:label>
                    <asp:TextBox CssClass="form-control" ID="TB_NombreConsultorio_Editar" MaxLength="20" runat="server" />

                    <asp:RequiredFieldValidator
                        CssClass="RFV_CampoVacio"
                        ValidationGroup="BTN_EditarConsultorio" 
                        ErrorMessage="El campo esta vacio" 
                        ControlToValidate="TB_NombreConsultorio_Editar" 
                        runat="server" Display="Dynamic" 
                        ForeColor="#FF5050" />

                    <asp:RegularExpressionValidator
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_NombreConsultorio_Editar" 
                        ValidationGroup="BTN_EditarConsultorio" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>

                <div class="form-group">
                    <asp:label runat="server" CssClass="LB_Descripcion" ID="LB_DescripcionConsultorio_Modificar" ></asp:label>
                    <asp:TextBox CssClass="form-control" ID="TB_DescripcionConsultorio_Editar" MaxLength="20" runat="server" />

                    <asp:RequiredFieldValidator
                        CssClass="RFV_CampoVacio"
                        ValidationGroup="BTN_EditarConsultorio" 
                        ErrorMessage="El campo esta vacio" 
                        ControlToValidate="TB_DescripcionConsultorio_Editar" 
                        runat="server" Display="Dynamic" 
                        ForeColor="#FF5050" />

                    <asp:RegularExpressionValidator
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_DescripcionConsultorio_Editar" 
                        ValidationGroup="BTN_EditarConsultorio" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>

                <div class="form-group">
                    <div>
                        <asp:CheckBox ID="CB_DisponibilidadConsultorio" Text="" runat="server" />
                        <asp:Label Text="Disponibildiad" CssClass="LB_Disponibilidad" runat="server" />
                    </div>
                </div>

                <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_EditarConsultorio" ValidationGroup="BTN_EditarConsultorio" OnClick="BTN_EditarConsultorio_Click" runat="server" />
            </asp:Panel>

            <asp:Panel CssClass="d-flex justify-content-center mt-4" ID="P_MensajesConsultorio" runat="server"></asp:Panel>
            
            <div class="table-responsive d-flex justify-content-center mt-4">
                <asp:GridView 
                    CssClass="table table-hover" 
                    ID="GV_Consultorios" 
                    runat="server" 
                    AutoGenerateColumns="False"
                    DataSourceID="ODS_Consultorios" 
                    AllowPaging="True" 
                    CellPadding="0" 
                    GridLines="None" 
                    EmptyDataText="No hay información que mostrar"
                    EmptyDataRowStyle-CssClass="GV_Vacio">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                        <asp:BoundField HeaderStyle-CssClass="LB_Nombre" DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField HeaderStyle-CssClass="LB_Descripcion" DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
                        <asp:BoundField HeaderStyle-CssClass="LB_Disponibilidad" DataField="Disponibilidad" HeaderText="Disponibilidad" SortExpression="Disponibilidad" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_MostrarEditorConsultorio" CommandName='<%# Eval("Id") %>' Text="Editar" OnClick="BTN_MostrarEditorConsultorio_Click" runat="server"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button CssClass="btn btn-danger BTN_Eliminar" ID="BTN_EliminarConsultorio" CommandName='<%# Eval("Id") %>' Text="Eliminar" OnClick="BTN_EliminarConsultorio_Click" runat="server"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
                    <HeaderStyle CssClass="thead-light" />
                </asp:GridView>
            </div>

            <asp:ObjectDataSource ID="ODS_Consultorios" runat="server" SelectMethod="getAll" TypeName="Logica.Clases.Medico.LConsultorio"></asp:ObjectDataSource>

            <div class="d-flex justify-content-center"><asp:Button CssClass="btn btn-dark BTN_Auditoria" ID="BTN_AuditoriaConsultorio" OnClick="BTN_AuditoriaConsultorio_Click" Text="Auditoria" runat="server" /></div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BTN_AgregarConsultorio" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="BTN_EditarConsultorio" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="GV_Consultorios" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdatePanel class="crud" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:label runat="server" ID="LB_GestionEspecialidades" class="subtitulo LB_GestionarEspecialidades">Gestionar especialidades</asp:label>

            <div class="d-flex flex-column align-items-center">
                <div class="form-group">
                    <asp:label runat="server" CssClass="LB_Nombre" ID="LB_NombreEspecialidad"></asp:label>
                    <asp:TextBox CssClass="form-control" ID="TB_NombreEspecialidad" MaxLength="20" runat="server" />

                    <asp:RequiredFieldValidator
                        CssClass="RFV_CampoVacio"
                        ValidationGroup="BTN_AgregarEspecialidad" 
                        ErrorMessage="El campo esta vacio" 
                        ControlToValidate="TB_NombreEspecialidad" 
                        runat="server" Display="Dynamic" 
                        ForeColor="#FF5050" />

                    <asp:RegularExpressionValidator
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_NombreEspecialidad" 
                        ValidationGroup="BTN_AgregarEspecialidad" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>

                <asp:Button CssClass="btn btn-success BTN_Agregar" ID="BTN_AgregarEspecialidad" ValidationGroup="BTN_AgregarEspecialidad" runat="server" OnClick="BTN_AgregarEspecialidad_Click" />
            </div>

            <asp:Panel class="d-flex flex-column align-items-center" ID="P_EditarEspecialidad" Visible="false" runat="server">
                <asp:Label ID="LB_IdEspecialidad" Text="" Visible="false" runat="server" />

                <div class="form-group">
                    <asp:label runat="server" CssClass="LB_Nombre" ID="LB_NombreEspcialidad_Editar"></asp:label>
                    <asp:TextBox CssClass="form-control" ID="TB_NombreEspecialidadEditar" MaxLength="20" runat="server" />

                    <asp:RequiredFieldValidator
                        CssClass="RFV_CampoVacio"
                        ValidationGroup="BTN_EditarEspecialidad" 
                        ErrorMessage="El campo esta vacio" 
                        ControlToValidate="TB_NombreEspecialidadEditar" 
                        runat="server" Display="Dynamic" 
                        ForeColor="#FF5050" />

                    <asp:RegularExpressionValidator
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_NombreEspecialidadEditar" 
                        ValidationGroup="BTN_EditarEspecialidad" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>

                <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_EditarEspecialidad" ValidationGroup="BTN_EditarEspecialidad" runat="server" OnClick="BTN_EditarEspecialidad_Click" />
            </asp:Panel>

            <asp:Panel CssClass="d-flex justify-content-center mt-4" ID="P_MensajesEspecialidad"  runat="server"></asp:Panel>

            <div class="table-responsive d-flex justify-content-center">
                <asp:GridView 
                    CssClass="table table-hover" 
                    ID="GV_Especialidades" 
                    runat="server" 
                    AutoGenerateColumns="False" 
                    GridLines="None" 
                    EmptyDataText="No hay información que mostrar"
                    EmptyDataRowStyle-CssClass="GV_Vacio"
                    DataSourceID="ODS_Especialidades" 
                    AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                        <asp:BoundField HeaderStyle-CssClass="LB_Nombre" DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_MostrarEditorEspecialidad" CommandName='<%# Eval("Id") %>' Text="Editar" OnClick="BTN_MostrarEditorEspecialidad_Click" runat="server"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button CssClass="btn btn-danger BTN_Eliminar" ID="BTN_EliminarEspecialidad" CommandName='<%# Eval("Id") %>' Text="Eliminar" OnClick="BTN_EliminarEspecialidad_Click" runat="server"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
                    <HeaderStyle CssClass="thead-light" />
                </asp:GridView>
            </div>

            <asp:ObjectDataSource ID="ODS_Especialidades" runat="server" SelectMethod="getAll" TypeName="Logica.Clases.Medico.LEspecialidad"></asp:ObjectDataSource>


            <div class="d-flex justify-content-center">
                <asp:Button CssClass="btn btn-dark BTN_Auditoria" ID="BTN_AuditoriaEspecialidad" OnClick="BTN_AuditoriaEspecialidad_Click" runat="server" />
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script src="../../js/seguridad.js"></script>

</asp:Content>

