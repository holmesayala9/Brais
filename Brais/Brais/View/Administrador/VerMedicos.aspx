﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/View/Administrador/MPAdministrador.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/VerMedicos.aspx.cs" Inherits="View_Administrador_VerMedicos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

    <div class="medicos_registrados">

        <asp:label ID="LB_MedicosRegistrados" class="titulo LB_MedicosRegistrados" runat="server"></asp:label>

        <div class="form-inline d-flex justify-content-center mb-4">
            <asp:TextBox CssClass="form-control mr-2 TB_Buscar" ID="TB_Buscar" MaxLength="20" runat="server"></asp:TextBox>

            <asp:Button CssClass="btn btn-primary mr-5 BTN_Buscar" ID="BTN_Buscar" ValidationGroup="BTN_Buscar" runat="server" />

            <asp:Button CssClass="btn btn-success BTN_AgregarNuevoMedico" ID="BTN_AgregarMedico" OnClick="BTN_AgregarMedico_Click" runat="server" />
        </div>

        <div class="d-flex justify-content-center mb-4"> 
            <asp:RegularExpressionValidator
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_Buscar" 
                ValidationGroup="BTN_Buscar" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>

        <asp:Panel CssClass="flex flex_vertical" ID="P_ResultadosActivos" runat="server"></asp:Panel>

        <div class="table-responsive d-flex justify-content-center">
            <asp:GridView CssClass="table table-hover" ID="GV_Medicos" runat="server"
                AutoGenerateColumns="False"
                CellPadding="0"
                GridLines="None"
                EmptyDataText="No hay información que mostrar"
                EmptyDataRowStyle-CssClass="GV_Vacio"
                AllowPaging="True" 
                OnPageIndexChanging="GV_Medicos_PageIndexChanging">
                <Columns>
                    <asp:BoundField HeaderStyle-CssClass="LB_Identificacion" DataField="Identificacion" HeaderText="Identificacion" SortExpression="Id" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Nombre" DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Apellido" DataField="Apellido" HeaderText="Apellido" SortExpression="Apellido" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Correo" DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Telefono" DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Contrasena" DataField="Contrasena" HeaderText="Contrasena" SortExpression="Contrasena" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Especialidad" DataField="EEspecialidad.Nombre" HeaderText="Especialidad" SortExpression="Especialidad" NullDisplayText="Ninguno" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Consultorio" DataField="EConsultorio.Nombre" HeaderText="Consultorio" SortExpression="Consultorio" NullDisplayText="Ninguno" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button CssClass="btn btn-info BTN_Editar" ID="BTN_Modificar" CommandName='<%# Eval("Id") %>' Text="Editar" OnClick="BTN_Modificar_Click" runat="server"></asp:Button>
                            <asp:Button CssClass="btn btn-danger BTN_Deshabilitar" ID="BTN_Deshabilitar" CommandName='<%# Eval("Id") %>' Text="Deshabilitar" OnClick="BTN_Deshabilitar_Click" runat="server"></asp:Button>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
                <HeaderStyle CssClass="thead-light" />
            </asp:GridView>
        </div>

    </div>

    <div>
        <asp:label runat="server" class="titulo LB_Deshabilitados"></asp:label>

        <div class="form-inline justify-content-center mb-4">
            <asp:TextBox CssClass="form-control mr-2 TB_Buscar" ID="TB_BuscarDeshabilitados" MaxLength="20" runat="server"></asp:TextBox>
            <asp:Button CssClass="btn btn-primary BTN_Buscar" ID="BTN_BuscarDeshabilitados" ValidationGroup="BTN_BuscarDeshabilitados" runat="server" />
        </div>

        <div class="d-flex justify-content-center mb-4">
            <asp:RegularExpressionValidator
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_BuscarDeshabilitados" 
                ValidationGroup="BTN_BuscarDeshabilitados" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>

        <asp:Panel CssClass="flex flex_vertical" ID="P_ResultadosInactivos" runat="server"></asp:Panel>

        <div class="table-responsive d-flex justify-content-center">
            <asp:GridView CssClass="table table-hover" ID="GV_MedicosDeshabilitados"
                AutoGenerateColumns = "false"
                CellPadding="0"
                GridLines="None"
                EmptyDataText="No hay información que mostrar"
                EmptyDataRowStyle-CssClass="GV_Vacio"
                runat="server">
                <Columns>
                    <asp:BoundField HeaderStyle-CssClass="LB_Identificacion" DataField="Identificacion" HeaderText="Identificacion" SortExpression="Id" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Nombre" DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Apellido" DataField="Apellido" HeaderText="Apellido" SortExpression="Apellido" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Correo" DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Telefono" DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Contrasena" DataField="Contrasena" HeaderText="Contrasena" SortExpression="Contrasena" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Especialidad" DataField="EEspecialidad.Nombre" HeaderText="Especialidad" SortExpression="Especialidad" NullDisplayText="Ninguno" />
                    <asp:BoundField HeaderStyle-CssClass="LB_Consultorio" DataField="EConsultorio.Nombre" HeaderText="Consultorio" SortExpression="Consultorio" NullDisplayText="Ninguno" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button CssClass="btn btn-info BTN_Activar" ID="BTN_Activar" CommandName='<%# Eval("Id") %>' Text="Activar" OnClick="BTN_Activar_Click" runat="server"></asp:Button>
                            <asp:Button CssClass="btn btn-danger BTN_Eliminar" ID="BTN_Eliminar" CommandName='<%# Eval("Id") %>' Text="Eliminar" OnClick="BTN_Eliminar_Click" runat="server"></asp:Button>
                            <ajaxToolkit:ConfirmButtonExtender ID="BTN_EliminarMedico" runat="server" ConfirmText="¿Esta seguro que desea eliminar este médico?" TargetControlID="BTN_Eliminar" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="GV_Vacio"></EmptyDataRowStyle>
                <HeaderStyle CssClass="thead-light" />
            </asp:GridView>
        </div>
    </div>


</asp:Content>

