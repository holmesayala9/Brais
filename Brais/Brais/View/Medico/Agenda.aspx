﻿<%@ Page Title=""  EnableEventValidation="false" Language="C#" MasterPageFile="~/View/Medico/MPMedico.master" AutoEventWireup="true" CodeFile="~/Controller/Medico/Agenda.aspx.cs" Inherits="View_Medico_Agenda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

            <asp:label runat="server" CssClass="titulo LB_Agenda"></asp:label>

            <div class="flex flex_vertical" style="height:350px">
            <ajaxToolkit:DragPanelExtender ID="DPE_Calendario" TargetControlID="C_Cita" ViewStateMode="Enabled"  runat="server" />
                <asp:Calendar 
                    ID="C_Cita" 
                    runat="server" 
                    OnSelectionChanged="C_Cita_SelectionChanged" 
                    BackColor="White" 
                    BorderColor="#3366CC" 
                    BorderWidth="1px" 
                    CellPadding="1" 
                    DayNameFormat="Shortest" 
                    Font-Names="Verdana" 
                    Font-Size="15pt" 
                    ForeColor="#003399" 
                    Height="300px" 
                    Width="320px">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </div>

            <div class="flex">
                <asp:Button CssClass="BTN_Secundario BTN_ReporteAgenda" ID="BTN_ReporteAgenda" OnClick="BTN_ReporteAgenda_Click" Text="Imprimir agenda de hoy" runat="server" />
            </div>

            <asp:Panel CssClass="flex flex_vertical" ID="P_FechaSeleccionada" runat="server"></asp:Panel>

            <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

            <asp:GridView 
                CssClass="GV" 
                ID="GV_Citas" 
                runat="server" 
                EmptyDataText="No hay información que mostrar"
                EmptyDataRowStyle-CssClass="GV_Vacio"
                AutoGenerateColumns="False" 
                CellPadding="0" 
                GridLines="None" 
                AllowCustomPaging="False" 
                AllowPaging="True" 
                OnPageIndexChanging="GV_Citas_PageIndexChanging">
                <AlternatingRowStyle BackColor="#E2E2E2" />
                <Columns>
                    <asp:TemplateField HeaderText="Hora inicio" HeaderStyle-CssClass="LB_HoraInicio">
                        <ItemTemplate>
                            <asp:Label Text='<%# DateTime.Parse(Eval("HoraInicio").ToString()).ToShortTimeString() %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Hora fin" HeaderStyle-CssClass="LB_HoraFin">
                        <ItemTemplate>
                            <asp:Label Text='<%# DateTime.Parse(Eval("HoraFin").ToString()).ToShortTimeString() %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Usuario" HeaderStyle-CssClass="LB_Usuario">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("EUsuario.Nombre").ToString() + " " + Eval("EUsuario.Apellido").ToString() %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button CssClass="BTN_Comun BTN_VerInformacion" ID="BTN_VerInformacion" Text="Ver informacion" CommandName='<%# Eval("Id") %>' OnClick="BTN_VerInformacion_Click" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <EmptyDataRowStyle HorizontalAlign="Center" />
                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
            </asp:GridView>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

