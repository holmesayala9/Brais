﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Medico/MPMedico.master" AutoEventWireup="true" CodeFile="~/Controller/Medico/ReporteAgenda.aspx.cs" Inherits="View_Medico_ReporteAgenda" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="flex">
        <CR:CrystalReportViewer ID="CRV_Reporte" runat="server" AutoDataBind="True" Height="50px" ReportSourceID="CRS_Reporte" ToolPanelWidth="200px" Width="350px"></CR:CrystalReportViewer>
    </div>

    <CR:CrystalReportSource ID="CRS_Reporte" runat="server">
        <Report FileName="~\Reporte\Medico\CR_Agenda.rpt">
        </Report>
    </CR:CrystalReportSource>

</asp:Content>