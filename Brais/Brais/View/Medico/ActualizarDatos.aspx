﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Medico/MPMedico.master" AutoEventWireup="true" CodeFile="~/Controller/Medico/ActualizarDatos.aspx.cs" Inherits="View_Medico_ActualizarDatos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />
            <asp:Panel ID="P_Mensajes" runat="server" CssClass="flex flex_vertical P_Mensajes"></asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div>
        <asp:label runat="server" CssClass="subtitulo LB_ActualizarDatosMedico"></asp:label>

        <div class="flex flex_vertical">

            <div class="campo">
                <asp:label CssClass="LB_Identificacion" runat="server"></asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Identificacion" runat="server" ControlToValidate="TB_Identificacion" ErrorMessage="Identificacion vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <%--<asp:TextBox CssClass="TB" ID="TB_Identificacion" MaxLength="20" runat="server" TextMode="Number" />--%>
                <asp:Label CssClass="dato" ID="LB_Identificacion" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:label CssClass="LB_Nombre" runat="server"></asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TB_Nombre" ErrorMessage="Nombre vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <%--<asp:TextBox CssClass="TB" ID="TB_Nombre" MaxLength="20" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_Nombre" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:label CssClass="LB_Apellido" runat="server"></asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Apellido" runat="server" ControlToValidate="TB_Apellido" ErrorMessage="Apellido vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <%--<asp:TextBox CssClass="TB" ID="TB_Apellido" MaxLength="20" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_Apellido" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:label CssClass="LB_Correo" runat="server"></asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TB_Correo" ErrorMessage="Correo vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <asp:TextBox CssClass="TB" ID="TB_Correo" MaxLength="50" runat="server" TextMode="Email" />
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:RegularExpressionValidator 
                    CssClass="RFV_Correo"
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^\s*[a-zA-Z][\w-_+.]{1,20}@[\w]{1,20}([.][a-z]{2,4}){1,2}\s*$" 
                    ErrorMessage="Correo no valido, ejemplo: ejemplo@dominio.com" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:Label CssClass="dato" ID="LB_Correo" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:label CssClass="LB_Telefono" runat="server"></asp:label>
                <asp:TextBox CssClass="TB " ID="TB_Telefono" MaxLength="20" runat="server" />
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Telefono" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:Label CssClass="dato" ID="LB_Telefono" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:label runat="server" CssClass="LB_Especialidad"></asp:label>
                <%--<asp:TextBox CssClass="TB" ID="TB_Especialidad" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_Especialidad" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:Label CssClass="dato" ID="LB_ConsultorioActual" Text="" runat="server" Width="300px" />
            </div>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="campo">
                        <div> 
                            <asp:CheckBox 
                                ID="CB_ModificarContrasena" 
                                AutoPostBack="true"  
                                runat="server" 
                                OnCheckedChanged="CB_ModificarContrasena_CheckedChanged" /> 
                            <asp:Label 
                                CssClass = "CB_ModificarContrasena"
                                Text="Modificar contraseña" 
                                runat="server" />
                        </div>
                    </div>

                    <asp:Panel ID="P_ModificarContrasena" Visible="false" runat="server">
            
                        <div class="campo">
                            <asp:label runat="server" CssClass="LB_Contrasena" ></asp:label>
                            <asp:TextBox CssClass="TB" ID="TB_Contrasena" MaxLength="20" runat="server" TextMode="Password" />
                            <asp:RegularExpressionValidator 
                                CssClass="RFV_CaracteresNoValidos"
                                ControlToValidate="TB_Contrasena" 
                                ValidationGroup="BTN_Accion" 
                                ValidationExpression="^[^<>]*$" 
                                ErrorMessage="Caracteres no validos" 
                                Display="Dynamic" 
                                runat="server" 
                                ForeColor="#FF5050" />
                            <br />
                            <asp:Label ID="LB_status2" runat="server"></asp:Label>
                        </div>
                        <div class="campo">
                            <asp:label runat="server" CssClass="LB_NuevaContrasena" ></asp:label>
                            <asp:CompareValidator 
                                CssClass="RFV_ContrasenaNoCoincide"
                                runat="server" 
                                ErrorMessage="Las contraseñas no coinciden" 
                                ControlToCompare="TB_RepetirNuevaContrasena" 
                                ControlToValidate="TB_NuevaContrasena" 
                                Display="Dynamic" 
                                ForeColor="#FF5050" 
                                ValidationGroup="BTN_Accion">
                            </asp:CompareValidator>
                            <asp:TextBox CssClass="TB" ID="TB_NuevaContrasena" MaxLength="20" runat="server" TextMode="Password" />
                            <asp:RegularExpressionValidator 
                                CssClass="RFV_CaracteresNoValidos"
                                ControlToValidate="TB_NuevaContrasena" 
                                ValidationGroup="BTN_Accion" 
                                ValidationExpression="^[^<>]*$" 
                                ErrorMessage="Caracteres no validos" 
                                Display="Dynamic" 
                                runat="server" 
                                ForeColor="#FF5050" />
                        </div>
                        <div class="campo">
                            <asp:label CssClass="LB_RepetirNuevaContrasena" runat="server"></asp:label>
                            <asp:TextBox CssClass="TB" ID="TB_RepetirNuevaContrasena" MaxLength="20" runat="server" TextMode="Password" />
                            <asp:RegularExpressionValidator 
                                CssClass="RFV_CaracteresNoValidos"
                                ControlToValidate="TB_RepetirNuevaContrasena" 
                                ValidationGroup="BTN_Accion" 
                                ValidationExpression="^[^<>]*$" 
                                ErrorMessage="Caracteres no validos" 
                                Display="Dynamic" 
                                runat="server" 
                                ForeColor="#FF5050" />
                        </div>

                        <div class="flex">
                            <asp:Button CssClass="BTN_Comun" id="BTN_ModificarContrasena" OnClick="BTN_ModificarContrasena_Click" Text="Modificar contrasena" runat="server" />
                        </di>

                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Button CssClass="BTN_Comun BTN_Modificar" ID="BTN_Modificar" runat="server" OnClick="BTN_Modificar_Click" ValidationGroup="BTN_Accion" />

        </div>

    </div>

</asp:Content>

