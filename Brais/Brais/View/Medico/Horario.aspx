﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Medico/MPMedico.master" AutoEventWireup="true" CodeFile="~/Controller/Medico/Horario.aspx.cs" Inherits="View_Medico_Horario" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .horario{
            flex-wrap: wrap;
            align-items: stretch;
        }
        .dia{
            border: 1px solid #808080;
            border-radius: 5px;
            justify-content: flex-start;
        }
        .titulo_dia{
            width: 100%;
            text-align: center;
            background-color: var(--color_secundario);
            border-radius: 5px 5px 0 0;
            color: white;
        }
        .rango{
            /*background-color: aqua;*/
            padding: 10px;
            border-bottom: 1px solid #808080;
        }
        .hora_inicio{
            border-bottom: 2px dotted #000000;
        }

    </style>

    <asp:label runat="server" CssClass="titulo LB_GestionarHorario"></asp:label>

    <div>
        <asp:label runat="server" CssClass="subtitulo LB_AgregarRango"></asp:label>

        <div class="flex flex_vertical">
            <div class="campo">
                <asp:label CssClass="LB_Dia" runat="server" >Dia:</asp:label>
                <asp:DropDownList CssClass="DDL" ID="DDL_Dia" runat="server">
                    <asp:ListItem Value="1" Text="Lunes"/>
                    <asp:ListItem Value="2" Text="Martes"/>
                    <asp:ListItem Value="3" Text="Miercoles"/>
                    <asp:ListItem Value="4" Text="Jueves"/>
                    <asp:ListItem Value="5" Text="Viernes"/>
                    <asp:ListItem Value="6" Text="Sabado"/>
                    <asp:ListItem Value="0" Text="Domingo"/>
                </asp:DropDownList>
            </div>
            <div class="campo">
                <asp:label CssClass="LB_HoraInicio" runat="server" ></asp:label>
                <asp:DropDownList 
                    CssClass="DDL" 
                    ID="DDL_HoraInicio" 
                    runat="server" 
                    AutoPostBack="True" 
                    OnSelectedIndexChanged="DDL_HoraInicio_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="campo">
                <asp:label CssClass="LB_HoraFin" runat="server" ></asp:label>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                    <asp:DropDownList CssClass="DDL" ID="DDL_HoraFin" runat="server">
                    </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger 
                            ControlID="DDL_HoraInicio" 
                            EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>

            <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesRango" runat="server"></asp:Panel>

            <asp:Button CssClass="BTN_Comun BTN_Agregar" ID="BTN_Agregar" OnClick="BTN_Agregar_Click" runat="server" />
        </div>
    </div>

    <asp:Panel CssClass="horario flex" ID="P_Horario" runat="server" ViewStateMode="Enabled">

    </asp:Panel>
    <asp:Panel CssClass="flex flex_vertical" ID="P_MensajesHorario" runat="server"></asp:Panel>
    <div class="flex">
        <asp:Button 
            CssClass="BTN_Comun BTN_GuardarHorario" 
            ID="BTN_Guardar" 
            OnClick="BTN_Guardar_Click" runat="server" />
        <asp:Button 
            CssClass="BTN_Secundario BTN_GenerarCitas" 
            ID="BTN_GenerarCitas" 
            Text="Generar mis citas" 
            OnClick="BTN_GenerarCitas_Click"  runat="server" />
        <asp:Button 
            CssClass="BTN_Comun BTN_ReporteHorario" 
            ID="BTN_ReporteHorario"
            Text="Imprimir horario" 
            runat="server" OnClick="BTN_ReporteHorario_Click" />

    </div>

</asp:Content>

