﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Medico/MPMedico.master" AutoEventWireup="true" CodeFile="~/Controller/Medico/VerCita.aspx.cs" Inherits="View_Medico_VerCita" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:label runat="server" class="subtitulo LB_InformacionCita">Informacion de cita</asp:label>

    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
        <ContentTemplate>

    <div class="flex flex_vertical">
        <asp:Label ID="LB_IdCita" Text="" Visible="false" runat="server" />
        <div class="campo">
            <asp:Label CssClass="LB_Fecha" runat="server" />
            <asp:Label ID="LB_Fecha" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_HoraInicio" runat="server" />
            <asp:Label ID="LB_HoraInicio" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_HoraFin" runat="server" />
            <asp:Label ID="LB_HoraFin" Text="Hora_Fin" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_servicio" runat="server" />
            <asp:Label ID="LB_Servicio" Text="Servicio" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Estado" runat="server" />
            <asp:Label ID="LB_Estado" Text="Estado" runat="server" />
        </div>
        <asp:Panel ID="P_CambiarEstado" class="flex flex_vertical" runat="server" Visible="False">
            <div class="campo">
                <asp:label CssClass="LB_CambiarEstado" runat="server" ></asp:label>
                <asp:DropDownList ID="DDL_CambioEstado" runat="server">
                    <asp:ListItem Value="asistio" Text="Asistio" />
                    <asp:ListItem Value="no asistio" Text="No asistio" />
                </asp:DropDownList>
            </div>

            <asp:Panel ID="P_MensajesCita" runat="server"></asp:Panel>

            <asp:Button CssClass="BTN_Comun BTN_Cambiar" ID="BTN_CambiarEstado" OnClick="BTN_CambiarEstado_Click" runat="server" />
        </asp:Panel>
    </div>

            </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BTN_CambiarEstado" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:Panel CssClass="flex flex_vertical" ID="P_InformacionPaciente" runat="server" Visible="False">
        <h1 class="subtitulo LB_InformacionPaciente"></h1>
        <div class="campo">
            <asp:Label CssClass="LB_Identificacion" runat="server" />
            <asp:Label ID="LB_Identificacion" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Nombre" runat="server" />
            <asp:Label ID="LB_Nombre" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Apellido" runat="server" />
            <asp:Label ID="LB_Apellido" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Edad" runat="server" />
            <asp:Label ID="LB_Edad" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Genero" runat="server" />
            <asp:Label ID="LB_Genero" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Telefono" runat="server" />
            <asp:Label ID="LB_Telefono" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Correo" runat="server" />
            <asp:Label ID="LB_Correo" runat="server" />
        </div>

    </asp:Panel>
    

    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            
    <asp:Panel CssClass="flex flex_vertical" ID="P_HistorialCitaActual" runat="server" Visible="false">
        <h1 class="subtitulo LB_HistorialCita"></h1>
        <div class="campo">
            <asp:label CssClass="LB_MotivoConsulta" runat="server" ></asp:label>
            <asp:TextBox class="TB" ID="TB_MotivoConsulta" runat="server" MaxLength="50"/>

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ErrorMessage="El campo esta vacio" 
                ControlToValidate="TB_MotivoConsulta" 
                ValidationGroup="BTN_AgregarHistorial" 
                Display="Dynamic" ForeColor="#FF5050" 
                runat="server" />
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                ControlToValidate="TB_MotivoConsulta" 
                ValidationGroup="BTN_AgregarHistorial"
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>
        <div class="campo">
            <asp:label CssClass="LB_Observaciones" runat="server" ></asp:label>
            <asp:TextBox class="TB" ID="TB_Observaciones" runat="server" TextMode="MultiLine" Rows="5" Width="350px" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ErrorMessage="El campo esta vacio" 
                ControlToValidate="TB_Observaciones" 
                ValidationGroup="BTN_AgregarHistorial" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                runat="server" />
            <asp:RegularExpressionValidator 
                CssClass="RFV_MaximoCaracteres"
                ValidationExpression="^[^~]{0,300}$" 
                ErrorMessage="Máximo 300 caracteres" 
                ControlToValidate="TB_Observaciones" 
                ValidationGroup="BTN_HistorialCita" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                ControlToValidate="TB_Observaciones"
                ValidationGroup="BTN_HistorialCita" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>

        <asp:Panel ID="P_MensajesHistorialCitaActual" runat="server"></asp:Panel>

        <asp:Button CssClass="BTN_Comun BTN_Agregar" ID="BTN_AgregarHistorial" Visible="false" OnClick="BTN_AgregarHistorial_Click" ValidationGroup="BTN_HistorialCita" runat="server" />
        <asp:Button CssClass="BTN_Comun BTN_Modificar" ID="BTN_ModificarHistorial" Visible="false" OnClick="BTN_ModificarHistorial_Click" ValidationGroup="BTN_HistorialCita" runat="server" />
    </asp:Panel>

    <asp:Panel ID="P_HistorialesDeUsuario" Visible="false" runat="server">
        <h1 class="subtitulo LB_HistorialUsuario"></h1>
        <asp:GridView 
            CssClass="GV" 
            ID="GV_HistorialesUsuario" 
            EmptyDataText="No hay información que mostrar"
            EmptyDataRowStyle-CssClass="GV_Vacio"
            GridLines="None" 
            AutoGenerateColumns="False" 
            runat="server">
            <AlternatingRowStyle BackColor="#E2E2E2" />
            <Columns>
                <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="LB_Fecha">
                    <ItemTemplate>
                        <asp:Label ID="LB_Fecha" Text='<%# DateTime.Parse(Eval("ECita.Fecha").ToString()).ToString("dddd - dd/MMMM/yyyy") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Datos" HeaderStyle-CssClass="LB_Datos">
                    <ItemTemplate>
                        <asp:Label CssClass="json" ID="LB_Datos" Text='<%# Eval("Datos") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
            <RowStyle BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
        </asp:GridView>
    </asp:Panel>

        </ContentTemplate>
<%--        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BTN_AccionHistorial" EventName="Click" />
        </Triggers>--%>
    </asp:UpdatePanel>

    <script src="../../js/auditoria.js"></script>
</asp:Content>

