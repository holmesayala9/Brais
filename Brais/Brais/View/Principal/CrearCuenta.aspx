﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/View/Principal/MPPrincipal.master" CodeFile="~/Controller/Principal/CrearCuenta.aspx.cs" Inherits="View_Principal_CrearCuenta" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

    <asp:label runat="server" CssClass="titulo LB_CrearCuenta"></asp:label>

    <div class="d-flex flex-column m-auto" style="max-width: 300px;">
        <div class="form-group">
            <asp:label CssClass="LB_Identificacion" runat="server" ></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_Identificacion" MaxLength="20" runat="server" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Identificacion" 
                runat="server" 
                ControlToValidate="TB_Identificacion" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ID="RFV_CaracteresNoValidosIdentificacion"
                ControlToValidate="TB_Identificacion" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_CaracteresNoValidosIdentificacion"
                runat="server" 
                BehaviorID="VCE_RFV_CaracteresNoValidosIdentificacion" 
                TargetControlID="RFV_CaracteresNoValidosIdentificacion">
			</ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator 
                CssClass="RFV_SoloNumeros"
                ID="RFV_SoloNumerosIdentificacion"
                ControlToValidate="TB_Identificacion" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[\s]*[\d]+[\s]*$" 
                ErrorMessage="Digite solo numeros" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_SoloNumerosIdentificacion"
                runat="server" 
                BehaviorID="VCE_RFV_SoloNumerosIdentificacion" 
                TargetControlID="RFV_SoloNumerosIdentificacion">
			</ajaxToolkit:ValidatorCalloutExtender>
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_Nombre" runat="server" ></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_Nombre" MaxLength="20" runat="server" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Nombre" 
                runat="server" 
                ControlToValidate="TB_Nombre" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_CampoVacioNombre"
                runat="server" 
                BehaviorID="VCE_RFV_CampoVacioNombre" 
                TargetControlID="RFV_Nombre">
			</ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ID="RFV_CaracteresNoValidosNombre"
                ControlToValidate="TB_Nombre" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_CaracteresNoValidosNombre"
                runat="server" 
                BehaviorID="VCE_RFV_CaracteresNoValidosNombre" 
                TargetControlID="RFV_CaracteresNoValidosNombre">
			</ajaxToolkit:ValidatorCalloutExtender>
        </div>
        <div class="form-group">
            <ASP:label CssClass="LB_Apellido" runat="server" ></ASP:label>
            <asp:TextBox CssClass="form-control" ID="TB_Apellido" MaxLength="20" runat="server" ValidationGroup="BTN_Accion" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Apellido" 
                runat="server" 
                ControlToValidate="TB_Apellido" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_CampoVacioApellido"
                runat="server" 
                BehaviorID="VCE_RFV_CampoVacioApellido" 
                TargetControlID="RFV_Apellido">
			</ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ID="RFV_CaracteresNoValidosApellido"
                ControlToValidate="TB_Apellido" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_CaracteresNoValidosApellido"
                runat="server" 
                BehaviorID="VCE_RFV_CaracteresNoValidosApellido" 
                TargetControlID="RFV_CaracteresNoValidosApellido">
			</ajaxToolkit:ValidatorCalloutExtender>
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_FechaNacimiento" runat="server" ></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_FechaNacimiento" runat="server" TextMode="Date"/>

            <asp:RequiredFieldValidator
                CssClass="RFV_NoSeleccionado"
                ID="RFV_FechaNacimiento" 
                runat="server" 
                ControlToValidate="TB_FechaNacimiento" 
                ErrorMessage="No ha seleccionado" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_NoSeleccionadoFechaNacimiento"
                runat="server" 
                BehaviorID="VCE_RFV_NoSeleccionadoFechaNacimiento" 
                TargetControlID="RFV_FechaNacimiento">
			</ajaxToolkit:ValidatorCalloutExtender>
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_Genero" runat="server" ></asp:label>
            <asp:DropDownList CssClass="form-control" ID="DDL_Genero" runat="server">
                <asp:ListItem Text="Ninguno" Value="Ninguno" />
                <asp:ListItem Text="Masculino" Value="Masculino" />
                <asp:ListItem Text="Femenino" Value="Femenino"></asp:ListItem>
                <asp:ListItem Text="Otro" Value="Otro"></asp:ListItem>
            </asp:DropDownList>

            <asp:RequiredFieldValidator 
                CssClass="RFV_NoSeleccionado"
                ID="RFV_Genero" 
                runat="server" 
                ControlToValidate="DDL_Genero" 
                ErrorMessage="No ha seleccionado" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion" 
                InitialValue="Ninguno">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_NoSeleccionadoGenero"
                runat="server" 
                BehaviorID="VCE_RFV_NoSeleccionadoGenero" 
                TargetControlID="RFV_Genero">
			</ajaxToolkit:ValidatorCalloutExtender>
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_Correo" runat="server" ></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_Correo" MaxLength="50" runat="server" TextMode="Email" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Correo" 
                runat="server" 
                ControlToValidate="TB_Correo" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender 
                ID="VCE_RFV_CampoVacioCorreo"
                runat="server" 
                BehaviorID="VCE_RFV_CampoVacioCorreo" 
                TargetControlID="RFV_Correo">
			</ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_Correo" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
            <asp:RegularExpressionValidator 
                CssClass="RFV_Correo"
                ControlToValidate="TB_Correo" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^\s*[a-zA-Z][\w-_+.]{1,20}@[\w]{1,20}([.][a-z]{2,4}){1,2}\s*$" 
                ErrorMessage="Correo no valido, ejemplo: ejemplo@dominio.com" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_Telefono" runat="server" ></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_Telefono" MaxLength="20" runat="server"/>

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Telefono" 
                runat="server" 
                ControlToValidate="TB_Telefono" 
                ErrorMessage="El campo esta vacio"
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_Telefono" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$"
                ErrorMessage="Cacracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_Contrasena" runat="server"></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_Contrasena" MaxLength="20" runat="server" TextMode="Password" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Contrasena" 
                runat="server" 
                ControlToValidate="TB_Contrasena" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                CssClass="RFV_ContrasenaNoCoincide"
                runat="server" 
                ErrorMessage="Las contraseñas no coinciden" 
                ControlToCompare="TB_RepetirContrasena" 
                ControlToValidate="TB_Contrasena" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:CompareValidator>
            <asp:RegularExpressionValidator
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_Contrasena" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>
        <div class="form-group">
            <asp:label CssClass="LB_RepetirContrasena" runat="server" ></asp:label>
            <asp:TextBox CssClass="form-control" ID="TB_RepetirContrasena" MaxLength="20" runat="server" TextMode="Password" />

            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_RepetirContrasena" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>
            
        <asp:Button CssClass="btn btn-success align-self-center BTN_CrearCuenta" ID="BTN_CrearCuenta" runat="server" OnClick="BTN_CrearCuenta_Click" ValidationGroup="BTN_Accion" />

    </div>

</asp:Content>
