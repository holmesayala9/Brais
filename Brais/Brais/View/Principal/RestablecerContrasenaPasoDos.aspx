﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Principal/MPPrincipal.master" AutoEventWireup="true" CodeFile="~/Controller/Principal/RestablecerContrasenaPasoDos.aspx.cs" Inherits="View_Principal_RestablecerContrasenaPasoDos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:label runat="server" class="titulo LB_RestablecerContrasena">Restablecer contraseña</asp:label>

    <div class="d-flex flex-column m-auto max-form">
        <div class="form-group">
            <asp:Label CssClass="LB_NuevaContrasena" Text="Nueva contraseña" runat="server" />
            <asp:TextBox CssClass="form-control" ID="TB_NuevaContrasena" runat="server" MaxLength="20" TextMode="Password" />
            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Contrasena" 
                runat="server" 
                ControlToValidate="TB_NuevaContrasena" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050"
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                CssClass="RFV_ContrasenaNoCoincide"
                runat="server" 
                ErrorMessage="Las contraseñas no coinciden" 
                ControlToCompare="TB_RepetirNuevaContrasena" 
                ControlToValidate="TB_NuevaContrasena" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:CompareValidator>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_NuevaContrasena" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>
        <div class="form-group">
            <asp:Label CssClass="LB_RepetirNuevaContrasena" Text="Repetir nueva contraseña" runat="server" />
            <asp:TextBox CssClass="form-control" ID="TB_RepetirNuevaContrasena" MaxLength="20" runat="server" TextMode="Password" />
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_RepetirNuevaContrasena" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>

        <asp:Panel ID="P_Mensajes" CssClass="flex flex_vertical P_Mensajes" runat="server"></asp:Panel>

        <asp:Button CssClass="btn btn-primary align-self-center BTN_Cambiar" ID="BTN_CambiarContrasena" OnClick="BTN_CambiarContrasena_Click" ValidationGroup="BTN_Accion" Text="Cambiar" runat="server" />
    </div>

</asp:Content>

