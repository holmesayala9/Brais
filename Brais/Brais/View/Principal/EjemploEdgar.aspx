﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EjemploEdgar.aspx.cs" Inherits="View_Principal_EjemploEdgar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Button ID="BTN_Servicio" Text="Servicio" OnClick="BTN_Servicio_Click" runat="server" />

            <asp:GridView ID="GV_Servicio" runat="server"></asp:GridView>

        </div>
    </form>
</body>
</html>
