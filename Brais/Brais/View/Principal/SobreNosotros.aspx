﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Principal/MPPrincipal.master" AutoEventWireup="true" CodeFile="~/Controller/Principal/SobreNosotros.aspx.cs" Inherits="View_Principal_SobreNosotros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        section .contenido{
            height: 400px;
        }
        section h2 {
            text-align: center;
        }
        tr {
            height: 100px;
        }
        .mision {
            width: 100%;
        }
        img {
            width: 100%;
        }
        .img-sobre-nosotros {
            width: 70%;
        }
        .img {
            width: 40%;
        }
        .mision-vision {
            margin: 10px;
            padding: 30px;
            text-align: center;
        }
        .subtitulos {
            width: 10%;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:label runat="server" class="titulo LB_SobreNosotros">Sobre nosotros</asp:label>
    <section class="contenido">


        <table class="mision">
            <tr>
                <td rowspan="2"class="img">
                    <asp:Image runat="server" ImageUrl="~/View/Images/sobreNosotros.png"/>
                </td>
                <td class="subtitulos">
                    <asp:label runat="server" CssClass="LB_Mision">Misión:</asp:label>
                </td>
                <td class="mision-vision">
                    <asp:Label runat="server" CssClass="LB_MensajeMision">
                        Brais es una herramienta a dispocisión de los ciudadanos que quiere brindar la oportunidad a sus pacientes 
                        de sacar citas sin complicaciones.
                    </asp:Label>
                    

                </td> 
            </tr>
            <tr>
                <td class="subtitulos">
                    <asp:Label CssClass="LB_Vision" runat="server">Visión:</asp:Label> 
                </td>
                <td class="mision-vision">
                    <asp:Label runat="server"  CssClass="LB_MensajeVision">
                        Para noviembre de 2018 ser unos estudiantes que pasen el limpio el semestre.
                        Siendo reconocidos a nivel maestral.
                    </asp:Label>
                    
                </td>
            </tr>
        </table>
    </section>

</asp:Content>

