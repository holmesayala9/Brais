﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class View_Principal_EjemploEdgar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void BTN_Servicio_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dataTable = new DataTable();

            wsServicio.WebServiceSoapClient web = new wsServicio.WebServiceSoapClient();

            wsServicio.clsSeguridad obclsSeguridad = new wsServicio.clsSeguridad { stToken=DateTime.Now.ToString("yyyyMMdd") };

            string stToken = web.AutenticacionUsuario(obclsSeguridad);

            if (stToken.Equals("-1"))
            {
                throw new Exception("Token incorrecto");
            }

            obclsSeguridad.AutenticacionToken = stToken;

            string aux = web.traer_categorias_productos(obclsSeguridad);

            dataTable = JsonConvert.DeserializeObject<DataTable>(aux);

            GV_Servicio.DataSource = dataTable;
            GV_Servicio.DataBind();
        }
        catch (Exception)
        {
            throw;
        }

   }

}