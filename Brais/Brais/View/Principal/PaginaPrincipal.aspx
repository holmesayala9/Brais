﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Principal/MPPrincipal.master" AutoEventWireup="true" CodeFile="~/Controller/Principal/PaginaPrincipal.aspx.cs" Inherits="View_Principal_PaginaPrincipal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <section>

        <asp:Image runat="server" Height="307px" ImageUrl="~/View/Images/fondo.png" Width="100%" />

    </section>
    <section>
        <asp:label runat="server" CssClass="titulo LB_PaginaPrincipal">Conoce a nuestros profesionales</asp:label>
        <table>
            <tr>
                <td style="width:60%">
                    <asp:label runat="server" >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vestibulum arcu sem, id viverra urna mollis sit amet. Aenean eleifend vel ipsum ac eleifend. In condimentum sagittis dolor id fermentum. Sed ultricies, ipsum eget ultrices laoreet, velit nisi dictum nibh, sit amet sagittis erat odio sed nisi. Nulla lobortis sem urna. Pellentesque condimentum arcu et dui pellentesque pellentesque. Ut laoreet elit eget erat sollicitudin placerat.
                    </asp:label> 
                </td>
                <td style="width:40%">
                    <asp:Image runat="server" ImageUrl="~/View/Images/profesionales.jpg" Width="100%"/>
                </td>
            </tr>
        </table>
    </section>

</asp:Content>

