﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Principal/MPPrincipal.master" AutoEventWireup="true" CodeFile="~/Controller/Principal/Login.aspx.cs" Inherits="View_Principal_Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <%--<ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />--%>

    <asp:Panel CssClass="d-flex" ID="P_Mensajes" runat="server"></asp:Panel>

    <div class="login">
        <asp:label runat="server" CssClass="titulo LB_Login">Acceder</asp:label>
        
        <div class="d-flex flex-column m-auto max-form">
            <div class="form-group">
                <asp:label CssClass="LB_Identificacion" runat="server" >Identificacion:</asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Identificacion" runat="server" MaxLength="20" TextMode="SingleLine" />

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ID="RFV_CaracteresNoValidosIdentificacion"
                    ControlToValidate="TB_Identificacion" 
                    ValidationGroup="BTN_Ingresar" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server"
                    ForeColor="#FF5050" />

                <ajaxToolkit:ValidatorCalloutExtender 
                    ID="VCE_RFV_CaracteresNoValidosIdentificacion"
                    runat="server" 
                    BehaviorID="VCE_RFV_CaracteresNoValidosIdentificacion" 
                    TargetControlID="RFV_CaracteresNoValidosIdentificacion">
				</ajaxToolkit:ValidatorCalloutExtender>
            </div>
            <div class="form-group">
                <asp:label CssClass="LB_Contrasena" runat="server">Contraseña:</asp:label>
                <asp:TextBox CssClass="form-control" ID="TB_Contrasena" runat="server" TextMode="Password" MaxLength="20"/>

                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ID="RFV_CaracteresNoValidosContrasena"
                    ControlToValidate="TB_Contrasena" 
                    ValidationGroup="BTN_Ingresar" 
                    ErrorMessage="Caracteres no validos" 
                    ValidationExpression="^[^<>]*$"
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />

                <ajaxToolkit:ValidatorCalloutExtender 
                    ID="VCE_RFV_CaracteresNoValidosContrasena"
                    runat="server" 
                    BehaviorID="VCE_RFV_CaracteresNoValidosContrasena" 
                    TargetControlID="RFV_CaracteresNoValidosContrasena">
				</ajaxToolkit:ValidatorCalloutExtender>
            </div>

            <asp:HyperLink  
                CssClass="HL_RecuperarContrasena mb-4"
                runat="server" 
                ForeColor="#0066FF"
                NavigateUrl="~/View/Principal/RestablecerContrasenaPasoUno.aspx">¿Olvido su contraseña?
            </asp:HyperLink>
            
            <asp:Button CssClass="btn btn-primary align-self-center BTN_Ingresar" ID="BTN_Ingresar" ValidationGroup="BTN_Ingresar" Text="Ingresar" runat="server" OnClick="BTN_Ingresar_Click" />
        </div>

    </div>

</asp:Content>
