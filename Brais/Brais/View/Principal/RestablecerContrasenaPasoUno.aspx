﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Principal/MPPrincipal.master" AutoEventWireup="true" CodeFile="~/Controller/Principal/RestablecerContrasenaPasoUno.aspx.cs" Inherits="View_Principal_RestablecerContrasenaPasoUno" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

    <asp:label runat="server" CssClass="titulo LB_RestablecerContrasena">Restablecer contrasena</asp:label>

    <div class="d-flex flex-column m-auto max-form">
        <div class="form-group">
            <asp:Label CssClass="LB_Identificacion" ID="LB_Identificacion" Text="Identificacion" runat="server" />
            <asp:TextBox CssClass="form-control" ID="TB_Identificacion" runat="server" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Identificacion" 
                runat="server" 
                ControlToValidate="TB_Identificacion" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic"
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_Identificacion"
                ValidationGroup="BTN_Accion"
                ValidationExpression="^[^<>]*$"
                ErrorMessage="Caracteres no validos"
                Display="Dynamic"
                runat="server"
                ForeColor="#FF5050" />
        </div>
        <div class="form-group">
            <asp:Label CssClass="LB_Correo" ID="LB_Correo" Text="Correo" runat="server" />
            <asp:TextBox CssClass="form-control" ID="TB_Correo" runat="server" />

            <asp:RequiredFieldValidator 
                CssClass="RFV_CampoVacio"
                ID="RFV_Correo" 
                runat="server" 
                ControlToValidate="TB_Correo" 
                ErrorMessage="El campo esta vacio" 
                Display="Dynamic" 
                ForeColor="#FF5050" 
                ValidationGroup="BTN_Accion">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator 
                CssClass="RFV_CaracteresNoValidos"
                ControlToValidate="TB_Correo" 
                ValidationGroup="BTN_Accion" 
                ValidationExpression="^[^<>]*$" 
                ErrorMessage="Caracteres no validos" 
                Display="Dynamic" 
                runat="server" 
                ForeColor="#FF5050" />
        </div>

        <asp:Button CssClass="btn btn-primary align-self-center BTN_Solicitar" ID="BTN_Solicitar" OnClick="BTN_Solicitar_Click" ValidationGroup="BTN_Accion" Text="Solicitar" runat="server" />
    </div>

</asp:Content>

