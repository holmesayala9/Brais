﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/Historial.aspx.cs" Inherits="View_Usuario_Historial" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:label runat="server" CssClass="titulo LB_HistorialUsuario">Historial</asp:label>

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel ID="P_Mensajes" CssClass="flex flex_vertical P_Mensajes" runat="server"></asp:Panel>

    <div class="flex flex_vertical">
        <div class="flex">
            <asp:TextBox CssClass="TB" ID="TB_Buscar" MaxLength="20" runat="server" />
            <asp:Button CssClass="BTN_Comun BTN_Buscar" ID="BTN_Buscar" Text="Buscar" ValidationGroup="BTN_Buscar" runat="server" />
        </div>
        <asp:RegularExpressionValidator 
            CssClass="RFV_CaracteresNoValidos"
            ControlToValidate="TB_Buscar" 
            ValidationGroup="BTN_Buscar" 
            ValidationExpression="^[^<>]*$" 
            ErrorMessage="Caracteres no validos" 
            Display="Dynamic" 
            runat="server" 
            ForeColor="#FF5050" />
    </div>

    <div class="flex">
        <asp:GridView 
            ID="GV_Historial" 
            EmptyDataText="No hay información que mostrar"
            EmptyDataRowStyle-CssClass="GV_Vacio"
            GridLines="None" 
            AutoGenerateColumns="false" 
            runat="server">
            <AlternatingRowStyle BackColor="#eeeeee" />
            <Columns>
                <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="LB_Fecha">
                    <ItemTemplate> <asp:Label ID="LB_Fecha" Text='<%# DateTime.Parse(Eval("ECita.Fecha").ToString()).ToString("dddd - dd/MMMM/yyyy") %>' runat="server" /> </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hora" HeaderStyle-CssClass="LB_Hora">
                    <ItemTemplate> 
                        <asp:Label ID="LB_HoraInicio" Text='<%# DateTime.Parse(Eval("ECita.HoraInicio").ToString()).ToShortTimeString() %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
               
                <asp:BoundField HeaderStyle-CssClass="LB_servicio" HeaderText="Servicio" DataField="ECita.Servicio" />

                 <asp:TemplateField>
                    <ItemTemplate> <asp:Label CssClass="json" ID="LB_Datos" Text='<%# Eval("Datos") %>' runat="server" /> </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
            <RowStyle BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
        </asp:GridView>
    </div>

    <script src="../../js/auditoria.js"></script>
</asp:Content>

