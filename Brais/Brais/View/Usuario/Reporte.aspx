﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/Reporte.aspx.cs" Inherits="View_Usuario_Reporte" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .contenedor_rv{
            display: inline-block;
        }

        .RV_Clase{

        }
    </style>

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

    <asp:label runat="server" CssClass="titulo LB_Reporte">Reporte</asp:label>

    <div class="flex">
        <CR:CrystalReportViewer ID="CRV_Reporte" runat="server" AutoDataBind="True" EnableDatabaseLogonPrompt="False" Height="50px" ReuseParameterValuesOnRefresh="True" ToolPanelWidth="200px" Width="350px"></CR:CrystalReportViewer>
    </div>

</asp:Content>

