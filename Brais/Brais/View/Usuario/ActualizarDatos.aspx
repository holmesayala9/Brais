﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/ActualizarDatos.aspx.cs" Inherits="View_Usuario_ActualizarDatos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

        <div>
        <asp:label class="subtitulo LB_ActualizarDatosUsuario" runat="server">Mis datos</asp:label>

        <div class="flex flex_vertical">

            <div class="campo">
                <asp:label CssClass="LB_Identificacion" runat="server">Identificacion:</asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Identificacion" runat="server" ControlToValidate="TB_Identificacion" ErrorMessage="Identificacion vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <%--<asp:TextBox CssClass="TB" ID="TB_Identificacion" MaxLength="20" runat="server" TextMode="Number" />--%>
                <asp:Label CssClass="dato" ID="LB_Identificacion" Text="" runat="server" Width="300px"/>            
            </div>
            <div class="campo">
                <asp:label runat="server" CssClass="LB_Nombre">Nombre:</asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TB_Nombre" ErrorMessage="Nombre vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <%--<asp:TextBox CssClass="TB" ID="TB_Nombre" MaxLength="20" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_Nombre" Text="" runat="server" Width="300px"/>
            </div>
            <div class="campo">
                <asp:label runat="server" CssClass="LB_Apellido">Apellido:</asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Apellido" runat="server" ControlToValidate="TB_Apellido" ErrorMessage="Apellido vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <%--<asp:TextBox CssClass="TB" ID="TB_Apellido" MaxLength="20" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_Apellido" Text="" runat="server" Width="300px"/>
            </div>
            <div class="campo">
                <asp:label CssClass="LB_Correo" runat="server">Correo:</asp:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TB_Correo" ErrorMessage="Correo vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <asp:TextBox CssClass="TB" ID="TB_Correo" MaxLength="50" runat="server"/>
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:RegularExpressionValidator 
                    CssClass="RFV_Correo"
                    ControlToValidate="TB_Correo" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^\s*[a-zA-Z][\w-_+.]{1,20}@[\w]{1,20}([.][a-z]{2,4}){1,2}\s*$" 
                    ErrorMessage="Correo no valido, ejemplo: ejemplo@dominio.com" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:Label CssClass="dato" ID="LB_Correo" Text="" runat="server" />
            </div>
            <div class="campo">
                <ASP:label runat="server" CssClass="LB_Telefono" >Telefono:</ASP:label>
                <%--<asp:RequiredFieldValidator ID="RFV_Telefono" runat="server" ControlToValidate="TB_Telefono" ErrorMessage="Telefono vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                <asp:TextBox CssClass="TB" ID="TB_Telefono" MaxLength="20" runat="server" />
                <asp:RegularExpressionValidator 
                    CssClass="RFV_CaracteresNoValidos"
                    ControlToValidate="TB_Telefono" 
                    ValidationGroup="BTN_Accion" 
                    ValidationExpression="^[^<>]*$" 
                    ErrorMessage="Caracteres no validos" 
                    Display="Dynamic" 
                    runat="server" 
                    ForeColor="#FF5050" />
                <asp:Label CssClass="dato" ID="LB_Telefono" Text="" runat="server" />
            </div>
            <div class="campo">
                <asp:label CssClass="LB_FechaNacimiento" runat="server">Fecha de nacimiento:</asp:label>
                <%--<asp:TextBox CssClass="TB" ID="TB_FechaNacimiento" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_FechaNacimiento" Text="" runat="server" Width="300px"/>
            </div>
            <div class="campo">
                <asp:label CssClass="LB_Genero" runat="server">Genero:</asp:label>
                <%--<asp:TextBox CssClass="TB" ID="TB_Genero" runat="server" />--%>
                <asp:Label CssClass="dato" ID="LB_Genero" Text="" runat="server" Width="300px"/>
            </div>
            <div class="campo">
                <div> 
                    <asp:CheckBox 
                        ID="CB_ModificarContrasena"
                        AutoPostBack="true"  
                        runat="server" 
                        OnCheckedChanged="CB_ModificarContrasena_CheckedChanged" />
                    <asp:Label 
                        CssClass = "CB_ModificarContrasena"
                        Text="Modificar contraseña" 
                        runat="server" />
                </div>
            </div>

            <asp:Panel ID="P_ModificarContrasena" Visible="false" runat="server">
            
                <div class="campo">
                    <asp:label CssClass="LB_Contrasena"  runat="server">Contraseña:</asp:label>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TB_Contrasena" ErrorMessage="Contraseña vacio" Display="Dynamic" ForeColor="#FF5050" ValidationGroup="BTN_Accion"></asp:RequiredFieldValidator>--%>
                    <asp:TextBox CssClass="TB" ID="TB_Contrasena" MaxLength="20" runat="server" TextMode="Password" />
                    <asp:RegularExpressionValidator 
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_Contrasena" 
                        ValidationGroup="BTN_Accion" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>
                <div class="campo">
                    <asp:label runat="server" CssClass="LB_NuevaContrasena" >Nueva contraseña:</asp:label>
                    <asp:RequiredFieldValidator 
                        CssClass="RFV_CampoVacio"
                        ID="RFV_Contrasena" 
                        runat="server" 
                        ControlToValidate="TB_NuevaContrasena" 
                        ErrorMessage="El campo esta vacio" 
                        Display="Dynamic" 
                        ForeColor="#FF5050" 
                        ValidationGroup="BTN_Accion">
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator 
                        CssClass="RFV_ContrasenaNoCoincide"
                        runat="server" 
                        ErrorMessage="Las contraseñas no coinciden" 
                        ControlToCompare="TB_RepetirNuevaContrasena" 
                        ControlToValidate="TB_NuevaContrasena" 
                        Display="Dynamic" ForeColor="#FF5050" 
                        ValidationGroup="BTN_Accion">
                    </asp:CompareValidator>
                    <asp:TextBox CssClass="TB" ID="TB_NuevaContrasena" MaxLength="20" runat="server" TextMode="Password" />
                    <asp:RegularExpressionValidator 
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_NuevaContrasena" 
                        ValidationGroup="BTN_Accion" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                    <br />
                    <asp:Label ID="LB_status2" runat="server"></asp:Label>
                </div>
                <div class="campo">
                    <asp:label CssClass="LB_RepetirNuevaContrasena" runat="server" >Repetir nueva contraseña:</asp:label>
                    <asp:TextBox CssClass="TB" ID="TB_RepetirNuevaContrasena" MaxLength="20" runat="server" TextMode="Password" />
                    <asp:RegularExpressionValidator 
                        CssClass="RFV_CaracteresNoValidos"
                        ControlToValidate="TB_RepetirNuevaContrasena" 
                        ValidationGroup="BTN_Accion" 
                        ValidationExpression="^[^<>]*$" 
                        ErrorMessage="Caracteres no validos" 
                        Display="Dynamic" 
                        runat="server" 
                        ForeColor="#FF5050" />
                </div>

            </asp:Panel>

            <asp:Panel ID="P_Mensajes" CssClass="flex flex_vertical P_Mensajes" runat="server"></asp:Panel>

            <asp:Button CssClass="BTN_Comun BTN_Modificar" ID="BTN_Modificar" Text="Modificar" runat="server" OnClick="BTN_Modificar_Click" ValidationGroup="BTN_Accion" />

        </div>

    </div>

</asp:Content>

