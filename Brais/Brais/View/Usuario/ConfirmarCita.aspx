﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/ConfirmarCita.aspx.cs" Inherits="View_Usuario_ConfirmarCita" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <%--<ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />--%>

    <asp:Panel ID="P_Mensajes" CssClass="flex flex_vertical P_Mensajes" runat="server"></asp:Panel>

    <asp:label runat="server" CssClass="titulo LB_ConfirmarCita">Confirmar cita</asp:label>

    <asp:Label ID="LB_IdCita" Text="" Visible="false" runat="server" />

    <div class="flex flex_vertical">
        <div class="campo">
            <asp:label CssClass="LB_HoraInicio" runat="server">Hora Inicio: </asp:label>
            <asp:Label ID="LB_HoraInicio" Text="" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_HoraFin" runat="server" />
            <asp:Label ID="LB_HoraFin" Text="" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Fecha" Text="Fecha: " runat="server" />
            <asp:Label ID="LB_Fecha" Text="" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Servicio" runat="server" />
            <asp:Label ID="LB_Servicio" Text="" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Medico" runat="server" />
            <asp:Label ID="LB_Medico" Text="" runat="server" />
        </div>
        <div class="campo">
            <asp:Label CssClass="LB_Estado" runat="server" />
            <asp:Label ID="LB_Estado" Text="" runat="server" />
        </div>

        <asp:Button CssClass="BTN_Comun BTN_Reservar" ID="BTN_Reservar" OnClick="BTN_Reservar_Click" Text="Confirmar" runat="server" />
    </div>


</asp:Content>

