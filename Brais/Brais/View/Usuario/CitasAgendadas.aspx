﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/CitasAgendadas.aspx.cs" Inherits="View_Usuario_CitasAgendadas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:label class="titulo LB_CitasAgendadas" runat="server" >Mis citas</asp:label>
    
    <%--<ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />--%>

    <asp:Panel CssClass="flex" ID="P_Mensajes" runat="server">

    </asp:Panel>

    <asp:GridView 
        CssClass="GV" 
        ID= GV_Citas 
        AutoGenerateColumns="false" 
        GridLines="None" 
        EmptyDataText="No hay información que mostrar"
        EmptyDataRowStyle-CssClass="GV_Vacio"
        runat=server>
        <AlternatingRowStyle BackColor="#E2E2E2" />
        <Columns>
            <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="LB_Fecha">
                <ItemTemplate>
                    <asp:Label Text='<%# DateTime.Parse(Eval("Fecha").ToString()).ToString("dddd - dd/MMMM/yyyy") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Hora inicio" HeaderStyle-CssClass="LB_HoraInicio">
                <ItemTemplate>
                    <asp:Label Text='<%# DateTime.Parse(Eval("HoraInicio").ToString()).ToShortTimeString() %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Hora fin" HeaderStyle-CssClass="LB_HoraFin">
                <ItemTemplate>
                    <asp:Label Text='<%# DateTime.Parse(Eval("HoraFin").ToString()).ToShortTimeString() %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField HeaderStyle-CssClass="LB_servicio" HeaderText="Servicio" DataField="Servicio" />
            <asp:BoundField HeaderStyle-CssClass="LB_Estado" HeaderText="Estado" DataField="Estado" />
            <asp:BoundField HeaderStyle-CssClass="LB_Consultorio" HeaderText="Consultorio" DataField="EMedico.EConsultorio.Nombre" NullDisplayText="Ninguno"/>

            <asp:TemplateField HeaderText="Medico" HeaderStyle-CssClass="LB_Medico">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("EMedico.Nombre").ToString() + " " + Eval("EMedico.Apellido").ToString() %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button CssClass="BTN_Azul BTN_Reprogramar" ID="BTN_Reprogramar" CommandName='<%#Eval("Id")%>' OnClick="BTN_Reprogramar_Click" Text="Reprogramar" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button CssClass="BTN_Rojo BTN_Cancelar" ID="BTN_Cancelar" CommandName='<%#Eval("Id")%>' OnClick="BTN_Cancelar_Click" Text="Cancelar" runat="server" />
                    <ajaxToolkit:ConfirmButtonExtender 
                        ID="BTN_EliminarMedico" 
                        runat="server" 
                        ConfirmText="¿Esta seguro que desea cancelar la cita?" 
                        TargetControlID="BTN_Cancelar" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
        <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
    </asp:GridView>

</asp:Content>

