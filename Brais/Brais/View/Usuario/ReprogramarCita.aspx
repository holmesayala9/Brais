﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/ReprogramarCita.aspx.cs" Inherits="View_Usuario_ReprogramarCita" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>


    <asp:label runat="server" class="titulo LB_ReprogramarCita">Reprogramar cita</asp:label>

    <div class="flex flex_vertical">

        <asp:label runat="server" CssClass="subtitulo LB_CitaActual">Cita actual</asp:label>
        
        <asp:Label ID="LB_Id" Text="" Visible="false" runat="server" />

        <div class="campo"> 
            <asp:Label CssClass="LB_Fecha" Text="Fecha" runat="server" /> 
            <asp:Label ID="LB_Fecha" Text="" runat="server" /> 
        </div>

        <div class="campo"> <asp:Label ID="LB_HoraInicio" Text="" runat="server" /> </div>

        <div class="campo"> <asp:Label ID="LB_HoraFin" Text="" runat="server" /> </div>

        <div class="campo"> <asp:Label ID="LB_Servicio" Text="" runat="server" /> </div>

        <div class="campo"> <asp:Label ID="LB_Medico" Text="" runat="server" /> </div>
    </div>

    <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

    <asp:Panel class="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

    <div>
        <h1 class="subtitulo LB_Remplazo">Seleccionar reemplazo</h1>

        <div class="flex">
            <asp:Calendar ID="C_FechasDisponibles" runat="server" OnDayRender="C_FechasDisponibles_DayRender" OnSelectionChanged="C_FechasDisponibles_SelectionChanged" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="15pt" ForeColor="#003399" Height="300px" Width="320px">
                <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                <WeekendDayStyle BackColor="#CCCCFF" />
            </asp:Calendar>
        </div>

        <div class="flex">
            <asp:GridView 
                ID="GV_Horarios" 
                AutoGenerateColumns="false" 
                GridLines="None" 
                EmptyDataText="No hay información que mostrar"
                EmptyDataRowStyle-CssClass="GV_Vacio"
                runat="server">
                <AlternatingRowStyle BackColor="#E2E2E2" />
                <Columns>
                    <asp:TemplateField HeaderText="Hora inicio" HeaderStyle-CssClass="LB_HoraInicio">
                        <ItemTemplate>
                            <asp:Label Text='<%# DateTime.Parse(Eval("HoraInicio").ToString()).ToShortTimeString() %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Hora fin" HeaderStyle-CssClass="LB_HoraFin">
                        <ItemTemplate>
                            <asp:Label Text='<%# DateTime.Parse(Eval("HoraFin").ToString()).ToShortTimeString() %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Medico" HeaderStyle-CssClass="LB_Medico">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("EMedico.Nombre").ToString() + " " + Eval("EMedico.Apellido").ToString() %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button CssClass="BTN_Azul BTN_Reprogramar" ID="BTN_Reemplazar" OnClick="BTN_Reemplazar_Click" CommandName='<%# Eval("Id") %>' Text="Reemplazar" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
            </asp:GridView>
        </div>

    </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

