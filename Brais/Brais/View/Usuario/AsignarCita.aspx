﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Usuario/MPUsuario.master" AutoEventWireup="true" CodeFile="~/Controller/Usuario/AsignarCita.aspx.cs" Inherits="View_Usuario_AsignarCita" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .img{
            width: 30px;
        }

        .paso{
            display: flex;
            align-items: center;
            justify-content: flex-start;
            width: 400px;
            padding: 10px;
        }
        .paso label{
            margin: 0;
            margin-left: 10px;
        }
    </style>

    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
        <ContentTemplate>

        <ajaxToolkit:AlwaysVisibleControlExtender ID="AVC" runat="server" HorizontalSide="Right" VerticalSide="Bottom" TargetControlID="P_Mensajes" />

        <asp:Panel CssClass="flex flex_vertical P_Mensajes" ID="P_Mensajes" runat="server"></asp:Panel>

        <asp:label runat="server" class="titulo LB_AsignarCita">Asignar cita</asp:label>

        <div class="flex_vertical">
            <div class="paso">
                <asp:Image class="img" runat="server" ImageUrl="~/Img/uno.svg"></asp:Image>
                <asp:label CssClass="LB_PasoUno" runat="server">Seleccione el tipo de servicio</asp:label>
            </div>
            <div class="paso">
                <asp:Image class="img" runat="server" ImageUrl="~/Img/dos.svg"></asp:Image>
                <asp:label CssClass="LB_PasoDos" runat="server">Seleccione un día disponible marcado con: </asp:label>
                <asp:Image class="img" runat="server" ImageUrl="~/Img/dia_seleccionable.svg"></asp:Image>
            </div>
            <div class="paso">
                <asp:Image class="img" runat="server" ImageUrl="~/Img/tres.svg"></asp:Image>
                <asp:label CssClass="LB_PasoTres" runat="server">Seleccione una cita segun la disponibilidad</asp:label>
            </div>
            <div class="paso">
                <asp:Image class="img" runat="server" ImageUrl="~/Img/informacion.svg"></asp:Image>
                <asp:label runat="server" CssClass="LB_pasoCuatro">Tenga en cuenta: <br /> 
                    - Solo puede agendar de a una cita por tipo
                </asp:label>
            </div>
        </div>

        <div class="flex flex_vertical">
            <div class="campo">
                <asp:label CssClass="LB_servicio" runat="server">Servicio:</asp:label>
                <asp:DropDownList ID="DDL_Servicio" runat="server" AutoPostBack="True" DataTextField="Nombre" DataValueField="Id">
                </asp:DropDownList>
            </div>
            <div class="campo" style="height:350px">
                <ajaxToolkit:DragPanelExtender ID="DPE_Calendario" TargetControlID="C_Fechas" ViewStateMode="Enabled"  runat="server" />
                <asp:Calendar ID="C_Fechas" runat="server" OnDayRender="C_Fechas_DayRender" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="15pt" ForeColor="#003399" Height="300px" Width="320px">
                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                    <OtherMonthDayStyle ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                    <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                    <WeekendDayStyle BackColor="#CCCCFF" />
                </asp:Calendar>
            </div>

            <div class="flex flex_vertical">
                <asp:GridView 
                    runat="server" 
                    ID="GV_Horarios" 
                    GridLines="None"  
                    EmptyDataText="No hay información que mostrar"
                    EmptyDataRowStyle-CssClass="GV_Vacio"
                    AutoGenerateColumns="false" 
                    AllowPaging="True"
                    OnPageIndexChanging="GV_Horarios_PageIndexChanging">
                    <AlternatingRowStyle BackColor="#E2E2E2" />
                    <Columns>
                        <asp:TemplateField HeaderText="Hora inicio" HeaderStyle-CssClass="LB_HoraInicio">
                            <ItemTemplate>
                                <asp:Label Text='<%# DateTime.Parse(Eval("HoraInicio").ToString()).ToShortTimeString() %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Hora fin" HeaderStyle-CssClass="LB_HoraFin">
                            <ItemTemplate>
                                <asp:Label Text='<%# DateTime.Parse(Eval("HoraFin").ToString()).ToShortTimeString() %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Medico" HeaderStyle-CssClass="LB_Medico">
                            <ItemTemplate>
                                <asp:Label Text='<%# Eval("EMedico.Nombre").ToString() + " " + Eval("EMedico.Apellido").ToString() %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button CssClass="BTN_Comun BTN_Reservar" ID="BTN_Reservar" OnClick="BTN_Reservar_Click" CommandName='<%# Eval("Id") %>' Text="Reservar" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" BorderStyle="None" BackColor="#006FDD" ForeColor="White" />
                    <RowStyle HorizontalAlign="Center" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" BackColor="White" />
                </asp:GridView>
            </div>
        </div>    

            </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

