﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class MisDatosController : Controller
    {
        // GET: Usuario/MisDatos
        public ActionResult MisDatos()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getUsuario(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EUsuario eUsuario = (EUsuario)LUsuario.obtenerUsuario(eAcceso.IdUsuario);

                return Json(Respuesta.newRespuestaCorrecto("", eUsuario));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarDatosBasicos(string Correo, string Telefono, Int64 IdUsuario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);
                Respuesta respuesta = LUsuario.modificarDatosBasicos(Correo, Telefono, eAcceso.IdUsuario, IdAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarContrasena(string Contrasena, string NuevaContrasena, Int64 IdUsuario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LUsuario.modificarContrasena(eAcceso.IdUsuario, Contrasena, NuevaContrasena, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}