﻿using Logica.Clases;
using Logica.Clases.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class HistorialController : Controller
    {
        // GET: Usuario/Historial
        public ActionResult Historial()
        {
            return View();
        }

        [HttpPost]
        public JsonResult mostrarHistorial(int IdAcceso, string Parametro)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);
                Respuesta respuesta = LHistorial.buscarByIdUsuarioAndParametro(eAcceso.IdUsuario, Parametro);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}