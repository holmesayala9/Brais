﻿using Logica.Clases;
using Logica.Clases.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class ConfirmarCitaController : Controller
    {
        // GET: Usuario/ConfirmarCita
        public ActionResult ConfirmarCita()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getCita(int IdCita)
        {
            try
            {
                Respuesta respuesta = LCita.get(IdCita);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        public JsonResult ReservarCita(int IdCita, EAcceso Acceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(Acceso.Id);
                Respuesta respuesta = LCita.reservarCita(IdCita, eAcceso.IdUsuario, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}