﻿using Logica.Clases;
using Logica.Clases.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class ReprogramarCitaController : Controller
    {
        // GET: Usuario/ReprogramarCita
        public ActionResult ReprogramarCita()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getCitasDisponibles(string Fecha, EAcceso Acceso, int IdCitaOld)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(Acceso.Id);
                ECita eCita = (ECita)LCita.get(IdCitaOld).Data; 
                Respuesta respuesta = LCita.getCitasDisponiblesParaUsuario(eAcceso.IdUsuario, eCita.EMedico.EEspecialidad.Id, Fecha);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult reprogramarCitaFinal(EAcceso Acceso, int IdCitaNew, int IdCitaOld)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(Acceso.Id);
                Respuesta respuesta = LCita.reprogramarCita(IdCitaNew, IdCitaOld, eAcceso.IdUsuario, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}