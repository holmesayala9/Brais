﻿using Logica.Clases;
using Logica.Clases.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class CitasAgendadasController : Controller
    {
        // GET: Usuario/CitasAgendadas
        public ActionResult CitasAgendadas()
        {
            return View();
        }

        [HttpPost]
        public JsonResult cargarCitas(EAcceso Acceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(Acceso.Id);
                Respuesta respuesta = LCita.getCitasVigentesDeUsuario(eAcceso.IdUsuario);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult validarCita(int IdCita)
        {
            try
            {
                Respuesta respuesta = LCita.verificarReprogramacionDeCita(IdCita);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult eliminarCita(int IdCita, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);
                Respuesta respuesta = LCita.cancelarCita(IdCita, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}