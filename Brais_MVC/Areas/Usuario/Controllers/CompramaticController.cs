﻿using Logica.Clases;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class CompramaticController : Controller
    {
        CompramaticServicioProductos.clsSeguridad autenticacionProductos = new CompramaticServicioProductos.clsSeguridad { nomEmp = "Brais", stToken = "61f69fd26f27beaccb9f009ca51dac07" };
        CompramaticServicioProductos.WebServiceSoapClient compramaticServicioProductos = new CompramaticServicioProductos.WebServiceSoapClient();

        CompramaticServicioComentarios.clsSeguridad autenticacionComentario = new CompramaticServicioComentarios.clsSeguridad { nomEmp = "Brais", stToken = "61f69fd26f27beaccb9f009ca51dac07" };
        CompramaticServicioComentarios.ServicioComentariosSoapClient compramaticServicioComentarios = new CompramaticServicioComentarios.ServicioComentariosSoapClient();

        public ActionResult Compramatic()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getCategorias()
        {
            try
            {
                autenticarProductos();
                string categorias = compramaticServicioProductos.traer_categorias_productos(autenticacionProductos);
                return Json(Respuesta.newRespuestaCorrecto("", categorias));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getProductosPorCategoria(int IdCategoria)
        {
            try
            {
                autenticarProductos();
                string productos = compramaticServicioProductos.traer_por_categoria(autenticacionProductos, IdCategoria);
                return Json(Respuesta.newRespuestaCorrecto("", productos));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult enviarComentario(int IdAcceso, int IdEmpresa, string Producto, string Mensaje)
        {
            try
            {
                autenticarComentarios();

                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EUsuario eUsuario = LUsuario.obtenerUsuario(eAcceso.IdUsuario);

                string resultado = compramaticServicioComentarios.InsertarDudasEmpresa(autenticacionComentario, Mensaje, IdEmpresa, eUsuario.Nombre + " " + eUsuario.Apellido, eUsuario.Correo);

                if (resultado.Contains("Ha Ocurrido Un Error Inesperado"))
                    throw new Exception("No se pudo enviar");

                return Json(Respuesta.newRespuestaCorrecto("Correcto"));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        protected void autenticarProductos()
        {
            string tokenApp = compramaticServicioProductos.AutenticacionUsuario(autenticacionProductos);

            if (tokenApp.Equals("-1")) throw new Exception("Error de autenticacion");

            autenticacionProductos.AutenticacionToken = tokenApp;
        }

        protected void autenticarComentarios()
        {
            string tokenApp = compramaticServicioComentarios.AutenticacionUsuario(autenticacionComentario);

            if (tokenApp.Equals("-1")) throw new Exception("Error de autenticacion");

            autenticacionComentario.AutenticacionToken = tokenApp;
        }
    }
}