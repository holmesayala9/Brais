﻿using Brais_MVC.App_Code;
using Logica.Clases;
using Logica.Clases.Medico;
using Logica.Clases.Usuario;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Usuario.Controllers
{
    public class AsignarCitaController : BaseController
    {

        public ActionResult AsignarCita()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getDDLServicio()
        {
            try
            {
                return Json(Respuesta.newRespuestaCorrecto( "", PartialViewToString(PartialView("_DDLServicio", LEspecialidad.getAll()))));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getAsignarCita(int IdAcceso, string Fecha, int IdEspecialidad)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);
                Respuesta respuesta = LCita.getCitasDisponiblesParaUsuario(eAcceso.IdUsuario, IdEspecialidad, Fecha);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        public JsonResult validarCita(int idCita)
        {
            try
            {
                Respuesta respuesta = LCita.verificarCita(idCita);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}