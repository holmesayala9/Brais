﻿using Brais_MVC.App_Code;
using Logica.Clases;
using Logica.Clases.Medico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class ConsultoriosController : BaseController
    {
        // GET: Administrador/Consultorios
        public ActionResult Consultorios()
        {
            return View();
        }

        [HttpPost]
        public JsonResult buscar(string Parametro)
        {
            try
            {
                List<EConsultorio> list = LConsultorio.buscar(Parametro);

                return Json(Respuesta.newRespuestaCorrecto("", list));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getConsultorios()
        {
            try
            {
                List<EConsultorio> list = LConsultorio.getAll();

                return Json(Respuesta.newRespuestaCorrecto("", list));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getConsultoriosDisponibles()
        {
            try
            {
                Respuesta respuesta = LConsultorio.getDisponibles();

                return Json(Respuesta.newRespuestaCorrecto("", respuesta.Data));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult addConsultorio(EConsultorio eConsultorio, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LConsultorio.add(eConsultorio, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateConsultorio(EConsultorio eConsultorio, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LConsultorio.update(eConsultorio, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getConsultorio(int IdConsultorio)
        {
            try
            {
                Respuesta respuesta = LConsultorio.get(IdConsultorio);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }


        public JsonResult deleteConsultorio(int IdConsultorio, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LConsultorio.delete(IdConsultorio, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

    }
}