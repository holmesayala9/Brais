﻿using Logica.Clases;
using Logica.Clases.Administrador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Idioma;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class IdiomasController : Controller
    {
        public ActionResult Idiomas()
        {
            return View();
        }


        [HttpPost]
        public JsonResult getComponentesPorIdioma(int idIdioma)
        {
            try
            {
                return Json(Respuesta.newRespuestaCorrecto("", LIdioma.getComponentesPorIdioma(idIdioma)));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }


        [HttpPost]
        public JsonResult getIdiomas()
        {
            try
            {
                List<EIdioma> list = LIdioma.getIdioma();

                return Json(Respuesta.newRespuestaCorrecto("", list));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getIdioma(int Id)
        {
            try
            {
                EIdioma eIdioma = LIdioma.getIdioma(Id);

                return Json(Respuesta.newRespuestaCorrecto("", eIdioma));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult agregarIdioma(EIdioma eIdioma, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LIdioma.agregarIdioma(eIdioma, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarIdioma(EIdioma eIdioma, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LIdioma.update(eIdioma, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult eliminarIdioma(int Id, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LIdioma.remove(Id, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }


        [HttpPost]
        public JsonResult getComponentes()
        {
            try
            {
                Respuesta respuesta = LComponente.getAll();

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getComponente(int Id)
        {
            try
            {
                Respuesta respuesta = LComponente.get(Id);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult agregarComponente(EComponente eComponente, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LComponente.add(eComponente, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarComponente(EComponente eComponente, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LComponente.update(eComponente, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult eliminarComponente(int Id, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LComponente.delete(Id, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult buscarComponente(string Parametro)
        {
            try
            {
                Respuesta respuesta = LComponente.buscarComponente(Parametro);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getIdiomaComponentes()
        {
            try
            {
                Respuesta respuesta = LComponente.getAllIdiomaComponente();

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult buscarIdiomaComponentes(string Parametro)
        {
            try
            {
                Respuesta respuesta = LComponente.buscarIdiomaComponente(Parametro);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult agregarIdiomaComponente(int IdIdioma, string NombreComponente, string Texto, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LComponente.addIdiomaToComponente(IdIdioma, NombreComponente, Texto, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult eliminarIdiomaComponente(int Id, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LComponente.deleteIdiomaComponente(Id, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}