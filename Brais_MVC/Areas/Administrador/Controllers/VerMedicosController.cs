﻿using Brais_MVC.App_Code;
using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Newtonsoft.Json.Linq;
using Logica.Clases.Medico;
using Newtonsoft.Json;
using Utilitaria.Clases.Medico;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class VerMedicosController : BaseController
    {
        public ActionResult VerMedicos()
        {
            return View();
        }

        [HttpPost]
        public JsonResult buscarMedico(string Parametro)
        {
            try
            {
                Respuesta respuesta = LMedico.buscarMedico(Parametro);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getMedico(Int64 IdMedico)
        {
            try
            {
                Respuesta respuesta = LMedico.get(IdMedico);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult agregarMedico(EMedico eMedico, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.add(eMedico, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarDatosBasicos(EMedico eMedico, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.modificarDatosBasicos(eMedico, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
        
        [HttpPost]
        public JsonResult cambiarEspecialidad(Int64 IdMedico, int IdEspecialidad, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.cambiarEspecialidad(IdMedico, IdEspecialidad, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult cambiarConsultorio(Int64 IdMedico, int IdConsultorio, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.cambiarConsultorio(IdMedico, IdConsultorio, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult activarMedico(Int64 IdMedico, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.activarMedico(IdMedico, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult desactivarMedico(Int64 IdMedico, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.desactivarMedico(IdMedico, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult cambiarContrasena(Int64 IdMedico, string Contrasena, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EMedico eMedico = (EMedico)LMedico.get(IdMedico).Data;

                Respuesta respuesta = LMedico.modificarContrasena(IdMedico, eMedico.Contrasena, Contrasena, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}