﻿using Brais_MVC.App_Code;
using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class VerUsuariosController : BaseController
    {
        // GET: Administrador/VerUsuarios
        public ActionResult VerUsuarios()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getUsuario(Int64 IdUsuario)
        {
            try
            {
                EUsuario eUsuario = LUsuario.obtenerUsuario(IdUsuario);

                return Json(Respuesta.newRespuestaCorrecto("", eUsuario));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult buscarUsuario(string Parametro)
        {
            try
            {
                Respuesta respuesta = LUsuario.buscarUsuario(Parametro);

                return Json(Respuesta.newRespuestaCorrecto("", respuesta.Data));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult agregarUsuario(EUsuario eUsuario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LUsuario.agregarUsuario(eUsuario, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarDatosBasicos(EUsuario eUsuario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                return Json(LUsuario.modificarDatosBasicos(eUsuario, eAcceso));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult activarUsuario(Int64 IdUsuario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LUsuario.activarUsuario(IdUsuario, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult desactivarUsuario(Int64 IdUsuario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LUsuario.desactivarUsuario(IdUsuario, eAcceso);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult cambiarContrasena(Int64 IdUsuario, string Contrasena, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EUsuario eUsuario = LUsuario.obtenerUsuario(IdUsuario);

                return Json(LUsuario.modificarContrasena(IdUsuario, eUsuario.Contrasena, Contrasena, eAcceso));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}