﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class MasterPageController : Controller
    {
        // GET: Administrador/MasterPage
        public ActionResult MasterPage()
        {
            return View();
        }
    }
}