﻿using Brais_MVC.App_Code;
using Logica.Clases;
using Logica.Clases.Medico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class EspecialidadesController : BaseController
    {
        // GET: Administrador/Especialidades
        public ActionResult Especialidades()
        {
            return View();
        }



        [HttpPost]
        public JsonResult addEspecialidad(EEspecialidad eEspecialidad, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LEspecialidad.add(eEspecialidad, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateEspecialidad(EEspecialidad eEspecialidad, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LEspecialidad.update(eEspecialidad, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult deleteEspecialidad(int IdEspecialidad, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LEspecialidad.delete(IdEspecialidad, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getEspecialidad(int IdEspecialidad)
        {
            try
            {
                Respuesta respuesta = LEspecialidad.get(IdEspecialidad);

                return Json(Respuesta.newRespuestaCorrecto("", respuesta.Data));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getEspecialidades()
        {
            try
            {
                List<EEspecialidad> list = LEspecialidad.getAll();

                return Json(Respuesta.newRespuestaCorrecto("", list));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult buscar(string Parametro)
        {
            try
            {
                List<EEspecialidad> list = LEspecialidad.buscar(Parametro);

                return Json(Respuesta.newRespuestaCorrecto("", list));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

    }
}
