﻿using Logica.Clases;
using Logica.Clases.Administrador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Administrador.Controllers
{
    public class ParametrosController : Controller
    {
        // GET: Administrador/Parametros
        public ActionResult Parametros()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getParametros()
        {
            try
            {
                Parametro parametro = LParametro.get();
                return Json(Respuesta.newRespuestaCorrecto("", parametro));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateSesionesSimultaneas(int Sesiones, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LParametro.updateSesionesSimultaneas(Sesiones, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateIntentosAcceso(int IntentosAcceso, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LParametro.updateIntentosIngreso(IntentosAcceso, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateDiasLaborales(List<int> DiasLaborales, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LParametro.updateDiasLaborales(DiasLaborales, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateHorario(string HoraInicio, string HoraFin, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LParametro.updateHorarioDeTrabajo(HoraInicio, HoraFin, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateDuracionCitas(int Duracion, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LParametro.updateDuracionCitas(Duracion.ToString(), eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult generarCitas(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LAdministrador.generarCitas(eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}