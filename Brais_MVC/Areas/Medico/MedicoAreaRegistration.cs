﻿using System.Web.Mvc;

namespace Brais_MVC.Areas.Medico
{
    public class MedicoAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Medico";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Medico_default",
                "Medico/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Brais_MVC.Areas.Medico.Controllers" }
            );
        }
    }
}