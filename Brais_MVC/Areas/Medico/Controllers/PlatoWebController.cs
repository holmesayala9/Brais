﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Servicios;

namespace Brais_MVC.Areas.Medico.Controllers
{
    public class PlatoWebController : Controller
    {
        // GET: Medico/PlatoWeb
        public ActionResult PlatoWeb()
        {
            return View();
        }

        [HttpPost]
        public JsonResult traerMenu()
        {
            //ReservaPlato reservaPlato = new ReservaPlato();
            //String menu = reservaPlato.traerMenu();

            try
            {
                ServiciosPlatoWeb.ServiciosSoapClient servicio = new ServiciosPlatoWeb.ServiciosSoapClient();

                ServiciosPlatoWeb.Seguridad obSeguridad = new ServiciosPlatoWeb.Seguridad()
                {
                    stToken = DateTime.Now.ToString("yyyyMMdd")
                };

                String StToken = servicio.AutenticationUsuario(obSeguridad);
                if (StToken.Equals("-1")) throw new Exception("Requiere Validacion");

                obSeguridad.AutenticationToken = StToken;

                String menu = servicio.LMenu(obSeguridad);

                JArray menuJArray = JArray.Parse(menu);

                List<PlatoWeb> platos = new List<PlatoWeb>();

                foreach (var item in menuJArray.Children())
                {
                    PlatoWeb platoWeb = new PlatoWeb();

                    platoWeb.Id = int.Parse(item["id"].ToString());
                    platoWeb.Nombre = item["nombre"].ToString();
                    platoWeb.Descripcion = item["descripcion"].ToString();
                    platoWeb.Precio = item["precio"].ToString();
                    platoWeb.Imagen = item["imagen"].ToString();

                    platos.Add(platoWeb);
                }

                return Json(Respuesta.newRespuestaCorrecto("", platos));

            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult reservarPlato(String Fecha, String Hora, int CantidadPersonas, int IdPlato)
        {
            try
            {
                ServiciosPlatoWeb.ServiciosSoapClient servicio = new ServiciosPlatoWeb.ServiciosSoapClient();

                ServiciosPlatoWeb.Seguridad obSeguridad = new ServiciosPlatoWeb.Seguridad()
                {
                    stToken = DateTime.Now.ToString("yyyyMMdd")
                };

                String StToken = servicio.AutenticationUsuario(obSeguridad);
                if (StToken.Equals("-1")) throw new Exception("Requiere Validacion");

                obSeguridad.AutenticationToken = StToken;

                //DataTable data =  servicio.Login(obSeguridad, "pepitopaciente", "2");

                servicio.Reserva(obSeguridad, Fecha, Hora, CantidadPersonas, IdPlato.ToString());

                return Json("Reserva programada");
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult contactosPlatoWeb(String Nombre, String Email, String Telefono, String Detalle)
        {
            try
            {
                ServiciosPlatoWeb.ServiciosSoapClient servicio = new ServiciosPlatoWeb.ServiciosSoapClient();

                ServiciosPlatoWeb.Seguridad obSeguridad = new ServiciosPlatoWeb.Seguridad()
                {
                    stToken = DateTime.Now.ToString("yyyyMMdd")
                };

                String StToken = servicio.AutenticationUsuario(obSeguridad);
                if (StToken.Equals("-1")) throw new Exception("Requiere Validacion");

                obSeguridad.AutenticationToken = StToken;

                servicio.Contactenos(obSeguridad, Nombre, Email, Telefono, Detalle);

                return Json("Mensaje enviado a Plato a la Web");

            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}