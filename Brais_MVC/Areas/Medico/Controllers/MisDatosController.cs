﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Brais_MVC.Areas.Medico.Controllers
{
    public class MisDatosController : Controller
    {
        public ActionResult MisDatos()
        {
            return View();
        }

        [HttpPost]
        public JsonResult modificarDatosBasicos(string Correo, string Telefono, Int64 IdMedico, int IdAcceso)
        {
            try
            {
                Respuesta respuesta = LMedico.modificarDatosBasicos(Correo, Telefono, IdMedico, IdAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarContrasena(string Contrasena, string NuevaContrasena, Int64 IdMedico, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.updateContrasena(Contrasena, NuevaContrasena, IdMedico, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getMedico(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EMedico eMedico = (EMedico)LMedico.get(eAcceso.IdUsuario).Data;

                return Json(Respuesta.newRespuestaCorrecto("", eMedico));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}