﻿using Logica.Clases;
using Logica.Clases.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Areas.Medico.Controllers
{
    public class AgendaController : Controller
    {

        public ActionResult Agenda()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getAgenda(int IdAcceso, string Fecha)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LCita.getAgendaDeMedicoPorFecha(eAcceso.IdUsuario, Fecha);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}