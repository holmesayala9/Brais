﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Logica.Clases;
using Logica.Clases.Medico;
using Logica.Clases.Usuario;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Brais_MVC.Areas.Medico.Controllers
{
    public class HorarioController : Controller
    {
        public ActionResult Horario()
        {
            return View();
        }
        
        [HttpPost]
        public JsonResult getHorario(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EMedico eMedico = (EMedico)LMedico.get(eAcceso.IdUsuario).Data;

                return Json(Respuesta.newRespuestaCorrecto("", eMedico.Horario));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult guardarHorario(Horario horario, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LMedico.modificarHorario(horario, eAcceso.IdUsuario, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult generarCitas(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EMedico eMedico = (EMedico)LMedico.get(eAcceso.IdUsuario).Data;

                Respuesta respuesta = LCita.generarcCitasMedico(eMedico, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult solicitarReporte(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Session["Id_Medico_Reporte"] = eAcceso.IdUsuario;

                return Json(Respuesta.newRespuestaCorrecto(""));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        public ActionResult reporte()
        {
            ReportDocument report = new ReportDocument();
            var ruta = Server.MapPath("~/Reportes/Medico/CR_Horario.rpt");

            report.Load(ruta);

            Int64 idMedico = Int64.Parse(Session["Id_Medico_Reporte"].ToString());

            EMedico eMedico = (EMedico)LMedico.get(idMedico).Data;

            List<ReporteHorario> reportehorarios = LReportesMedico.dataHorario(eMedico.Horario);

            report.SetDataSource(reportehorarios);
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
    }
}