﻿using Logica.Clases;
using Logica.Clases.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Brais_MVC.Areas.Medico.Controllers
{
    public class VerCitaController : Controller
    {
        public ActionResult VerCita()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getCita(int IdCita)
        {
            try
            {
                Respuesta respuesta = LCita.get(IdCita);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getUsuario(int IdCita)
        {
            try
            {
                ECita eCita = (ECita)LCita.get(IdCita).Data;

                EUsuario eUsuario = LUsuario.obtenerUsuario(eCita.IdUsuario);
                return Json(Respuesta.newRespuestaCorrecto("", eUsuario));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult updateEstado(int IdCita, string Estado, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                ECita eCita = (ECita)LCita.get(IdCita).Data;
                eCita.Estado = Estado;

                Respuesta respuesta = LCita.update(eCita, eAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}