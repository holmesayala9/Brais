﻿using Logica.Clases;
using Logica.Clases.Usuario;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Brais_MVC.Areas.Medico.Controllers
{
    public class HistorialController : Controller
    {
        // GET: Medico/Historial
        public ActionResult Historial()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getHistorialUsuario(string Identificacion, int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                EMedico eMedico = (EMedico)LMedico.get(eAcceso.IdUsuario).Data;

                Respuesta respuesta = LHistorial.getByIdentificacionAndIdEspecialidad(Identificacion, eMedico.IdEspecialidad);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getHistorialPorCita(int IdCita)
        {
            try
            {
                Respuesta respuesta = LHistorial.getByIdCita(IdCita);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult agregarHistorial(string Datos, int IdCita, int IdAcceso)
        {
            try
            {
                Respuesta respuesta = LHistorial.add(Datos, IdCita, IdAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult modificarHistorial(string Datos, int IdHistorial, int IdAcceso)
        {
            try
            {
                Respuesta respuesta = LHistorial.update(Datos, IdHistorial, IdAcceso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}