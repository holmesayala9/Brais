﻿using Logica.Clases;
using Logica.Clases.Medico;
using Logica.Clases.Seguridad;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;
using Utilitaria.Clases.Seguridad;

namespace Brais_MVC.Servicios
{
    [WebService(Namespace = "Servicios Brais")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class BraisServicioMedicos : System.Web.Services.WebService
    {
        protected const string ID_SERVICIO = "Medicos";

        public const int DURACION_TOKEN_MINUTOS = 1;

        LTokenApp lTokenApp = new LTokenApp();


        [WebMethod]
        public string getAllEspecialidades(string tokenApp)
        {
            try
            {
                verificarTokenApp(tokenApp);

                List<EEspecialidad> especialidades = LEspecialidad.getAll();

                JArray jArray = new JArray();

                especialidades.ForEach(e =>
                {
                    JObject jObject = new JObject { { "Id", e.Id }, { "Nombre", e.Nombre } };
                    jArray.Add(jObject);
                }
                );

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", jArray));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string getMedicosByEspecialidad(string tokenApp, int idEspecialidad)
        {
            try
            {
                verificarTokenApp(tokenApp);

                List<EMedico> medicos = LMedico.getMedicosByEspecialidad(idEspecialidad);

                JArray jArray = new JArray();

                medicos.ForEach(m =>
                {
                    JObject jObject = new JObject {
                        { "Id", m.Id },
                        { "Identificacion", m.Identificacion },
                        { "Nombre", m.Nombre },
                        { "Apellido", m.Apellido },
                        { "Correo", m.Correo },
                        { "Telefono", m.Telefono },
                        { "IdEspecialidad", m.IdEspecialidad },
                    };
                    jArray.Add(jObject);
                }
                );

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", jArray));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }


        protected void verificarTokenApp(string token)
        {
            TokenApp tokenApp = lTokenApp.get(token);

            if (tokenApp == null || tokenApp.IdServicio != ID_SERVICIO) throw new Exception("token no valido");

            if ((DateTime.Now - DateTime.Parse(tokenApp.Fecha)).TotalMinutes > DURACION_TOKEN_MINUTOS) throw new Exception("token caducado");
        }


        [WebMethod]
        public string GenerarToken(string Usuario, string Contrasena)
        {
            Dictionary<string, string> apps = new Dictionary<string, string>();
            apps.Add("Uniempleo", "isuIHhf8wq37RET4yfHG6isdj");
            apps.Add("Colegio", "iser4556uIHhf8wq37RET4yfHG6isdj");

            try
            {
                if (!apps.Contains(new KeyValuePair<string, string>(Usuario, Contrasena)))
                {
                    JObject jObject = new JObject();

                    jObject.Add("1", Usuario);
                    jObject.Add("2", Contrasena);
                    jObject.Add("3", apps.Keys.ToList().ToString());

                    throw new Exception(jObject.ToString());
                    throw new Exception("Holmes grosero me dijo que esta maricada no funciona, " + apps.ContainsKey(Usuario));
                }

                TokenApp tokenApp = new TokenApp();
                tokenApp.UsuarioApp = Usuario;
                tokenApp.IdServicio = ID_SERVICIO;
                tokenApp.Fecha = tokenApp.Fecha = DateTime.Now.ToString();
                tokenApp.Token = Utilidades.encriptar(tokenApp.UsuarioApp + tokenApp.IdServicio + tokenApp.Fecha);

                lTokenApp.add(tokenApp);

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", tokenApp.Token));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}
