﻿using Logica.Clases;
using Logica.Clases.Medico;
using Logica.Clases.Seguridad;
using Logica.Clases.Usuario;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;
using Utilitaria.Clases.Seguridad;
using Utilitaria.Clases.Usuario;

namespace Brais_MVC.Servicios
{
    /// <summary>
    /// Descripción breve de BraisServicioGestorCitas
    /// </summary>
    [WebService(Namespace = "Gestor Citas")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class BraisServicioGestorCitas : System.Web.Services.WebService
    {
        protected const string ID_SERVICIO = "Cita";

        public const int DURACION_TOKEN_MINUTOS = 60;

        LTokenApp lTokenApp = new LTokenApp();
        LTokenUsuario lTokenUsuario = new LTokenUsuario();

        [WebMethod]
        public string ReprogramarCita(string tokenApp, string tokenUser, int idCitaNew, int idCitaOld)
        {
            try
            {
                VerificarTokenApp(tokenApp);
                VerificarTokenUsuario(tokenUser);
                TokenUsuario tokenUsuario = lTokenUsuario.get(tokenUser);

                EUsuario eUsuario = LUsuario.obtenerUsuario(tokenUsuario.IdUsuario);
                eUsuario.EAcceso = LSeguridad.getAcceso(eUsuario.IdAcceso);
                LCita.verificarReprogramacionDeCita(idCitaOld);
                Respuesta respuesta = LCita.reprogramarCita(idCitaNew, idCitaOld, eUsuario.Id, eUsuario.EAcceso);

                return JsonConvert.SerializeObject(respuesta);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string EliminarCita(string tokenApp, string tokenUser, int idCita)
        {
            try
            {
                VerificarTokenApp(tokenApp);
                VerificarTokenUsuario(tokenUser);
                TokenUsuario tokenUsuario = lTokenUsuario.get(tokenUser);

                EUsuario eUsuario = LUsuario.obtenerUsuario(tokenUsuario.IdUsuario);
                eUsuario.EAcceso = LSeguridad.getAcceso(eUsuario.IdAcceso);
                Respuesta respuesta = LCita.cancelarCita(idCita, eUsuario.EAcceso);

                return JsonConvert.SerializeObject(respuesta);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string VerCitasAgendadas(string tokenApp, string tokenUser)
        {
            try
            {
                VerificarTokenApp(tokenApp);
                VerificarTokenUsuario(tokenUser);
                TokenUsuario tokenUsuario = lTokenUsuario.get(tokenUser);

                List<ECita> citas = (List<ECita>)LCita.getCitasVigentesDeUsuario(tokenUsuario.IdUsuario).Data;

                JArray jArray = new JArray();

                citas.ForEach(e =>
                {
                    JObject jObject = new JObject {
                            { "Id", e.Id },
                            { "Fecha", e.Fecha },
                            { "HoraInicio", e.HoraInicio },
                            { "HoraFin", e.HoraFin },
                            { "Servicio", e.Servicio },
                            { "Estado", e.Estado },
                            { "Consultorio", e.EMedico.EConsultorio.Nombre },
                            { "NombreMedico", e.EMedico.Nombre + e.EMedico.Apellido }
                        };
                    jArray.Add(jObject);
                }
                );

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", jArray));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string AsignarCita(string tokenApp, string tokenUser, int idCita)
        {
            try
            {
                VerificarTokenApp(tokenApp);
                VerificarTokenUsuario(tokenUser);
                TokenUsuario tokenUsuario = lTokenUsuario.get(tokenUser);

                LCita.verificarCita(idCita);
                EUsuario eUsuario = LUsuario.obtenerUsuario(tokenUsuario.IdUsuario);
                eUsuario.EAcceso = LSeguridad.getAcceso(eUsuario.IdAcceso);
                Respuesta respuesta = LCita.reservarCita(idCita, eUsuario.Id, eUsuario.EAcceso);

                return JsonConvert.SerializeObject(respuesta);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string MostrarCitasDisponibles(string tokenApp, string tokenUser, int idEspecialidad, string fecha)
        {
            try
            {
                VerificarTokenApp(tokenApp);
                VerificarTokenUsuario(tokenUser);

                TokenUsuario tokenUsuario = lTokenUsuario.get(tokenUser);

                List<ECita> citas = (List<ECita>)LCita.getCitasDisponiblesParaUsuario(tokenUsuario.IdUsuario, idEspecialidad, fecha).Data;

                JArray jArray = new JArray();

                citas.ForEach(e =>
                {
                    JObject jObject = new JObject {
                            { "Id", e.Id },
                            { "Fecha", e.Fecha },
                            { "HoraInicio", e.HoraInicio },
                            { "HoraFin", e.HoraFin },
                            { "NombreMedico", e.EMedico.Nombre + e.EMedico.Apellido }
                        };
                    jArray.Add(jObject);
                }
                );

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", jArray));

            }
            catch (Exception ex)
            {

                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string ObtenerEspecialidades(string tokenApp)
        {
            try
            {
                VerificarTokenApp(tokenApp);

                List<EEspecialidad> especialidades = LEspecialidad.getAll();

                JArray jArray = new JArray();

                especialidades.ForEach(e =>
                {
                    JObject jObject = new JObject { { "Id", e.Id }, { "Nombre", e.Nombre } };
                    jArray.Add(jObject);
                }
                );

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", jArray));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        protected void VerificarTokenApp(string token)
        {
            TokenApp tokenApp = lTokenApp.get(token);

            if (tokenApp == null || tokenApp.IdServicio != ID_SERVICIO) throw new Exception("token no valido");

            if ((DateTime.Now - DateTime.Parse(tokenApp.Fecha)).TotalMinutes > DURACION_TOKEN_MINUTOS) throw new Exception("token caducado");
        }

        protected void VerificarTokenUsuario(string token)
        {
            TokenUsuario tokenUsuario = lTokenUsuario.get(token);

            if (tokenUsuario == null || tokenUsuario.IdServicio != ID_SERVICIO) throw new Exception("token no valido");

            if ((DateTime.Now - DateTime.Parse(tokenUsuario.Fecha)).TotalMinutes > DURACION_TOKEN_MINUTOS) throw new Exception("token caducado");
        }

        [WebMethod]
        public string GenerarTokenUsuario(string Identificacion, string Contrasena)
        {
            try
            {
                LFuncion lFuncion = new LFuncion();
                EUsuario eUsuario = lFuncion.validarUsuarioExistente(Identificacion, Contrasena);

                TokenUsuario tokenUsuario = new TokenUsuario();
                tokenUsuario.IdUsuario = eUsuario.Id;
                tokenUsuario.IdServicio = ID_SERVICIO;
                tokenUsuario.Fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                tokenUsuario.Token = Utilidades.encriptar(Identificacion + tokenUsuario.IdServicio + tokenUsuario.Fecha);

                lTokenUsuario.add(tokenUsuario);

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", tokenUsuario.Token));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string GenerarToken(string Usuario, string Contrasena)
        {
            JObject apps = new JObject();
            apps.Add("Brais", "666");

            try
            {
                if (apps[Usuario] == null || !(apps[Usuario].ToString() == Contrasena))
                {
                    throw new Exception("Usuario o contraseña incorrecto");
                }

                TokenApp tokenApp = new TokenApp();
                tokenApp.UsuarioApp = Usuario;
                tokenApp.IdServicio = ID_SERVICIO;
                tokenApp.Fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                tokenApp.Token = Utilidades.encriptar(tokenApp.UsuarioApp + tokenApp.IdServicio + tokenApp.Fecha);

                lTokenApp.add(tokenApp);

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", tokenApp.Token));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}
