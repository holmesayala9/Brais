﻿using Logica.Clases.Seguridad;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Utilitaria.Clases;
using Utilitaria.Clases.Seguridad;

namespace Brais_MVC
{
    [WebService(Namespace = "Servicios Brais")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class BraisServicioHorario : System.Web.Services.WebService
    {
        protected const string ID_SERVICIO = "Horario";

        public const int DURACION_TOKEN_MINUTOS = 1;

        LTokenApp lTokenApp = new LTokenApp();

        [WebMethod]
        public string AgregarRango(string tokenApp, string horario, string rango)
        {
            try
            {
                verificarTokenApp(tokenApp);

                Horario h = verificarFormatoHorario(horario);

                Rango r = verificarFormatoRango(rango);

                h.agregarRango(r);

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", h));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [WebMethod]
        public string EliminarRango(string tokenApp, string horario, string rango)
        {
            try
            {
                verificarTokenApp(tokenApp);

                Horario h = verificarFormatoHorario(horario);

                Rango r = verificarFormatoRango(rango);

                h.eliminarRango(r);

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", h));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        protected Horario verificarFormatoHorario(string horario)
        {
            try
            {
                horario = horario.Equals("") ? "{}" : horario;

                Horario h = JsonConvert.DeserializeObject<Horario>(horario);

                return h;
            }
            catch (Exception)
            {
                throw new Exception("Error: formato del horario");
            }
        }

        protected Rango verificarFormatoRango(string rango)
        {
            try
            {
                Rango r = JsonConvert.DeserializeObject<Rango>(rango);
                r.Inicio = DateTime.Parse(r.Inicio).ToString("HH:mm");
                r.Fin = DateTime.Parse(r.Fin).ToString("HH:mm");

                return r;
            }
            catch (Exception)
            {
                throw new Exception("Error: formato del rango");
            }
        }

        protected void verificarTokenApp(string token)
        {
            TokenApp tokenApp = lTokenApp.get(token);

            if (tokenApp == null || tokenApp.IdServicio != ID_SERVICIO) throw new Exception("token no valido");

            if ( (DateTime.Now - DateTime.Parse(tokenApp.Fecha)).TotalMinutes > DURACION_TOKEN_MINUTOS ) throw new Exception("token caducado");
        }

        [WebMethod]
        public string GenerarToken(string Usuario, string Contrasena)
        {
            JObject apps = new JObject();
            apps.Add("Brais", "666");

            try
            {
                if ( apps[Usuario] == null || !(apps[Usuario].ToString() == Contrasena) )
                {
                    throw new Exception("Usuario o contraseña incorrecto");
                }

                TokenApp tokenApp = new TokenApp();
                tokenApp.UsuarioApp = Usuario;
                tokenApp.IdServicio = ID_SERVICIO;
                tokenApp.Fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                tokenApp.Token = Utilidades.encriptar(tokenApp.UsuarioApp+tokenApp.IdServicio+tokenApp.Fecha);

                lTokenApp.add(tokenApp);

                return JsonConvert.SerializeObject(Respuesta.newRespuestaCorrecto("", tokenApp.Token));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

    }
}
