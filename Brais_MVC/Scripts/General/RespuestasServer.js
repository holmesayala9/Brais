﻿
function mostrarMensaje(m) {
    var mensaje = $('<div/>');

    if (m.Estado == MENSAJE_CORRECTO) {
        mensaje.addClass("alert alert-success").removeClass("alert-danger");
    }
    else if (m.Estado == MENSAJE_ALERTA) {
        mensaje.addClass("alert alert-danger").removeClass("alert-success");
    }
    else if (m.Estado == MENSAJE_INFORMATIVO) {
        mensaje.addClass("alert alert-info");
    }

    console.log(m.Mensaje);

    mensaje.text(getTextoMensaje(m.Mensaje));

    $("#mensajes").append(mensaje);

    setTimeout(function () { mensaje.remove() }, 3000);

}

function mostrarMensajeRespuestaPost(respuesta) {
    var mensaje = $('<div/>');

    if (respuesta.Estado == MENSAJE_CORRECTO) {
        mensaje.addClass("alert alert-success").removeClass("alert-danger");
    }
    else if (respuesta.Estado == MENSAJE_ALERTA) {
        mensaje.addClass("alert alert-danger").removeClass("alert-success");
    }

    console.log(respuesta.Mensaje);

    mensaje.text(getTextoMensaje(respuesta.Mensaje));

    if (respuesta.Mensaje != "") {
        $("#mensajes").append(mensaje);

        setTimeout(function () { mensaje.remove() }, 3000);
    }
}

function mostrarErrorRespuestaPost(error) {
    console.log("Error devuelto post...");
    console.log(error.responseText);
}

function mostrarMensajeRespuestaActionLink(respuesta) {
    var mensaje = $('<div/>');

    if (respuesta.responseJSON.Estado == MENSAJE_CORRECTO) {
        mensaje.addClass("alert alert-success").removeClass("alert-danger");
    }
    else if (respuesta.responseJSON.Estado == MENSAJE_ALERTA) {
        mensaje.addClass("alert alert-danger").removeClass("alert-success");
    }

    console.log(respuesta.responseJSON.Mensaje);

    mensaje.text(respuesta.responseJSON.Mensaje);

    $("#mensajes").html(mensaje);
}