﻿

$.validator.addMethod("cVacio", function (value, element) {
    return value.trim() != "";
}, "<p class='RFV_CampoVacio text-danger font-italic mb-0'>" + getTextoComponente("LB_CampoVacio") + "</p>");
$.validator.addClassRules("campo_vacio", { cVacio: true });

$.validator.addMethod("pVacio", function (value, element) {
    return value != "";
}, "<p class='RFV_CampoVacio text-danger font-italic mb-0'>" + getTextoComponente("LB_CampoVacio") + "</p>");
$.validator.addClassRules("contrasena_vacio", { pVacio: true });

$.validator.addMethod("sNumeros", $.validator.methods.digits, "<p class='text-danger font-italic mb-0'>" + getTextoComponente("LB_SoloNumeros") + "</p>");
$.validator.addClassRules("solo_numeros", { sNumeros: true });

$.validator.addMethod("vEmail", $.validator.methods.email, "<p class='text-danger font-italic mb-0'>" + getTextoComponente("LB_CorreoNoValido") + "</p>");
$.validator.addClassRules("email_valido", { vEmail: true });

$.validator.addMethod("vCaracteresEspeciales", function (value, element) {
    return /^[\w\s-,'ñÑíÍóÓ¿?]*$/i.test(value);
}, "<p class='text-danger font-italic mb-0'>" + getTextoComponente("LB_CaracteresEspeciales") + "</p>");
$.validator.addClassRules("caracteres_especiales", { vCaracteresEspeciales: true });

window.onload = function () {
    $("form").each(function () {
        $(this).attr("autocomplete", "off");
    });
};