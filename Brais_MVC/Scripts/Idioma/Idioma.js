﻿
window.addEventListener("load",
    function (event) {
        aplicarIdioma();
    }
);

function getTextoComponente(txt) {
    if (localStorage.getItem("Componentes")) {
        var componentes = JSON.parse(localStorage.getItem("Componentes"));
        if (componentes[txt]) {
            return componentes[txt];
        } else {
            return "componente no registrado";
        }
    }
    else {
        return "idioma vacio";
    }
}

function getTextoMensaje(txt) {
    if (localStorage.getItem("Componentes")) {
        var componentes = JSON.parse(localStorage.getItem("Componentes"));

        txt = txt.replace(/[\s]/g, "_");

        if (componentes[txt]) {
            return componentes[txt];
        } else {
            return txt;
        }
    }
    else {
        return "idioma vacio";
    }
}

function aplicarIdioma() {
    if (localStorage.getItem("Componentes")) {
        var componentes = JSON.parse(localStorage.getItem("Componentes"));
        for (key in componentes) {
            var elements = document.getElementsByClassName(key);
            for (let e of elements) {
                e.innerHTML = componentes[key];
            }
        }
    }
}