﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Controllers
{
    public class RecuperarContrasenaPasoUnoController : Controller
    {

        public ActionResult RecuperarContrasenaPasoUno()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RecuperarContrasena(string Identificacion, string Correo)
        {
            try
            {
                Respuesta respuesta = LFuncion.solicitarRestablecerContrasena(Identificacion, Correo);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

    }
}