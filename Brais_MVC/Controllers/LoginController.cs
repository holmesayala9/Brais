﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Utilitaria.Clases;
using Logica.Clases;

namespace Brais_MVC.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IniciarSesion(string Identificacion, string Contrasena)
        {
            try
            {
                Respuesta respuesta = LFuncion.iniciarSesionMVC(Identificacion, Contrasena);
                
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}