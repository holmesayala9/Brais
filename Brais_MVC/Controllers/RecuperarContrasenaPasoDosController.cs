﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Controllers
{
    public class RecuperarContrasenaPasoDosController : Controller
    {

        public ActionResult RecuperarContrasenaPasoDos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RecuperarContrasenaPasoDos(string hash)
        {
            if(hash == null || hash == "")
            {
                ViewBag.Estado = false;
            }
            else
            {
                ViewBag.Estado = true;
                ViewBag.hash = hash;
            }

            return View();
        }

        [HttpPost]
        public JsonResult RecuperarContrasena(string Hash, string Contrasena)
        {
            try
            {
                Respuesta respuesta = LFuncion.restablecerContrasena(Hash, Contrasena);
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}