﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Brais_MVC.App_Code;
using Logica.Clases.Administrador;
using Utilitaria.Clases;

namespace Brais_MVC.Controllers
{
    public class MasterPageController : BaseController
    {
        // GET: MasterPage
        public ActionResult MasterPage()
        {
            return View();
        }

    }
}