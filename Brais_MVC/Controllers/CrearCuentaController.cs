﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Brais_MVC.Controllers
{
    public class CrearCuentaController : Controller
    {
        // GET: CrearCuenta
        public ActionResult CrearCuenta()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CrearCuenta(EUsuario eUsuario)
        {
            try
            {
                Respuesta respuesta = LUsuario.agregarUsuario(eUsuario, new EAcceso());
                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}