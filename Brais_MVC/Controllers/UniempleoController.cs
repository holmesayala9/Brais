﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;
using Utilitaria.Clases.Servicios;

namespace Brais_MVC.Controllers
{
    public class UniempleoController : Controller
    {
        // GET: Uniempleo
        public ActionResult Uniempleo()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getOfertas()
        {
            try
            {
                UniempleoServicioOfertas.ServidorUniempleoSoapClient servidorUniempleo = new UniempleoServicioOfertas.ServidorUniempleoSoapClient();

                servidorUniempleo.ClientCredentials.UserName.UserName = "Brais";
                servidorUniempleo.ClientCredentials.UserName.Password = "FM2qHIxQ9m";

                UniempleoServicioOfertas.SeguridadToken objSeguridad = new UniempleoServicioOfertas.SeguridadToken()
                {
                    username = "Brais",
                    Pass = "FM2qHIxQ9m"
                };
                String token = servidorUniempleo.AutenticacionUsuario(objSeguridad);

                if (token.Equals("-1"))
                {
                    throw new Exception("Requiere validación");
                }

                objSeguridad.Token_Autenticacion = token;

                List<JoinOferta> ofertas = JsonConvert.DeserializeObject<List<JoinOferta>>(servidorUniempleo.Ofertas_Detalladas(objSeguridad));

                return Json(Respuesta.newRespuestaCorrecto("", ofertas));

            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}