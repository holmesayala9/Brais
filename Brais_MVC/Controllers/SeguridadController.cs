﻿using Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitaria.Clases;

namespace Brais_MVC.Controllers
{
    public class SeguridadController : Controller
    {
        protected ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult verificarAcceso(int IdAcceso, string Recurso)
        {
            try
            {
                Respuesta respuesta = LSeguridad.verificarAcceso(IdAcceso, Recurso);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult getTipoUsuario(int IdAcceso)
        {
            try
            {
                EAcceso eAcceso = LSeguridad.getAcceso(IdAcceso);

                Respuesta respuesta = LSeguridad.getTipoUsuario(eAcceso.IdUsuario);

                return Json(respuesta);
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult cerrarAcceso(int IdAcceso)
        {
            try
            {
                LSeguridad.cerrarAcceso(IdAcceso);

                return Json(Respuesta.newRespuestaCorrecto(""));
            }
            catch (Exception ex)
            {
                return Json(Respuesta.newRespuestaAlerta(ex.Message));
            }
        }
    }
}