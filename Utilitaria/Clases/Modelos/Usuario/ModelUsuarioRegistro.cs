﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Modelos.Usuario
{
    public class ModelUsuarioRegistro
    {
        [Required(ErrorMessage = "Campo vacio")]
        public string Identificacion { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Apellido { get; set; }

        [Required]
        public string Correo { get; set; }

        [Required]
        public string Telefono { get; set; }

        [Required]
        public string Contrasena { get; set; }

        [Required]
        public string RepetirContrasena { get; set; }

        [Required]
        public string FechaNacimiento { get; set; }

        [Required]
        public string Genero { get; set; }
    }
}
