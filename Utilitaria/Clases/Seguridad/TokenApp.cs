﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Seguridad
{
    [Serializable]
    [Table("token_app", Schema = "seguridad")]
    public class TokenApp
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("token")]
        public string Token { get; set; }

        [Column("fecha")]
        public string Fecha { get; set; }

        [Column("id_servicio")]
        public string IdServicio { get; set; }

        [Column("usuario_app")]
        public string UsuarioApp { get; set; }

        public TokenApp()
        {

        }
    }
}
