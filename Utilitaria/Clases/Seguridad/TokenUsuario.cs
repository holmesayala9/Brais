﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Seguridad
{

    [Serializable]
    [Table("token_usuario", Schema = "seguridad")]
    public class TokenUsuario
    {
        public TokenUsuario()
        {

        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("id_usuario")]
        public long IdUsuario { get; set; }

        [Column("token")]
        public string Token { get; set; }

        [Column("fecha")]
        public string Fecha { get; set; }

        [Column("id_servicio")]
        public string IdServicio { get; set; }
    }
}
