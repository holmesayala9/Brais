﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


public class UsuarioBasico
{

    private Int64 id = -1;

    private string identificacion = "Ninguno";

    private string nombre = "Ninguno";

    private string apellido = "";

    private string correo = "Ninguno";

    private string telefono = "Ninguno";

    private string contrasena = "Ninguno";

    private Boolean activo = false;

    private int idAcceso = -1;

    private EAcceso eAcceso;

    public UsuarioBasico()
    {

    }

    [Key]
    [Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public Int64 Id { get => id; set => id = value; }

    [NotMapped]
    public string IdStr { get => Id.ToString(); }

    [Column("identificacion")]
    public string Identificacion { get => identificacion; set => identificacion = value; }

    [Column("nombre")]
    public string Nombre { get => nombre; set => nombre = value; }

    [Column("apellido")]
    public string Apellido { get => apellido; set => apellido = value; }

    [Column("correo")]
    public string Correo { get => correo; set => correo = value; }

    [Column("telefono")]
    public string Telefono { get => telefono; set => telefono = value; }

    [Column("contrasena")]
    public string Contrasena { get => contrasena; set => contrasena = value; }

    [Column("activo")]
    public bool Activo { get => activo; set => activo = value; }

    [Column("id_acceso")]
    public int IdAcceso { get => idAcceso; set => idAcceso = value; }

    [NotMapped]
    public EAcceso EAcceso { get => eAcceso; set => eAcceso = value; }
}