﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases
{
    [Serializable]
    [Table("estado_ingreso", Schema = "seguridad")]
    public class EEstadoIngreso
    {

        private int id;

        private Int64 idUsuario;

        private int cantidadSesiones;

        private int cantidadIntentos;

        private string fechaBloqueo;

        private EEstadoIngreso()
        {
            Id = -1;
            IdUsuario = -1;
            CantidadSesiones = 0;
            CantidadIntentos = 0;
            FechaBloqueo = "";
        }

        public static EEstadoIngreso newEmpty()
        {
            return new EEstadoIngreso();
        }

        public static EEstadoIngreso fromDataRow(DataRow dataRow)
        {
            EEstadoIngreso eEstadoIngreso = EEstadoIngreso.newEmpty();

            eEstadoIngreso.Id = int.Parse(dataRow["id"].ToString());
            eEstadoIngreso.IdUsuario = int.Parse(dataRow["id_usuario"].ToString());
            eEstadoIngreso.CantidadSesiones = int.Parse(dataRow["cantidad_sesiones"].ToString());
            eEstadoIngreso.CantidadIntentos = int.Parse(dataRow["cantidad_intentos"].ToString());
            eEstadoIngreso.FechaBloqueo = dataRow["fecha_bloqueo"].ToString();

            return eEstadoIngreso;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("id_usuario")]
        public Int64 IdUsuario { get => idUsuario; set => idUsuario = value; }

        [Column("cantidad_sesiones")]
        public int CantidadSesiones { get => cantidadSesiones; set => cantidadSesiones = value; }

        [Column("cantidad_intentos")]
        public int CantidadIntentos { get => cantidadIntentos; set => cantidadIntentos = value; }

        [Column("fecha_bloqueo")]
        public string FechaBloqueo { get => fechaBloqueo; set => fechaBloqueo = value; }
    }
}
