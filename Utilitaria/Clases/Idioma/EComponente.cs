﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    [Serializable]
    [Table("componente", Schema = "idioma")]
    public class EComponente
    {
        private int id;

        private string name;

        private int idAcceso;

        public EComponente()
        {
            id = -1;
            name = "None";
            IdAcceso = -1;
        }

        public static EComponente newEmpty()
        {
            return new EComponente();
        }

        public static EComponente fromDataRow(DataRow dataRow)
        {
            EComponente eComponente = new EComponente();

            eComponente.Id = int.Parse(dataRow["id"].ToString());
            eComponente.Name = dataRow["name"].ToString();

            return eComponente;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("name")]
        public string Name { get => name; set => name = value; }

        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }
    }
}
