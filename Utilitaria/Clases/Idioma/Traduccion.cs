﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    public class Traduccion
    {
        private string componente = "";

        private string txt = "";

        public Traduccion()
        {
        }

        public string Componente { get => componente; set => componente = value; }

        public string Txt { get => txt; set => txt = value; }
    }
}
