﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    [Serializable]
    [Table("mensaje", Schema = "idioma")]
    public class EMensaje
    {
        private int id = -1;

        private int idIdioma = -1;

        private string txtOriginal = "None";

        private string txtTraducido = "None";

        public EMensaje()
        {
        }

        public static EMensaje newEmpty()
        {
            return new EMensaje();
        }

        public static EMensaje fromDataRow(DataRow dataRow)
        {
            EMensaje eMensaje = new EMensaje();

            eMensaje.Id = int.Parse(dataRow["id"].ToString());
            eMensaje.IdIdioma = int.Parse(dataRow["id_idioma"].ToString());
            eMensaje.TxtOriginal = dataRow["txt_original"].ToString();
            eMensaje.TxtTraducido = dataRow["txt_traducido"].ToString();
            return eMensaje;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("id_idioma")]
        public int IdIdioma { get => idIdioma; set => idIdioma = value; }

        [Column("txt_original")]
        public string TxtOriginal { get => txtOriginal; set => txtOriginal = value; }

        [Column("txt_traducido")]
        public string TxtTraducido { get => txtTraducido; set => txtTraducido = value; }
    }
}
