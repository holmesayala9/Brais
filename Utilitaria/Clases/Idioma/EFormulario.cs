﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    [Serializable]
    [Table("formulario",  Schema = "idioma")]
    public class EFormulario
    {
        private int id;

        private string name;

        private string url;

        private int idAcceso;

        private EFormulario()
        {
            Id = -1;
            Name = "None";
            Url = "None";
            IdAcceso = -1;
        }

        public static EFormulario newEmpty()
        {
            return new EFormulario();
        }

        public static EFormulario fromDataRow(DataRow dataRow)
        {
            EFormulario eFormulario = new EFormulario();

            eFormulario.Id = int.Parse(dataRow["id"].ToString());
            eFormulario.Name = dataRow["name"].ToString();
            eFormulario.Url = dataRow["url"].ToString();

            return eFormulario;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("name")]
        public string Name { get => name; set => name = value; }

        [Column("url")]
        public string Url { get => url; set => url = value; }

        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }
    }
}
