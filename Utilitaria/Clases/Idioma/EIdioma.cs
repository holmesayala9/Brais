﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    [Serializable]
    [Table("idioma", Schema = "idioma")]
    public class EIdioma
    {
        private int id;

        private string name;

        private string terminacion;

        private int idAcceso;

        public EIdioma()
        {
            id = -1;
            name = "None";
            terminacion = "None";
            IdAcceso = -1;
        }

        public static EIdioma newEmpty()
        {
            return new EIdioma();
        }

        public static EIdioma fromDataRow(DataRow dataRow)
        {
            EIdioma eIdioma = new EIdioma();

            eIdioma.Id = int.Parse(dataRow["id"].ToString());
            eIdioma.Name = dataRow["name"].ToString();
            eIdioma.Terminacion = dataRow["terminacion"].ToString();
            eIdioma.IdAcceso = int.Parse(dataRow["id_acceso"].ToString());
            return eIdioma;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }
        [Column("name")]
        public string Name { get => name; set => name = value; }
        [Column("terminacion")]
        public string Terminacion { get => terminacion; set => terminacion = value; }

        public string NameTerminacion { get => Name + " " + Terminacion;}
        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }
    }
}
