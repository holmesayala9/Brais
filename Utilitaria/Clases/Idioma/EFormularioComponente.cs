﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    [Serializable]
    [Table("formulario_componente", Schema = "idioma")]
    public class EFormularioComponente
    {
        private int id = -1;

        private int idFormulario = -1;

        private int idComponente = -1;

        private int idAcceso = -1;

        private EFormulario eFormulario = EFormulario.newEmpty();

        private EComponente eComponente = EComponente.newEmpty();

        private EFormularioComponente()
        {
        }

        public static EFormularioComponente newEmpty()
        {
            return new EFormularioComponente();
        }

        public static EFormularioComponente fromDataRow(DataRow dataRow)
        {
            EFormularioComponente eFormularioComponente = newEmpty();

            eFormularioComponente.Id = int.Parse(dataRow["id"].ToString());
            eFormularioComponente.IdFormulario = int.Parse(dataRow["id_formulario"].ToString());
            eFormularioComponente.IdComponente = int.Parse(dataRow["id_componente"].ToString());

            return eFormularioComponente;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("id_formulario")]
        public int IdFormulario { get => idFormulario; set => idFormulario = value; }

        [Column("id_componente")]
        public int IdComponente { get => idComponente; set => idComponente = value; }

        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }

        [NotMapped]
        public EFormulario EFormulario { get => eFormulario; set => eFormulario = value; }

        [NotMapped]
        public EComponente EComponente { get => eComponente; set => eComponente = value; }
    }
}
