﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Idioma
{
    [Serializable]
    [Table("idioma_componente", Schema = "idioma")]
    public class EIdiomaComponente
    {

        private int id = -1;

        private int idIdioma = -1;

        private int idComponente = -1;

        private string txt = "";

        private int idAcceso = -1;

        private EIdioma eIdioma = EIdioma.newEmpty();

        private EComponente eComponente = EComponente.newEmpty();

        public EIdiomaComponente()
        {
        }

        public static EIdiomaComponente newEmpty()
        {
            return new EIdiomaComponente();
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("id_idioma")]
        public int IdIdioma { get => idIdioma; set => idIdioma = value; }

        [Column("id_componente")]
        public int IdComponente { get => idComponente; set => idComponente = value; }

        [Column("txt")]
        public string Txt { get => txt; set => txt = value; }

        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }

        [NotMapped]
        public EIdioma EIdioma { get => eIdioma; set => eIdioma = value; }

        [NotMapped]
        public EComponente EComponente { get => eComponente; set => eComponente = value; }
    }
}
