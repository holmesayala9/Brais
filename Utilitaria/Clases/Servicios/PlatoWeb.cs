﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Servicios
{
    public class PlatoWeb
    {
        private int id;
        private String nombre;
        private String descripcion;
        private String precio;
        private String imagen;

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Precio { get => precio; set => precio = value; }
        public string Imagen { get => imagen; set => imagen = value; }
    }
}
