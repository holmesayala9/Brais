﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;

[Serializable]
[Table("administrador", Schema = "administrador")]
public class EAdministrador : UsuarioBasico
{
    public const string TABLA = "administrador";

    public const string ESQUEMA = "administrador";

    public EAdministrador()
    {

    }

    public static EAdministrador newEmpty()
    {
        return new EAdministrador();
    }
}