﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;

[Serializable]
[Table("parametro", Schema = "administrador")]
public class Parametro
{
    public const string ESQUEMA = "administrador";

    public const string TABLA = "parametro";

    private string duracionCitas;

    private string horaInicio;

    private string horaFin;

    private List<int> diasLaborales;

    private int sesiones;

    private int intentosIngreso;

    private int id = -1;

    private string sParametro = "{}";

    private int idAcceso = -1;

    public Parametro()
    {

    }

    public static Parametro fromDataRow(DataRow dataRow)
    {
        return JsonConvert.DeserializeObject<Parametro>(dataRow["parametro"].ToString());
    }

    [NotMapped]
    public string DuracionCitas { get => duracionCitas; set => duracionCitas = value; }

    [NotMapped]
    public string HoraInicio { get => horaInicio; set => horaInicio = value; }

    [NotMapped]
    public string HoraFin { get => horaFin; set => horaFin = value; }

    [NotMapped]
    public List<int> DiasLaborales { get => diasLaborales; set => diasLaborales = value; }

    [NotMapped]
    public int Sesiones { get => sesiones; set => sesiones = value; }

    [NotMapped]
    public int IntentosIngreso { get => intentosIngreso; set => intentosIngreso = value; }

    [Key]
    [Column("id")]
    public int Id { get => id; set => id = value; }

    [Column("parametro")]
    public string SParametro { get => sParametro; set => sParametro = value; }

    [Column("id_acceso")]
    public int IdAcceso { get => idAcceso; set => idAcceso = value; }
}