﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases
{
    [Serializable]
    [Table("restablecer_contrasena", Schema = "seguridad")]
    public class ESolicitudContrasena
    {
        private int id = -1;

        private Int64 idUsuario = -1;

        private string hash = "";

        private string fecha = "";

        private int idAcceso = -1;

        public ESolicitudContrasena()
        {
        }

        public static ESolicitudContrasena fromDataRow(DataRow dataRow)
        {
            ESolicitudContrasena eSolicitudContrasena = new ESolicitudContrasena();

            eSolicitudContrasena.id = int.Parse(dataRow["id"].ToString());
            eSolicitudContrasena.IdUsuario = int.Parse(dataRow["id_usuario"].ToString());
            eSolicitudContrasena.Hash = dataRow["hash"].ToString();
            eSolicitudContrasena.Fecha = dataRow["fecha"].ToString();

            return eSolicitudContrasena;
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("id_usuario")]
        public Int64 IdUsuario { get => idUsuario; set => idUsuario = value; }

        [Column("hash")]
        public string Hash { get => hash; set => hash = value; }

        [Column("fecha")]
        public string Fecha { get => fecha; set => fecha = value; }

        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }
    }
}
