﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases
{
    public abstract class Utilidades
    {

        public static string encriptar(string input)
        {
            SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();

            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = provider.ComputeHash(inputBytes);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }

        public static void enviarCorreo(String correoDestino, String asunto, String mensaje)
        {
            List<string> destinatarios = new List<string>()
            {
                correoDestino
            };

            string body = mensaje;

            MailMessage mail = new MailMessage()
            {
                From = new MailAddress("brais.contacto@gmail.com", "Brais"),
                Body = body,
                Subject = asunto,
                IsBodyHtml = false
            };

            foreach (string item in destinatarios)
            {
                mail.To.Add(new MailAddress(item));
            }

            SmtpClient smtp = new SmtpClient()
            {
                Host = "smtp.gmail.com",
                Port = 587,
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = new NetworkCredential("brais.contacto@gmail.com", "wmf7ddCnq5aUMqe8Oue8") 
            };
            smtp.Send(mail);
        }
    }
}
