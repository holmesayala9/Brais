﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;
using Utilitaria.Clases.Usuario;

[Serializable]
[Table("historial", Schema = "usuario")]
public class EHistorial
{
    public const string TABLA = "historial";

    public const string ESQUEMA = "usuario";

    private int id = -1;

    private string datos = "{}";

    private Int64 idUsuario = -1;

    private int idCita = -1;

    private int idAcceso = -1;

    private EUsuario eUsuario = EUsuario.newEmpty();

    private ECita eCita = ECita.newEmpty();

    public EHistorial()
    {

    }

    public static EHistorial newEmpty()
    {
        return new EHistorial();
    }

    [Key]
    [Column("id")]
    public int Id { get => id; set => id = value; }

    [Column("datos")]
    public string Datos { get => datos; set => datos = value; }

    [Column("id_usuario")]
    public Int64 IdUsuario { get => idUsuario; set => idUsuario = value; }

    [Column("id_cita")]
    public int IdCita { get => idCita; set => idCita = value; }

    [Column("id_acceso")]
    public int IdAcceso { get => idAcceso; set => idAcceso = value; }

    [NotMapped]
    public EUsuario EUsuario { get => eUsuario; set => eUsuario = value; }

    [NotMapped]
    public ECita ECita { get => eCita; set => eCita = value; }
}