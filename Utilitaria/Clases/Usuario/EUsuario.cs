﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;

namespace Utilitaria.Clases.Usuario
{
    [Serializable]
    [Table("usuario", Schema = "usuario")]
    public class EUsuario : UsuarioBasico
    {
        public const string TABLA = "usuario";

        public const string ESQUEMA = "usuario";

        private string fechaNacimiento = DateTime.Now.ToShortDateString();

        private string genero = "Ninguno";

        public EUsuario()
        {

        }

        public static EUsuario newEmpty()
        {
            return new EUsuario();
        }

        public Dictionary<string, int> getEdad()
        {
            TimeSpan diferencia = DateTime.Today - DateTime.Parse(FechaNacimiento);
            int anios = (int)(diferencia.TotalDays / 360);
            int meses = (int)((diferencia.TotalDays - anios * 365.25) / 30);
            int dias = (int)(diferencia.TotalDays - anios * 365.25 - meses * 30);

            Dictionary<string, int> dict = new Dictionary<string, int>();
            dict.Add("anios", anios);
            dict.Add("meses", meses);
            dict.Add("dias", dias);

            return dict;
        }

        [Column("fecha_nacimiento")]
        public string FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }

        [Column("genero")]
        public string Genero { get => genero; set => genero = value; }
    }
}
