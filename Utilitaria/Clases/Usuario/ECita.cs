﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;
using Utilitaria.Clases.Medico;
using Utilitaria.Clases.Usuario;

[Serializable]
[Table("cita", Schema = "usuario")]
public class ECita
{
    public const string ESQUEMA = "usuario";
    public const string TABLA = "cita";

    public const string DISPONIBLE = "disponible";
    public const string RESERVADO = "reservado";
    public const string ASISTIO = "asistio";
    public const string NO_ASISTIO = "no asistio";

    private int id = -1;

    private string horaInicio = DateTime.MinValue.ToLongTimeString();

    private string horaFin = DateTime.MinValue.ToLongTimeString();

    private string fecha = DateTime.MinValue.ToString();

    private string servicio = "";

    private string estado = "";

    private Int64 idMedico = -1;

    private Int64 idUsuario = -1;

    private int idAcceso = -1;

    private EMedico eMedico = EMedico.newEmpty();

    private EUsuario eUsuario = EUsuario.newEmpty();

    public ECita()
    {

    }

    public static ECita newEmpty()
    {
        return new ECita();
    }

    [Key]
    [Column("id")]
    public int Id { get => id; set => id = value; }

    [Column("hora_inicio")]
    public string HoraInicio { get => horaInicio; set => horaInicio = value; }

    [Column("hora_fin")]
    public string HoraFin { get => horaFin; set => horaFin = value; }

    [Column("fecha")]
    public string Fecha { get => fecha; set => fecha = value; }

    [Column("servicio")]
    public string Servicio { get => servicio; set => servicio = value; }

    [Column("estado")]
    public string Estado { get => estado; set => estado = value; }

    [Column("id_medico")]
    public Int64 IdMedico { get => idMedico; set => idMedico = value; }

    [Column("id_usuario")]
    public Int64 IdUsuario { get => idUsuario; set => idUsuario = value; }

    [Column("id_acceso")]
    public int IdAcceso { get => idAcceso; set => idAcceso = value; }

    [NotMapped]
    public EMedico EMedico { get => eMedico; set => eMedico = value; }

    [NotMapped]
    public EUsuario EUsuario { get => eUsuario; set => eUsuario = value; }
}