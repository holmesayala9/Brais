﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;

namespace Utilitaria.Clases.Medico
{
    [Serializable]
    [Table("especialidad", Schema = "medico")]
    public class EEspecialidad
    {
        public const string TABLA = "especialidad";

        public const string ESQUEMA = "medico";

        private int id = -1;

        private string nombre = "Ninguna";

        private int idAcceso = -1;

        public EEspecialidad()
        {
        }

        public static EEspecialidad newEmpty()
        {
            return new EEspecialidad();
        }

        [Key]
        [Column("id")]
        public int Id { get => id; set => id = value; }

        [Column("nombre")]
        public string Nombre { get => nombre; set => nombre = value; }

        [Column("id_acceso")]
        public int IdAcceso { get => idAcceso; set => idAcceso = value; }
    }
}