﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases.Medico
{
    public class ReporteAgenda
    {
        private string hora;

        private string identificacion;

        private string nombre_paciente;

        private string telefono;

        private string correo;

        public ReporteAgenda()
        {

        }

        public string Hora { get => hora; set => hora = value; }

        public string Identificacion { get => identificacion; set => identificacion = value; }

        public string Nombre_paciente { get => nombre_paciente; set => nombre_paciente = value; }

        public string Telefono { get => telefono; set => telefono = value; }

        public string Correo { get => correo; set => correo = value; }
    }
}
