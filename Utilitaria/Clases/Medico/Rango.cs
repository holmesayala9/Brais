﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Rango
/// </summary>
public class Rango
{
    public static List<string> DIAS = new List<string> { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };

    public static List<int> DIAS_NUM = new List<int> { 1, 2, 3, 4, 5, 6, 0 };

    private int dia;

    private string inicio;

    private string fin;

    public Rango()
    {

    }

    public Boolean seCruzaCon(Rango r)
    {
        return estaEntre(r) || r.estaEntre(this) || esIgual(r);
    }

    public Boolean estaEntre(Rango r)
    {
        DateTime inicioA = DateTime.Parse(Inicio);
        DateTime finA = DateTime.Parse(Fin);
        DateTime inicioB = DateTime.Parse(r.Inicio);
        DateTime finB = DateTime.Parse(r.Fin);
        return (inicioB < inicioA && inicioA < finB) || (inicioB < finA && finA < finB);
    }
    
    public Boolean esIgual(Rango r)
    {
        return Dia == r.Dia && Inicio == r.Inicio && Fin == r.Fin;
    }

    public string Inicio
    {
        get
        {
            return inicio;
        }

        set
        {
            inicio = value;
        }
    }

    public string Fin
    {
        get
        {
            return fin;
        }

        set
        {
            fin = value;
        }
    }

    public int Dia
    {
        get
        {
            return dia;
        }

        set
        {
            dia = value;
        }
    }
}