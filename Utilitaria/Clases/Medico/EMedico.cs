﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace Utilitaria.Clases.Medico
{
    [Serializable]
    [Table("medico", Schema = "medico")]
    public class EMedico : UsuarioBasico
    {
        public const string TABLA = "medico";

        public const string ESQUEMA = "medico";

        private Horario horario;

        private string sHorario = "{}";

        private int idEspecialidad = -1;

        private int idConsultorio = -1;

        private EEspecialidad eEspecialidad = EEspecialidad.newEmpty();

        private EConsultorio eConsultorio = EConsultorio.newEmpty();

        public EMedico()
        {
            horario = new Horario();
        }

        public static EMedico newEmpty()
        {
            return new EMedico();
        }
        
        [NotMapped]
        public Horario Horario { get => horario; set => horario = value; }

        [Column("horario")]
        public string SHorario { get => sHorario; set => sHorario = value; }

        [Column("id_especialidad")]
        public int IdEspecialidad { get => idEspecialidad; set => idEspecialidad = value; }

        [Column("id_consultorio")]
        public int IdConsultorio { get => idConsultorio; set => idConsultorio = value; }

        [NotMapped]
        public EEspecialidad EEspecialidad { get => eEspecialidad; set => eEspecialidad = value; }

        [NotMapped]
        public EConsultorio EConsultorio { get => eConsultorio; set => eConsultorio = value; }
    }
}
