﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Web;

[Serializable]
[Table("consultorio", Schema = "medico")]
public class EConsultorio
{
    public const string TABLA = "consultorio";

    public const string ESQUEMA = "medico";

    private int id = -1;

    private string nombre = "Ninguno";

    private string descripcion = "Ninguno";

    private Boolean disponibilidad = false;

    private int idAcceso = -1;

    public EConsultorio()
    {

    }

    public static EConsultorio newEmpty()
    {
        return new EConsultorio();
    }

    [Key]
    [Column("id")]
    public int Id { get => id; set => id = value; }

    [Column("nombre")]
    public string Nombre { get => nombre; set => nombre = value; }

    [Column("descripcion")]
    public string Descripcion { get => descripcion; set => descripcion = value; }

    [Column("disponibilidad")]
    public bool Disponibilidad { get => disponibilidad; set => disponibilidad = value; }

    [Column("id_acceso")]
    public int IdAcceso { get => idAcceso; set => idAcceso = value; }
}