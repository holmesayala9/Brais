﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Horario
/// </summary>
public class Horario
{

    private List<Rango> rangos;

    public Horario()
    {
        Rangos = new List<Rango>();
    }

    public static Horario newEmpty()
    {
        return new Horario();
    }

    public void eliminarRango(Rango rango)
    {
        if (!this.Rangos.Exists(r => r.Dia == rango.Dia && r.Inicio == rango.Inicio && r.Fin == rango.Fin))
            throw new Exception("El rango no esta en el horario"); ;

        this.Rangos.RemoveAll( r => r.Dia == rango.Dia && r.Inicio == rango.Inicio && r.Fin == rango.Fin );
    }

    public void agregarRango(Rango rango)
    {
        if(!Rango.DIAS_NUM.Exists(d => d == rango.Dia))
            throw new Exception("Dia no valido");

        if (DateTime.Parse(rango.Fin) < DateTime.Parse(rango.Inicio))
            throw new Exception("Hora inicio debe ser menor a hora fin");

        if ( validarCruceDeRango(rango) )
        {
            this.Rangos.Add(rango);
        }
        else
        {
            throw new Exception("El rango se cruza");
        }
    }

    public Boolean validarCruceDeRango(Rango rango)
    {
        return !this.Rangos.Exists( r => r.Dia == rango.Dia && (r.esIgual(rango) || r.estaEntre(rango) || rango.estaEntre(r)) );
    }

    public List<Rango> Rangos
    {
        get
        {
            return rangos;
        }

        set
        {
            rangos = value;
        }
    }

    
}