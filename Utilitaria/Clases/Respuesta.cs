﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaria.Clases
{

    public class Respuesta
    {
        public const string NO_REDIRECCIONAR = "";
        public const int ALERTA = 1;
        public const int CORRECTO = 2;
        public const int INFORMATIVO = 3;

        private Respuesta()
        {
            Estado = CORRECTO;
            Mensaje = "";
            Url = Respuesta.NO_REDIRECCIONAR;
            Data = null;
        }

        private Respuesta(int estado, string mensaje, string url, object data)
        {
            this.Estado = estado;
            this.Mensaje = mensaje;
            this.Url = url;
            this.Data = data;
        }

        public static Respuesta newRespuesta()
        {
            return new Respuesta();
        }

        public static Respuesta newRespuesta(int estado, string mensaje, string url, object data)
        {
            return new Respuesta(estado, mensaje, url, data);
        }

        public static Respuesta newRespuestaCorrecto(string mensaje)
        {
            return new Respuesta(Respuesta.CORRECTO, mensaje, NO_REDIRECCIONAR, null);
        }

        public static Respuesta newRespuestaCorrecto(string mensaje, object data)
        {
            return new Respuesta(Respuesta.CORRECTO, mensaje, NO_REDIRECCIONAR, data);
        }

        public static Respuesta newRespuestaInformativo(string mensaje)
        {
            return new Respuesta(Respuesta.INFORMATIVO, mensaje, NO_REDIRECCIONAR, null);
        }

        public static Respuesta newRespuestaAlerta(string mensaje)
        {
            return new Respuesta(Respuesta.ALERTA, mensaje, NO_REDIRECCIONAR, null);
        }

        public int Estado { get; set; }
        public string Mensaje { get; set; }
        public string Url { get; set; }
        public object Data { get; set; }
    }

}
