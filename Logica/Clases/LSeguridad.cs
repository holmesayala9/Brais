﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;

namespace Logica.Clases
{
    public class LSeguridad
    {
        public static Respuesta verificarUsuario(UsuarioBasico usuarioBasico, string recurso)
        {
            if (usuarioBasico == null || !usuarioBasico.Activo)
            {
                return Respuesta.newRespuesta(Respuesta.ALERTA, "Usuario inactivo", "~/View/Principal/Login.aspx", null);
            }

            string tipoUsuario = DAOFuncion.getTipoUsuario(usuarioBasico.Id);

            if (tipoUsuario.Equals("usuario"))
            {
                if (recurso.IndexOf("~/View/Usuario/") == -1)
                {
                    return Respuesta.newRespuesta(Respuesta.ALERTA, "Acceso denegado", "~/View/Principal/Login.aspx", null);
                }
            }
            if (tipoUsuario.Equals("medico"))
            {
                if (recurso.IndexOf("~/View/Medico/") == -1)
                {
                    return Respuesta.newRespuesta(Respuesta.ALERTA, "Acceso denegado", "~/View/Principal/Login.aspx", null);
                }
            }
            if (tipoUsuario.Equals("administrador"))
            {
                if (recurso.IndexOf("~/View/Administrador/") == -1)
                {
                    return Respuesta.newRespuesta(Respuesta.ALERTA, "Acceso denegado", "~/View/Principal/Login.aspx", null);
                }
            }

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta getTipoUsuario(Int64 idUsuario)
        {
            return Respuesta.newRespuestaCorrecto("", DAOFuncion.getTipoUsuario(idUsuario));
        }

        public static Respuesta getTipoUsuario(string identificacion)
        {
            return Respuesta.newRespuestaCorrecto("", DAOFuncion.getTipoUsuario(identificacion));
        }

        public static Respuesta verificarAcceso(int idAcceso, string recurso)
        {
            EAcceso eAcceso = DAOAcceso.get(idAcceso);

            if(eAcceso.FechaFin != "")
            {
                throw new Exception("Acceso no valido");
            }

            string tipoUsuario = DAOFuncion.getTipoUsuario(eAcceso.IdUsuario);

            Boolean tienePermisoSobreElRecurso = true;

            UsuarioBasico usuarioBasico = null;

            if (tipoUsuario.Equals("usuario"))
            {
                if (recurso.IndexOf("/Usuario/") == -1)
                {
                    tienePermisoSobreElRecurso = false;
                }

                usuarioBasico = DAOUsuario.get(eAcceso.IdUsuario);
            }
            else if (tipoUsuario.Equals("medico"))
            {
                if (recurso.IndexOf("/Medico/") == -1)
                {
                    tienePermisoSobreElRecurso = false;
                }

                usuarioBasico = DAOMedico.get(eAcceso.IdUsuario);
            }
            else if (tipoUsuario.Equals("administrador"))
            {
                if (recurso.IndexOf("/Administrador/") == -1)
                {
                    tienePermisoSobreElRecurso = false;
                }

                usuarioBasico = DAOAdministrador.get(eAcceso.IdUsuario);
            }
            else
            {
                throw new Exception("Usuario no valido");
            }

            if( !tienePermisoSobreElRecurso )
            {
                cerrarAcceso(idAcceso);
                throw new Exception("Acceso denegado");
            }

            if (!usuarioBasico.Activo)
            {
                cerrarAcceso(idAcceso);
                throw new Exception("Usuario inactivo");
            }

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static UsuarioBasico renovarUsuario(UsuarioBasico usuarioBasico)
        {
            string tipo = DAOFuncion.getTipoUsuario(usuarioBasico.Id);

            UsuarioBasico nuevo = new UsuarioBasico();

            if(tipo == "usuario")
            {
                nuevo = DAOUsuario.get(usuarioBasico.Id);
            }
            else if(tipo == "medico")
            {
                nuevo = DAOMedico.get(usuarioBasico.Id);
            }
            else if (tipo == "administrador")
            {
                nuevo = DAOAdministrador.get(usuarioBasico.Id);
            }

            nuevo.EAcceso = usuarioBasico.EAcceso;

            return nuevo;
        }

        public static EAcceso getAcceso(int idAcceso)
        {
            return DAOAcceso.get(idAcceso);
        }

        public static void cerrarAcceso(int idAcceso)
        {
            DAOAcceso.cerrarAcceso(idAcceso);
        }

        public static void cerrarSesion(UsuarioBasico usuarioBasico)
        {
            DAOAcceso.cerrarAcceso(usuarioBasico.EAcceso.Id);
        }

        public static void reportarActividad(EAcceso eAcceso)
        {
            eAcceso.UltimaActividad = DateTime.Now.ToString();
            DAOAcceso.update(eAcceso);
        }
    }
}
