﻿using Data.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Logica.Clases.Medico
{
    public abstract class LEspecialidad
    {
        public static Respuesta get(int id)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", "", DAOEspecialidad.get(id));
        }

        public static Respuesta add(EEspecialidad eEspecialidad, EAcceso eAcceso)
        {
            DAOEspecialidad.add(eEspecialidad, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta update(EEspecialidad eEspecialidad, EAcceso eAcceso)
        {
            DAOEspecialidad.update(eEspecialidad, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta delete(int id, EAcceso eAcceso)
        {
            EEspecialidad eEspecialidad = DAOEspecialidad.get(id);

            if (DAOMedico.getAll().Exists( eMedico => eMedico.IdEspecialidad == eEspecialidad.Id ))
            {
                throw new Exception("Un medico la tiene asignada");
            }

            DAOEspecialidad.delete(eEspecialidad, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static List<EEspecialidad> getAll()
        {
            return DAOEspecialidad.getAll();
        }

        public static List<EEspecialidad> buscar(string parametro)
        {
            return DAOEspecialidad.buscar(parametro);
        }
    }
}
