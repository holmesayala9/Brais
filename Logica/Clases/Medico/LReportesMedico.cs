﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;
using Logica.Clases.Administrador;
using Utilitaria.Clases.Usuario;

namespace Logica.Clases.Medico
{
    public class LReportesMedico
    {

        public static List<ReporteAgenda> dataAgenda(Int64 idMedico, string fecha)
        {
            List<ReporteAgenda> reporteAgendas = new List<ReporteAgenda>();

            EMedico eMedico = DAOMedico.get(idMedico);

            List<ECita> eCitas = DAOCita.obtenerCitasDeMedicoPorFecha(eMedico, fecha);

            foreach (ECita eCita in eCitas)
            {
                ReporteAgenda reporteAgenda = new ReporteAgenda();

                EUsuario eUsuario = DAOUsuario.get(eCita.IdUsuario);

                reporteAgenda.Hora = DateTime.Parse(eCita.HoraInicio).ToShortTimeString();
                reporteAgenda.Identificacion = eCita.EUsuario.Identificacion;
                reporteAgenda.Nombre_paciente = eCita.EUsuario.Nombre + " " + eCita.EUsuario.Apellido;
                reporteAgenda.Telefono = eCita.EUsuario.Telefono;
                reporteAgenda.Correo = eCita.EUsuario.Correo;

                reporteAgendas.Add(reporteAgenda);
            }

            return reporteAgendas;
        }

        public static List<ReporteHorario> dataHorario(Horario horario)
        {
            List<ReporteHorario> reporteHorarios = new List<ReporteHorario>();
            string mensaje = "";

            for (int i = 0; i < horario.Rangos.Count; i++)
            {
                String aux = "";
                aux = guardarHoras(horario.Rangos[i]);

                if (aux != "")
                {
                    if (horario.Rangos[i].Dia == 0)
                    {
                        mensaje = mensaje + "/" + "d" + aux;
                    }
                    else if (horario.Rangos[i].Dia == 1)
                    {
                        mensaje = mensaje + "/" + "l" + aux;
                    }
                    else if (horario.Rangos[i].Dia == 2)
                    {
                        mensaje = mensaje + "/" + "t" + aux;
                    }
                    else if (horario.Rangos[i].Dia == 3)
                    {
                        mensaje = mensaje + "/" + "x" + aux;
                    }
                    else if (horario.Rangos[i].Dia == 4)
                    {
                        mensaje = mensaje + "/" + "j" + aux;
                    }
                    else if (horario.Rangos[i].Dia == 5)
                    {
                        mensaje = mensaje + "/" + "v" + aux;
                    }
                    else if (horario.Rangos[i].Dia == 6)
                    {
                        mensaje = mensaje + "/" + "s" + aux;
                    }
                }
            }

            reporteHorarios = llenarInforme(mensaje);
            return reporteHorarios;
        }

        public static List<ReporteHorario> llenarInforme(String mensaje)
        {
            List<ReporteHorario> reporteHorarioAux = new List<ReporteHorario>();

            String[] dias = mensaje.Split('/');

            for (int i = 0; i < dias.Count(); i++)
            {
                ReporteHorario reporteHorarioHoraInicio = new ReporteHorario();
                ReporteHorario reporteHorarioHoraFin = new ReporteHorario();

                String rango = dias[i];
                if (rango != "")
                {
                    if (rango[0] == 'l')
                    {
                        String[] dia = rango.Split('l');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Lunes = separado[0];
                        reporteHorarioHoraFin.Lunes = separado[1];
                    }
                    else if (rango[0] == 't')
                    {
                        String[] dia = rango.Split('t');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Martes = separado[0];
                        reporteHorarioHoraFin.Martes = separado[1];
                    }
                    else if (rango[0] == 'x')
                    {
                        String[] dia = rango.Split('x');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Miercoles = separado[0];
                        reporteHorarioHoraFin.Miercoles = separado[1];
                    }
                    else if (rango[0] == 'j')
                    {
                        String[] dia = rango.Split('j');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Jueves = separado[0];
                        reporteHorarioHoraFin.Jueves = separado[1];
                    }
                    else if (rango[0] == 'v')
                    {
                        String[] dia = rango.Split('v');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Viernes = separado[0];
                        reporteHorarioHoraFin.Viernes = separado[1];
                    }
                    else if (rango[0] == 's')
                    {
                        String[] dia = rango.Split('s');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Sabado = separado[0];
                        reporteHorarioHoraFin.Sabado = separado[1];
                    }
                    else if (rango[0] == 'd')
                    {
                        String[] dia = rango.Split('d');
                        String[] separado = dia[1].Split('-');
                        reporteHorarioHoraInicio.Domingo = separado[0];
                        reporteHorarioHoraFin.Domingo = separado[1];
                    }
                    reporteHorarioAux.Add(reporteHorarioHoraInicio);
                    reporteHorarioAux.Add(reporteHorarioHoraFin);
                }
            }

            List<ReporteHorario> horarios = new List<ReporteHorario>();

            return horarios;
        }

        public static String guardarHoras(Rango rango)
        {
            //String mensaje = "Hora Inicio: " +  DateTime.Parse(rango.Inicio).ToString("hh:mm tt") + "Hora Fin: " + DateTime.Parse(rango.Fin).ToShortTimeString();

            String mensaje = DateTime.Parse(rango.Inicio).ToString("hh:mm tt") + "-" + DateTime.Parse(rango.Fin).ToShortTimeString();
            return mensaje;
        }
    }
}