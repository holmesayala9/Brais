﻿using Logica.Clases.Medico;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Logica.Clases
{
    public abstract class LMedico
    {

        public static Respuesta get(Int64 id)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", "", DAOMedico.get(id));
        }

        public static EMedico get(string identificacion)
        {
            return DAOMedico.get(identificacion);
        }

        public static List<EMedico> getMedicosByEspecialidad(int idEspecialidad)
        {
            return DAOMedico.getMedicosByEspecialidad(idEspecialidad);
        }

        public static Respuesta buscarMedico(string parametro)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOMedico.buscarMedico(parametro));
        }

        public static Respuesta buscarMedicoHabilitado(string parametro)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOMedico.buscarMedico(parametro).FindAll(m => m.Activo));
        }

        public static Respuesta buscarMedicoDeshabilitado(string parametro)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOMedico.buscarMedico(parametro).FindAll(m => !m.Activo));
        }


        public static Respuesta modificarHorario(Horario horario, Int64 idMedico, EAcceso eAcceso)
        {
            EMedico eMedico = DAOMedico.get(idMedico);

            eMedico.Horario = horario;

            DAOMedico.modificarHorario(eMedico, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta modificarHorario(EMedico eMedico, EAcceso eAcceso)
        {
            DAOMedico.modificarHorario(eMedico, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta delete(Int64 id, EAcceso eAcceso)
        {
            if (DAOCita.medicoTieneCitasPendientes(id))
            {
                throw new Exception("Tiene citas pendientes por atender");
            }

            DAOMedico.delete(id, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta desactivarMedico(Int64 idMedico, EAcceso eAcceso)
        {
            if (DAOCita.medicoTieneCitasPendientes(idMedico))
            {
                throw new Exception("Tiene citas pendientes por atender");
            }

            EMedico eMedico = DAOMedico.get(idMedico);
            eMedico.Activo = false;
            DAOMedico.update(eMedico, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta activarMedico(Int64 idMedico, EAcceso eAcceso)
        {
            EMedico eMedico = DAOMedico.get(idMedico);
            eMedico.Activo = true;
            DAOMedico.update(eMedico, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta add(EMedico eMedico, EAcceso eAcceso)
        {
            if (!eMedico.Identificacion.Trim().Equals("") && !DAOFuncion.verificarIdentificacion(eMedico.Id, eMedico.Identificacion))
            {
                throw new Exception("Identificacion ya registrada");
            }

            if (!eMedico.Correo.Trim().Equals("") && !DAOFuncion.verificarCorreo(eMedico.Id, eMedico.Correo))
            {
                throw new Exception("Correo ya registrado");
            }

            if (DAOEspecialidad.get(eMedico.IdEspecialidad).Id == -1)
            {
                throw new Exception("Debe seleccionar la especialidad");
            }

            EConsultorio eConsultorio = DAOConsultorio.get(eMedico.IdConsultorio);

            if (eConsultorio.Id != -1 && !eConsultorio.Disponibilidad)
            {
                throw new Exception("El consultorio no se encuentra disponible");
            }

            EEspecialidad eEspecialidad = DAOEspecialidad.get(eMedico.IdEspecialidad);
            if (eEspecialidad.Id == -1)
            {
                throw new Exception("Especialidad no disponible");
            }

            if (eConsultorio.Id != -1)
            {
                eConsultorio.Disponibilidad = false;
                DAOConsultorio.update(eConsultorio, eAcceso);
            }

            eMedico.Activo = true;
            DAOMedico.agregarMedico(eMedico, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta update(EMedico eMedicoWithNewData, EAcceso eAcceso)
        {
            EMedico eMedicoWithOldData = DAOMedico.get(eMedicoWithNewData.Id);

            if (eMedicoWithNewData.Identificacion.Trim() != "")
            {
                if (!DAOFuncion.verificarIdentificacion(eMedicoWithNewData.Id, eMedicoWithNewData.Identificacion))
                {
                    throw new Exception("Identificacion ya registrada");
                }
                eMedicoWithOldData.Identificacion = eMedicoWithNewData.Identificacion;
            }

            if (eMedicoWithNewData.Correo.Trim() != "")
            {
                if (!DAOFuncion.verificarCorreo(eMedicoWithNewData.Id, eMedicoWithNewData.Correo))
                {
                    throw new Exception("Correo ya registrado");
                }
                eMedicoWithOldData.Correo = eMedicoWithNewData.Correo;
            }

            EConsultorio eConsultorioNew = DAOConsultorio.get(eMedicoWithNewData.IdConsultorio);
            EConsultorio eConsultorioOld = DAOConsultorio.get(eMedicoWithOldData.IdConsultorio);

            if (eConsultorioNew.Id != -1 && !eConsultorioNew.Disponibilidad)
            {
                throw new Exception("El consultorio no se encuentra disponible");
            }

            if (eConsultorioNew.Id != -1)
            {
                eConsultorioNew.Disponibilidad = false;
                DAOConsultorio.update(eConsultorioNew, eAcceso);

                if (eConsultorioOld.Id != -1)
                {
                    eConsultorioOld.Disponibilidad = true;
                    DAOConsultorio.update(eConsultorioOld, eAcceso);
                }
            }

            if (eMedicoWithNewData.IdEspecialidad != -1 && eMedicoWithNewData.IdEspecialidad != eMedicoWithOldData.IdEspecialidad && DAOCita.medicoTieneCitasPendientes(eMedicoWithNewData.Id))
            {
                throw new Exception("Tiene citas pendientes por atender");
            }

            eMedicoWithOldData.Identificacion = eMedicoWithNewData.Identificacion.Trim() != "" ? eMedicoWithNewData.Identificacion.Trim() : eMedicoWithOldData.Identificacion;
            eMedicoWithOldData.Nombre = eMedicoWithNewData.Nombre.Trim() != "" ? eMedicoWithNewData.Nombre.Trim() : eMedicoWithOldData.Nombre;
            eMedicoWithOldData.Apellido = eMedicoWithNewData.Apellido.Trim() != "" ? eMedicoWithNewData.Apellido.Trim() : eMedicoWithOldData.Apellido;
            eMedicoWithOldData.Correo = eMedicoWithNewData.Correo.Trim() != "" ? eMedicoWithNewData.Correo.Trim() : eMedicoWithOldData.Correo;
            eMedicoWithOldData.Telefono = eMedicoWithNewData.Telefono.Trim() != "" ? eMedicoWithNewData.Telefono.Trim() : eMedicoWithOldData.Telefono;
            eMedicoWithOldData.Contrasena = eMedicoWithNewData.Contrasena != "" ? eMedicoWithNewData.Contrasena : eMedicoWithOldData.Contrasena;
            eMedicoWithOldData.IdEspecialidad = DAOEspecialidad.get(eMedicoWithNewData.IdEspecialidad).Id != -1 ? eMedicoWithNewData.IdEspecialidad : eMedicoWithOldData.IdEspecialidad;
            eMedicoWithOldData.IdConsultorio = DAOConsultorio.get(eMedicoWithNewData.IdConsultorio).Id != -1 ? eMedicoWithNewData.IdConsultorio : eMedicoWithOldData.IdConsultorio;

            DAOMedico.update(eMedicoWithOldData, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta modificarDatosBasicos(EMedico eMedicoWithNewData, EAcceso eAcceso)
        {
            EMedico eMedicoWithOldData = DAOMedico.get(eMedicoWithNewData.Id);

            if (eMedicoWithNewData.Identificacion.Trim() != "")
            {
                if (!DAOFuncion.verificarIdentificacion(eMedicoWithNewData.Id, eMedicoWithNewData.Identificacion))
                {
                    throw new Exception("Identificacion ya registrada");
                }
                eMedicoWithOldData.Identificacion = eMedicoWithNewData.Identificacion;
            }

            if (eMedicoWithNewData.Correo.Trim() != "")
            {
                if (!DAOFuncion.verificarCorreo(eMedicoWithNewData.Id, eMedicoWithNewData.Correo))
                {
                    throw new Exception("Correo ya registrado");
                }
                eMedicoWithOldData.Correo = eMedicoWithNewData.Correo;
            }

            eMedicoWithOldData.Identificacion = eMedicoWithNewData.Identificacion.Trim() != "" ? eMedicoWithNewData.Identificacion.Trim() : eMedicoWithOldData.Identificacion;
            eMedicoWithOldData.Nombre = eMedicoWithNewData.Nombre.Trim() != "" ? eMedicoWithNewData.Nombre.Trim() : eMedicoWithOldData.Nombre;
            eMedicoWithOldData.Apellido = eMedicoWithNewData.Apellido.Trim() != "" ? eMedicoWithNewData.Apellido.Trim() : eMedicoWithOldData.Apellido;
            eMedicoWithOldData.Correo = eMedicoWithNewData.Correo.Trim() != "" ? eMedicoWithNewData.Correo.Trim() : eMedicoWithOldData.Correo;
            eMedicoWithOldData.Telefono = eMedicoWithNewData.Telefono.Trim() != "" ? eMedicoWithNewData.Telefono.Trim() : eMedicoWithOldData.Telefono;

            DAOMedico.update(eMedicoWithOldData, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta modificarDatosBasicos(string correo, string telefono, Int64 idMedico, int idAcceso)
        {
            EMedico eMedico = DAOMedico.get(idMedico);

            if (eMedico.Id == -1) throw new Exception("Medico no valido");
            
            if (!DAOFuncion.verificarCorreo(eMedico.Id, correo))
            {
                throw new Exception("Correo ya registrado");
            }

            eMedico.Correo = correo;
            eMedico.Telefono = telefono;

            DAOMedico.update(eMedico, DAOAcceso.get(idAcceso));
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta cambiarEspecialidad(Int64 idMedico, int idEspecialidad, EAcceso eAcceso)
        {
            if(idEspecialidad == -1)
            {
                throw new Exception("Especialidad no valida");
            }

            EMedico eMedico = DAOMedico.get(idMedico);
            eMedico.IdEspecialidad = idEspecialidad;
            DAOMedico.update(eMedico, eAcceso);

            return Respuesta.newRespuestaCorrecto("Correcto");
        }

        public static Respuesta cambiarConsultorio(Int64 idMedico, int idConsultorio, EAcceso eAcceso)
        {
            EConsultorio eConsultorio = DAOConsultorio.get(idConsultorio);

            if (eConsultorio.Id != -1 && !eConsultorio.Disponibilidad)
            {
                throw new Exception("Consultorio no disponible");
            }

            if(eConsultorio.Id != -1)
            {
                eConsultorio.Disponibilidad = false;
                DAOConsultorio.update(eConsultorio, eAcceso);
            }

            EMedico eMedico = DAOMedico.get(idMedico);

            if(eMedico.IdConsultorio != -1)
            {
                EConsultorio cosultorioMedico = DAOConsultorio.get(eMedico.IdConsultorio);
                cosultorioMedico.Disponibilidad = true;
                DAOConsultorio.update(cosultorioMedico, eAcceso);
            }

            eMedico.IdConsultorio = eConsultorio.Id;

            DAOMedico.update(eMedico, eAcceso);

            return Respuesta.newRespuestaCorrecto("Correcto");
        }

        public static Respuesta modificarContrasena(Int64 idMedico, string contrasena, string newContrasena, EAcceso eAcceso)
        {
            EMedico eMedico = DAOMedico.get(idMedico);

            if (eMedico.Contrasena != contrasena)
            {
                throw new Exception("Contraseña incorrecta");
            }

            eMedico.Contrasena = newContrasena;
            DAOMedico.update(eMedico, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta updateContrasena(string contrasenaActual, string nuevaContrasena, Int64 idMedico, EAcceso eAcceso)
        {
            EMedico eMedico = DAOMedico.get(idMedico);

            if (eMedico.Contrasena != contrasenaActual)
            {
                throw new Exception("Contraseña incorrecta");
            }

            eMedico.Contrasena = nuevaContrasena;
            DAOMedico.update(eMedico, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static void validarHorarioSinGuardar(string horarioMedico, string horarioModificado)
        {
            if (horarioModificado != "null")
            {
                if (!horarioMedico.Equals(horarioModificado))
                {
                    throw new Exception("Tiene cambios en el horario sin guardar");
                }
            }
        }

        public static DataTable cargarHorarioInicio(Parametro parametro,DateTime dateTime)
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("HORA");
            tabla.Columns.Add("INDICE");
            DataRow Fila;

            int i = 1;
         
            while (dateTime < DateTime.Parse(parametro.HoraFin))
            {
                
                Fila = tabla.NewRow();
                Fila["HORA"] = dateTime.ToShortTimeString();
                Fila["INDICE"] = i;
                tabla.Rows.Add(Fila);
        
                dateTime = dateTime.AddHours(1);
                i++;
            }

            return tabla;
        }

        public static DataTable cargarHorarioFin(Parametro parametro, DateTime dateTime)
        {

            DataTable tabla = new DataTable();
            tabla.Columns.Add("HORA");
            tabla.Columns.Add("INDICE");
            DataRow Fila;

            int i = 1;

            while (dateTime <= DateTime.Parse(parametro.HoraFin))
            {

                Fila = tabla.NewRow();
                Fila["HORA"] = dateTime.ToShortTimeString();
                Fila["INDICE"] = i;
                tabla.Rows.Add(Fila);

                dateTime = dateTime.AddHours(1);
                i++;
            }

            return tabla;
        }

    }
}
