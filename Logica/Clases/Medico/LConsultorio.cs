﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;

namespace Logica.Clases.Medico
{
    public abstract class LConsultorio
    {
        public static Respuesta add(EConsultorio eConsultorio, EAcceso eAcceso)
        {
            eConsultorio.Disponibilidad = true;
            DAOConsultorio.add(eConsultorio, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta get(int id)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", "", DAOConsultorio.get(id));
        }

        public static List<EConsultorio> getAll()
        {
            return DAOConsultorio.getAll();
        }

        public static List<EConsultorio> buscar(string parametro)
        {
            return DAOConsultorio.buscar(parametro);
        }

        public static Respuesta update(EConsultorio newEConsultorio, EAcceso eAcceso)
        {
            if (newEConsultorio.Disponibilidad && DAOMedico.getAll().Exists(eMedico => eMedico.IdConsultorio == newEConsultorio.Id))
            {
                throw new Exception("Un medico tiene asignado el consultorio");
            }

            DAOConsultorio.update(newEConsultorio, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta delete(int id, EAcceso eAcceso)
        {
            EConsultorio eConsultorio = DAOConsultorio.get(id);

            if (DAOMedico.getAll().Exists(eMedico => eMedico.IdConsultorio == eConsultorio.Id))
            {
                throw new Exception("Un medico tiene asignado el consultorio");
            }

            DAOConsultorio.delete(eConsultorio.Id, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta getDisponibles()
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", "", DAOConsultorio.getAll().FindAll(x => x.Disponibilidad));
        }

        public static Respuesta liberarConsultorioDe(EMedico eMedico, EAcceso eAcceso)
        {
            EConsultorio eConsultorio = DAOConsultorio.get(eMedico.IdConsultorio);

            if (eConsultorio.Id == -1)
            {
                return Respuesta.newRespuesta(Respuesta.INFORMATIVO, "No tiene un consultorio asignado", "", null);
            }

            eConsultorio.Disponibilidad = true;
            DAOConsultorio.update(eConsultorio, eAcceso);

            eMedico.IdConsultorio = -1;
            DAOMedico.update(eMedico, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Consultorio liberado", "", null);
        }
    }
}
