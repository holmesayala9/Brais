﻿using Data.Clases.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases.Seguridad;

namespace Logica.Clases.Seguridad
{
    public class LTokenUsuario
    {
        public LTokenUsuario()
        {

        }

        public void add(TokenUsuario tokenUsuario)
        {
            DAOTokenUsuario dAOTokenUsuario = new DAOTokenUsuario();
            dAOTokenUsuario.add(tokenUsuario);
        }

        public TokenUsuario get(string token)
        {
            DAOTokenUsuario dAOTokenUsuario = new DAOTokenUsuario();
            return dAOTokenUsuario.get(token);
        }
    }
}
