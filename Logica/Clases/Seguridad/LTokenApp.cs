﻿using Data.Clases.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases.Seguridad;

namespace Logica.Clases.Seguridad
{
    public class LTokenApp
    {
        public LTokenApp()
        {

        }

        public void add(TokenApp tokenApp)
        {
            DAOTokenApp.add(tokenApp);
        }

        public TokenApp get(string token)
        {
            return DAOTokenApp.get(token);
        }

    }
}
