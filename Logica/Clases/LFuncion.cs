﻿
using System;
using Utilitaria.Clases;
using Data.Clases;
using System.Collections.Generic;
using Data.Clases.Administrador;
using Utilitaria.Clases.Usuario;
using Utilitaria.Clases.Medico;

namespace Logica.Clases
{
    public class LFuncion
    {

        public static Respuesta iniciarSesion(string identificacion, string contrasena)
        {
            string tipoUsuario = DAOFuncion.getTipoUsuario(identificacion);

            UsuarioBasico usuarioBasico = null;

            Respuesta respuesta = Respuesta.newRespuesta();

            if (tipoUsuario.Equals("usuario"))
            {
                usuarioBasico = DAOUsuario.get(identificacion);
                //respuesta.Url = "~/View/Usuario/AsignarCita.aspx";
                respuesta.Mensaje = "usuario";
            }
            else if (tipoUsuario.Equals("medico"))
            {
                usuarioBasico = DAOMedico.get(identificacion);
                //respuesta.Url = "~/View/Medico/Agenda.aspx";
                respuesta.Mensaje = "medico";
            }
            else if (tipoUsuario.Equals("administrador"))
            {
                usuarioBasico = DAOAdministrador.get(identificacion);
                //respuesta.Url = "~/View/Administrador/VerUsuarios.aspx";
                respuesta.Mensaje = "administrador";
            }
            else
            {
                throw new Exception("Identificacion o contraseña incorrecto");
            }

            verificarEstadoIngreso(usuarioBasico, contrasena);

            if (usuarioBasico.Contrasena.Equals(contrasena))
            {
                usuarioBasico.EAcceso = DAOAcceso.registrarYObtenerAcceso(usuarioBasico.Id);

                respuesta.Data = usuarioBasico;
                respuesta.Estado = Respuesta.CORRECTO;
                return respuesta;
            }

            throw new Exception("Identificacion o contraseña incorrecto");
        }

        public static Respuesta iniciarSesionMVC(string identificacion, string contrasena)
        {
            string tipoUsuario = DAOFuncion.getTipoUsuario(identificacion);

            UsuarioBasico usuarioBasico = null;

            Respuesta respuesta = Respuesta.newRespuesta();

            if (tipoUsuario.Equals("usuario"))
            {
                usuarioBasico = DAOUsuario.get(identificacion);
                respuesta.Mensaje = "usuario";
            }
            else if (tipoUsuario.Equals("medico"))
            {
                usuarioBasico = DAOMedico.get(identificacion);
                respuesta.Mensaje = "medico";
            }
            else if (tipoUsuario.Equals("administrador"))
            {
                usuarioBasico = DAOAdministrador.get(identificacion);
                respuesta.Mensaje = "administrador";
            }
            else
            {
                throw new Exception("Identificacion o contraseña incorrecto");
            }

            verificarEstadoIngreso(usuarioBasico, contrasena);

            if (usuarioBasico.Contrasena.Equals(contrasena))
            {
                respuesta.Data = DAOAcceso.registrarYObtenerAcceso(usuarioBasico.Id);
                respuesta.Estado = Respuesta.CORRECTO;
                return respuesta;
            }

            throw new Exception("Identificacion o contraseña incorrecto");
        }

        public static void verificarEstadoIngreso(UsuarioBasico usuarioBasico, string contrasena)
        {
            string tipoUsuario = DAOFuncion.getTipoUsuario(usuarioBasico.Id);

            EEstadoIngreso eEstadoIngreso = DAOEstadoIngreso.getAll().Find(i => i.IdUsuario == usuarioBasico.Id);

            List<EAcceso> accesosAbiertos = DAOAcceso.getAll().FindAll(a => a.IdUsuario == usuarioBasico.Id && a.FechaFin == "");

            foreach (EAcceso eAcceso in accesosAbiertos)
            {
                DateTime ultimaActividad = DateTime.Parse(eAcceso.UltimaActividad);
                if ((DateTime.Now - ultimaActividad).TotalMinutes > 30)
                {
                    eAcceso.FechaFin = eAcceso.UltimaActividad;
                    DAOAcceso.update(eAcceso);
                }
            }

            int sesionesAbiertas = DAOAcceso.getAll().FindAll(a => a.IdUsuario == usuarioBasico.Id && a.FechaFin == "").Count;

            if (eEstadoIngreso == null)
            {
                DAOEstadoIngreso.add(usuarioBasico.Id);
                eEstadoIngreso = DAOEstadoIngreso.getAll().Find(i => i.IdUsuario == usuarioBasico.Id);
                String fecha = DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss");
                eEstadoIngreso.FechaBloqueo = fecha;
                DAOEstadoIngreso.update(eEstadoIngreso);
            }

            Parametro parametro = DAOParametro.get();

            double minutosEspera = Math.Ceiling((DateTime.Parse(eEstadoIngreso.FechaBloqueo) - DateTime.Now).TotalMinutes);

            if (minutosEspera > 0)
            {
                throw new Exception("Su cuenta esta bloqueada");
            }

            if (usuarioBasico.Contrasena != contrasena)
            {
                eEstadoIngreso.CantidadIntentos++;
                DAOEstadoIngreso.update(eEstadoIngreso);

                if (eEstadoIngreso.CantidadIntentos >= parametro.IntentosIngreso)
                {
                    eEstadoIngreso.CantidadIntentos = 0;
                    String fechaBloqueo = DateTime.Now.AddMinutes(30).ToString("yyyy-MM-dd HH:mm:ss");
                    eEstadoIngreso.FechaBloqueo = fechaBloqueo;

                    DAOEstadoIngreso.update(eEstadoIngreso);

                    throw new Exception("Cuenta bloqueada por 30 minutos");
                }
            }
            else if ((tipoUsuario == "administrador" && sesionesAbiertas >= 1) || sesionesAbiertas >= parametro.Sesiones)
            {
                throw new Exception("Ha superado la cantidad de sesiones activas");
            }

            if (usuarioBasico.Contrasena == contrasena)
            {
                eEstadoIngreso.CantidadIntentos = 0;
                DAOEstadoIngreso.update(eEstadoIngreso);
            }
        }

        public static Respuesta solicitarRestablecerContrasena(string identificacion, string correo)
        {
            string tipoUsuario = DAOFuncion.getTipoUsuario(identificacion);

            UsuarioBasico usuarioBasico;

            if (tipoUsuario.Equals("usuario"))
            {
                usuarioBasico = DAOUsuario.get(identificacion);
            }
            else if (tipoUsuario.Equals("medico"))
            {
                usuarioBasico = DAOMedico.get(identificacion);
            }
            else if (tipoUsuario.Equals("administrador"))
            {
                usuarioBasico = DAOAdministrador.get(identificacion);
            }
            else
            {
                throw new Exception("Identificacion o correo no valido");
            }

            if (!usuarioBasico.Identificacion.Equals(identificacion) || !usuarioBasico.Correo.Equals(correo))
            {
                throw new Exception("Identificacion o correo no valido");
            }

            ESolicitudContrasena eSolicitudContrasena = DAOSeguridad.getSolicitudContrasena(usuarioBasico.Id);

            if (eSolicitudContrasena != null)
            {
                if ((DateTime.Now - DateTime.Parse(eSolicitudContrasena.Fecha)).TotalMinutes <= 30)
                {
                    return Respuesta.newRespuesta(Respuesta.ALERTA, "Ya dispone de una solicitud valida enviada a su correo", "", null);
                }
            }

            string hash = Utilidades.encriptar(usuarioBasico.Id.ToString() + DateTime.Now.ToLongDateString());

            string mensaje = "Su link para restablecer su contraseña: " + "http://35.194.166.98/RecuperarContrasenaPasoDos/RecuperarContrasenaPasoDos?hash=" + hash;

            Utilidades.enviarCorreo(usuarioBasico.Correo, "Brais -> Restablecer contraseña.", mensaje);

            DAOSeguridad.solicitarRestablecerContrasena(usuarioBasico.Id, hash);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Se ha enviado informacion a su correo", "", null);
        }

        public static Respuesta restablecerContrasena(string hash, string contrasena)
        {
            ESolicitudContrasena eSolicitudContrasena = DAOSeguridad.getSolicitudContrasena(hash);

            if (eSolicitudContrasena == null || (DateTime.Now - DateTime.Parse(eSolicitudContrasena.Fecha)).TotalMinutes > 30)
            {
                throw new Exception("No dispone de una solicitud valida");
            }

            string tipoUsuario = DAOFuncion.getTipoUsuario(eSolicitudContrasena.IdUsuario);

            UsuarioBasico usuarioBasico = new UsuarioBasico();

            if (tipoUsuario.Equals("usuario"))
            {
                usuarioBasico = DAOUsuario.get(eSolicitudContrasena.IdUsuario);
            }
            else if (tipoUsuario.Equals("medico"))
            {
                usuarioBasico = DAOMedico.get(eSolicitudContrasena.IdUsuario);
            }
            else if (tipoUsuario.Equals("administrador"))
            {
                usuarioBasico = DAOAdministrador.get(eSolicitudContrasena.IdUsuario);
            }
            else
            {
                throw new Exception("No dispone de una solicitud valida");
            }

            usuarioBasico.Contrasena = contrasena;

            EAcceso eAcceso = new EAcceso();
            eAcceso.Id = -1;

            if (tipoUsuario.Equals("usuario"))
            {
                DAOUsuario.update((EUsuario)usuarioBasico, eAcceso);
            }
            else if (tipoUsuario.Equals("medico"))
            {
                DAOMedico.update((EMedico)usuarioBasico, eAcceso);
            }
            else if (tipoUsuario.Equals("administrador"))
            {
                DAOAdministrador.update((EAdministrador)usuarioBasico, eAcceso);
            }

            DAOSeguridad.removeSolicitudContrasena(eSolicitudContrasena.Id);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static void validarSession(EMedico eMedico)
        {
            if (eMedico == null && eMedico.GetType() != typeof(EMedico))
            {
                throw new Exception();
            }
        }

        public static void validarSessionUsuario(EUsuario eUsuario)
        {
            if (eUsuario == null && eUsuario.GetType() != typeof(EUsuario))
            {
                throw new Exception();
            }
        }

        public static EMedico validar_postback_medico(EMedico medico, bool postb)
        {
            EMedico medic2 = new EMedico();

            if (postb == false)
            {

                medic2.Horario = medico.Horario;
            }
            else
            {
                throw new Exception();
            }

            return medic2;
        }

        public static EAdministrador validar_postback_administrador(EAdministrador eAdministrador, bool postb)
        {
            EAdministrador auxiliar = new EAdministrador();

            if (postb == false)
            {

                auxiliar.Identificacion = eAdministrador.Identificacion;
            }
            else
            {
                throw new Exception();
            }

            return auxiliar;
        }

        public static EUsuario validar_postback_usuario(EUsuario eUsuario, bool postb)
        {
            EUsuario auxiliar = new EUsuario();

            if (postb == false)
            {
                auxiliar.Identificacion = eUsuario.Identificacion;
            }
            else
            {
                throw new Exception();
            }

            return auxiliar;
        }

        public static void validarSessionAdministrador(EAdministrador eAdministrador)
        {
            if (eAdministrador == null && eAdministrador.GetType() != typeof(EAdministrador))
            {
                throw new Exception();
            }
        }

        public EUsuario validarUsuarioExistente(string Identificacion, string Contrasena)
        {
            Boolean resultado = DAOFuncion.validarInicioSesion(Identificacion, Contrasena);

            if (resultado.Equals(false))
            {
                throw new Exception("Identificacion o contraseña incorrecto");
            }
            else
            {
                return DAOUsuario.get(Identificacion);
            }
        }
    }
}
