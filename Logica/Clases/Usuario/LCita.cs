﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Medico;
using Utilitaria.Clases.Usuario;

namespace Logica.Clases.Usuario
{
    public abstract class LCita
    {
        public static Respuesta getCitasVigentesDeUsuario(Int64 idUsuario)
        {
            List<ECita> eCitas = DAOCita.obtenerCitasDeUsuario(idUsuario).FindAll( c => DateTime.Parse(c.Fecha) >= DateTime.Today );

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, eCitas);
        }

        public static Respuesta getAgendaDeMedicoPorFecha(EMedico eMedico, string fecha)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOCita.obtenerCitasDeMedicoPorFecha(eMedico, fecha));
        }

        public static Respuesta getAgendaDeMedicoPorFecha(Int64 idMedico, string fecha)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOCita.obtenerCitasDeMedicoPorFecha(idMedico, fecha));
        }

        public static Respuesta get(int id)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOCita.get(id));
        }

        public static Respuesta update(ECita eCita, EAcceso eAcceso)
        {
            DAOCita.update(eCita, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta cancelarCita(int idCita, EAcceso eAcceso)
        {
            ECita eCita = DAOCita.get(idCita);

            if ( !(DateTime.Parse(eCita.Fecha + " " + eCita.HoraInicio).AddHours(-1) >= DateTime.Now) )
            {
                throw new Exception("Solo puede cancelar con una hora de anticipacion");
            }
            if (eCita.Estado != ECita.RESERVADO)
            {
                throw new Exception("No se encuentra en estado reservado");
            }
            if (DAOHistorial.obtenerHistorialPorCita(eCita.Id).Id != -1)
            {
                throw new Exception("Ya tiene un historial asignado");
            }

            eCita.IdUsuario = -1;
            eCita.Estado = ECita.DISPONIBLE;

            DAOCita.update(eCita, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta verificarReprogramacionDeCita(int idCita)
        {
            ECita eCita = DAOCita.get(idCita);

            if ( !(DateTime.Parse(eCita.Fecha+" "+ eCita.HoraInicio).AddHours(-1) >= DateTime.Now) )
            {
                throw new Exception("Solo puede reprogramar con una hora de anticipacion");
            }
            if ( eCita.Estado != ECita.RESERVADO )
            {
                throw new Exception("No se encuentra en estado reservado");
            }
            if (DAOHistorial.obtenerHistorialPorCita(eCita.Id).Id != -1)
            {
                throw new Exception("Ya tiene un historial asignado");
            }

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta reprogramarCita(int idNewCita, int idOldCita, Int64 idUsuario, EAcceso eAcceso)
        {
            verificarReprogramacionDeCita(idOldCita);

            verificarCita(idNewCita);

            ECita newECita = DAOCita.get(idNewCita);

            ECita oldCita = DAOCita.get(idOldCita);

            oldCita.Estado = ECita.DISPONIBLE;
            oldCita.IdUsuario = -1;

            newECita.Estado = ECita.RESERVADO;
            newECita.IdUsuario = idUsuario;

            DAOCita.update(newECita, eAcceso);
            DAOCita.update(oldCita, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta getFechasActualesDeCitasSegunEspecialidad( int idEspecialidad )
        {
            List<ECita> eCitas = DAOCita.obtenerCitasActualesSegunEspecialidad(idEspecialidad);

            List<DateTime> fechas = new List<DateTime>();

            foreach (ECita eCita in eCitas)
            {
                DateTime dateTime = DateTime.Parse(eCita.Fecha);

                if (!fechas.Exists( f => f == dateTime ))
                {
                    fechas.Add(dateTime);
                }
            }

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, fechas);
        }

        public static Respuesta getCitasDisponiblesParaUsuario(Int64 idUsuario, int idEspecialidad, string fecha)
        {
            EUsuario eUsuario = DAOUsuario.get(idUsuario);

            List<ECita> citasDeUsuarioSegunFecha = DAOCita.obtenerCitasDeUsuario(eUsuario.Id).FindAll(c => DateTime.Parse(c.Fecha) == DateTime.Parse(fecha));

            List<ECita> citasSegunEspecialidadYFecha = DAOCita.obtenerCitasActualesSegunEspecialidad(idEspecialidad).FindAll(eCita => DateTime.Parse(eCita.Fecha) == DateTime.Parse(fecha));

            List<ECita> citasValidas = new List<ECita>();

            foreach (ECita cita in citasSegunEspecialidadYFecha)
            {
                Rango rangoA = new Rango();
                rangoA.Inicio = cita.HoraInicio;
                rangoA.Fin = cita.HoraFin;
                rangoA.Dia = (int)DateTime.Parse(cita.Fecha).DayOfWeek;

                Boolean citaValida = true;

                foreach (ECita citaUsuario in citasDeUsuarioSegunFecha)
                {
                    Rango rangoB = new Rango();
                    rangoB.Inicio = citaUsuario.HoraInicio;
                    rangoB.Fin = citaUsuario.HoraFin;
                    rangoB.Dia = (int)DateTime.Parse(citaUsuario.Fecha).DayOfWeek;

                    if (rangoA.seCruzaCon(rangoB))
                    {
                        citaValida = false;
                        break;
                    }
                }

                if (citaValida) { citasValidas.Add(cita); }
            }

            if (DateTime.Parse(fecha) == DateTime.Today)
            {
                citasValidas = citasValidas.FindAll(c => DateTime.Parse(c.HoraInicio).AddHours(-1) >= DateTime.Now);
            }

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, citasValidas);
        }

        public static Respuesta verificarCita(int idCita)
        {
            ECita eCita = DAOCita.get(idCita);

            if (eCita.Id == -1 || eCita.Estado != ECita.DISPONIBLE)
            {
                throw new Exception("Cita no disponible");
            }

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta reservarCita(int idCita, Int64 idUsuario, EAcceso eAcceso)
        {
            verificarCita(idCita);

            ECita eCita = DAOCita.get(idCita);

            List<ECita> eCitas = DAOCita.obtenerCitasDeUsuario(idUsuario).FindAll( c => DateTime.Parse(c.Fecha) >= DateTime.Today && eCita.Servicio.ToLower().Equals(c.Servicio.ToLower()) );

            if(eCitas.Count > 0)
            {
                throw new Exception("Solo puede reservar de a una cita por especialidad");
            }
            
            eCita.IdUsuario = idUsuario;
            eCita.Estado = ECita.RESERVADO;

            DAOCita.update(eCita, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta generarcCitasMedico(EMedico eMedico, EAcceso eAcceso)
        {
            DAOCita.generarCitasDeMedico(eMedico.Id, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Citas generadas segun su horario", Respuesta.NO_REDIRECCIONAR, null);
        }
    }
}
