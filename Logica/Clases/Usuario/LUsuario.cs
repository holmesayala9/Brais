﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Logica.Clases
{
    public class LUsuario
    {
        public static Respuesta agregarUsuario(EUsuario eUsuario, EAcceso eAcceso)
        {

            if (eUsuario != null && eAcceso != null)
            {
                if (eUsuario.Identificacion.Trim() != "")
                {
                    if (!DAOFuncion.verificarIdentificacion(eUsuario.Id, eUsuario.Identificacion))
                    {
                        throw new Exception("Identificacion ya registrada");
                    }
                }

                if (DateTime.Parse(eUsuario.FechaNacimiento.Trim()) > DateTime.Now)
                {
                    throw new Exception("Su fecha de nacimiento debe ser menor a la fecha actual");
                }

                if (eUsuario.Correo.Trim() != "")
                {
                    if (!DAOFuncion.verificarCorreo(eUsuario.Id, eUsuario.Correo))
                    {
                        throw new Exception("Correo ya registrado");
                    }
                }

                eUsuario.Activo = true;
                eUsuario.FechaNacimiento = DateTime.Parse(eUsuario.FechaNacimiento).ToString("dd/MM/yyyy");
                DAOUsuario.add(eUsuario, eAcceso);
                return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
            }
            return Respuesta.newRespuesta(Respuesta.ALERTA, "Error", "", null);
        }

        public static Respuesta buscarUsuario(string parametro)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOUsuario.buscarUsuario(parametro));
        }

        public static Respuesta buscarUsuarioHabilitado(string parametro)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOUsuario.buscarUsuario(parametro).FindAll(u => u.Activo));
        }

        public static Respuesta buscarUsuarioDeshabilitado(string parametro)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOUsuario.buscarUsuario(parametro).FindAll(u => !u.Activo));
        }

        public static Respuesta desactivarUsuario(Int64 id, EAcceso eAcceso)
        {
            EUsuario eUsuario = DAOUsuario.get(id);
            eUsuario.Activo = false;
            DAOUsuario.update(eUsuario, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta activarUsuario(Int64 id, EAcceso eAcceso)
        {
            EUsuario eUsuario = DAOUsuario.get(id);
            eUsuario.Activo = true;
            DAOUsuario.update(eUsuario, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta eliminarUsuario(Int64 id, EAcceso eAcceso)
        {
            DAOUsuario.delete(id, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static EUsuario obtenerUsuario(Int64 id)
        {
            EUsuario eUsuario = new EUsuario();
            eUsuario = DAOUsuario.get(id);

            return eUsuario;
        }

        public static Respuesta modificarUsuario(EUsuario eUsuarioWithNewData, EAcceso eAcceso)
        {
            EUsuario eUsuarioWithOldData = DAOUsuario.get(eUsuarioWithNewData.Id);

            if (eUsuarioWithNewData.Identificacion.Trim() != "")
            {
                if (!DAOFuncion.verificarIdentificacion(eUsuarioWithNewData.Id, eUsuarioWithNewData.Identificacion))
                {
                    throw new Exception("Identificacion ya registrada");
                }
                eUsuarioWithOldData.Identificacion = eUsuarioWithNewData.Identificacion;
            }
            if (eUsuarioWithNewData.FechaNacimiento.ToString() != "")
            {
                if (DateTime.Parse(eUsuarioWithNewData.FechaNacimiento.Trim()) >= DateTime.Today)
                {
                    throw new Exception("Su fecha de nacimiento debe ser menor a la fecha actual");
                }
            }

            if (eUsuarioWithNewData.Correo.Trim() != "")
            {
                if (!DAOFuncion.verificarCorreo(eUsuarioWithNewData.Id, eUsuarioWithNewData.Correo))
                {
                    throw new Exception("Correo ya registrado");
                }
                eUsuarioWithOldData.Correo = eUsuarioWithNewData.Correo;
            }

            eUsuarioWithOldData.Nombre = eUsuarioWithNewData.Nombre.Trim() != "" ? eUsuarioWithNewData.Nombre.Trim() : eUsuarioWithOldData.Nombre;
            eUsuarioWithOldData.Apellido = eUsuarioWithNewData.Apellido.Trim() != "" ? eUsuarioWithNewData.Apellido.Trim() : eUsuarioWithOldData.Apellido;
            eUsuarioWithOldData.FechaNacimiento = eUsuarioWithNewData.FechaNacimiento.Trim() != "" ? eUsuarioWithNewData.FechaNacimiento.Trim() : eUsuarioWithOldData.FechaNacimiento;
            eUsuarioWithOldData.Genero = eUsuarioWithNewData.Genero.Trim() != "" ? eUsuarioWithNewData.Genero.Trim() : eUsuarioWithOldData.Genero;
            eUsuarioWithOldData.Telefono = eUsuarioWithNewData.Telefono.Trim() != "" ? eUsuarioWithNewData.Telefono.Trim() : eUsuarioWithOldData.Telefono;
            eUsuarioWithOldData.Correo = eUsuarioWithNewData.Correo.Trim() != "" ? eUsuarioWithNewData.Correo.Trim() : eUsuarioWithOldData.Correo;
            eUsuarioWithOldData.Contrasena = eUsuarioWithNewData.Contrasena.Trim() != "" ? eUsuarioWithNewData.Contrasena.Trim() : eUsuarioWithOldData.Contrasena;
            DAOUsuario.update(eUsuarioWithOldData, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta modificarDatosBasicos(string correo, string telefono, Int64 idUsuario, int idAcceso)
        {
            EUsuario eUsuario = DAOUsuario.get(idUsuario);

            if (eUsuario.Id == -1) throw new Exception("Usuario no valido");

            if (!DAOFuncion.verificarCorreo(eUsuario.Id, correo))
            {
                throw new Exception("Correo ya registrado");
            }

            eUsuario.Correo = correo;
            eUsuario.Telefono = telefono;

            DAOUsuario.update(eUsuario, DAOAcceso.get(idAcceso));
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta modificarDatosBasicos(EUsuario eUsuarioWithNewData, EAcceso eAcceso)
        {
            EUsuario eUsuarioWithOldData = DAOUsuario.get(eUsuarioWithNewData.Id);

            if (eUsuarioWithNewData.Identificacion.Trim() != "")
            {
                if (!DAOFuncion.verificarIdentificacion(eUsuarioWithNewData.Id, eUsuarioWithNewData.Identificacion))
                {
                    throw new Exception("Identificacion ya registrada");
                }
                eUsuarioWithOldData.Identificacion = eUsuarioWithNewData.Identificacion;
            }
            if (eUsuarioWithNewData.FechaNacimiento.ToString() != "")
            {
                if (DateTime.Parse(eUsuarioWithNewData.FechaNacimiento.Trim()) >= DateTime.Today)
                {
                    throw new Exception("Su fecha de nacimiento debe ser menor a la fecha actual");
                }
            }

            if (eUsuarioWithNewData.Correo.Trim() != "")
            {
                if (!DAOFuncion.verificarCorreo(eUsuarioWithNewData.Id, eUsuarioWithNewData.Correo))
                {
                    throw new Exception("Correo ya registrado");
                }
                eUsuarioWithOldData.Correo = eUsuarioWithNewData.Correo;
            }

            eUsuarioWithOldData.Nombre = eUsuarioWithNewData.Nombre.Trim() != "" ? eUsuarioWithNewData.Nombre.Trim() : eUsuarioWithOldData.Nombre;
            eUsuarioWithOldData.Apellido = eUsuarioWithNewData.Apellido.Trim() != "" ? eUsuarioWithNewData.Apellido.Trim() : eUsuarioWithOldData.Apellido;
            eUsuarioWithOldData.FechaNacimiento = eUsuarioWithNewData.FechaNacimiento.Trim() != "" ? eUsuarioWithNewData.FechaNacimiento.Trim() : eUsuarioWithOldData.FechaNacimiento;
            eUsuarioWithOldData.Genero = eUsuarioWithNewData.Genero.Trim() != "" ? eUsuarioWithNewData.Genero.Trim() : eUsuarioWithOldData.Genero;
            eUsuarioWithOldData.Telefono = eUsuarioWithNewData.Telefono.Trim() != "" ? eUsuarioWithNewData.Telefono.Trim() : eUsuarioWithOldData.Telefono;
            eUsuarioWithOldData.Correo = eUsuarioWithNewData.Correo.Trim() != "" ? eUsuarioWithNewData.Correo.Trim() : eUsuarioWithOldData.Correo;
            eUsuarioWithOldData.FechaNacimiento = DateTime.Parse(eUsuarioWithOldData.FechaNacimiento).ToString("dd/MM/yyyy");
            DAOUsuario.update(eUsuarioWithOldData, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta modificarContrasena(Int64 idUsuario, string contrasena, string newContrasena, EAcceso eAcceso)
        {
            EUsuario eUsuario = DAOUsuario.get(idUsuario);

            if (eUsuario.Contrasena.Equals(contrasena))
            {
                eUsuario.Contrasena = newContrasena;
                DAOUsuario.update(eUsuario, eAcceso);
                return Respuesta.newRespuestaCorrecto("Correcto");
            }

            throw new Exception("Contraseña incorrecta");
        }

        public static Respuesta validarMensaje(String mensaje)
        {
            Respuesta respuesta = Respuesta.newRespuesta();

            if (mensaje != null)
            {
                respuesta.Mensaje = mensaje;
                respuesta.Estado = Respuesta.CORRECTO;
                
            }
            else
            {
                respuesta.Mensaje = "";
            }
            return respuesta;
        }

        public static EAcceso obtenerAcceso(Int64 id)
        {
            EAcceso eAcceso = DAOAcceso.registrarYObtenerAcceso(id);
            return eAcceso;
        }
    }

}