﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Utilitaria.Clases.Usuario;

namespace Logica.Clases.Usuario
{
    public abstract class LHistorial
    {
        public static Respuesta add(EHistorial eHistorial, EAcceso eAcceso)
        {
            DAOHistorial.add(eHistorial, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta add(string datos, int idCita, int idAcceso)
        {
            ECita eCita = DAOCita.get(idCita);

            if (eCita.Id == -1 || eCita.IdUsuario == -1 || eCita.IdMedico == -1) throw new Exception("Cita no disponible");

            EHistorial eHistorial = new EHistorial();
            eHistorial.Datos = datos;
            eHistorial.IdUsuario = eCita.IdUsuario;
            eHistorial.IdCita = eCita.Id;

            DAOHistorial.add(eHistorial, DAOAcceso.get(idAcceso));

            return Respuesta.newRespuestaCorrecto("Correcto");
        }

        public static Respuesta update(EHistorial eHistorial, EAcceso eAcceso)
        {
            DAOHistorial.update(eHistorial, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta update(string datos, int idHistorial, int idAcceso)
        {
            EHistorial eHistorial = DAOHistorial.get(idHistorial);

            if (eHistorial.Id == -1) throw new Exception("Historial no valido");

            eHistorial.Datos = datos;

            DAOHistorial.update(eHistorial, DAOAcceso.get(idAcceso));
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta getByIdCita(int idCita)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOHistorial.obtenerHistorialPorCita(idCita));
        }

        public static Respuesta getByIdUsuario(Int64 idUsuario)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOHistorial.obtenerHistorialPorUsuario(idUsuario));
        }

        public static Respuesta getByIdentificacionAndIdEspecialidad(string identificacion, int idEspecialidad)
        {
            if (DAOFuncion.getTipoUsuario(identificacion).Equals("usuario"))
            {
                EUsuario eUsuario = DAOUsuario.get(identificacion);

                List<EHistorial> list = DAOHistorial.obtenerHistorialPorUsuario(eUsuario.Id);

                list = list.FindAll(x => x.ECita.EMedico.EEspecialidad.Id == idEspecialidad);

                return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, list);
            }
            else
            {
                throw new Exception("Usuario no valido");
            }
        }

        public static Respuesta getByIdUsuarioAndIdEspecialidad(Int64 idUsuario, int idEspecialidad)
        {
            List<EHistorial> list = DAOHistorial.obtenerHistorialPorUsuario(idUsuario);

            list = list.FindAll(x => x.ECita.EMedico.EEspecialidad.Id == idEspecialidad);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, list);
        }

        public static Respuesta buscarByIdUsuarioAndParametro(Int64 idUsuario, string parametro)
        {
            List<EHistorial> eHistorials = DAOHistorial.obtenerHistorialPorUsuario(idUsuario);

            parametro = parametro.ToLower();

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, eHistorials.FindAll( h => (h.ECita.Servicio.ToLower().IndexOf(parametro) != -1) || DateTime.Parse(h.ECita.Fecha).ToString("dddd - dd/MMMM/yyyy").IndexOf(parametro) != -1 ));
        }

    }
}
