﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Data.Clases;
using System.Collections.Generic;

namespace Logica.Clases
{
    public class LAuditoria
    {
        public static List<EAuditoria> getAuditoriaTabla(string nombreTabla)
        {
            return DAOAuditoria.getAuditoriaTabla(nombreTabla);
        }
    }
}
