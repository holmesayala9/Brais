﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases.Idioma;
using Utilitaria.Clases;
using Data.Clases.Administrador;
using System.Data;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Logica.Clases.Administrador
{
    public abstract class LIdioma
    {
        public static string getComponentesPorIdioma(int idIdioma)
        {
            return DAOIdioma.getComponentesPorIdioma(idIdioma);
        }

        public static string getTraduccion(int idIdioma, string urlFormulario)
        {
            urlFormulario = urlFormulario.Substring(2);

            EFormulario eFormulario = DAOIdioma.getAllFormulario().Find(f => f.Url == urlFormulario);

            if (eFormulario == null)
            {
                return "{}";
            }

            return getTraduccion(idIdioma, eFormulario.Id);
        }

        public static string getTraduccion(int idIdioma, int idFormulario)
        {
            List<Traduccion> list = DAOIdioma.getTraduccion(idIdioma, idFormulario);

            JArray jArray = new JArray();

            foreach (Traduccion traduccion in list)
            {
                JObject jObject = new JObject();
                jObject["componente"] = traduccion.Componente;
                jObject["txt"] = traduccion.Txt;

                jArray.Add(jObject);
            }

            return jArray.ToString();
        }

        public static List<EIdioma> getIdioma()
        {
            return DAOIdioma.getAllIdioma();
        }

        public static EIdioma getIdioma(int id)
        {
            return DAOIdioma.getIdioma(id);
        }

        public static Respuesta agregarIdioma(EIdioma eIdioma, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllIdioma().Exists(i => i.Terminacion.ToLower() == eIdioma.Terminacion.ToLower()))
            {
                throw new Exception("Extension ya registrada");
            }

            DAOIdioma.addIdioma(eIdioma, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta update(EIdioma eIdioma, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllIdioma().Exists(i => i.Terminacion.ToLower() == eIdioma.Terminacion.ToLower() && eIdioma.Id != i.Id))
            {
                throw new Exception("Extension ya registrada");
            }

            if (eIdioma != null && eAcceso != null)
            {
                DAOIdioma.updateIdioma(eIdioma, eAcceso);
                return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
            }
            return Respuesta.newRespuesta(Respuesta.ALERTA, "No se pudo Editar el idioma", "", null);
        }

        public static Respuesta remove(int id, EAcceso eAcceso)
        {
            EIdioma eIdioma = DAOIdioma.getIdioma(id);

            if (DAOIdioma.getAllIdiomaComponente().Exists(i => i.EIdioma.Id == id))
            {
                throw new Exception("El idioma tiene enlazado componentes");
            }

            DAOIdioma.deleteIdioma(eIdioma, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }
        
        public static void verificarSeleccionDeIdioma(int idIdioma)
        {
            if (DAOIdioma.getIdioma(idIdioma).Id == -1)
            {
                throw new Exception("Idioma no disponible");
            }
        }

        public static int validarIdioma(string idIdioma)
        {
            if (idIdioma == "null")
            {
                List<EIdioma> eIdiomas = new List<EIdioma>();
                eIdiomas = DAOIdioma.getAllIdioma();
                for (int i = 0; i <= eIdiomas.Count; i++)
                {
                    if(eIdiomas[i].Name.ToString() == "Español" || eIdiomas[i].Terminacion.ToString() == "es-CO")
                    {
                        return eIdiomas[i].Id;
                    }
                }
                
            }
            if(idIdioma == "0") //Si es el item seleccione
            {
                List<EIdioma> eIdiomas = new List<EIdioma>();
                eIdiomas = DAOIdioma.getAllIdioma();
                for (int i = 0; i <= eIdiomas.Count; i++)
                {
                    if (eIdiomas[i].Name.ToString() == "Español" || eIdiomas[i].Terminacion.ToString() == "es-CO")
                    {
                        return eIdiomas[i].Id;
                    }
                }
            }
            return int.Parse(idIdioma);
        }

        public static EMensaje obtenerMensaje(int id_idioma, string ex)
        {
            EMensaje eMensaje = DAOIdioma.obtenerMensajeTraducido(id_idioma, ex);

            if (eMensaje == null)
            {
                return EMensaje.newEmpty();
            }

            return eMensaje;
        }

        public static EMensaje validarMensaje(EMensaje eMensaje, Exception ex)
        {
            if (eMensaje.TxtTraducido == "None")
            {
                eMensaje.TxtTraducido = ex.Message;
            }
            return eMensaje;
        }

        public static EIdioma ObtenerIdiomaSeleccionado()
        {
            EIdioma eIdioma = DAOIdioma.getIdioma(7);
            return eIdioma;
        }
    }
}
