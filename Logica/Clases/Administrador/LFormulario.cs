﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Data.Clases.Administrador;
using Utilitaria.Clases.Idioma;


namespace Logica.Clases.Administrador
{
    public abstract class LFormulario
    {

        public static Respuesta get(int id)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOIdioma.getFormulario(id));
        }

        public static List<EFormulario> getFormulario()
        {
            return DAOIdioma.getAllFormulario() ;
        }

        public static Respuesta getAll()
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOIdioma.getAllFormulario());
        }

        public static Respuesta buscar(string parametro)
        {
            parametro = parametro.ToLower();
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, 
                DAOIdioma.getAllFormulario().FindAll(f => f.Name.ToLower().IndexOf(parametro) != -1 || f.Url.ToLower().IndexOf(parametro) != -1));
        }

        public static Respuesta add(EFormulario eFormulario, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllFormulario().Exists(i => i.Name.ToLower() == eFormulario.Name.ToLower()))
            {
                throw new Exception("Nombre ya registrado");
            }
            if (DAOIdioma.getAllFormulario().Exists(i => i.Url.ToLower() == eFormulario.Url.ToLower()))
            {
                throw new Exception("Url ya registrado");
            }

            DAOIdioma.addFormulario(eFormulario, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Agregado", Respuesta.NO_REDIRECCIONAR, null);
        }
        public static Respuesta update(EFormulario eFormulario, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllFormulario().Exists(i => i.Name.ToLower() == eFormulario.Name.ToLower() && i.Id != eFormulario.Id))
            {
                throw new Exception("Nombre ya registrado");
            }
            if (DAOIdioma.getAllFormulario().Exists(i => i.Url.ToLower() == eFormulario.Url.ToLower() && i.Id != eFormulario.Id))
            {
                throw new Exception("Url ya registrado");
            }

            DAOIdioma.updateFormulario(eFormulario, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Editado", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta delete(int id, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllFormularioComponente().Exists(f => f.IdFormulario == id))
            {
                throw new Exception("Hay componentes enlazados al formulario");
            }

            DAOIdioma.deleteFormulario(id, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Eliminado", Respuesta.NO_REDIRECCIONAR, null);
        }

    }
}
