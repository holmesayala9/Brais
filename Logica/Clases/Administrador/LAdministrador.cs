﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;

namespace Logica.Clases.Administrador
{
    public abstract class LAdministrador
    {
        public static Respuesta generarCitas(EAcceso eAcceso)
        {
            DAOMedico.getAll().ForEach(eMedico => DAOCita.generarCitasDeMedico(eMedico.Id, eAcceso));
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static DataTable getRangoDeHorarioTrabajoForView()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("value");
            dataTable.Columns.Add("text");

            DataRow dataRow = dataTable.NewRow();
            dataRow["value"] = "";
            dataRow["text"] = "Seleccione";
            dataTable.Rows.Add(dataRow);

            DateTime dateTime = DateTime.Parse("01:00");

            while (dateTime <= DateTime.Parse("23:00"))
            {
                DataRow dr = dataTable.NewRow();
                dr["value"] = dateTime.ToLongTimeString();
                dr["text"] = dateTime.ToShortTimeString();
                dataTable.Rows.Add(dr);
                dateTime = dateTime.AddHours(1);
            }

            return dataTable;
        }

        public static DataTable getDuracionCitasForView()
        {   
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("value");
            dataTable.Columns.Add("text");

            DataRow dataRow = dataTable.NewRow();
            dataRow["value"] = "";
            dataRow["text"] = "Seleccione";
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["value"] = 20;
            dataRow["text"] = "20 min";
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["value"] = 30;
            dataRow["text"] = "30 min";
            dataTable.Rows.Add(dataRow);

            dataRow = dataTable.NewRow();
            dataRow["value"] = 40;
            dataRow["text"] = "40 min";
            dataTable.Rows.Add(dataRow);

            return dataTable;
        }
        
        public static void cargarDiasLaborales(Parametro parametro, int tamanio)
        {

        }

        public static void mostrarHorario(Horario horario, int i)
        {
            if (horario.Rangos.Exists(r => r.Dia == i))
            {
                
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
