﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;
using Data.Clases.Administrador;
using Utilitaria.Clases.Idioma;

namespace Logica.Clases.Administrador
{
    public abstract class LComponente
    {
        public static Respuesta get(int id)
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOIdioma.getComponente(id));
        }

        public static List<EComponente> getComponente()
        {
            return DAOIdioma.getAllComponente();
        }

        public static Respuesta getAll()
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOIdioma.getAllComponente());
        }

        public static Respuesta buscarComponente(string parametro)
        {
            parametro = parametro.ToLower();
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR,
                DAOIdioma.getAllComponente().FindAll(c => c.Name.ToLower().IndexOf(parametro) != -1));
        }

        public static Respuesta add(EComponente eComponente, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllComponente().Exists(c => c.Name.ToLower() == eComponente.Name.ToLower()))
            {
                throw new Exception("Nombre ya registrado");
            }

            DAOIdioma.addComponente(eComponente, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta update(EComponente eComponente, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllComponente().Exists(c => c.Name.ToLower() == eComponente.Name.ToLower() && c.Id != eComponente.Id))
            {
                throw new Exception("Nombre ya registrado");
            }

            DAOIdioma.updateComponente(eComponente, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta delete(int id, EAcceso eAcceso)
        {
            DAOIdioma.deleteComponente(id, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta addIdiomaToComponente(int idIdioma, string nombreComponente, string txt, EAcceso eAcceso)
        {
            if (!DAOIdioma.getAllComponente().Exists(c => c.Name == nombreComponente))
            {
                throw new Exception("Componente no encontrado");
            }

            EComponente eComponente = DAOIdioma.getAllComponente().Find(c => c.Name == nombreComponente);

            return addIdiomaToComponente(idIdioma, eComponente.Id, txt, eAcceso);
        }

        public static Respuesta addIdiomaToComponente(int idIdioma, int idComponente, string txt, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllIdiomaComponente().Exists( i => i.EComponente.Id == idComponente && i.EIdioma.Id == idIdioma ))
            {
                throw new Exception("El componente ya tiene enlazado el idioma");
            }

            DAOIdioma.addIdiomaToComponente(idIdioma, idComponente, txt, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta getAllIdiomaComponente()
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOIdioma.getAllIdiomaComponente());
        }

        public static Respuesta buscarIdiomaComponente(string parametro)
        {
            parametro = parametro.ToLower();
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR,
                DAOIdioma.getAllIdiomaComponente().FindAll(c => c.EComponente.Name.ToLower().IndexOf(parametro) != -1 || c.EIdioma.Name.ToLower().IndexOf(parametro) != -1));
        }

        public static Respuesta getIdiomaComponente(int id)
        {
            EIdiomaComponente eIdiomaComponente = DAOIdioma.getAllIdiomaComponente().Find(i => i.Id == id);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, eIdiomaComponente);
        }

        public static Respuesta updateIdiomaComponente(EIdiomaComponente eIdiomaComponente, EAcceso eAcceso)
        {
            DAOIdioma.updateIdiomaComponente(eIdiomaComponente, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta deleteIdiomaComponente(int id, EAcceso eAcceso)
        {
            DAOIdioma.deleteIdiomaComponente(id, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta addComponenteToFormulario(string nombreFormulario, string nombreComponente, EAcceso eAcceso)
        {
            if (!DAOIdioma.getAllFormulario().Exists(f => f.Name == nombreFormulario))
            {
                throw new Exception("Formulario no encontrado");
            }

            if (!DAOIdioma.getAllComponente().Exists(c => c.Name == nombreComponente))
            {
                throw new Exception("Componente no encontrado");
            }

            EFormulario eFormulario = DAOIdioma.getAllFormulario().Find(f => f.Name == nombreFormulario);
            EComponente eComponente = DAOIdioma.getAllComponente().Find(c => c.Name == nombreComponente);

            return addComponenteToFormulario(eFormulario.Id, eComponente.Id, eAcceso);
        }

        public static Respuesta addComponenteToFormulario(int idFormulario, int idComponente, EAcceso eAcceso)
        {
            if (DAOIdioma.getAllFormularioComponente().Exists(i => i.IdFormulario == idFormulario && i.IdComponente == idComponente))
            {
                throw new Exception("El formulario ya tiene enlazado el componente");
            }

            DAOIdioma.addComponenteToFormulario(idFormulario, idComponente, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Agregado", Respuesta.NO_REDIRECCIONAR, null);
        }

        public static Respuesta getAllFormularioComponente()
        {
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR, DAOIdioma.getAllFormularioComponente());
        }

        public static Respuesta buscarFormularioComponente(string parametro)
        {
            parametro = parametro.ToLower();
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "", Respuesta.NO_REDIRECCIONAR,
                DAOIdioma.getAllFormularioComponente().FindAll(c => c.EComponente.Name.ToLower().IndexOf(parametro) != -1 || c.EFormulario.Name.ToLower().IndexOf(parametro) != -1 || c.EFormulario.Url.ToLower().IndexOf(parametro) != -1));
        }

        public static Respuesta deleteFormularioComponente(int id, EAcceso eAcceso)
        {
            DAOIdioma.deleteFormularioComponente(id, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Eliminado", Respuesta.NO_REDIRECCIONAR, null);
        }
    }
}
