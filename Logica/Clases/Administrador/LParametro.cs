﻿using Data.Clases.Administrador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaria.Clases;

namespace Logica.Clases.Administrador
{
    public class LParametro
    {
        public static Parametro get()
        {
            return DAOParametro.get();
        }

        public static Respuesta updateDiasLaborales(List<int> diasLaborales, EAcceso eAcceso)
        {
            Parametro parametro = DAOParametro.get();
            parametro.DiasLaborales = diasLaborales;

            DAOParametro.update(parametro, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta updateDiasSemana(Dictionary<int, Boolean> diasSemana, EAcceso eAcceso)
        {
            List<int> newDiasSemana = new List<int>();

            foreach (KeyValuePair<int, Boolean> d in diasSemana)
            {
                if (d.Value)
                {
                    newDiasSemana.Add(d.Key);
                }
            }

            Parametro parametro = DAOParametro.get();
            parametro.DiasLaborales = newDiasSemana;

            DAOParametro.update(parametro, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta updateSesionesSimultaneas(int sesiones, EAcceso eAcceso)
        {
            if (sesiones < 1 || sesiones > 5)
            {
                throw new Exception("La cantidad de sesiones debe estar entre 1 y 5");
            }

            Parametro parametro = DAOParametro.get();

            parametro.Sesiones = sesiones;
            DAOParametro.update(parametro, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta updateIntentosIngreso(int intentosIngreso, EAcceso eAcceso)
        {
            if (intentosIngreso < 1 || intentosIngreso > 10)
            {
                throw new Exception("La cantidad de intentos debe estar entre 1 y 10");
            }

            Parametro parametro = DAOParametro.get();

            parametro.IntentosIngreso = intentosIngreso;
            DAOParametro.update(parametro, eAcceso);

            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta updateHorarioDeTrabajo(string horaInicio, string horaFin, EAcceso eAcceso)
        {
            Parametro parametro = DAOParametro.get();

            DateTime inicio = DateTime.Parse(parametro.HoraInicio);
            DateTime fin = DateTime.Parse(parametro.HoraFin);

            if (!horaInicio.Equals(""))
            {
                inicio = DateTime.Parse(horaInicio);
            }
            if (!horaFin.Equals(""))
            {
                fin = DateTime.Parse(horaFin);
            }

            if (inicio >= fin)
            {
                throw new Exception("La hora de inicio debe ser menor a la hora fin");
            }

            parametro.HoraInicio = inicio.ToString("HH:mm");
            parametro.HoraFin = fin.ToString("HH:mm");

            DAOParametro.update(parametro, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

        public static Respuesta updateDuracionCitas(string duracion, EAcceso eAcceso)
        {
            if (duracion.Equals(""))
            {
                throw new Exception("Duracion de citas no valida");
            }

            Parametro parametro = DAOParametro.get();

            parametro.DuracionCitas = duracion;

            DAOParametro.update(parametro, eAcceso);
            return Respuesta.newRespuesta(Respuesta.CORRECTO, "Correcto", "", null);
        }

    }
}
