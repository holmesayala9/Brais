-- Comandos
/*
	alter table eps.eps add primary key (id);

	ALTER TABLE seguridad.acceso ALTER COLUMN id_usuario TYPE integer using CAST(id_usuario AS integer);

	select pg_size_pretty(pg_total_relation_size('seguridad.acceso'));

	SELECT pg_size_pretty (pg_database_size ('brais'));
*/

/*		TESTEO		*/

truncate seguridad.restablecer_contrasena;

SELECT * FROM seguridad.token_app;

select * from seguridad.restablecer_contrasena;
update seguridad.restablecer_contrasena set fecha = '2018-08-24 22:13:54.519345-05' where id = 3

UPDATE usuario.usuario set genero = 'Masculino' where id = 2

select * from usuario.usuario;

select * from usuario.f_buscar_usuario('re');

update usuario.usuario set fecha_nacimiento = '1998-01-05' where id = 636770581332306978;

truncate usuario.cita;

UPDATE usuario.cita SET estado = 'reservado', id_usuario = 636770581332306978 where id = 341;

select * from usuario.cita ORDER BY id;

truncate usuario.historial;

select * from usuario.historial;

DELETE from usuario.historial WHERE id_cita = 303;

truncate table medico.medico;

SELECT * FROM medico.medico;

select * from medico.especialidad;

INSERT INTO medico.medico 
(identificacion, 
	nombre, 
	apellido, 
	correo, 
	telefono, 
	id_especialidad,
	id_consultorio,
	id_acceso,
	activo,
	id
	) VALUES (
		'2',
		'Solar',
		'Talula',
		'Solara@talual.com',
		'3197595071',
		1,
		-1,
		-1,
		TRUE,
		54321);

INSERT INTO medico.especialidad(nombre) values('Medicina general');
INSERT INTO medico.especialidad(nombre) values('Odontologia');

INSERT INTO medico.consultorio (nombre, descripcion, disponibilidad) VALUES ('101A', 'Torre A piso 1', TRUE);
INSERT INTO medico.consultorio (nombre, descripcion, disponibilidad) VALUES ('201A', 'Torre A piso 2', TRUE);
INSERT INTO medico.consultorio (nombre, descripcion, disponibilidad) VALUES ('301A', 'Torre A piso 3', TRUE);

UPDATE medico.consultorio set disponibilidad=TRUE where TRUE



select * from medico.consultorio;

select * from administrador.administrador;

select * from administrador.parametro;

select * from seguridad.acceso ORDER BY id;

-- select CONCAT('(',e.id, ',', e.nombre, ',', '0),') from medico.especialidad e;

TRUNCATE seguridad.auditoria;

select * from seguridad.auditoria;

SELECT * FROM seguridad.acceso ORDER BY id;

TRUNCATE seguridad.estado_ingreso;

SELECT * FROM seguridad.estado_ingreso;

SELECT * FROM idioma.idioma;
87e0e2e9-097e-480f-a932-a66c511eb853
SELECT * FROM idioma.formulario;

SELECT * FROM idioma.componente ORDER BY id;

SELECT ic.* FROM idioma.idioma_componente ic where id_componente = 48 ORDER BY id;

update idioma.idioma_componente set id_idioma = 2, txt = 'End time' WHERE id = 96;

select CONCAT('(?',i.name,'?', ', ?',i.terminacion,'?)' ) from idioma.idioma i order by id;

select CONCAT('(?',c.name,'?)' ) from idioma.componente c order by id;

select CONCAT('(',i.id_idioma,', ',i.id_componente,', ?',i.txt,'?)' ) from idioma.idioma_componente i order by id;

SELECT * FROM idioma.idioma_componente ic WHERE id_idioma = 2 ORDER BY id_idioma, id;

SELECT * FROM generate_series(1, 75);

SELECT * FROM idioma.formulario_componente;

SELECT idioma.f_add_idioma( 'Español', 'ES-co', -1);

SELECT idioma.f_add_formulario( 'AgregarUsuario', 'View/Administrador/AgregarUsuario.aspx', -1 );

SELECT idioma.f_add_componente( 'LB_Nombre', -1 );

SELECT idioma.f_add_idioma_to_componente(1, 5, 'Agregar', -1);

SELECT idioma.f_add_componente_to_formulario(1, 5, -1);

SELECT * FROM idioma.f_get_all_componente();



-- Ñam

select CONCAT('(',i.id, ', "', i.name, '", "', i.terminacion, '", ', i.id_acceso,', ', i.actual, '),') from idioma.idioma i;

select CONCAT('(',ic.id, ', "', ic.name, '", ', ic.id_acceso,'),') from idioma.componente ic;

select CONCAT('(',f.id, ', "', f.name, '", "', f.url, '", ', f.id_acceso, '),') from idioma.formulario f;

select CONCAT('(',iic.id, ', ', iic.id_idioma, ', ', iic.id_componente, ', "', iic.txt, '", ', iic.id_acceso ,'),') from idioma.idioma_componente iic order by iic.id;

select CONCAT('(',ifc.id, ', ', ifc.id_formulario, ', ', ifc.id_componente, ', ', ifc.id_acceso ,'),') from idioma.formulario_componente ifc order by ifc.id;








Correcto
Nombre ya registrado
Componente no encontrado
El componente ya tiene enlazado el idioma
Extension ya registrada
No se pudo editar el idioma
El idioma tiene enlazado componentes
La cantidad de sesiones debe estar entre 1 y 5
La cantidad de intentos debe estar entre 1 y 10
La hora de inicio debe ser menor a la hora fin
Duracion de citas no valida
Un medico tiene asignado el consultorio
Un medico la tiene asignada
Tiene citas pendientes por atender
Identificacion ya registrada
Correo ya registrado
Debe seleccionar la especialidad
El consultorio no se encuentra disponible
Especialidad no disponible
Medico no valido
Especialidad no valida
Consultorio no disponible
Contraseña incorrecta
Solo puede cancelar con una hora de anticipacion
No se encuentra en estado reservado
Ya tiene un historial asignado
Solo puede reprogramar con una hora de anticipacion
Cita no disponible
Solo puede reservar de a una cita por especialidad
Citas generadas segun su horario
Historial no valido
Usuario no valido
Su fecha de nacimiento debe ser menor a la fecha actual
Error
Identificacion o contraseña incorrecto
Su cuenta esta bloqueada
Cuenta bloqueada por 30 minutos
Ha superado la cantidad de sesiones activas
Identificacion o correo no valido
Ya dispone de una solicitud valida enviada a su correo
Se ha enviado informacion a su correo
No dispone de una solicitud valida
Usuario inactivo
Acceso denegado
Acceso no valido
El rango no esta en el horario
Dia no valido
Hora inicio debe ser menor a hora fin
El rango se cruza



