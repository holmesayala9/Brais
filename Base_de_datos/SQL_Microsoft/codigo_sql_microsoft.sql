
-- BASE DE DATOS

--
GO
create schema usuario;
GO

create table usuario.usuario(
	id bigint,
	identificacion NVARCHAR(MAX),
	nombre NVARCHAR(MAX),
	apellido NVARCHAR(MAX),
	fecha_nacimiento NVARCHAR(MAX),
	genero NVARCHAR(MAX),
	correo NVARCHAR(MAX),
	telefono NVARCHAR(MAX),
	contrasena NVARCHAR(MAX),
	activo bit DEFAULT 1,
	id_acceso integer DEFAULT -1,
	primary key (id)
);

-- Estados: disponible, reservado, asistio, no asistio

create table usuario.cita(
	id int identity(1,1),
	hora_inicio NVARCHAR(MAX),
	hora_fin NVARCHAR(MAX),
	fecha NVARCHAR(MAX),
	servicio NVARCHAR(MAX),
	id_medico bigint DEFAULT -1,
	id_usuario bigint DEFAULT -1,
	estado NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table usuario.historial(
	id int identity(1,1),
	id_usuario bigint DEFAULT -1,
	id_cita integer DEFAULT -1,
	datos NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	primary key(id)
);

GO
create schema medico;
GO

create table medico.medico(
	id bigint,
	identificacion NVARCHAR(MAX),
	nombre NVARCHAR(MAX),
	apellido NVARCHAR(MAX),
	correo NVARCHAR(MAX),
	telefono NVARCHAR(MAX),
	contrasena NVARCHAR(MAX),
	horario NVARCHAR(MAX) DEFAULT '{}',
	activo bit DEFAULT 1,
	id_especialidad integer DEFAULT -1,
	id_consultorio integer DEFAULT -1,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table medico.especialidad(
	id int identity(1,1),
	nombre NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table medico.consultorio(
	id int identity(1,1),
	nombre NVARCHAR(MAX),
	descripcion NVARCHAR(MAX),
	disponibilidad bit DEFAULT 1,
	id_acceso integer DEFAULT -1,
	primary key(id)
);

GO
create schema administrador;
GO

create table administrador.administrador(
	id bigint,
	identificacion NVARCHAR(MAX),
	nombre NVARCHAR(MAX),
	apellido NVARCHAR(MAX),
	correo NVARCHAR(MAX),
	telefono NVARCHAR(MAX),
	contrasena NVARCHAR(MAX),
	activo bit DEFAULT 1,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table administrador.parametro(
	id int identity(1,1),
	parametro NVARCHAR(MAX) DEFAULT '{}',
	id_acceso integer DEFAULT -1,
	primary key(id)
);


insert into administrador.parametro (parametro) values
	( '{"DuracionCitas":"20",
		"HoraInicio":"07:00",
		"HoraFin":"18:00",
		"DiasLaborales":[0,1,2,3,4,5,6], 
		"Sesiones": 1, 
		"IntentosIngreso": 3
		}');

GO
create schema seguridad;
GO

create table seguridad.restablecer_contrasena(
	id int identity(1,1),
	id_usuario bigint DEFAULT -1,
	hash NVARCHAR(MAX),
	fecha NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);


create table seguridad.acceso(
	id int identity(1,1),
	id_usuario bigint,
	ip NVARCHAR(MAX),
	mac NVARCHAR(MAX),
	fecha_inicio NVARCHAR(MAX),
	fecha_fin NVARCHAR(MAX),
	ultima_actividad NVARCHAR(MAX),
	primary key (id)
);


CREATE TABLE seguridad.estado_ingreso(
	id int identity(1,1),
	id_usuario bigint DEFAULT -1,
	cantidad_sesiones INTEGER DEFAULT 0,
	cantidad_intentos INTEGER DEFAULT 0,
	fecha_bloqueo NVARCHAR(MAX) DEFAULT '',
	PRIMARY KEY (id)
);


create table seguridad.auditoria(
	id int identity(1,1),
	fecha NVARCHAR(MAX),
	accion NVARCHAR(MAX),
	usuariodb NVARCHAR(MAX),
	esquema NVARCHAR(MAX),
	tabla NVARCHAR(MAX),
	id_acceso INTEGER DEFAULT -1,
	data NVARCHAR(MAX) DEFAULT '{}',
	PRIMARY KEY(id)
);


create table seguridad.token_app(
	id int identity(1,1),
	token NVARCHAR(MAX),
	fecha NVARCHAR(MAX),
	id_servicio NVARCHAR(MAX),
	usuario_app NVARCHAR(MAX),
	PRIMARY KEY(id)
);

create table seguridad.token_usuario(
	id int identity(1,1),
	id_usuario bigint,
	token NVARCHAR(MAX),
	fecha NVARCHAR(MAX),
	id_servicio NVARCHAR(MAX),
	PRIMARY KEY(id)
);


GO
CREATE SCHEMA idioma;
GO

CREATE TABLE idioma.idioma(
	id int identity(1,1),
	name NVARCHAR(MAX),
	terminacion NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);


CREATE TABLE idioma.componente(
	id int identity(1,1),
	name NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);


CREATE TABLE idioma.idioma_componente(
	id int identity(1,1),
	id_idioma integer DEFAULT -1,
	id_componente integer DEFAULT -1,
	txt NVARCHAR(MAX),
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);

--Mensajes y excepciones

CREATE TABLE idioma.mensaje
(
    id int identity(1,1),
    id_idioma integer DEFAULT -1,
    txt_original NVARCHAR(MAX),
    txt_traducido NVARCHAR(MAX),
    CONSTRAINT mensaje_pkey PRIMARY KEY (id)
);

/*		USUARIOS BASE		*/

insert into administrador.administrador values(100000, '1', 'admin', 'admin', 'holmesayala9@gmail.com', '3197595071', '1', 1, -1);