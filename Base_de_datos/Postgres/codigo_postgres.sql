
-- BASE DE DATOS

create schema usuario;

create table usuario.usuario(
	id bigint,
	identificacion text,
	nombre text,
	apellido text,
	fecha_nacimiento text,
	genero text,
	correo text,
	telefono text,
	contrasena text,
	activo boolean DEFAULT TRUE,
	id_acceso integer DEFAULT -1,
	primary key (id)
);

-- Estados: disponible, reservado, asistio, no asistio

create table usuario.cita(
	id serial not null,
	hora_inicio text,
	hora_fin text,
	fecha text,
	servicio text,
	id_medico BIGINT DEFAULT -1,
	id_usuario BIGINT DEFAULT -1,
	estado text,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table usuario.historial(
	id serial not null,
	id_usuario BIGINT DEFAULT -1,
	id_cita integer DEFAULT -1,
	datos text,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create schema medico;


create table medico.medico(
	id bigint,
	identificacion text,
	nombre text,
	apellido text,
	correo text,
	telefono text,
	contrasena text,
	horario text DEFAULT '{}',
	activo boolean DEFAULT TRUE,
	id_especialidad integer DEFAULT -1,
	id_consultorio integer DEFAULT -1,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table medico.especialidad(
	id serial not null,
	nombre text,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table medico.consultorio(
	id serial not null,
	nombre text,
	descripcion text,
	disponibilidad boolean,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create schema administrador;


create table administrador.administrador(
	id bigint,
	identificacion text,
	nombre text,
	apellido text,
	correo text,
	telefono text,
	contrasena text,
	activo boolean DEFAULT TRUE,
	id_acceso integer DEFAULT -1,
	primary key(id)
);


create table administrador.parametro(
	id serial not null,
	parametro text DEFAULT '{}',
	id_acceso integer DEFAULT -1,
	primary key(id)
);


insert into administrador.parametro values
(DEFAULT, 
	'{"DuracionCitas":"20",
	"HoraInicio":"07:00",
	"HoraFin":"18:00",
	"DiasLaborales":[0,1,2,3,4,5,6], 
	"Sesiones": 1, 
	"IntentosIngreso": 3
	}', 
	DEFAULT);


create schema seguridad;


create table seguridad.restablecer_contrasena(
	id serial not null,
	id_usuario BIGINT DEFAULT -1,
	hash text,
	fecha text,
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);


create table seguridad.acceso(
	id serial not null,
	id_usuario BIGINT,
	ip text,
	mac text,
	fecha_inicio TEXT,
	fecha_fin TEXT,
	ultima_actividad TEXT,
	primary key (id)
);


CREATE TABLE seguridad.estado_ingreso(
	id SERIAL NOT NULL,
	id_usuario BIGINT DEFAULT -1,
	cantidad_sesiones INTEGER DEFAULT 0,
	cantidad_intentos INTEGER DEFAULT 0,
	fecha_bloqueo TEXT DEFAULT '',
	PRIMARY KEY (id)
);


create table seguridad.auditoria(
	id serial,
	fecha text,
	accion text,
	usuariodb text,
	esquema text,
	tabla text,
	id_acceso INTEGER DEFAULT -1,
	data TEXT DEFAULT '{}',
	PRIMARY KEY(id)
);


create table seguridad.token_app(
	id SERIAL NOT NULL,
	token TEXT DEFAULT '',
	fecha TEXT DEFAULT '',
	id_servicio TEXT DEFAULT '',
	usuario_app TEXT DEFAULT ''
);


create table seguridad.token_usuario(
	id SERIAL NOT NULL,
	token TEXT DEFAULT '',
	fecha TEXT DEFAULT '',
	id_servicio TEXT DEFAULT '',
	PRIMARY KEY(id)
);


CREATE SCHEMA idioma;


CREATE TABLE idioma.idioma(
	id serial,
	name text,
	terminacion text,
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);


CREATE TABLE idioma.componente(
	id serial,
	name text,
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);


CREATE TABLE idioma.idioma_componente(
	id serial,
	id_idioma integer DEFAULT -1,
	id_componente integer DEFAULT -1,
	txt text,
	id_acceso integer DEFAULT -1,
	PRIMARY KEY(id)
);

/*		USUARIOS BASE		*/

insert into administrador.administrador values(1000000, '1', 'admin', 'admin', 'holmesayala9@gmail.com', '3197595071', '1', TRUE, -1);